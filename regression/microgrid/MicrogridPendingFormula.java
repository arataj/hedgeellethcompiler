package example;

import java.util.Random;

import hc.*;
import hc.game.*;

/**
 * Model.state*() before a meaningless operation/at a method's end.
 */
public class MicrogridPendingFormula {
    /**
     * Number of days.
     */
    public static final int DAYS = 3;
    /**
     * Number of intervals per day.
     */
    public static final int INTERVALS = 16;
    /**
     * Number of rounds.
     */
    public static final int MAX_TIME = DAYS*INTERVALS;
    
    /**
     * Demand curve.
     */
    public static final double[] DEMAND = {
        0.0614, 0.0392, 0.0304, 0.0304,
        0.0355, 0.0518, 0.0651, 0.0643,
        0.0625, 0.0618, 0.0614, 0.0695,
        0.0887, 0.1013, 0.1005, 0.0762,
    };
    /**
     * Number of households.
     */
    public static final int NUM_HOUSEHOLDS = 3;
    /**
     * Households.
     */
    protected static Household[] households;
    /**
     * Current round.
     */
    public static int/*@(0, MAX_TIME)*/ time;
    /**
     * Current number of jobs.
     */
    public static int/*@(0, NUM_HOUSEHOLDS)*/ numJobs;

    /**
     * Returns the current load demand.
     */
    public static double getDemand() {
        return DEMAND[time%INTERVALS];
    }
    /**
     * A potential price of a load to be bought.
     */
    public static double getPrice() {
      return numJobs + 1.0;
    }
    /**
     * Called by the scheduler at the end of each round.
     *
     * @return if the simulation is finished
     */
    public static boolean tick() {
        // reduce job counters
        for(int i = 0; i < MicrogridPendingFormula.NUM_HOUSEHOLDS; ++i)
              households[i].tick();
        return ++time < MAX_TIME;
    }
    public static void main(String[] args) {
        // player ids at the table: 0 schedule, 1..NUM_HOUSEHOLDS households
        TurnGame table = new TurnGame();
        Model.name(table);
        // create an array of households
        households = new Household[NUM_HOUSEHOLDS];
        for(int i = 0; i < NUM_HOUSEHOLDS; ++i) {
            Household h = new Household(table, 1 + i);
            h.start();
            Model.name(h, "", "" + i);
            Model.player(h, "p" + (NUM_HOUSEHOLDS - i));
            households[i] = h;
        }
        // create the scheduler
        Scheduler s = new Scheduler(table, households);
        s.start();
        Model.name(s);
        // reset time
        time = 0;
        table.start(0);
    }
}

class Scheduler extends TurnPlayer {
    protected final static boolean NON_DETERMINISTIC = false;
    
    protected final Household[] households;
    protected final Random random;

    public Scheduler(TurnGame table, Household[] households) {
        super(table, 0);
        this.households = households;
        random = new Random();
    }
    @Override
    public void run() {
        households[1].job = 0;
    }
}

class Household extends TurnPlayer {
    /**
     * Maximum time of running a single job, in intervals.
     */
    protected final static int MAX_JOB_TIME = 4;
    /**
     * Expected number of jobs per day.
     */
    protected final static int EXPECTED_JOBS = 9;
    /**
     * Price limit, above which this household may
     * back--off.
     */
    protected final static double PRICE_LIMIT = 1.5;
    /**
     * Probability of starting a task independently of the cost.
     */
    protected final static double P_OVER_LIMIT = 0.8;
    /**
     * A running job, in intervals.
     */
    public int/*@(0, MAX_JOB_TIME)*/ job = 0;
    protected final Random random;
    private final String[] name;
    private int f;
    
    public Household(TurnGame table, int playerNum) {
        super(table, playerNum);
        random = new Random();
        name = new String[2];
        name[1] = "player" + playerNum;
        name[0] = "notnull";
    }
    /**
     * Called by the scheduler after each round.
     */
    public void tick() {
        if(job > 0)
          --job;
    }
    @Override
    public void run() {
        Model.stateAnd("start" + 0);
        // the following conditon is always false,
        // but is not removed because of the
        // state formula preceeding it
        if(job > 0)
            --job;
        if(job > 0)
            job = 0;
        Model.stateOr(name[0]);
        int c = job;
        Model.stateAnd("end");
    }
}
