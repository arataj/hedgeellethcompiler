package example;

import java.util.Random;

import hc.*;
import hc.game.*;

/**
 * Dereference on a null array.
 */
public class MicrogridNullArray {
    /**
     * Number of households.
     */
    public static final int NUM_HOUSEHOLDS = 3;
    /**
     * Households.
     */
    protected static Household[] households;
    /**
     * Current round.
     */
    public static int/*@(0, 1)*/ time;
    /**
     * Current number of jobs.
     */
    public static int/*@(0, NUM_HOUSEHOLDS)*/ numJobs;

    public static void main(String[] args) {
        // player ids at the table: 0 schedule, 1..NUM_HOUSEHOLDS households
        TurnGame table = new TurnGame();
        Model.name(table);
        // create the scheduler
        Scheduler s = new Scheduler(table, households);
        s.start();
        Model.name(s);
        Model.player(s, "scheduler");
        // create an array of households
        households = new Household[NUM_HOUSEHOLDS];
        for(int i = 0; i < NUM_HOUSEHOLDS; ++i) {
            Household h = new Household(table, 1 + i);
            h.start();
            Model.name(h, "", "" + i);
            Model.player(h, "h" + i);
            households[i] = h;
        }
        // reset time
        time = 0;
        // players consist of the scheduler and of the households
        table.start(0);
        Model.check("<<1>> R{\"value1\"}max=? [F time=MAX_TIME]");
        Model.check("all", "<<1, 2, 3>> R{\"value123\"}max=? [F time=MAX_TIME]");
    }
}

class Scheduler extends TurnPlayer {
    protected final Household[] households;
    protected final Random random;

    public Scheduler(TurnGame table, Household[] households) {
        super(table, 0);
        this.households = households;
        random = new Random();
    }
    @Override
    public void run() {
        for(int i = 0; i < MicrogridNullArray.NUM_HOUSEHOLDS; ++i)
            if(households[i].job > 0)
                ++MicrogridNullArray.numJobs;
    }
}

class Household extends TurnPlayer {
    public int job;
    
    public Household(TurnGame table, int playerNum) {
        super(table, playerNum);
    }
    @Override
    public void run() {
    }
}
