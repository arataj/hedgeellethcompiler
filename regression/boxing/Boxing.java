public class Boxing extends Thread {
    Double f;

    public Boxing(Double v) {
	f = v;
    }
    @Override
    public void run() {
	f *= 2;
    }
    public static void main() {
 	Double a = 2.0;
	Double b = a + 2;
        (new Boxing(b)).start();
    }
}

