public class Optimize extends Thread {
    Double f;

    public Optimize(Double v) {
	f = new Double(v.doubleValue());
    }
    @Override
    public void run() {
	f = new Double(f.doubleValue()*2);
    }
    public static void main(String[] args) {
 	Double a = new Double(2.0);
	Double b = new Double(a.doubleValue() + 2);
        (new Optimize(b)).start();
    }
}
