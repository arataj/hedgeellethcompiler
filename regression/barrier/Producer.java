package example;

import java.util.Random;

import hc.*;

/**
 * A producer, that uses a barrier.
 */
public class Producer extends Thread {
  AbstractBarrier barrier;
  
  public Producer(AbstractBarrier barrier) {
    this.barrier = barrier;
  }
  @Override
  public void run() {
    while(true) {
      Sleep.min(100);
      barrier.produce();
    }
  }
}

