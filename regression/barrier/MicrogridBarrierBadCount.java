//
// Barrier.activate() declared with an invalid number of players.
//

package example;

import java.util.*;

import hc.*;

class Table {
    /**
     * Common barrier for all players ath this table.
     */
    public final Barrier BARRIER = new Barrier();
    /**
     * Number of the player that has currently a turn.
     */
    public int turn = 0;

    /**
     * Starts a game at this table.
     *
     * @param numOfPlayers total number of players
     */    
    public void start(int numOfPlayers) {
        BARRIER.activate(numOfPlayers);
    }
}

/**
 * A microgrid demand--side management game, as described in [1].
 *
 * [1] H. Hildmann and F. Saffre. Influence of variable supply and load
 * flexibility on Demand-Side Management. In Proc. 8th International
 * Conference on the European Energy Market (EEM'11), pages 63-68. 2011.
 */
public class MicrogridBarrier {
    /**
     * Number of households.
     */
    public static final int HOUSEHOLDS = 3;

    public static void main(String[] args) {
        // player ids at the table: 0 schedule, 1..HOUSEHOLDS households
        Table table = new Table();
        // create an array of households
        final Household[] households = new Household[HOUSEHOLDS];
        for(int i = 0; i < HOUSEHOLDS; ++i) {
            Household h = new Household(table, 1 + i);
            h.start();
            households[i] = h;
        }
        // create the scheduler
        new Scheduler(table, households).start();
        // players consist of the scheduler and of the households,
        // this declared one player less
        table.start(HOUSEHOLDS);
    }
}

abstract class Player extends Thread implements BooleanClosure {
    /**
     * Table, at which this player sits.
     */
    private Table table;
    /**
     * Number of turn for this player.
     */
    private final int playerNum;
    
    protected Player(Table table, int playerNum) {
      this.table = table;
      this.playerNum = playerNum;
    }
    public boolean booleanClosure() {
        return table.turn == playerNum;
    }
    protected void turnWait() {
        table.BARRIER.barrier(this);
    }
    protected void turnNext(int playerNum) {
        table.turn = playerNum;
    }
}

class Scheduler extends Player {
    protected final static boolean NON_DETERMINISTIC = false;
    
    protected final Household[] households;
    protected final Random random;

    public Scheduler(Table table, Household[] households) {
        super(table, 0);
        this.households = households;
        random = new Random();
    }
    @Override
    public void run() {
        while(true) {
            // select a household
            int address;
            if(NON_DETERMINISTIC)
                // non--deterministic choice
                address = random.nextInt(MicrogridBarrier.HOUSEHOLDS);
            else
                // probabilistic choice
                address = (int)
                    (Math.random()*MicrogridBarrier.HOUSEHOLDS);
            // contact the selected household
            turnNext(1 + address);
            turnWait();
        }
    }
}

class Household extends Player {
    protected final Random random;
    protected int choice = 0;
    
    public Household(Table table, int playerNum) {
        super(table, playerNum);
        random = new Random();
    }
    @Override
    public void run() {
        turnWait();
        
        turnNext(0);
    }
}
