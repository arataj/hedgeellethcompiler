package example;

import java.util.Random;

import hc.*;
import hc.game.*;

/**
 * Reading/writing matrices using <code>MatrixIO</code>.
 */
public class MatrixAccess {
    /**
     * Number of days.
     */
    public static final int DAYS = 3;
    /**
     * Number of intervals per day.
     */
    public static final int INTERVALS = 11;
    /**
     * Number of rounds.
     */
    public static final int MAX_TIME = DAYS*INTERVALS;
    
    /**
     * Demand curve.
     */
    public static final double[] DEMAND = {
        0.0614, 0.0392, 0.0304, 0.0304,
        0.0355, 0.0518, 0.0651, 0.0643,
        0.0625, 0.0618/*, 0.0614 */
    };
    /**
     * Number of households.
     */
    public static final int NUM_HOUSEHOLDS = 1;
    /**
     * Households.
     */
    protected static Household[] households;
    /**
     * Current round.
     */
    public static int/*@(0, MAX_TIME)*/ time;
    /**
     * Current number of jobs.
     */
    public static int/*@(0, NUM_HOUSEHOLDS)*/ numJobs;

    /**
     * Returns the current load demand.
     */
    public static double getDemand() {
        return DEMAND[time%INTERVALS];
    }
    /**
     * How many households currently generate a load.
     */
    public static int/*@(0, NUM_HOUSEHOLDS - 0)*/ getNumJobs() {
      int/*@(0, NUM_HOUSEHOLDS)*/ numJobs = 0;
      for(int i = 0; i < NUM_HOUSEHOLDS; ++i)
          if(households[i].job > 0)
              ++numJobs;
      return numJobs;
    }
    /**
     * A price of a load to be bought.
     */
    public static double getPrice() {
      return getNumJobs() + 1.0;
    }
    /**
     * Called by the scheduler at the end of each turn.
     *
     * @return if the simulation is finished
     */
    public static boolean turnEnd() {
        numJobs = getNumJobs();
        Model.stateAnd("measure");
        numJobs = 0;
        // reduce job counters
        for(int i = 0; i < MatrixAccess.NUM_HOUSEHOLDS; ++i)
              households[i].tick();
        return ++time < MAX_TIME;
    }
    public static void main(String[] args) {
        // player ids at the table: 0 schedule, 1..NUM_HOUSEHOLDS households
        TurnGame table = new TurnGame();
        Model.name(table);
        // create an array of households
        households = new Household[NUM_HOUSEHOLDS];
        for(int i = 0; i < NUM_HOUSEHOLDS; ++i) {
            Household h = new Household(table, 1 + i);
            h.start();
            Model.name(h, "", "" + (i + 1));
            Model.player(h, "h" + (i + 1));
            households[i] = h;
        }
        // create the scheduler
        Scheduler s = new Scheduler(table, households);
        s.start();
        Model.name(s);
        Model.player(s, "scheduler");
        // reset time
        time = 0;
        // players consist of the scheduler and of the households
        table.start(1 + NUM_HOUSEHOLDS);
        Model.check("<<1>> R{\"value1\"}max=? [F time=MAX_TIME]");
        Model.check("all", "<<1, 2, 3>> R{\"value123\"}max=? [F time=MAX_TIME]");
    }
}

class Scheduler extends TurnPlayer {
    protected final static boolean NON_DETERMINISTIC = false;
    
    protected final Household[] households;
    protected final java.util.Random strategy;
    
    protected double[] xA;
    protected double[] yA;

    public Scheduler(TurnGame table, Household[] households) throws IOException {
        super(table, 0);
        this.households = households;
        strategy = new java.util.Random();
        // in the case of tests, PWD is the compiler's directory
        MatrixIO io = new hc.MatrixIO("regression/matrixio/density.txt");
        xA = io.getNext();
        yA = io.getNext();
        io.close();
        io = new MatrixIO("regression/matrixio/MatrixAccess_out.txt");
        yA[1] = 129;
        yA[0] = io.getRowIndex() + 14 + yA[0];
        io.addNext(yA);
        yA[2]--;
        yA[0] = io.getRowIndex();
        Model.name(yA, "a", "b");
        io.addNext(yA);
        io.addNext(xA);
        io.close();
    }
    @Override
    public void run() {
        do {
            // select a household
            int address;
            if(NON_DETERMINISTIC)
                // non--deterministic choice
                address = strategy.nextInt(MatrixAccess.NUM_HOUSEHOLDS);
            else
                // probabilistic choice
                address = (int)
                    (Math.random()*MatrixAccess.NUM_HOUSEHOLDS);
            // contact the selected household
            turnNext(1 + address);
            turnWait();
            hc.Sleep.exact(Dist.tab(xA, yA));
            // end of a round
        } while(MatrixAccess.turnEnd());
    }
}

class Household extends TurnPlayer {
    /**
     * Maximum time of running a single job, in intervals.
     */
    protected final static int MAX_JOB_TIME = 4;
    /**
     * Expected number of jobs per day.
     */
    protected final static int EXPECTED_JOBS = 9;
    /**
     * Price limit, above which this household may
     * back--off.
     */
    protected final static double PRICE_LIMIT = 1.5;
    /**
     * Probability of starting a task independently of the cost.
     */
    protected final static double P_OVER_LIMIT = 0.8;
    /**
     * A running job, in intervals.
     */
    public int/*@(0, MAX_JOB_TIME)*/ job = 0;
    protected final Random strategy;
    
    public Household(TurnGame table, int playerNum) {
        super(table, playerNum);
        strategy = new Random();
    }
    /**
     * Called by the scheduler after each turn.
     */
    public void tick() {
        if(job > 0)
          --job;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(job == 0 && Math.random() < MatrixAccess.getDemand()*
                  EXPECTED_JOBS) {
                // decide if to generate a load
                boolean lowPrice = MatrixAccess.getPrice() < PRICE_LIMIT;
                // for efficiency, reset numJobs before a choice
                // is made
                MatrixAccess.numJobs = 0;
                if(lowPrice || Math.random() < P_OVER_LIMIT ||
                        // a right to back--off is granted; decide,
                        // if to generate a load anyway
                        strategy.nextInt(2) == 0)
                    job = 1 + (int)(Math.random()*MAX_JOB_TIME);
                MatrixAccess.numJobs = 0;
            }
            turnNext(0);
        }
    }
}
