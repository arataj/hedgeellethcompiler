package pmgc;

import java.util.Random;

import hc.*;
import hc.game.*;

public class Dialogue2 {
    final public static int NUM_SOURCES = 2;
    final public static int NUM_TURNS = 7;
    final public static int BASE = 100;
    final public static Source[] SOURCES = new Source[NUM_SOURCES];
    final public static int COMP_A = 0;
    final public static int COMP_B = 1;
    final public static int CONJ_ESPECIALLY = 0;
    final public static int CONJ_DESPITE    = 1;
    final public static int LEVELS = 3;
    final private static int CHEATING_INT = 1;
    final public static boolean CHEATING_POSSIBLE = CHEATING_INT == 1;
    /* -1 for variable declaration */
    final public static int FIX_DECLARATION = 1;
    
    public static Listener LISTENER;
    public static int /*@(0, Dialogue2.NUM_TURNS + 1)*/ turnCount = 0;
    static int[] A_LIST = { 80, 80 };
    static int[] B_LIST = { 80, 80 };
    
    public static boolean isEnd() {
        ++turnCount;
        return turnCount == Dialogue2.NUM_TURNS + 1;
    }
    public static void main(String[] args) {
        TurnGame table = new TurnGame();
        Model.name(table);
        // create sources
        for(int i = 0; i < NUM_SOURCES; ++i) {
            Source c = new Source(table, i, A_LIST[i], B_LIST[i]);
            c.start();
            Model.name(c, "", "" + (i + 1));
            Model.player(c, "s" + (i + 1));
            SOURCES[i] = c;
        }
        LISTENER = new Listener(table, NUM_SOURCES,
            COMP_A, CONJ_DESPITE, COMP_B);
        LISTENER.start();
        Model.name(LISTENER, "", "");
        Model.player(LISTENER, "l");
        table.start(NUM_SOURCES + 1);
        // Model.check("game ends",
        //     "<<2>> Pmin=? [F turnCount=3]");
        Model.check("persuasion min",
            "<<2>> Pmin=? [F persuaded=1]");
        Model.check("persuasion max",
            "<<2>> Pmax=? [F persuaded=1]");
        Model.waitFinish();
        System.out.println("finished");
        // intterupt any thread still running
        SOURCES[0].interrupt();
    }
}
class Source extends TurnPlayer {
    protected final Random strategy;
    public final int A;
    public final int B;
    // sensors always give some information (to avoid division by zero in the listener)
    public int/*@(getNum() == 0 ? 1 : 0, Dialogue2.LEVELS - 1)*/ declareA =
        (getNum() == 0 || Dialogue2.FIX_DECLARATION == -1 ? Dialogue2.LEVELS - 1 : Dialogue2.FIX_DECLARATION);
    public int/*@(getNum() == 0 ? 1 : 0, Dialogue2.LEVELS - 1)*/ declareB =
        (getNum() == 0 || Dialogue2.FIX_DECLARATION == -1 ? Dialogue2.LEVELS - 1 : Dialogue2.FIX_DECLARATION);
    public int outA;
    public int outB;
    
    public Source(TurnGame table, int playerNum,
            int a, int b) {
        super(table, playerNum);
        strategy = new Random();
        A = a;
        B = b;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(isInterrupted())
                break;
            say();
            turnNext(getNum() + 1);
        }
    }
    protected void say() {
        if(getNum() == 0) {
        /*
            // let it be allowed to replace an intermediate value
            declareA = strategy.nextInt(Dialogue2.LEVELS - 1);
            declareA = 1 + declareA;
            // let it be allowed to replace an intermediate value
            declareB = strategy.nextInt(Dialogue2.LEVELS - 1);
            declareB = 1 + declareB;
         */
            // sensors give exact values
            declareA = Dialogue2.LEVELS - 1;
            declareB = Dialogue2.LEVELS - 1;
        } else {
            // choose declaration strength
          if(Dialogue2.FIX_DECLARATION == -1) {
              declareA = strategy.nextInt(Dialogue2.LEVELS);
              declareB = strategy.nextInt(Dialogue2.LEVELS);
          } else {
              declareA = Dialogue2.FIX_DECLARATION;
              declareB = Dialogue2.FIX_DECLARATION;
          }
    //        declareA = 0;
    //        declareB = 0;
        }
        // choose if to cheat
        outA = getOutValue2(A);
        outB = getOutValue3(B);
    }
    protected int getOutValue2(int in) {
        if(!Dialogue2.CHEATING_POSSIBLE || getNum() == 0 || strategy.nextInt(2) == 0)
            return in;
        else
            return -in;
    }
    protected int getOutValue3(int in) {
        if(!Dialogue2.CHEATING_POSSIBLE || getNum() == 0 || strategy.nextInt(2) == 0)
            return in;
        else if(strategy.nextInt(2) == 0)
            return -in;
        else
            return 0;
    }
}
class Listener extends TurnPlayer {
    protected final Random strategy;
    final int COMP_1;
    final int CONJ;
    final int COMP_2;
    int[]/*@(0, Dialogue2.BASE)*/ compliance = {90, 50};
    int[]/*@(0, Dialogue2.BASE)*/ defiance = {0, 10};
    int/*@(0, Dialogue2.BASE)*/ a;
    int/*@(0, Dialogue2.BASE)*/ b;
    int/*@(0, Dialogue2.NUM_TURNS)*/ yes = 0;
    int/*@(0, 1)*/ persuaded = 0;
    
    public Listener(TurnGame table, int playerNum,
            int comp1, int conj, int comp2) {
        super(table, playerNum);
        strategy = new Random();
        COMP_1 = comp1;
        CONJ = conj;
        COMP_2 = comp2;
        // Model.name(compliance, "", "");
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(Dialogue2.isEnd()) {
                break;
            }
            decide();
            turnNext(0);
        }
        if(Math.random() < yes*1.0/Dialogue2.NUM_TURNS) {
//            Model.stateAnd("persuaded");
            persuaded = 1;
        }
        Model.finish();
    }
    protected int getWeightedSignal(int source, int declare, int out) {
        Source s = Dialogue2.SOURCES[source];
        return (compliance[source] + defiance[source])*declare*
                (compliance[source] - defiance[source])*out;
    }
    protected int getWeight(int source, int declare) {
        Source s = Dialogue2.SOURCES[source];
        return (compliance[source] + defiance[source])*declare*
                Dialogue2.BASE;
    }
    protected void decide() {
        final int MAX_WEIGHT = Dialogue2.BASE*Dialogue2.NUM_SOURCES*(Dialogue2.LEVELS - 1);
        int/*@(0, MAX_WEIGHT*Dialogue2.BASE*Dialogue2.BASE)*/ sSumA = 0;
        int/*@(0, MAX_WEIGHT*Dialogue2.BASE)*/ wSumA = 0;
        int/*@(0, MAX_WEIGHT*Dialogue2.BASE*Dialogue2.BASE)*/ sSumB = 0;
        int/*@(0, MAX_WEIGHT*Dialogue2.BASE)*/ wSumB = 0;
//        Dialogue2.SOURCES[1].outA = -80;
//        Dialogue2.SOURCES[1].outB = 80;
//        Dialogue2.SOURCES[1].declareA = 2;
//        Dialogue2.SOURCES[1].declareB = 0;
        for(int i = 0; i < Dialogue2.NUM_SOURCES; ++i) {
            Source s = Dialogue2.SOURCES[i];
            sSumA = sSumA + getWeightedSignal(i, s.declareA, s.outA);
            wSumA = wSumA + getWeight(i, s.declareA);
            sSumB = sSumB + getWeightedSignal(i, s.declareB, s.outB);
            wSumB = wSumB + getWeight(i, s.declareB);
        }
        a = sSumA/wSumA;
        b = sSumB/wSumB;
        int primary;
        switch(COMP_1) {
            case Dialogue2.COMP_A:
                primary = a;
                break;
                
            case Dialogue2.COMP_B:
                primary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component A");
        }
        int secondary;
        switch(COMP_2) {
            case Dialogue2.COMP_A:
                secondary = a;
                break;
                
            case Dialogue2.COMP_B:
                secondary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component B");
        }
        if(Math.random() < 0.4 + 0.2*primary/Dialogue2.BASE +
                (CONJ == Dialogue2.CONJ_ESPECIALLY ? 0.1 : -0.1)*
                    (secondary - Dialogue2.BASE/2)/(Dialogue2.BASE/2))
            ++yes;
        // decrease trust, if cheating discovered
        modifyAttitude(0);
        modifyAttitude(1);
    }
    protected void modifyAttitude(int comp) {
        int out0;
        int out1;
        int declare;
        switch(comp) {
            case 0:
                out0 = Dialogue2.SOURCES[0].outA;
                out1 = Dialogue2.SOURCES[1].outA;
                declare = Dialogue2.SOURCES[1].declareA;
                break;
                
            case 1:
                out0 = Dialogue2.SOURCES[0].outB;
                out1 = Dialogue2.SOURCES[1].outB;
                declare = Dialogue2.SOURCES[1].declareB;
                break;
                
            default:
                throw new RuntimeException("unknown component");
        }
        if(out0 != out1) {
            if(declare == Dialogue2.LEVELS - 1)
                compliance[1] = 0;
            else if(declare > 0) {
                compliance[1] = compliance[1] -
                        Math.abs(out1 - out0)*(Dialogue2.BASE/10)/Dialogue2.BASE;
                if(compliance[1] < 0)
                    compliance[1] = 0;
            }
        }
    }
}
