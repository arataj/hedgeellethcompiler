package pmgc;

import java.util.Random;

import hc.*;
import hc.game.*;

public class DialogueUnusedBranch {
    final public static int NUM_SOURCES = 2;
    final public static int NUM_TURNS = 1;
    final public static int BASE = 100;
    final public static Source[] SOURCES = new Source[NUM_SOURCES];
    final public static int COMP_A = 0;
    final public static int COMP_B = 1;
    final public static int CONJ_ESPECIALLY = 0;
    final public static int CONJ_DESPITE    = 1;
    final public static int LEVELS = 3;
    final public static boolean CHEATING_POSSIBLE = true;
    /* -1 for variable declaration */
    final public static int FIX_DECLARATION = 0;
    
    public static Listener LISTENER;
    public static int /*@(0, DialogueUnusedBranch.NUM_TURNS + 1)*/ turnCount = 0;
    static int[] A_LIST = { 80, 80 };
    static int[] B_LIST = { 80, 80 };
    
    public static boolean isEnd() {
        ++turnCount;
        return turnCount == DialogueUnusedBranch.NUM_TURNS + 1;
    }
    public static void main(String[] args) {
        TurnGame table = new TurnGame();
        Model.name(table);
        // create sources
        for(int i = 0; i < NUM_SOURCES; ++i) {
            Source c = new Source(table, i, A_LIST[i], B_LIST[i]);
            c.start();
            Model.name(c, "", "" + (i + 1));
            Model.player(c, "s" + (i + 1));
            SOURCES[i] = c;
        }
        LISTENER = new Listener(table, NUM_SOURCES,
            COMP_A, CONJ_DESPITE, COMP_B);
        LISTENER.start();
        Model.name(LISTENER, "", "");
        Model.player(LISTENER, "l");
        table.start(NUM_SOURCES + 1);
        // Model.check("game ends",
        //     "<<2>> Pmin=? [F turnCount=3]");
        Model.check("persuasion min",
            "<<2>> Pmin=? [F persuaded=1]");
        Model.check("persuasion max",
            "<<2>> Pmax=? [F persuaded=1]");
        Model.waitFinish();
        System.out.println("finished");
        // intterupt any thread still running
        SOURCES[0].interrupt();
    }
}
class Source extends TurnPlayer {
    protected final Random strategy;
    public final int A;
    public final int B;
    // sensors always give some information (to avoid division by zero in the listener)
    public int/*@(getNum() == 0 ? 1 : 0, DialogueUnusedBranch.LEVELS - 1)*/ declareA =
        (getNum() == 0 || DialogueUnusedBranch.FIX_DECLARATION == -1 ? DialogueUnusedBranch.LEVELS - 1 : DialogueUnusedBranch.FIX_DECLARATION);
    public int/*@(getNum() == 0 ? 1 : 0, DialogueUnusedBranch.LEVELS - 1)*/ declareB =
        (getNum() == 0 || DialogueUnusedBranch.FIX_DECLARATION == -1 ? DialogueUnusedBranch.LEVELS - 1 : DialogueUnusedBranch.FIX_DECLARATION);
    public int outA;
    public int outB;
    
    public Source(TurnGame table, int playerNum,
            int a, int b) {
        super(table, playerNum);
        strategy = new Random();
        A = a;
        B = b;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(isInterrupted())
                break;
            say();
            turnNext(getNum() + 1);
        }
    }
    protected void say() {
        if(getNum() == 0) {
        /*
            // let it be allowed to replace an intermediate value
            declareA = strategy.nextInt(DialogueUnusedBranch.LEVELS - 1);
            declareA = 1 + declareA;
            // let it be allowed to replace an intermediate value
            declareB = strategy.nextInt(DialogueUnusedBranch.LEVELS - 1);
            declareB = 1 + declareB;
         */
            // sensors give exact values
            declareA = DialogueUnusedBranch.LEVELS - 1;
            declareB = DialogueUnusedBranch.LEVELS - 1;
        } else {
            // choose declaration strength
          if(DialogueUnusedBranch.FIX_DECLARATION == -1) {
              declareA = strategy.nextInt(DialogueUnusedBranch.LEVELS);
              declareB = strategy.nextInt(DialogueUnusedBranch.LEVELS);
          } else {
              declareA = DialogueUnusedBranch.FIX_DECLARATION;
              declareB = DialogueUnusedBranch.FIX_DECLARATION;
          }
    //        declareA = 0;
    //        declareB = 0;
        }
        // choose if to cheat
        if(!DialogueUnusedBranch.CHEATING_POSSIBLE || getNum() == 0 || strategy.nextInt(2) == 8)
            outA = A;
        else
            outA = -A;
        if(!DialogueUnusedBranch.CHEATING_POSSIBLE || getNum() == 0 || strategy.nextInt(2) == 8)
            outB = B;
        else
            outB = -B;
    }
}
class Listener extends TurnPlayer {
    protected final Random strategy;
    final int COMP_1;
    final int CONJ;
    final int COMP_2;
    int[]/*@(0, DialogueUnusedBranch.BASE)*/ compliance = {100, 50};
    int[]/*@(0, DialogueUnusedBranch.BASE)*/ defiance = {0, 20};
    int/*@(0, DialogueUnusedBranch.BASE)*/ a;
    int/*@(0, DialogueUnusedBranch.BASE)*/ b;
    int/*@(0, DialogueUnusedBranch.NUM_TURNS)*/ yes = 0;
    int/*@(0, 1)*/ persuaded = 0;
    
    public Listener(TurnGame table, int playerNum,
            int comp1, int conj, int comp2) {
        super(table, playerNum);
        strategy = new Random();
        COMP_1 = comp1;
        CONJ = conj;
        COMP_2 = comp2;
        // Model.name(compliance, "", "");
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(DialogueUnusedBranch.isEnd()) {
                break;
            }
            decide();
            turnNext(0);
        }
        if(Math.random() < yes*1.0/DialogueUnusedBranch.NUM_TURNS) {
//            Model.stateAnd("persuaded");
            persuaded = 1;
        }
        Model.finish();
    }
    protected void decide() {
        final int MAX_WEIGHT = DialogueUnusedBranch.BASE*DialogueUnusedBranch.NUM_SOURCES*(DialogueUnusedBranch.LEVELS - 1);
        int/*@(0, MAX_WEIGHT*DialogueUnusedBranch.BASE*DialogueUnusedBranch.BASE)*/ sSumA = 0;
        int/*@(0, MAX_WEIGHT*DialogueUnusedBranch.BASE)*/ wSumA = 0;
        int/*@(0, MAX_WEIGHT*DialogueUnusedBranch.BASE*DialogueUnusedBranch.BASE)*/ sSumB = 0;
        int/*@(0, MAX_WEIGHT*DialogueUnusedBranch.BASE)*/ wSumB = 0;
        for(int i = 0; i < DialogueUnusedBranch.NUM_SOURCES; ++i) {
            Source s = DialogueUnusedBranch.SOURCES[i];
            sSumA = sSumA + (compliance[i] + defiance[i])*s.declareA*
                (compliance[i] - defiance[i])*s.outA;
            wSumA = wSumA + (compliance[i] + defiance[i])*s.declareA*
                DialogueUnusedBranch.BASE;
            sSumB = sSumB + (compliance[i] + defiance[i])*s.declareB*
                (compliance[i] - defiance[i])*s.outB;
            wSumB = wSumB + (compliance[i] + defiance[i])*s.declareB*
                DialogueUnusedBranch.BASE;
        }
        a = sSumA/wSumA;
        b = sSumB/wSumB;
        int primary;
        switch(COMP_1) {
            case DialogueUnusedBranch.COMP_A:
                primary = a;
                break;
                
            case DialogueUnusedBranch.COMP_B:
                primary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component A");
        }
        int secondary;
        switch(COMP_2) {
            case DialogueUnusedBranch.COMP_A:
                secondary = a;
                break;
                
            case DialogueUnusedBranch.COMP_B:
                secondary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component B");
        }
        if(Math.random() < 0.2 + 0.4*primary/DialogueUnusedBranch.BASE +
                (CONJ == DialogueUnusedBranch.CONJ_ESPECIALLY ? 0.1 : -0.1)*
                    (secondary - DialogueUnusedBranch.BASE/2)/(DialogueUnusedBranch.BASE/2))
            ++yes;
        // decrease trust, if cheating discovered
        if(DialogueUnusedBranch.SOURCES[0].outA != DialogueUnusedBranch.SOURCES[1].outA) {
            if(DialogueUnusedBranch.SOURCES[1].declareA == DialogueUnusedBranch.LEVELS - 1)
                compliance[1] = 0;
            else if(DialogueUnusedBranch.SOURCES[1].declareA > 0 && compliance[1] > 0)
                compliance[1] = compliance[1] - 1;
        }
        if(DialogueUnusedBranch.SOURCES[0].outB != DialogueUnusedBranch.SOURCES[1].outB) {
            if(DialogueUnusedBranch.SOURCES[1].declareB == DialogueUnusedBranch.LEVELS - 1)
                compliance[1] = 0;
            else if(DialogueUnusedBranch.SOURCES[1].declareB > 0 && compliance[1] > 0)
                compliance[1] = compliance[1] - 1;
        }
    }
}
