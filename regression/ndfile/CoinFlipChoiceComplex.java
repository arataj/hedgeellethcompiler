package example.sim;

import java.util.*;
import hc.*;

/*
 * Optimiser could not simplify the choice subautomaton.
 */
class CoinFlipChoiceComplex extends Beam {
  final static int N = 100;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlipChoiceComplex() {
    super();
  }
  protected boolean decide() {
    return unknown(4, N)*2 >= 4;
  }
  public void run() {
    init();
    for(; flips < N; ++flips) {
      if(decide()) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
      step();
    }
    done();
  }
  public static void main(String[] args) {
    CoinFlipChoiceComplex cf = new CoinFlipChoiceComplex();
    Model.name(cf);
    cf.start();
    Model.check("50/50", "Pmax=? [F flips=N & heads=N/2]");
  }
}
