package example.sim;

import java.util.*;
import hc.*;

class CoinFlipChoiceSwitchDefault extends Beam {
  final static int N = 100;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlipChoiceSwitchDefault() {
    super();
  }
  public void run() {
    for(; flips < N; ++flips) {
      switch(unknown(10, flips)) {
        case 0:
          if(Math.random() < 0.5)
            ++heads;
          break;
          
        case 2:
          if(Math.random() < 0.6)
            ++heads;
          break;
          
        default:
          if(Math.random() < 0.4)
            ++heads;
      }
    }
  }
  public static void main(String[] args) {
    CoinFlipChoiceSwitchDefault cf = new CoinFlipChoiceSwitchDefault();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}
