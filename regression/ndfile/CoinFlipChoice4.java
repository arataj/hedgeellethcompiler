package example.sim;

import java.util.*;
import hc.*;

class CoinFlipChoice4 extends Beam {
  final static int N = 100;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlipChoice4() {
    super();
  }
  protected boolean decide() {
    return unknown(4, N) >= 1 + 1;
  }
  public void run() {
    init();
    for(; flips < N; ++flips) {
      if(decide()) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
      step();
    }
    done();
  }
  public static void main(String[] args) {
    CoinFlipChoice4 cf = new CoinFlipChoice4();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}
