package example.sim;

import java.util.*;
import hc.*;

class CoinFlipArgs extends Beam {
  final static int N = 100;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlipArgs() {
    super();
  }
  protected boolean decide() {
    return unknown(2, N, flips, heads) == 0;
  }
  public void run() {
    init();
    for(; flips < N; ++flips) {
      if(decide()) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
      step();
    }
    done();
  }
  public static void main(String[] args) {
    CoinFlipArgs cf = new CoinFlipArgs();
    Model.name(cf);
    cf.start();
    Model.check("", "Pmax=? [F flips=N & heads=N/2]");
  }
}
