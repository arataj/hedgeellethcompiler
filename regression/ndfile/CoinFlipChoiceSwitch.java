package example.sim;

import java.util.*;
import hc.*;

class CoinFlipChoiceSwitch extends Beam {
  final static int N = 100;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public CoinFlipChoiceSwitch() {
    super();
  }
  public void run() {
    init();
    for(; flips < N; ++flips) {
      switch(unknown(3)) {
        case 0:
          if(Math.random() < 0.5)
            ++heads;
          break;
          
        case 1:
          if(Math.random() < 0.6)
            ++heads;
          break;
          
      }
      step();
    }
    done();
  }
  public static void main(String[] args) {
    CoinFlipChoiceSwitch cf = new CoinFlipChoiceSwitch();
    Model.name(cf);
    cf.start();
    Model.check("three choices", "Pmax=? [F flips=N & heads=N/2]");
  }
}
