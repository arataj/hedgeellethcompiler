package test;

class Experiment extends Thread {
  public static int x = 1~3:2#5;
  public static int[] y = {7, 1~2};

  public void run() {
    y[0] = x;
  }
  public static void main(String[] args) {
    Experiment e = new Experiment();
    e.start();
  }
}
