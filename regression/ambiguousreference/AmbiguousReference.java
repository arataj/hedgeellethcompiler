class AmbiguousReference extends Thread {
    int fi;
    AmbiguousReference fr1;
    AmbiguousReference fr2;
    
    public void run() {
        AmbiguousReference r = fr1;
        do {
            AmbiguousReference s = r;
            // depending on iteration, s can point to
            // different instances
            fi = s.fi/8;
            r = fr2;
        } while(fi > 0);
    }
    public static void main(String[] args) {
        AmbiguousReference a1 = new AmbiguousReference();
        AmbiguousReference a2 = new AmbiguousReference();
        a1.fr1 = a1;
        a1.fr2 = a2;
        a1.start();
    }
}
