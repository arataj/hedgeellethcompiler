class UnambiguousReference extends Thread {
    int fi;
    UnambiguousReference fr1;
    UnambiguousReference fr2;
    
    public void run() {
        UnambiguousReference r = fr1;
        do {
            UnambiguousReference s = r;
            fi = s.fi/8;
            r = fr1;
        } while(fi > 0);
    }
    public static void main(String[] args) {
        UnambiguousReference u = new UnambiguousReference();
        u.fr1 = u;
        u.fr2 = u;
        u.start();
    }
}
