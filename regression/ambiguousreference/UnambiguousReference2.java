// more complex case to trace tham UnambiguousReference.java,
// as fields need to be replaced by constants for the tracing
// to work
class UnambiguousReference2 extends Thread {
    int fi;
    UnambiguousReference2 fr1;
    UnambiguousReference2 fr2;
    
    public void run() {
        UnambiguousReference2 r = fr1;
        do {
            UnambiguousReference2 s = r;
            fi = s.fi/8;
            r = fr2;
        } while(fi > 0);
    }
    public static void main(String[] args) {
        UnambiguousReference2 a = new UnambiguousReference2();
        a.fr1 = a;
        a.fr2 = a;
        a.start();
    }
}
