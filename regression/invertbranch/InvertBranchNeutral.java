class InvertBranchNeutral extends Thread {
  static boolean fBoolean = true;
  static int fInt = 2;
  
  public void run() {
      do {
          boolean b1 = fBoolean;
          boolean b2 = fInt == 1;
          b2 = !b2;
          b1 = !b1;
          fBoolean = true;
          if(b1)
              fInt = 1;
          else
              fInt = 0;
          if(b2)
              fInt = 2;
      } while(true);
  }
  public static void main(String[] args) {
      InvertBranchNeutral ib = new InvertBranchNeutral();
      ib.start();
  }
}
