/* Author: Andrzej Zbrzezny (c) 2007 */

package modelchecking.abp.v2;

import java.util.Random;

public class LossyChannel {
  private boolean protocolBit;
  private boolean channelEmpty = true;

  private boolean ackBit = true;

  private Random random;

  public LossyChannel() {
    random = new Random();
  }

  public synchronized boolean getAckBit() {
    //notify();
    return ackBit;
  }

  public synchronized void putAckBit(boolean ackBit) {
    this.ackBit = ackBit;
    System.out.print("ACK: " + this.ackBit);
    int ignoreBit = random.nextInt(5);
    if (ignoreBit > 2) {
      System.out.println(" -- bit ignored");
      this.ackBit = !this.ackBit;
    } else {
      System.out.println("");
    }
    //notify();
  }

  public synchronized boolean get() {
    if (channelEmpty) {
      try {
        wait();
      } catch (InterruptedException e) {
      }
    }
    channelEmpty = true;
    notify();
    return protocolBit;
  }

  public synchronized void put(boolean protocolBit) {
    if (!channelEmpty) {
      try {
        wait();
      } catch (InterruptedException e) {
      }
    }
    this.protocolBit = protocolBit;
    channelEmpty = false;
    System.out.print("CHANNEL: ");
    System.out.print("Bit -> " + protocolBit);
    int ignorePacket = random.nextInt(5);
    if (ignorePacket > 2) {
      channelEmpty = true;
      System.out.println(" -- packed ignored");
    } else {
      System.out.println("");
    }
    notify();
  }
}

