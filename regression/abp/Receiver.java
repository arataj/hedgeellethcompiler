/* Author: Andrzej Zbrzezny (c) 2007 */

package modelchecking.abp.v2;

import java.util.Random;

public class Receiver implements Runnable {
  private LossyChannel channel;

  public Receiver(LossyChannel channel) {
    this.channel = channel;
  }

  public void run() {
    Random random = new Random();
    while (true) {
      boolean protocolBit = channel.get();
      channel.putAckBit(protocolBit);
      try {
        Thread.sleep(random.nextInt(150));
      } catch (InterruptedException e) {
      }
    }
  }
}
