package test;

class A {
  private int fieldPrivate;
  protected int fieldProtected;
  public int fieldPublic;

  private void methodPrivate() {}
  protected void methodProtected() {}
  public void methodPublic() {}

  private A() {
  }
  protected int m() {
    return 5;
  }
  public void p() {
  }
}
