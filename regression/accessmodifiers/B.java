package test;

class B extends A {
  int test() {
    int i = fieldPrivate + fieldProtected + fieldPublic + fieldPrivate;
    methodPrivate();
    methodProtected();
    methodPublic();
  }
  private int m() {
    return 5;
  }
}
