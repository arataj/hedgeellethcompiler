/*
 * Barrier conditions not atomic.
 */

package example;

import java.util.Random;

import hc.*;

public class Producer extends Thread {
    Barrier SYNC;
    
    public Producer(Barrier sync) {
        this.SYNC = sync;
    }
    @Overwrite
    public void run() {
        while(true) {
            // produce
            Thread.sleep(1000);
            SYNC.produce();
        }
    }
}

public class Consumer extends Thread {
    Barrier SYNC;
    
    public Consumer(Barrier sync) {
        this.SYNC = sync;
    }
    @Overwrite
    public void run() {
        while(true) {
            SYNC.consume();
            // consume
            Thread.sleep(2000);
        }
    }
}

public class AtomicAsymmetricTimed {
    public static void main(String[] args) {
        Barrier barrier = new Barrier();
        Producer p1 = new Producer(barrier);
        Producer p2 = new Producer(barrier);
        Consumer c1 = new Consumer(barrier);
        Consumer c2 = new Consumer(barrier);
        p1.start();
        p2.start();
        c1.start();
        c2.start();
        barrier.activate(2, 2);
    }
}
