/*
 * Barrier conditions not atomic.
 */

package example;

import java.util.Random;

import hc.*;

public class Producer extends Thread implements BooleanClosure {
    Barrier SYNC;
    public static int counterS;
    public int counterProducer;
    
    public Producer(Barrier sync) {
        this.SYNC = sync;
    }
    public boolean booleanClosure() {
        return counterProducer%2*2 <= -Consumer.counterS;
    }
    @Overwrite
    public void run() {
        while(true) {
            // produce
            Thread.sleep(1000);
            SYNC.produce();
            // produce
            Thread.sleep(2000);
            SYNC.produce(this);
            counterProducer = (counterProducer + 1)%4;
        }
    }
}

public class Consumer extends Thread implements BooleanClosure {
    Barrier SYNC;
    public static int counterS;
    public int counterConsumer;
    
    public Consumer(Barrier sync) {
        this.SYNC = sync;
    }
    public boolean booleanClosure() {
        return (counterConsumer - 8)*2 > counterS + Producer.counterS;
    }
    @Overwrite
    public void run() {
        while(true) {
            counterConsumer = (counterConsumer + 1)%3;
            SYNC.consume();
            // consume
            Thread.sleep(1000);
            SYNC.consume(this);
            // consume
            Thread.sleep(2000);
        }
    }
}

public class BarrierAsymmetricComplex {
    public static void main(String[] args) {
        Barrier barrier = new Barrier(false);
        Producer p1 = new Producer(barrier);
        Producer p2 = new Producer(barrier);
        Producer p3 = new Producer(barrier);
        Consumer c1 = new Consumer(barrier);
        Consumer c2 = new Consumer(barrier);
        p1.start();
        p2.start();
        p3.start();
        c1.start();
        c2.start();
        barrier.activate(3, 2);
    }
}
