/*
 * AbstractNode.java
 *
 * Created on Nov 5, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.Sleep;

/**
 * A node in a Mirela dependency graph.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractNode extends Thread {
    /**
     * A next node index to acquire using <code>acquireIndex()</code>.
     */
    private static int nextIndex;
    /**
     * An unique index of this node.
     */
    public final int INDEX;
    /**
     * Maximum possible number of consumers.
     */
    protected final int MAX_NUM_CONSUMERS = 3;
    /**
     * Consumers, at <code>0 ... numListeners - 1<code>.
     */
    protected AbstractConsumer[] consumers;
    /**
     * Number of consumers to this node.
     */
    protected int numConsumers;
    
    /**
     * Creates a new node.
     */
    public AbstractNode(Mirela system) {
        INDEX = acquireIndex();
        consumers = new AbstractConsumer[MAX_NUM_CONSUMERS];
        numConsumers = 0;
    }
    private static int acquireIndex() {
        return nextIndex++;
    }
    /**
     * Adds another node to this node's output.
     * 
     * @param consumer the node to add
     */
    public void addOut(AbstractConsumer consumer) {
        consumers[numConsumers] = consumer;
        consumer.addProducer(this);
        ++numConsumers;
    }
    /**
     * Sleeps.
     * 
     * @param delay determines the period of time to sleep
     */
    protected void sleep(Delay delay) {
        Sleep.exact(delay.min);
        Sleep.max(delay.max - delay.min);
    }
    /**
     * Creates a product for all its consumers.
     */
    protected void produce() {
        ((First)consumers[1]).put(INDEX);
        for(int i = 0; i < numConsumers; ++i)
            if(consumers[i] instanceof First)
                ((Both)consumers[i]).put(INDEX);
            else if(consumers[i] instanceof Both)
                ((Both)consumers[i]).put(INDEX);
    }
    /**
     * Adds this node to the system.
     */
    public void active() {
        start();
    }
}
