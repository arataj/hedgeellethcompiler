package example;

import hc.*;
import mirela.*;

public class Urgent {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        final int CAMERA_WARM_UP = 500;
        Delay cameraLoop = new Delay(2000, 3000);
        Delay cameraStart = new Delay(
            cameraLoop.getMin() + CAMERA_WARM_UP,
            cameraLoop.getMax() + CAMERA_WARM_UP);
        Periodic camera1 = new Periodic(m, cameraStart, cameraLoop);
        Periodic camera2 = new Periodic(m, cameraStart, cameraLoop);
        Delay acquireDelay = new Delay(1000, 2000);
        Delay[] delays = { acquireDelay, acquireDelay, };
        First acquire = new First(m, delays);
        camera1.addOut(acquire);
        camera2.addOut(acquire);
        m.enable();
    }
}
