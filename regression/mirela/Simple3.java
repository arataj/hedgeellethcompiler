package example;

import java.util.Random;

import hc.*;

public class Producer extends Thread {
    Barrier SYNC;
    
    public Producer(Barrier sync) {
        this.SYNC = sync;
    }
    @Overwrite
    public void run() {
        while(true) {
            // produce
            Thread.sleep(100);
            SYNC.barrier();
            Thread.sleep(200);
            SYNC.barrier();
        }
    }
}

public class Consumer extends Thread {
    Barrier SYNC;
    
    public Consumer(Barrier sync) {
        this.SYNC = sync;
    }
    @Overwrite
    public void run() {
        while(true) {
            SYNC.barrier();
            // consume
            Thread.sleep(100);
        }
    }
}

public class Simple3 {
    public static void main(String[] args) {
        Barrier barrier = new Barrier();
        Producer p1 = new Producer(barrier);
        Producer p2 = new Producer(barrier);
        Consumer c = new Consumer(barrier);
        p1.start();
        p2.start();
        c.start();
        barrier.activate(3);
    }
}
