/*
 * Barrier conditions atomic.
 */

package example;

import java.util.Random;

import hc.*;

public class Producer extends Thread implements BooleanClosure {
    Barrier SYNC;
    public static int counterS;
    public int counterProducer;
    
    public Producer(Barrier sync) {
        this.SYNC = sync;
    }
    public boolean booleanClosure() {
        return counterProducer == Consumer.counterS;
    }
    @Overwrite
    public void run() {
        while(true) {
            // produce
            Thread.sleep(1000);
            SYNC.produce();
            // produce
            Thread.sleep(2000);
            SYNC.produce(this);
            counterProducer = (counterProducer + 1)%4;
        }
    }
}

public class Consumer extends Thread implements BooleanClosure {
    Barrier SYNC;
    public static int counterS;
    public int counterConsumer;
    
    public Consumer(Barrier sync) {
        this.SYNC = sync;
    }
    public boolean booleanClosure() {
        return counterConsumer == Producer.counterS;
    }
    @Overwrite
    public void run() {
        while(true) {
            counterConsumer = (counterConsumer + 1)%3;
            SYNC.consume();
            // consume
            Thread.sleep(1000);
            SYNC.consume(this);
            // consume
            Thread.sleep(2000);
        }
    }
}

public class BarrierAsymmetricSimple {
    public static void main(String[] args) {
        Barrier barrier = new Barrier();
        Producer p1 = new Producer(barrier);
        Producer p2 = new Producer(barrier);
        Producer p3 = new Producer(barrier);
        Consumer c = new Consumer(barrier);
        p1.start();
        p2.start();
        p3.start();
        c.start();
        barrier.activate(3, 1);
    }
}
