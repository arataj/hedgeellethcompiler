package example;

import hc.*;
import mirela.*;

public class Mirela1 {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Delay cameraStart = new Delay(2500, 3500);
        Delay cameraLoop = new Delay(2000, 3000);
        Periodic camera1 = new Periodic(m, cameraStart, cameraLoop);
        Periodic camera2 = new Periodic(m, cameraStart, cameraLoop);
        Delay[] acquireDelay = { new Delay(2000, 3000) };
        First acquireFrame = new First(m, acquireDelay);
        camera1.addOut(acquireFrame);
        camera2.addOut(acquireFrame);
        camera1.active();
        camera2.active();
        acquireFrame.active();
    }
}
