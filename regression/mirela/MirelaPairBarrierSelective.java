package models.mirela;

import mirela.*;

public class MirelaPairBarrierSelective {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Periodic c = new Periodic(m,
            new Delay(2000, 3000), new Delay(3500, 4500));
        Aperiodic o = new Aperiodic(m, 50);
        Priority pr = new Priority(m, new Delay(20, 30), new Delay(200, 300));
        c.addOut(pr);
        o.addOut(pr);
        m.enable();
    }
}
