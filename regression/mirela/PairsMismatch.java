package example;

import java.util.Random;

import hc.*;

public class Producer extends Thread implements BooleanClosure {
    PairBarrier SYNC_SYMMETRIC;
    PairBarrier SYNC_ASYMMETRIC;
    public int counterProducer;
    
    public Producer(PairBarrier syncSymmetric, PairBarrier syncAsymmetric) {
        this.SYNC_SYMMETRIC = syncSymmetric;
        this.SYNC_ASYMMETRIC = syncAsymmetric;
    }
    public boolean booleanClosure() {
        return 0 == counterProducer;
    }
    @Overwrite
    public void run() {
        while(true) {
            SYNC_SYMMETRIC.produce(this);
            SYNC_ASYMMETRIC.barrier();
            ++counterProducer;
        }
    }
}

public class Consumer extends Thread implements BooleanClosure {
    PairBarrier SYNC_SYMMETRIC;
    PairBarrier SYNC_ASYMMETRIC;
    public int counterConsumer;
    
    public Consumer(PairBarrier syncSymmetric, PairBarrier syncAsymmetric) {
        this.SYNC_SYMMETRIC = syncSymmetric;
        this.SYNC_ASYMMETRIC = syncAsymmetric;
    }
    public boolean booleanClosure() {
        return 0 == counterConsumer;
    }
    @Overwrite
    public void run() {
        while(true) {
            SYNC_ASYMMETRIC.barrier(this);
            SYNC_SYMMETRIC.consume();
            ++counterConsumer;
        }
    }
}

public class Pairs {
    public static void main(String[] args) {
        PairBarrier barrierSymmetric = new PairBarrier();
        PairBarrier barrierAsymmetric = new PairBarrier();
        Producer p1 = new Producer(barrierSymmetric, barrierAsymmetric);
        Producer p2 = new Producer(barrierSymmetric, barrierAsymmetric);
        Consumer c1 = new Consumer(barrierSymmetric, barrierAsymmetric);
        Consumer c2 = new Consumer(barrierSymmetric, barrierAsymmetric);
        p1.start();
        p2.start();
        c1.start();
        c2.start();
        barrierSymmetric.activate(4);
        barrierAsymmetric.activate(2, 2);
    }
}
