package example;

import hc.*;
import mirela.*;

public class Mirela1Prob {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        final int CAMERA_WARM_UP = 500;
        Delay cameraLoop = new Delay(1000, 2000);
        AbstractDelay cameraStart = new Delay(
            cameraLoop.getMin() + CAMERA_WARM_UP,
            cameraLoop.getMax() + CAMERA_WARM_UP);
        Periodic camera1 = new Periodic(m, cameraStart, cameraLoop);
        Periodic camera2 = new Periodic(m, cameraStart, cameraLoop);
        AbstractDelay acquireDelay = new Delay(1000, 2000);
        Both acquire = new Both(m, acquireDelay);
        camera1.addOut(acquire);
        camera2.addOut(acquire);
        Aperiodic inertial = new Aperiodic(m, 1000);
        final int FRAME_NUM = 2;
        final int PROCESS_FRAME_DELAY = 2000;
        AbstractDelay[] createSceneDelay = {
            // process stereo image
            new MarginDelay(
                new Delay(
                    2000 + PROCESS_FRAME_DELAY*FRAME_NUM,
                    3000 + PROCESS_FRAME_DELAY*FRAME_NUM),
                new Delay(
                    1000 + PROCESS_FRAME_DELAY*FRAME_NUM,
                    4000 + PROCESS_FRAME_DELAY*FRAME_NUM),
                0.9),
            // process inertial data
            new Delay(1000, 2000),
        };
        First update = new First(m, createSceneDelay);
        inertial.addOut(update);
        acquire.addOut(update);
        Both disk = new Both(m, /* seek delay */ new Delay(1000, 1500));
        inertial.addOut(disk);
        Link network = new Link(m, new Delay(500, 1000), 0.01);
        disk.addOut(network);
        AbstractDelay[] writeScene = {
            // write a newly generated scene
            new Delay(2000, 3000),
            // write a scene recorded on disk
            new Delay(500, 1000),
        };
        Memory scene = new Memory(m, writeScene);
        update.addOut(scene);
        network.addOut(scene);
        AbstractDelay readScene = new Delay(500, 1000);
        final int RENDER_BUFFER_MIN = 500;
        final int RENDER_BUFFER_MAX = 1000;
        AbstractDelay displayScene2d =
            new Delay(RENDER_BUFFER_MIN, RENDER_BUFFER_MAX);
        AbstractDelay displayScene3d =
            new Delay(2*RENDER_BUFFER_MIN, 2*RENDER_BUFFER_MAX);
        Rendering display2d = new Rendering(m,
            scene, readScene, displayScene2d);
        Rendering display3d = new Rendering(m,
            scene, readScene, displayScene3d);
        m.enable();
    }
}
