package models.mirela;

import mirela.*;

public class MirelaEx2c {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Periodic ca = new Periodic(m,
            new Delay(2000, 3000), new Delay(3500, 4500));
        Periodic i = new Periodic(m,
            new Delay(400, 600), new Delay(900, 1000));
        Delay[] uDelay = {
            new Delay(7000, 9000),
        };
        First u = new First(m, uDelay);
        ca.addOut(u);
        Delay[] fDelay = {
            new Delay(40, 60), new Delay(40, 60),
        };
        Priority f = new PriorityS(m, fDelay[0], fDelay[1]);
        Delay[] lDelay = {
            new Delay(20, 30), new Delay(20, 30),
        };
        Priority l = new Priority(m, lDelay[0], lDelay[1]);
        f.addOut(l);
        Delay[] m1Delay = {
            new Delay(300, 400), new Delay(10, 20),
        };
        Memory m1 = new Memory(m, m1Delay);
        u.addOut(m1);
        u.addOut(l);
        l.addOut(m1);
        l.addOut(f);
        i.addOut(f);
        Rendering g = new Rendering(m, m1,
                new Delay(310, 420), new Delay(500, 750));
        Rendering h = new Rendering(m, m1,
                new Delay(20, 30), new Delay(80, 120));
        m.enable();
    }
}
