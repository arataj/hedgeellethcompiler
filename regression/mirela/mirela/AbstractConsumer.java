/*
 * AbstractConsumer.java
 *
 * Created on Nov 29, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * An abstract consumer.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractConsumer extends AbstractNode {
    /**
     * Maximum possible number of producers.
     */
    protected final int MAX_NUM_CONSUMERS = 3;
    /**
     * Listeners, at <code>0 ... numProducers - 1<code>.
     */
    protected AbstractNode[] producers;
    /**
     * Number of producers to this node.
     */
    protected int numProducers;
    
    /**
     * Creates a new node.
     * 
     * @param system Mirela system, to which this node will be added
     */
    public AbstractConsumer(Mirela system) {
        super(system);
        producers = new AbstractConsumer[MAX_NUM_CONSUMERS];
        numProducers = 0;
    }
    /**
     * Informs a consumer about its input node. Called
     * automatically on producers by addOut().
     * 
     * @param producer the producer to add
     */
    public void addProducer(AbstractNode producer) {
        producers[numProducers] = producer;
        ++numProducers;
    }
    /**
     * Puts a product to this consumer.
     * 
     * @param index the producer's index
     */
    abstract protected void put(int index);
}
