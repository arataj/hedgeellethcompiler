/*
 * MarginDelay.java
 *
 * Created on Dec 13, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

/**
 * <p>Represents two non--deterministic delays, bounded with a minimum and
 * a maximum value. A probability value decides on which one is chosen.</p>
 * 
 * <p>By convention, the 2nd delay is an `outlier' one, stretching beyond the 1st one
 * and having a lower probability.</p>
 * 
 * @author Artur Rataj
 */
public class MarginDelay implements AbstractDelay {
    /**
     * The 1st delay, by convention the typical one.
     */
    AbstractDelay typical;
    /**
     * The 2nd delay, by convention an outlier.
     */
    AbstractDelay outlier;
    /**
     * Probability of choosing the 1st delay.
     */
    double typicalProb;
    
    /**
     * Creates a new instance of <code>MarginDelay</code>.
     * 
     * @param typical the 1st delay
     * @param outlier the 2nd delay
     * @param typicalProb probaility of choosing the 1st delay
     */
    public MarginDelay(AbstractDelay typical, AbstractDelay outlier, double typicalProb) {
        this.typical = typical;
        this.outlier = outlier;
        this.typicalProb = typicalProb;
    }
    /**
     * Returns the 1st delay.
     * 
     * @return a delay
     */
    public AbstractDelay getTypical() {
        return typical;
    }
    /**
     * Returns the 2nd delay.
     * 
     * @return a delay
     */
    public AbstractDelay getOutlier() {
        return outlier;
    }
    /**
     * Returns the probability of choosing the 1st delay.
     * 
     * @return a probaility
     */
    public double getTypicalProb() {
        return typicalProb;
    }
    /* implements */
    public void sleep() {
        if(Math.random() < getTypicalProb())
            typical.sleep();
        else
            outlier.sleep();
    }
}
