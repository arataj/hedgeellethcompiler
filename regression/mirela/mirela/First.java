/*
 * First.java
 *
 * Created on Nov 18, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * A Mirela component of the type First.
 * 
 * @author Artur Rataj
 */
public class First extends AbstractConsumer {
    /**
     * Processing times of data from subsequent inputs.
     */
    AbstractDelay[] process;
    PairBarrier sync;
    
    /**
     * Creates a new instance of <code>First</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param process processing times of data from subsequent inputs
     */
    public First(Mirela system, AbstractDelay[] process) {
        super(system);
        this.process = process;
        sync = new PairBarrier();
    }
    protected void put(int index) {
        sync.produce(index);
    }
    @Override
    public void active() {
        super.active();
        if(numProducers > process.length)
            throw new RuntimeException("delay array too short");
        sync.activate(numProducers, 1);
    }
    @Override
    public void run() {
        boolean commonDelay = true;
        for(int i = 0; i < process.length - 1; ++i)
            if(!process[i].equals(process[i + 1])) {
                commonDelay = false;
                break;
            }
        while(true) {
            int p = sync.consume();
            if(commonDelay)
                process[0].sleep();
            else
                for(int i = 0; i < numProducers; ++i)
                    if(p == producers[i].INDEX) {
                        process[i].sleep();
                        break;
                    }
            produce();
        }
    }
}
