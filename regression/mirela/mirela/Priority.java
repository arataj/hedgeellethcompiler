/*
 * Priority.java
 *
 * Created on Feb 27, 2014
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * A Mirela component of the type Priority.
 * 
 * @author Artur Rataj
 */
public class Priority extends AbstractConsumer {
    /**
     * Processing time of data if synchronised with master only.
     */
    AbstractDelay master;
    /**
     * Processing time of data if slave was first to synchronise
     */
    AbstractDelay both;
    PairBarrier sync;
    
    /**
     * Creates a new instance of <code>Priority</code>. Master
     * should be added as the first producer, then slave.
     * 
     * @param system Mirela system, to which this node will be added
     * @param master processing time if synchronised with master only
     * @param both processing time if slave was first
     */
    public Priority(Mirela system, AbstractDelay master, AbstractDelay both) {
        super(system);
        this.master = master;
        this.both = both;
        sync = new PairBarrier();
    }
    protected void put(int index) {
        sync.produce(index);
    }
    @Override
    public void active() {
        super.active();
        if(numProducers != 2)
            throw new RuntimeException("requires exactly master and slave");
        sync.activate(numProducers, 1);
    }
    @Override
    public void run() {
        while(true) {
            int p = sync.consume();
            if(p == producers[0].INDEX)
                master.sleep();
            else {
                // synchronised with slave, and so now
                // wait exclusively for master
                sync.consume(producers[0].INDEX);
                both.sleep();
            }
        }
    }
}
