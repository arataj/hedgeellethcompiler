/*
 * AbstractDelay.java
 *
 * Created on Dec 12, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

/**
 *
 * @author Artur Rataj
 */
public interface AbstractDelay {
    /**
     * Sleeps.
     */
    public void sleep();
}
