/*
 * Rendering.java
 *
 * Created on Dec 3, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * A Mirela component of the type Rendering.
 * 
 * @author Artur Rataj
 */
public class Rendering extends AbstractNode {
    /**
     * Memory to read from.
     */
    Memory memory;
    /**
     * Processing time of reading the memory.
     */
    AbstractDelay read;
    /**
     * Processing time of rendering.
     */
    AbstractDelay render;
    
    /**
     * Creates a new instance of <code>First</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param memory memory to read from
     * @param read processing time of reading the memory
     * @param render processing time of rendering
     */
    public Rendering(Mirela system, Memory memory,
            AbstractDelay read, AbstractDelay render) {
        super(system);
        this.memory = memory;
        this.read = read;
        this.render = render;
    }
    @Override
    public void run() {
        while(true) {
            synchronized(memory) {
                read.sleep();
            }
            render.sleep();
        }
    }
}
