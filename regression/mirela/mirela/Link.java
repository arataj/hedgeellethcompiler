/*
 * Link.java
 *
 * Created on Dec 15, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * A Mirela component of the type Link. It losses a given percentage of
 * tokens, and also intorduces a delay.
 * 
 * @author Artur Rataj
 */
public class Link extends AbstractConsumer {
    /**
     * Processing times of data from subsequent inputs.
     */
    AbstractDelay transmit;
    /**
     * Probability of losing a token.
     */
    double lostProb;
    Barrier sync;
    
    /**
     * Creates a new instance of <code>First</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param transmit transmission time
     * @param lostProb probability of losing a token
     */
    public Link(Mirela system, AbstractDelay transmit, double lostProb) {
        super(system);
        this.transmit = transmit;
        this.lostProb = lostProb;
        sync = new Barrier();
    }
    protected void put(int index) {
        sync.produce();
    }
    @Override
    public void active() {
        super.active();
        sync.activate(numProducers, 1);
    }
    @Override
    public void run() {
        while(true) {
            sync.consume();
            transmit.sleep();
            if(Math.random() > lostProb)
                produce();
        }
    }
}
