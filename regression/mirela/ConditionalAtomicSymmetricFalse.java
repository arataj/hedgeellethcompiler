/*
 * Barrier conditions optimized to false.
 */

package example;

import java.util.Random;

import hc.*;

public class Producer extends Thread implements BooleanClosure {
    Barrier SYNC;
    public static int counterS;
    public int counterProducer;
    
    public Producer(Barrier sync) {
        this.SYNC = sync;
    }
    public boolean booleanClosure() {
        return counterS == Consumer.counterS;
    }
    @Overwrite
    public void run() {
        while(true) {
            // produce
            Thread.sleep(100);
            SYNC.barrier(this);
            // produce
            Thread.sleep(200);
            SYNC.barrier();
        }
    }
}

public class Consumer extends Thread implements BooleanClosure {
    Barrier SYNC;
    public static int counterS;
    public int counterConsumer;
    
    public Consumer(Barrier sync) {
        this.SYNC = sync;
    }
    public boolean booleanClosure() {
        return counterS == Producer.counterS;
    }
    @Overwrite
    public void run() {
        while(true) {
            SYNC.barrier(this);
            // consume
            Thread.sleep(100);
            SYNC.barrier(this);
            // consume
            Thread.sleep(100);
        }
    }
}

public class ConditionalAtomicSymmetricFalse {
    public static void main(String[] args) {
        Barrier barrier = new Barrier();
        Producer p1 = new Producer(barrier);
        Producer p2 = new Producer(barrier);
        Consumer c = new Consumer(barrier);
        p1.start();
        p2.start();
        c.start();
        Producer.counterS = -129;
        barrier.activate(3);
    }
}
