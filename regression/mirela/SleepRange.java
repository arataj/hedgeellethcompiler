package example;

import java.util.Random;

import hc.*;

public class Sleeps extends Thread {
    Barrier SYNC;
    public int j;
    
    public Sleeps(Barrier sync) {
        this.SYNC = sync;
    }
    @Overwrite
    public void run() {
        Sleep.min(100.0);
        Sleep.max(110.0);
        j = 1;
        Sleep.min(200.0);
        Sleep.exact(210.0);
        j = 2;
        Sleep.max(300.0);
        Sleep.exact(310.0);
        j = 3;
        Sleep.exact(400.0);
        Sleep.exact(410.0);
        j = 4;
        Sleep.exact(500.0);
        Sleep.min(510.0);
        Sleep.max(520.0);
        Sleep.exact(530.0);
        j = 5;
        Sleep.max(10);
        j = 6;
        Sleep.min(20);
        j = 7;
        Sleep.exact(30);
    }
}

public class SleepRange {
    public static void main(String[] args) {
        Barrier barrier = new Barrier();
        Sleeps s = new Sleeps(barrier);
        s.start();
        // barrier.activate(2);
    }
}
