package example;

import hc.*;
import mirela.*;

public class UrgentLock {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Memory scene = new Memory(m, new Delay[0]);
        Delay readScene = new Delay(1000, 2000);
        Delay displayScene2d = new Delay(3000, 4000);
        Rendering display2d = new Rendering(m, scene, readScene, displayScene2d);
        scene.active();
        display2d.active();
    }
}
