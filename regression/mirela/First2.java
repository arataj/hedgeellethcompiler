/*
 * First.java
 *
 * Created on Nov 18, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * A Mirela component of the type First.
 * 
 * @author Artur Rataj
 */
public class First extends AbstractConsumer {
    /**
     * Processing times of data from subsequent inputs.
     */
    Delay[] process;
    PairBarrier sync;
    
    /**
     * Creates a new instance of <code>First</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param process processing times of data from subsequent inputs
     */
    public First(Mirela system, Delay[] process) {
        super(system);
        this.process = process;
        sync = new PairBarrier();
    }
    protected void put(int index) {
        sync.produce(index);
    }
    @Override
    public void active() {
        super.active();
        sync.activate(numProducers, 1);
    }
    @Override
    public void run() {
        while(true) {
            int p = sync.consume();
            for(int i = 0; i < numProducers; ++i)
                if(p == producers[i].INDEX) {
                    process[i].sleep();
                    break;
                }
            produce();
        }
    }
}
