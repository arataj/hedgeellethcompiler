/*
 * Periodic.java
 *
 * Created on Nov 5, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

/**
 * After a warm--up time becomes periodic.
 * 
 * @author Artur Rataj
 */
public class Periodic extends AbstractNode {
    /**
     * Warm--up time.
     */
    AbstractDelay start;
    /**
     * Repetition time.
     */
    AbstractDelay repeat;
    
    /**
     * Creates a new instance of <code>Periodic</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param start warm--up time
     * @param repeat repetition time
     */
    public Periodic(Mirela system, AbstractDelay start, AbstractDelay repeat) {
        super(system);
        this.start = start;
        this.repeat = repeat;
    }
    @Override
    public void run() {
        start.sleep();
        while(true) {
            repeat.sleep();
            produce();
        }
    }
}
