/*
 * Aperiodic.java
 *
 * Created on Dec 1, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.Sleep;

/**
 * Produces events, given some minimum delay.
 * 
 * @author Artur Rataj
 */
public class Aperiodic extends AbstractNode {
    /**
     * Minimum period between two events.
     */
    double min;
    
    /**
     * Creates a new instance of <code>Aperiodic</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param min minimum time between two events
     */
    public Aperiodic(Mirela system, double min) {
        super(system);
        this.min = min;
    }
    @Override
    public void run() {
        while(true) {
            Sleep.min(min);
            produce();
        }
    }
}
