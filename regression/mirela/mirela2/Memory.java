/*
 * Memory.java
 *
 * Created on Dec 2, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * A Mirela component of the type Memory.
 * 
 * @author Artur Rataj
 */
public class Memory extends AbstractConsumer {
    /**
     * Processing times of writes from subsequent inputs.
     */
    AbstractDelay[] write;
    
    /**
     * Creates a new instance of <code>Memory</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param write processing times of writes from subsequent inputs
     */
    public Memory(Mirela system, AbstractDelay[] write) {
        super(system);
        this.write = write;
    }
    public synchronized void put(int index) {
        for(int i = 0; i < numProducers; ++i)
            if(index == producers[i].INDEX) {
                write[i].sleep();
                break;
            }
    }
    @Override
    public void active() {
        if(numProducers > write.length)
            throw new RuntimeException("delay array too short");
    }
    @Override
    public void run() {
        // a memory thread should not be started
        assert false;
    }
}
