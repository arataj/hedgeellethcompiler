package example;

import hc.*;

public class Producer extends Thread {
    final PairBarrier SYNC;
    final int NUM;
    int num;
    
    public Producer(PairBarrier sync, int num) {
        this.SYNC = sync;
        this.NUM = num;
    }
    @Overwrite
    public void run() {
        while(true) {
            SYNC.produce(NUM);
            SYNC.produce(NUM);
            Thread.sleep(1000);
            num = SYNC.consume() + NUM;
        }
    }
}

public class Consumer extends Thread {
    final PairBarrier SYNC;
    final int NUM;
    int num;
    
    public Consumer(PairBarrier sync, int num) {
        this.SYNC = sync;
        this.NUM = num;
    }
    @Overwrite
    public void run() {
        while(true) {
            num = SYNC.consume();
            if(NUM == 0)
                SYNC.produce();
            else
                SYNC.produce(NUM - 1);
        }
    }
}

public class ProducerNum {
    public static void main(String[] args) {
        PairBarrier barrier = new PairBarrier();
        Producer p1 = new Producer(barrier, -3);
        Producer p2 = new Producer(barrier, 19);
        Consumer c1 = new Consumer(barrier, 5);
        Consumer c2 = new Consumer(barrier, 0);
        p1.start();
        p2.start();
        c1.start();
        c2.start();
        barrier.activate(4, 4);
    }
}
