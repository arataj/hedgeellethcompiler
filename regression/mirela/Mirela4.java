package example;

import hc.*;
import mirela.*;

public class Mirela4 {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        final int CAMERA_WARM_UP = 500;
        Delay cameraLoop = new Delay(2000, 3000);
        Delay cameraStart = new Delay(
            cameraLoop.getMin() + CAMERA_WARM_UP,
            cameraLoop.getMax() + CAMERA_WARM_UP);
        Periodic camera1 = new Periodic(m, cameraStart, cameraLoop);
        Periodic camera2 = new Periodic(m, cameraStart, cameraLoop);
        Delay acquireDelay = new Delay(1000, 2000);
        Both acquire = new Both(m, acquireDelay);
        camera1.addOut(acquire);
        camera2.addOut(acquire);
        Aperiodic inertial = new Aperiodic(m, 1000);
        final int FRAME_NUM = 2;
        final int PROCESS_FRAME_DELAY = 2000;
        Delay[] createSceneDelay = {
            // process stereo image
            new Delay(
                2000 + PROCESS_FRAME_DELAY*FRAME_NUM,
                3000 + PROCESS_FRAME_DELAY*FRAME_NUM),
            // process inertial data
            new Delay(1000, 2000),
        };
        First update = new First(m, createSceneDelay);
        inertial.addOut(update);
        acquire.addOut(update);
        Both disk = new Both(m, /* seek delay */ new Delay(1000, 1500));
        inertial.addOut(disk);
        Delay[] writeScene = {
            // write a newly generated scene
            new Delay(3000, 4000),
            // write a scene recorded on disk
            new Delay(2000, 3000),
        };
        Memory scene = new Memory(m, writeScene);
        update.addOut(scene);
        disk.addOut(scene);
        Delay readScene = new Delay(1000, 2000);
        final int RENDER_BUFFER_MIN = 1000;
        final int RENDER_BUFFER_MAX = 10000;
        Delay displayScene2d = new Delay(RENDER_BUFFER_MIN, RENDER_BUFFER_MAX);
        Delay displayScene3d = new Delay(2*RENDER_BUFFER_MIN, 2*RENDER_BUFFER_MAX);
        Rendering display2d = new Rendering(m, scene, readScene, displayScene2d);
        Rendering display3d = new Rendering(m, scene, readScene, displayScene3d);
        camera1.active();
        camera2.active();
        acquire.active();
        inertial.active();
        disk.active();
        update.active();
        scene.active();
        display2d.active();
        display3d.active();
    }
}
