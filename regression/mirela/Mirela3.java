package example;

import hc.*;
import mirela.*;

public class Mirela3 {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Delay cameraStart = new Delay(2500, 3500);
        Delay cameraLoop = new Delay(2000, 3000);
        Periodic camera1 = new Periodic(m, cameraStart, cameraLoop);
        Periodic camera2 = new Periodic(m, cameraStart, cameraLoop);
        Delay acquireDelay = new Delay(1000, 2000);
        Both acquire = new Both(m, acquireDelay);
        camera1.addOut(acquire);
        camera2.addOut(acquire);
        Aperiodic inertial = new Aperiodic(m, 1000);
        Delay[] createSceneDelay = {
            // process stereo image
            new Delay(6000, 7000),
            // process inertial data
            new Delay(1000, 2000),
        };
        First update = new First(m, createSceneDelay);
        inertial.addOut(update);
        acquire.addOut(update);
        Both disk = new Both(m, /* seek delay */ new Delay(1000, 1500));
        inertial.addOut(disk);
        Delay[] writeScene = {
            new Delay(3000, 4000),
        };
        Memory scene = new Memory(m, writeScene);
        update.addOut(scene);
        disk.addOut(scene);
        Delay readScene = new Delay(1000, 2000);
        Delay displayScene2d = new Delay(1000, 10000);
        Delay displayScene3d = new Delay(2000, 20000);
        Rendering display2d = new Rendering(m, scene, readScene, displayScene2d);
        Rendering display3d = new Rendering(m, scene, readScene, displayScene3d);
        camera1.active();
        camera2.active();
        acquire.active();
        inertial.active();
        disk.active();
        update.active();
        scene.active();
        display2d.active();
        display3d.active();
    }
}
