test.FieldPrefix.run/ #this
  test.FieldPrefix #this
  boolean [?] f
  boolean [?] f1
    0 f1 = #this::f2
    1 f = #this::f2
    2 #this::f1 = f1
    3 #this::f2 = f
    4 f1 = #this::f1
    5 #this::f1 = f1
    6 #this::f2 = f1
    7 return