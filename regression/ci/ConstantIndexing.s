#default.ConstantIndexing.run/ #this
  long[] [<<0L, 1L>>] #c2
  [src]<<0L, 1L>>long [<<0L, 1L>>] #c3
  long[] [<<0L, 1L>>] #c6
  #default.ConstantIndexing #this
    0 #default.ConstantIndexing.x = 2L * #this::y
    1 #this::y = #default.ConstantIndexing.x
    2 #c2 = #1002 -> #system.#Array#107##default.ConstantIndexing::out [ length=2L ]
    3 #c3 = #this::y
    4 #default.ConstantIndexing.in[#default.ConstantIndexing.x] = #c2[#c3]
    5 #c6 = #1002 -> #system.#Array#107##default.ConstantIndexing::out [ length=2L ]
    6 #c6[0L] = #this::y
    7 return