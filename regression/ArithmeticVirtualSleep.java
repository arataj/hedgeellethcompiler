class ArithmeticVirtualSleep extends Thread {
    short result;
    
    protected int m(byte b) {
        return 1;
    }
    protected int m(short b) {
        return 2;
    }
    protected int m(int b) {
        return 4;
    }
    protected int m(long b) {
        return 8;
    }
    public void run() {
        result = (short)
            (m((byte)0) +
            m((short)0)*2 +
            m(0L)*4 +
            m(0)*8 + 40000);
        System.out.println("" + result);
        try {
            Thread.sleep(100);
        } catch(InterruptedException e) {
            /* empty */
        }
    }
    public static void main(String[] args) {
        (new ArithmeticVirtualSleep()).start();
    }
}
