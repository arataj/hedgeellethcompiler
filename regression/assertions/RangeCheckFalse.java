// the range assertion should be false

class RangeCheckFalse extends Thread {
    static int /*@(0, 10)*/f = 0;

    public void run() {
      ++f;
      while(true)
        f = (f + 2)%12;
    }
    public static void main(String[] args) {
      (new RangeCheckFalse()).start();
    }
}
