/**
 * AssertionsFailure.java
 *
 * Contains a failing assertion within the interpreted code.
 */

public class AssertionsFailure {
    public static void main(String args []) {
        final int N = 3;
        final int MAX_QUEUE = 4;
        
        Trigger trigger = new Trigger(MAX_QUEUE);
        assert N*MAX_QUEUE == 13;
        new Thread(trigger).start();
        assert true;
    }
}

class Trigger implements Runnable {
    private int maxQueue;
    protected Object o;

    public Trigger(int maxQueue) {
        this.maxQueue = maxQueue;
        o = o();
    }
    Object o() {
        return null;
    }
    public void run() {
        maxQueue = 2*maxQueue;
        assert maxQueue == 15;
    }
}
