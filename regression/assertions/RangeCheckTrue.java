// the range assertion should hold true

class RangeCheckTrue extends Thread {
    static int /*@(0, 10)*/f = 0;

    public void run() {
      while(true)
        f = (f + 2)%12;
    }
    public static void main(String[] args) {
      (new RangeCheckTrue()).start();
    }
}
