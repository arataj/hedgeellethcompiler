#default.Operators.run/ #this
  boolean [?] #c2
  int [?] #t0
  #default.Operators #this
    0 #c2 = #this::f == 7
    1 #c2 ? <null> : 4
    2 #t0 = 8 + #this::f
    3 goto 6
    4 #this::f = #this::f + 9
    5 #t0 = #this::f
    6 #this::f = #this::f - #t0
    7 return