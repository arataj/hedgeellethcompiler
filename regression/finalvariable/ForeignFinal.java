package example;

import java.util.*;

public class ForeignFinal {
    public static final int HOUSEHOLDS = 3;
    public static int TURN = 0;

    public static void main(String[] args) {
        Household[] households = new Household[HOUSEHOLDS];
        for(int i = 0; i < HOUSEHOLDS; ++i) {
            Household h = new Household(1 + i);
            h.start();
            households[i] = h;
        }
        Scheduler s = new Scheduler(households);
        s.NON_DETERMINISTIC = true;
        s.start();
    }
    public void run() {
        while(true) {
            ForeignFinal.TURN = 1;
            Scheduler.HOUSEHOLDS = 4;
        }
    }
}

class Scheduler extends Thread {
    public final boolean NON_DETERMINISTIC = false;
    public static final int HOUSEHOLDS = 3;
    
    protected Household[] households;

    public Scheduler(Household[] households) {
        super();
        this.households = households;
    }
}

class Household extends Thread {
    /**
     * Number of turn for this household.
     */
    protected int thisTurn;
    
    public Household(int thisTurn) {
        super();
        this.thisTurn = thisTurn;
    }
    @Override
    public synchronized void run() {
        ForeignFinal.HOUSEHOLDS = 4;
    }
}
 