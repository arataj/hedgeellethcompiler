class FinalSwitchLabel extends Thread {
  final int label1 = 5;
  final int label2 = get(6);
  int label3;

  private final int get(int value) {
    return value + label2;
  } 
  public int test(int y) {
    switch(label2) {
      case label1:
        return 0;

      case 6:
        return 1;

      default:
        return y + 1;
    }
  }
  public void run() {
    label3 = test(6);
  }
  public static void main(String[] args) throws Exception {
    FinalSwitchLabel l = new FinalSwitchLabel();
    l.start();
    // l.join();
    System.out.println("label3 = " + l.label3);
  }
}
