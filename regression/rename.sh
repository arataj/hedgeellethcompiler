#
# rename extension
#
#cd $1
#for f in *.pta.regression; do
# 	base=`basename $f .pta.regression`
# 	mv $f $base.nh.regression
#done
#rm *.pta
#cd -
#!/bin/bash
#!/bin/bash

#
# replace sequence in text file
#
OLD="..\/..\/library\/"
NEW="\$\{hcLibrary\}\/"
DPATH=$1/*.regression
BPATH=./backup
TFILE="/tmp/out.tmp.$$"
rm -rf $BPATH && mkdir -p $BPATH || :
for f in $DPATH
do
  if [ -f $f -a -r $f ]; then
    /bin/cp -f $f $BPATH
   sed "s/$OLD/$NEW/g" "$f" > $TFILE && mv $TFILE "$f"
  else
   echo "Error: Cannot read $f"
  fi
done
rm -rf $TFILE
