package example;

import java.lang.reflect.Field;

public class StateClassTypesState {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    example.Household o1;
    Field f2;
    example.Household o4;
    Field f5;
    example.Household o7;
    Field f8;
    Field f9;
    Field f10;
    Field f11;
    example.Household o13;
    Field f14;
    example.Household o16;
    Field f17;
    example.Household o19;
    Field f20;
    example.Household o22;
    Field f23;
    example.Household o25;
    Field f26;
    example.Household o28;
    Field f29;
    hc.game.TurnGame o32;
    Field f33;
    public StateClassTypesState() {
        try {
            Field f0 = getField(example.StateClassTypes.class, "households");
            f0.setAccessible(true);
            example.Household[] o0 = (example.Household[])f0.get(null);
            o1 = o0[0];
            f2 = getField(example.Household.class, "d");
            f2.setAccessible(true);
            Field f3 = getField(example.StateClassTypes.class, "households");
            f3.setAccessible(true);
            example.Household[] o3 = (example.Household[])f3.get(null);
            o4 = o3[1];
            f5 = getField(example.Household.class, "d");
            f5.setAccessible(true);
            Field f6 = getField(example.StateClassTypes.class, "households");
            f6.setAccessible(true);
            example.Household[] o6 = (example.Household[])f6.get(null);
            o7 = o6[2];
            f8 = getField(example.Household.class, "d");
            f8.setAccessible(true);
            f9 = getField(example.StateClassTypes.class, "d");
            f9.setAccessible(true);
            f10 = getField(example.StateClassTypes.class, "numJobs");
            f10.setAccessible(true);
            f11 = getField(example.StateClassTypes.class, "time");
            f11.setAccessible(true);
            Field f12 = getField(example.StateClassTypes.class, "households");
            f12.setAccessible(true);
            example.Household[] o12 = (example.Household[])f12.get(null);
            o13 = o12[0];
            f14 = getField(example.Household.class, "job");
            f14.setAccessible(true);
            Field f15 = getField(example.StateClassTypes.class, "households");
            f15.setAccessible(true);
            example.Household[] o15 = (example.Household[])f15.get(null);
            o16 = o15[1];
            f17 = getField(example.Household.class, "job");
            f17.setAccessible(true);
            Field f18 = getField(example.StateClassTypes.class, "households");
            f18.setAccessible(true);
            example.Household[] o18 = (example.Household[])f18.get(null);
            o19 = o18[2];
            f20 = getField(example.Household.class, "job");
            f20.setAccessible(true);
            Field f21 = getField(example.StateClassTypes.class, "households");
            f21.setAccessible(true);
            example.Household[] o21 = (example.Household[])f21.get(null);
            o22 = o21[0];
            f23 = getField(example.Household.class, "l");
            f23.setAccessible(true);
            Field f24 = getField(example.StateClassTypes.class, "households");
            f24.setAccessible(true);
            example.Household[] o24 = (example.Household[])f24.get(null);
            o25 = o24[1];
            f26 = getField(example.Household.class, "l");
            f26.setAccessible(true);
            Field f27 = getField(example.StateClassTypes.class, "households");
            f27.setAccessible(true);
            example.Household[] o27 = (example.Household[])f27.get(null);
            o28 = o27[2];
            f29 = getField(example.Household.class, "l");
            f29.setAccessible(true);
            Field f30 = getField(example.StateClassTypes.class, "households");
            f30.setAccessible(true);
            example.Household[] o30 = (example.Household[])f30.get(null);
            example.Household o31 = o30[0];
            Field f32 = getField(example.Household.class, "table");
            f32.setAccessible(true);
            o32 = (hc.game.TurnGame)f32.get(o31);
            f33 = getField(hc.game.TurnGame.class, "turn");
            f33.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    double d1() {
        try {
            return f2.getDouble(o1);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    double d2() {
        try {
            return f5.getDouble(o4);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    double d3() {
        try {
            return f8.getDouble(o7);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    double d() {
        try {
            return f9.getDouble(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int numJobs() {
        try {
            return f10.getInt(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int time() {
        try {
            return f11.getInt(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job1() {
        try {
            return f14.getInt(o13);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job2() {
        try {
            return f17.getInt(o16);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job3() {
        try {
            return f20.getInt(o19);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    long l1() {
        try {
            return f23.getInt(o22);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    long l2() {
        try {
            return f26.getInt(o25);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    long l3() {
        try {
            return f29.getInt(o28);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int turn() {
        try {
            return f33.getInt(o32);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return d1();
            case 1:
                return d2();
            case 2:
                return d3();
            case 3:
                return d();
            case 4:
                return numJobs();
            case 5:
                return time();
            case 6:
                return job1();
            case 7:
                return job2();
            case 8:
                return job3();
            case 9:
                return l1();
            case 10:
                return l2();
            case 11:
                return l3();
            case 12:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                throw new RuntimeException("state variable " +
                    "at index 0 has type double");
            case 1:
                throw new RuntimeException("state variable " +
                    "at index 1 has type double");
            case 2:
                throw new RuntimeException("state variable " +
                    "at index 2 has type double");
            case 3:
                throw new RuntimeException("state variable " +
                    "at index 3 has type double");
            case 4:
                return numJobs();
            case 5:
                return time();
            case 6:
                return job1();
            case 7:
                return job2();
            case 8:
                return job3();
            case 9:
                throw new RuntimeException("state variable " +
                    "at index 9 has type long");
            case 10:
                throw new RuntimeException("state variable " +
                    "at index 10 has type long");
            case 11:
                throw new RuntimeException("state variable " +
                    "at index 11 has type long");
            case 12:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                throw new RuntimeException("state variable " +
                    "at index 0 has type double");
            case 1:
                throw new RuntimeException("state variable " +
                    "at index 1 has type double");
            case 2:
                throw new RuntimeException("state variable " +
                    "at index 2 has type double");
            case 3:
                throw new RuntimeException("state variable " +
                    "at index 3 has type double");
            case 4:
                return numJobs();
            case 5:
                return time();
            case 6:
                return job1();
            case 7:
                return job2();
            case 8:
                return job3();
            case 9:
                return l1();
            case 10:
                return l2();
            case 11:
                return l3();
            case 12:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 13;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "d1";
            case 1:
                return "d2";
            case 2:
                return "d3";
            case 3:
                return "d";
            case 4:
                return "numJobs";
            case 5:
                return "time";
            case 6:
                return "job1";
            case 7:
                return "job2";
            case 8:
                return "job3";
            case 9:
                return "l1";
            case 10:
                return "l2";
            case 11:
                return "l3";
            case 12:
                return "turn";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
