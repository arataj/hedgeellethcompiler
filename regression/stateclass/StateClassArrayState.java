package test;

import java.lang.reflect.Field;

public class StateClassArrayState {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    long[] o0;
    long[] o2;
    Field f4;
    public StateClassArrayState() {
        try {
            Field f0 = getField(test.StateClassArray.class, "arrayi");
            f0.setAccessible(true);
            o0 = (long[])f0.get(null);
            Field f2 = getField(test.StateClassArray.class, "arrayi");
            f2.setAccessible(true);
            o2 = (long[])f2.get(null);
            f4 = getField(test.StateClassArray.class, "f");
            f4.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    long namedArrayi0() {
        return o0[0];
    }
    long namedArrayi1() {
        return o2[1];
    }
    long f() {
        try {
            return f4.getInt(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return namedArrayi0();
            case 1:
                return namedArrayi1();
            case 2:
                return f();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                throw new RuntimeException("state variable " +
                    "at index 0 has type long");
            case 1:
                throw new RuntimeException("state variable " +
                    "at index 1 has type long");
            case 2:
                throw new RuntimeException("state variable " +
                    "at index 2 has type long");
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                return namedArrayi0();
            case 1:
                return namedArrayi1();
            case 2:
                return f();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 3;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "namedArrayi0";
            case 1:
                return "namedArrayi1";
            case 2:
                return "f";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
