/**
 * Tests a path through a static array and also recursion
 * prevention.
 */
package example;

import java.util.*;
import hc.*;

class CoinFlipStaticArrayPath extends Beam {
  final int N = 100;
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  CoinFlipStaticArrayPath recursive;
  
  public CoinFlipStaticArrayPath() {
    super();
    recursive = this;
  }
  protected boolean decide() {
    final int a = 1;
    final int b = 2;
    final int c = 3;
    if(Model.isStatistical())
      return a*flips + b > c;
    else
      return random.nextInt(2) == 0;
  }
  public void run() {
    init();
    for(; flips < N; ++flips) {
      if(decide()) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
      step();
    }
    done();
  }
  public static void main(String[] args) {
    CoinFlipStaticArrayPath cf = new CoinFlipStaticArrayPath();
    Model.name(cf);
    cf.start();
  }
}
