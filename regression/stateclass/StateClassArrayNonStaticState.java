package test;

import java.lang.reflect.Field;

public class StateClassArrayNonStaticState {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    long[] o1;
    long[] o4;
    test.StateClassArrayNonStatic o6;
    Field f7;
    public StateClassArrayNonStaticState() {
        try {
            Field f0 = getField(test.Static.class, "o");
            f0.setAccessible(true);
            test.StateClassArrayNonStatic o0 = (test.StateClassArrayNonStatic)f0.get(null);
            Field f1 = getField(test.StateClassArrayNonStatic.class, "arrayi");
            f1.setAccessible(true);
            o1 = (long[])f1.get(o0);
            Field f3 = getField(test.Static.class, "o");
            f3.setAccessible(true);
            test.StateClassArrayNonStatic o3 = (test.StateClassArrayNonStatic)f3.get(null);
            Field f4 = getField(test.StateClassArrayNonStatic.class, "arrayi");
            f4.setAccessible(true);
            o4 = (long[])f4.get(o3);
            Field f6 = getField(test.Static.class, "o");
            f6.setAccessible(true);
            o6 = (test.StateClassArrayNonStatic)f6.get(null);
            f7 = getField(test.StateClassArrayNonStatic.class, "f");
            f7.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    long arrayName0() {
        return o1[0];
    }
    long arrayName1() {
        return o4[1];
    }
    long f() {
        try {
            return f7.getInt(o6);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return arrayName0();
            case 1:
                return arrayName1();
            case 2:
                return f();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                throw new RuntimeException("state variable " +
                    "at index 0 has type long");
            case 1:
                throw new RuntimeException("state variable " +
                    "at index 1 has type long");
            case 2:
                throw new RuntimeException("state variable " +
                    "at index 2 has type long");
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                return arrayName0();
            case 1:
                return arrayName1();
            case 2:
                return f();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 3;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "arrayName0";
            case 1:
                return "arrayName1";
            case 2:
                return "f";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
