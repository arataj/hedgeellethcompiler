class ArithmeticVirtual extends Thread {
    short result;
    
    protected int m(byte b) {
        return 1;
    }
    protected int m(short b) {
        return 2;
    }
    protected int m(int b) {
        return 4;
    }
    protected int m(long b) {
        return 8;
    }
    public void run() {
        result = (short)
            (m((byte)0) +
            m((short)0)*2 +
            m(0)*4 +
            m(0L)*8 + 40000);
    }
    public static void main(String[] args) {
        (new ArithmeticVirtual()).start();
    }
}
