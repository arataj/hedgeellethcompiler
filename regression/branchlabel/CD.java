package pmgc;

import java.util.Random;

import hc.*;
import hc.game.*;

public class CD {
    final public static int NUM_SOURCES = 2;
    final public static int NUM_TURNS = 5;
    final public static Source[] SOURCES = new Source[NUM_SOURCES];
    // must be odd
    final public static int LEVELS = 7;
    
    public static Listener LISTENER;
    public static int /*@(0, NUM_TURNS + 1)*/ turnCount = 0;
    public static int /*@(-LEVELS/2, LEVELS/2)*/ sunny = 0;
    
    public static int fix(double value) {
        return (int)(value*(LEVELS - 1)/1.0 + 0.5);
    }
    public static boolean isEnd() {
        ++turnCount;
        return turnCount == CD.NUM_TURNS + 1;
    }
    public static void main(String[] args) {
        System.out.println("turns = " + NUM_TURNS);
        TurnGame table = new TurnGame();
        Model.name(table);
        // create sources
        for(int i = 0; i < NUM_SOURCES; ++i) {
            Source c = new Source(table, i);
            c.start();
            Model.name(c, "", "" + (i + 1));
            Model.player(c, "s" + (i + 1));
            SOURCES[i] = c;
        }
        LISTENER = new Listener(table, NUM_SOURCES);
        LISTENER.start();
        Model.name(LISTENER, "", "");
        Model.player(LISTENER, "l");
        table.start(NUM_SOURCES + 1);
        // Model.check("game ends",
        //     "<<2>> Pmin=? [F turnCount=3]");
        Model.check("persuasion min",
            "<<l>> Pmin=? [F persuaded=1]");
        Model.check("persuasion max",
            "<<s2>> Pmax=? [F persuaded=1]");
        Model.waitFinish();
        System.out.println("finished");
        // intterupt any thread still running
        SOURCES[0].interrupt();
    }
}
class Source extends TurnPlayer {
    protected final Random strategy;
    // sensors always give some information (to avoid division by zero in the listener)
    // 9, 10
    public int/*@(getNum() == 0 ? CD.LEVELS - 1 : 0, CD.LEVELS - 1)*/ promise =
            CD.fix(1.0);
    public int/*@(-CD.LEVELS/2, CD.LEVELS/2)*/ event;
    
    public Source(TurnGame table, int playerNum) {
        super(table, playerNum);
        strategy = new Random();
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(isInterrupted())
                break;
            say();
            turnNext(getNum() + 1);
        }
    }
    protected void say() {
        if(getNum() == 0) {
            CD.sunny = (int)(Math.random()*CD.LEVELS) - CD.LEVELS/2;
            // just keep the maximum declaration
            event = CD.sunny;
        } else {
            // choose declaration strength
            promise = strategy.nextInt(CD.LEVELS);
            event = strategy.nextInt(CD.LEVELS) - CD.LEVELS/2;
        }
    }
}
class Listener extends TurnPlayer {
    protected final Strategy strategy;
    int[]/*@(0, CD.LEVELS - 1)*/ compliance = {CD.fix(0.9), CD.fix(0.5)};
    int[]/*@(0, CD.LEVELS - 1)*/ defiance = {CD.fix(0.0), CD.fix(0.1)};
    int/*@(0, CD.NUM_TURNS)*/ yes = 0;
    int/*@(0, 1)*/ persuaded = 0;
    
    public Listener(TurnGame table, int playerNum) {
        super(table, playerNum);
        String[] labels = { "chCompliance", "chDefiance", };
        strategy = new Strategy(labels);
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(CD.isEnd()) {
                break;
            }
            decide();
            turnNext(0);
        }
        if(Math.random() < yes*1.0/CD.NUM_TURNS) {
//            Model.stateAnd("persuaded");
            persuaded = 1;
        }
        Model.finish();
        System.out.println("ratio = " + yes*1.0/CD.turnCount);
    }
    protected int getWeightedSignal(int source, int promise, int event) {
        Source s = CD.SOURCES[source];
        return (compliance[source] + defiance[source])*promise*
               (compliance[source] - defiance[source])*event;
    }
    protected int getWeight(int source, int promise) {
        Source s = CD.SOURCES[source];
        return (compliance[source] + defiance[source])*promise*
               (CD.LEVELS - 1);
    }
    protected void decide() {
        final int MAX_WEIGHT = (CD.LEVELS - 1)*2*(CD.LEVELS - 1)*(CD.LEVELS - 1)*
            CD.NUM_SOURCES;
        int/*@(0, MAX_WEIGHT*(CD.LEVELS - 1))*/ sSum = 0;
        int/*@(0, MAX_WEIGHT)*/ wSum = 0;
        for(int i = 0; i < CD.NUM_SOURCES; ++i) {
            Source s = CD.SOURCES[i];
            sSum = sSum + getWeightedSignal(i, s.promise, s.event);
            wSum = wSum + getWeight(i, s.promise);
        }
        int/*@(-CD.LEVELS/2, CD.LEVELS/2)*/ event;
        event = sSum/wSum;
        // System.out.println("event = " + event);
        //if(event < -CD.LEVELS/2 || event > CD.LEVELS/2)
        //    throw new RuntimeException("sSum = " + sSum + " wSum = " + wSum);
        if(Math.random() < 0.5 + 0.2*event/(CD.LEVELS/2))
            ++yes;
        modifyAttitude();
        //
        // cull state space
        //
        CD.SOURCES[1].promise = CD.LEVELS - 1;
        CD.SOURCES[1].event = CD.sunny;
    }
    protected void modifyAttitude() {
        int event0 = CD.SOURCES[0].event;
        int event1 = CD.SOURCES[1].event;
        int promise = CD.SOURCES[1].promise;
        if(strategy.choose(2) == 0) {
           int/*@(0, CD.LEVELS*3/2)*/ c = compliance[1] -
                   promise*Math.abs(event1 - event0)/2/
                       (CD.LEVELS - 1);
           if(c < 0)
               c = 0;
           // assert c <= 6;
//           if(c > 6)
//               throw new RuntimeException("c = " + c);
           compliance[1] = c;
        } else {
           int/*@(0, CD.LEVELS*3/2)*/ d = defiance[1] +
                   promise*Math.abs(event1 - event0)/2/
                       (CD.LEVELS - 1);
           if(d > CD.LEVELS - 1)
               d = CD.LEVELS - 1;
           defiance[1] = d;
        }
    }
}
