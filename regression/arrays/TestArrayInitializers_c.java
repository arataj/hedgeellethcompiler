package test;

/**
 * Optimization of array's constant elements turned on.
 */
class TestArrayInitializers_c extends Thread {
  static int[] array = {
      1, 2, -3, -4, -5, -6,
  };
  
  public void run() {
      array[3] = array[2];
  }
  public static void main(String[] args) {
       TestArrayInitializers_c test = new TestArrayInitializers_c();
       int[][] a = {
         null,
         {8, 9},
         {10},
         null,
       };
       array[4] = a[1][1];
       array[5] = a[1][0];
       test.start();
  }
}
