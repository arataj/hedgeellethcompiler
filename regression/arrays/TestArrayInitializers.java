class TestArrayInitializers extends Thread {
  static int[] array = {
      1, 2, -3, -4, -5, -6,
  };
  
  public void run() {
      array[3] = array[2];
  }
  public static void main(String[] args) {
       TestArrayInitializers test = new TestArrayInitializers();
       int[][] a = {
         null,
         {8, 9},
         {10},
         null,
       };
       array[4] = a[1][1];
       array[5] = a[1][0];
       test.start();
  }
}
