class ArrayTest extends Thread {
  static int[] s;
  int[] a;
  ArrayTest t;

  public ArrayTest(int length) {
    s[19] = 'b';
    s[0] = s[19];
    a = new int[length];
    a[2] = length;
    a[5] = 3*a[2];
  }  
  public void run() {
    s[18] = 'a';
    a[a[1] + 'b'] = a[5] + s[a[s[7 + a[4] + 9]]];
  }
  public static void main(String[] args) {
    s = new int[20];
    for(int i = 0; i < 10; ++i)
      s[i] = -2*i + 5;
    ArrayTest arrays = new ArrayTest(10);
    arrays.start();
  }
}
