class Local extends Thread {
  static int[] s;
  int[] a;

  public Local() {
  }  
  public void run() {
    int[] local = s;
    a[5] = local[5];
  }
  public static void main(String[] args) {
    Local local = new Local();
    local.s = new int[10];
    local.a = new int[10];
    local.start();
  }
}
