package interfaces;

public interface InterfaceB extends InterfaceA1, InterfaceA2 {
  public int getP2();
  public int getP3();
}
