class Abstract2 extends AbstractA1 {
  // parent method methodA2 is not seen
  // in this class because of access rights,
  // thus, this method is not related to
  // the parent method methodA2 and in effect
  // does not implement the parent method
  int methodA2() {
    return 3;
  }
  protected int methodA2Protected() {
    return 3;
  }
}
