package abstract1;

public class Abstract1 {
  public Abstract1() {
  }
  int method0() {
    return 0;
  }
  abstract int methodA1();
  abstract int methodA2();
  abstract protected int methodA2Protected();
  int method2() {
    return 2;
  }
}
