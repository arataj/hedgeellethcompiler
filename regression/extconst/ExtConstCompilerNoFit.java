package example;

import java.util.Random;

import hc.*;
import hc.game.*;

/**
 * Constants defined in compiler's command line do
 * not fit into target types.
 */
public class ExtConstCompilerNoFit {
    /**
     * Number of days.
     */
    public static final int DAYS = Model.intConst("DAYS");
    /**
     * Number of intervals per day.
     */
    public static final short INTERVALS = (short)Model.intConst("INTERVALS");
    /**
     * Number of rounds.
     */
    public static final int MAX_TIME = DAYS*INTERVALS;
    
    /**
     * Demand curve.
     */
    public static final double[] DEMAND = {
        0.0614, 0.0392, 0.0304, 0.0304,
        0.0355, 0.0518, 0.0651, 0.0643,
        0.0625, 0.0618, 0.0614, 0.0695,
        0.0887, 0.1013, 0.1005, 0.0762,
    };
    /**
     * Number of households.
     */
    public static final int NUM_HOUSEHOLDS = 3;
    /**
     * Households.
     */
    protected static Household[] households;
    /**
     * Current time or interval.
     */
    public static int/*@(0, MAX_TIME)*/ time;
    /**
     * Current number of loads generated.
     */
    public static int/*@(0, NUM_HOUSEHOLDS)*/ numJobs;

    /**
     * Returns the current load demand.
     */
    public static double getDemand() {
        return DEMAND[time%INTERVALS];
    }
    /**
     * How many households currently generate a load.
     */
    public static int getNumJobs() {
        int/*@(0, NUM_HOUSEHOLDS)*/ numJobs = 0;
        for(int i = 0; i < NUM_HOUSEHOLDS; ++i)
            if(households[i].job > 0)
                ++numJobs;
        return numJobs;
    }
    /**
     * A price of a load to be bought.
     */
    public static double getPrice() {
        return getNumJobs() + 1.0;
    }
    /**
     * If <code>MAX_TIME</code> has been reached.
     *
     * @return if the simulation is finished
     */
    public static boolean isFinished() {
        return time == MAX_TIME;
    }
    /**
     * Called by the scheduler at the end of each interval.
     *
     */
    public static void endInterval() {
        numJobs = getNumJobs();
        Model.stateAnd("measure");
        numJobs = 0;
        // reduce job counters
        for(int i = 0; i < ExtConstCompilerNoFit.NUM_HOUSEHOLDS; ++i)
              households[i].tick();
        ++time;
    }
    public static void main(String[] args) {
        // player ids at the table: 0 scheduler, 1..NUM_HOUSEHOLDS households
        TurnGame table = new TurnGame();
        Model.name(table);
        // create an array of households
        households = new Household[NUM_HOUSEHOLDS];
        for(int i = 0; i < NUM_HOUSEHOLDS; ++i) {
            Household h = new Household(table, 1 + i);
            h.start();
            Model.name(h, "", "" + (i + 1));
            Model.player(h, "h" + (i + 1));
            households[i] = h;
        }
        // create the scheduler
        Scheduler s = new Scheduler(table);
        s.start();
        Model.name(s);
        Model.player(s, "scheduler");
        // reset time
        time = 0;
        // players consist of the scheduler and of the households
        table.start(1 + NUM_HOUSEHOLDS);
        for(int t = 1; t <= MAX_TIME/4; ++t)
            Model.check("at time " + t,
                "<<1, 2, 3>> R{\"value123\"}max=? [F time=" + t + "]");
        Model.waitFinish();
        System.out.println("time = " + time);
        // won't be executed by hc, but will within JVM
        //
        // releases all threads still waiting on the barrier
        households[0].interrupt();
    }
}

class Scheduler extends TurnPlayer {
    protected final static boolean NON_DETERMINISTIC = false;
    
    protected final Random strategy;

    public Scheduler(TurnGame table) {
        super(table, 0);
        strategy = new Random();
    }
    @Override
    public void run() {
        do {
            // select a household
            int address;
            if(NON_DETERMINISTIC)
                // non--deterministic choice
                address = strategy.nextInt(ExtConstCompilerNoFit.NUM_HOUSEHOLDS);
            else
                // probabilistic choice
                address = (int)
                    (Math.random()*ExtConstCompilerNoFit.NUM_HOUSEHOLDS);
            // contact the selected household
            turnNext(1 + address);
            turnWait();
            // end of the current round
            ExtConstCompilerNoFit.endInterval();
        } while(!ExtConstCompilerNoFit.isFinished());
        Model.finish();
    }
}

class Household extends TurnPlayer {
    /**
     * Maximum time of running a single job, in intervals.
     */
    protected final static int MAX_JOB_TIME = 4;
    /**
     * Expected number of jobs per day.
     */
    protected final static int EXPECTED_JOBS = 9;
    /**
     * Price limit, above which this household may
     * back--off.
     */
    protected final static double PRICE_LIMIT = 1.5;
    /**
     * Probability of starting a task independently of the cost.
     */
    protected final static double P_OVER_LIMIT = 0.8;
    /**
     * A running job, in intervals.
     */
    public int/*@(0, MAX_JOB_TIME)*/ job = 0;
    protected final Random strategy;
    
    public Household(TurnGame table, int playerNum) {
        super(table, playerNum);
        strategy = new Random();
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(isInterrupted())
                break;
            if(job == 0 && Math.random() < ExtConstCompilerNoFit.getDemand()*
                  EXPECTED_JOBS) {
                // decide if to generate a load
                boolean lowPrice = ExtConstCompilerNoFit.getPrice() < PRICE_LIMIT;
                if(lowPrice || Math.random() < P_OVER_LIMIT ||
                        // a right to back--off is granted; decide,
                        // if to generate a load anyway
                        strategy.nextInt(2) == 0)
                    job = 1 + (int)(Math.random()*MAX_JOB_TIME);
            }
            turnNext(0);
        }
    }
    /**
     * Called by the scheduler after each turn.
     */
    public void tick() {
        if(job > 0)
          --job;
    }
}
