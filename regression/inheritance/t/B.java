class B extends B0 {
  public static int staticF = -19;
  public int sh = 19;
  
  public B() {
    super.sh = 21;
  }
  public int a() {
    return 21;
  }
  public int b() {
    return 22;
  }
  public int c() {
    return 24;
  }
  // should not be called in A.test()
  // with "this" being B
  public int ov1(int l) {
    return 400;
  }
  public int ov2(long l) {
    return 401;
  }
  public long test() {
    int i = a() + this.b() + super.c();
    long l = i + super.test();
    return sh + super.sh + l +
      25*super.staticF + 26*staticF +
      27*B.staticF + 28*A.staticF +
      29*B0.staticF;
  }
}
