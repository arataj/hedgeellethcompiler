class Inheritance extends Thread {
  B fb;
  long f;
  
  public void run() {
    f = fb.test();
    System.out.println(f);
  }
  public static void main(String[] args) {
    Inheritance i = new Inheritance();
    i.fb = new B();
    i.start();
  }
}
