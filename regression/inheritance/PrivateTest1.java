class PrivateTest1 extends Thread {
  static long resultStatic = -1;
  long result = -1;
  static PrivateA a;
  static PrivateB b;
  
  public void run() {
    resultStatic *= a.value();
    result += resultStatic + b.m();
  }
  public static void main(String[] args) {
    a = new PrivateA();
    b = new PrivateB();
    PrivateTest1 thread = new PrivateTest1();
    thread.resultStatic = a.value() + b.m();
    thread.result = a.value() + b.m();
    thread.start();
  }
}
