class C extends B {
  public int sh = -1;
  
  public int a() {
    return -1;
  }
  public int b() {
    return -1;
  }
  public int c() {
    return -1;
  }
  public int fl(int i) {
    return -1;
  }
  public int fl(long l) {
    return -1;
  }
  public int ov1(int i) {
    return -1;
  }
  public int ov2(int i) {
    return -1;
  }
  public int ov1(long i) {
    return -1;
  }
  public int ov2(long i) {
    return -1;
  }
  public long test() {
    return -1;
  }
  @Override
  protected void testPrivileges() {
  }
}
