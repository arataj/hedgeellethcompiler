class A {
  public int sh = 18;
  public static int staticF = -18;

  public A() {
    sh = 20;
    staticF = -81;
  }
  public A(int j) {
    sh = 20;
    staticF = -82;
  }
  public int a() {
    return 11;
  }
  public int b() {
    return 12;
  }
  public int c() {
    return 14;
  }
  public long test() {
    return 1*fl(1) + 2*fl(1L) +
      3*ov1(2) + 4*ov2(2);
  }
  public int fl(int i) {
    return 101;
  }
  public int fl(long l) {
    return 111;
  }
  public int ov1(long l) {
    return 200;
  }
  // should not be called in A.test()
  // with "this" being B
  public int ov2(long l) {
    return 201;
  }
  protected void testPrivileges() {
  }
}
