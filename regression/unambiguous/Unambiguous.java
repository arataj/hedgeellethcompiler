package test;

class Unambiguous extends Thread {
    static VirtualA fa;
    static VirtualB fb;
    int i;
    
    public void run() {
        VirtualA a = fa;
        VirtualC c = (VirtualC)fb;
        i = a.u() + c.a() + ((VirtualC)fb).a();
    }
    public static void main(String[] args) {
        Unambiguous u = new Unambiguous();
        fa = new VirtualA();
        fb = new VirtualC();
        u.start();
    }
}
