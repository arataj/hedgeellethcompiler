package test;

class Ambiguous extends Thread {
    static VirtualB fbb;
    static VirtualB fb;
    int i;
    
    public void run() {
        VirtualB bb = fbb;
        VirtualB b = fb;
        i = b.a() - fbb.a()*bb.a();
    }
    public static void main(String[] args) {
        Ambiguous a = new Ambiguous();
        fbb = new VirtualB();
        fb = new VirtualC();
        a.start();
    }
}
