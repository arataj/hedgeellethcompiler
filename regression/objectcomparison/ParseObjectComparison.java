package test;

class ParseObjectComparison extends Thread {
    Thread a1;
    Clazz b1;
    Thread a2;
    Clazz b2;
    boolean error1;
    boolean error2;
    boolean ok1;
    boolean ok2;
    
    public void run() {
        if(a1 == b1)
            error1 = true;
        if(a1 == a2)
            ok1 = true;
        if(b2 != b1)
            error2 = true;
        if(a2 != b1)
            ok2 = true;
    }
    public static void main(String[] args) {
        ParseObjectComparison o = new ParseObjectComparison();
        o.a1 = new Thread();
        o.b1 = new Clazz();
        o.a2 = o.b1;
        o.b2 = o.a1;
        if(o.a1 == o.b1)
            o.error1 = true;
        if(o.a1 == o.a2)
            o.ok1 = true;
        if(o.b2 != o.b1)
            o.error2 = true;
        if(o.a2 != o.b1)
            o.ok2 = true;
        o.start();
    }
}
