package test;

class ObjectComparison extends Thread {
    Thread a1;
    Object b1;
    Thread a2;
    Object b2;
    Thread fn;
    boolean error1;
    boolean error2;
    boolean error3;
    boolean ok1;
    boolean ok2;
    boolean ok3;
    boolean ok4;

    
    public void run() {
        if(a1 == b1)
            error1 = true;
        if(a1 == a2)
            ok1 = true;
        if(b2 != b1)
            error2 = true;
        if(a2 != b1)
            ok2 = true;
        if(null == null)
            ok3 = true;
        if(null != a2)
            ok4 = true;
        if(fn != null)
            error3 = true;
    }
    public static void main(String[] args) {
        ObjectComparison o = new ObjectComparison();
        o.a1 = new Thread();
        o.b1 = new Thread();
        o.a2 = o.a1;
        o.b2 = o.b1;
        Thread n = null;
        if(o.a1 == o.b1)
            o.error1 = true;
        if(o.a1 == o.a2)
            o.ok1 = true;
        if(o.b2 != o.b1)
            o.error2 = true;
        if(o.a2 != o.b1)
            o.ok2 = true;
        if(null == null)
            o.ok3 = true;
        if(o.a2 != null)
            o.ok4 = true;
        if(n != null)
            o.error3 = true;
        o.start();
    }
}
