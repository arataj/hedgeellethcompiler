class SwitchLabelTest extends Thread {
  int f;

  public void run() {
    switch(f) {
      case f:
        break;
        
      case 2:
        break;

      case 3:
        break;

      case 1 + 1:
        break;
        
      case -1*2 + 16/5:
        break;
        
      case -1*2 + 16/4:
        break;
        
      default:
        break;

    }
  }
  public static void main(String[] args) {
    (new SwitchLabelTest()).start();
  }
}
