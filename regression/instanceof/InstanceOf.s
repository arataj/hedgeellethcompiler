<default>.InstanceOf.run/ #this
  boolean #c1
  <default>.InstanceOf #this
    0 #c1 = [instanceof <default>.ClassA]<default>.InstanceOf.c
    1 #c1 ? <null> : 3
    2 <default>.InstanceOf.isA = true
    3 #c1 = [instanceof <default>.ClassB]<default>.InstanceOf.c
    4 #c1 ? <null> : 6
    5 <default>.InstanceOf.isB = true
    6 #c1 = [instanceof <default>.ClassC]<default>.InstanceOf.c
    7 #c1 ? <null> : 9
    8 <default>.InstanceOf.isC = true
    9 return