class InstanceOf extends Thread {
  static ClassA c;
  static ClassB n;
  static boolean isA;
  static boolean isB;
  static boolean isC;
  static boolean error1;
  static boolean error2;
  
  public void run() {
      if(c instanceof ClassA)
          isA = true;
      if(c instanceof ClassC)
          isC = true;
      if(c instanceof ClassB)
          isB = true;
      if(c instanceof ClassC)
          isC = true;
      if(n instanceof ClassB)
          error1 = true;
      if(null instanceof Object)
          error2 = true;
  }
  public static void main(String[] args) {
      c = new ClassB();
      if(c instanceof ClassA)
          isA = true;
      if(c instanceof ClassB)
          isB = true;
      if(c instanceof ClassC)
          isC = true;
      c = null;
      if(c instanceof ClassA)
          error1 = true;
      if(null instanceof Object)
          error2 = true;
      InstanceOf iof = new InstanceOf();
      c = new ClassB();
      iof.start();
  }
}
