class ParseInstanceOf extends Thread {
  static ClassA c;
  static boolean isA;
  static boolean isB;
  static boolean isC;
  
  public void run() {
      if(c instanceof ClassA)
          isA = true;
      if(c instanceof Thread)
          isC = true;
      if(c instanceof ClassB)
          isB = true;
      if(c instanceof ClassC)
          isC = true;
  }
  public static void main(String[] args) {
      c = new ClassB();
      if(c instanceof ClassA)
          isA = true;
      if(c instanceof Thread)
          isB = true;
      if(c instanceof Object)
          isC = true;
      ParseInstanceOf iof = new ParseInstanceOf();
      iof.start();
  }
}
