/* header file of #default.B */

#ifndef HC___DEFAULT___B_H
#define HC___DEFAULT___B_H

#include "hc.h"

#include "A.h"
#include "C.h"

using namespace hc;

namespace root {
  class B : public root::A {
    public:
      virtual root::C* get();
      virtual ~B();
    
    protected:
      void __object();
      B();
  };
}

#endif /* !defined(HC___DEFAULT___B_H) */
