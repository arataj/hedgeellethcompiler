/* implementation file of #default.C */

#include "C.h"

using namespace hc;

namespace root {
  void C::__object() {
  }
  C::C() : B() {
    __object();
  }
  C::~C() {
    /* empty */
  }
}
