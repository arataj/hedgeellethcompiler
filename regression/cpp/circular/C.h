/* header file of #default.C */

#ifndef HC___DEFAULT___C_H
#define HC___DEFAULT___C_H

#include "hc.h"

#include "B.h"

using namespace hc;

namespace root {
  class C : public root::B {
    public:
      virtual ~C();
    
    protected:
      void __object();
      C();
  };
}

#endif /* !defined(HC___DEFAULT___C_H) */
