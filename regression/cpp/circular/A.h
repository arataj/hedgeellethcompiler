/* header file of #default.A */

#ifndef HC___DEFAULT___A_H
#define HC___DEFAULT___A_H

#include "hc.h"

namespace root {
  class B;
}

using namespace hc;

namespace root {
  class A : virtual public Object {
    public:
      virtual root::B* get();
      virtual ~A();
    
    protected:
      void __object();
      A();
  };
}

#endif /* !defined(HC___DEFAULT___A_H) */
