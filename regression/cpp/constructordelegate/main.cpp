#include "A.h"
#include "B.h"

using namespace root;

int main(int argc, char** argv) {
  B b = root::B(1, 2, 10);
  A a = b;
  printf("%i %i\n", a.f, b.f);
}
