/* header file of #default.B */

#ifndef HC___DEFAULT___B_H
#define HC___DEFAULT___B_H

#include "hc.h"

#include "A.h"

using namespace hc;

namespace root {
  class B : public root::A {
    public:
      signed int f;
      B();
      B(signed int i);
      B(signed int i, signed int j);
      B(signed int i, signed int j, signed int k);
      B(bool b, signed int i);
      B(bool b, signed int i, signed int j);
      B(bool b);
      virtual ~B();
    
    protected:
      void __object();
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__B();
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__B(signed int i);
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__B(signed int i, signed int j);
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__B(bool b, signed int i);
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__B(bool b, signed int i, signed int j);
  };
}

#endif /* !defined(HC___DEFAULT___B_H) */
