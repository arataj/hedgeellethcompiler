public class B extends A {
  public int f;
  
  public B() {
    super(7);
    f = 0;
  }
  public B(int i) {
    this();
    f = i;
  }
  public B(int i, int j) {
    this(i|j);
    f += 10;
  }
  public B(int i, int j, int k) {
    this(i|j, j|k);
    f += super.f;
  }
  public B(boolean b, int i) {
    super(i);
  }
  public B(boolean b, int i, int j) {
    this(b, j);
  }
  public B(boolean b) {
    this(b, 8, 5);
  }
}

/*
1 2 10

3 10

11

super.f = 7
this.f = 11 + 10 + 7 = 28
 */
