public class A {
  public int f;
  
  public A() {
    f = 0;
  }
  public A(int i) {
    this();
    f = i;
  }
  public A(int i, int j) {
    this(i|j);
  }
}
