/* implementation file of #default.B */

#include "B.h"

using namespace hc;

namespace root {
  void B::__object() {
    this->f = 0;
  }
  B::B() : A(7) {
    __object();
    this->f = 0;
  }
  B::B(signed int i) : A(7) {
    EQ__B();
    this->f = i;
  }
  B::B(signed int i, signed int j) : A(7) {
    signed int __c3;
    __c3 = i | j;
    EQ__B(__c3);
    this->f = this->f + 10;
  }
  B::B(signed int i, signed int j, signed int k) : A(7) {
    signed int __c4;
    signed int __c5;
    __c4 = i | j;
    __c5 = j | k;
    EQ__B(__c4, __c5);
    this->f = this->f + this->A::f;
  }
  B::B(bool b, signed int i) : A(i) {
    __object();
  }
  B::B(bool b, signed int i, signed int j) : A(j) {
    EQ__B(b, j);
  }
  B::B(bool b) : A(5) {
    EQ__B(b, 8, 5);
  }
  void B::EQ__B() {
    __object();
    this->f = 0;
  }
  void B::EQ__B(signed int i) {
    EQ__B();
    this->f = i;
  }
  void B::EQ__B(signed int i, signed int j) {
    signed int __c3;
    __c3 = i | j;
    EQ__B(__c3);
    this->f = this->f + 10;
  }
  void B::EQ__B(bool b, signed int i) {
    __object();
  }
  void B::EQ__B(bool b, signed int i, signed int j) {
    EQ__B(b, j);
  }
  B::~B() {
    /* empty */
  }
}
