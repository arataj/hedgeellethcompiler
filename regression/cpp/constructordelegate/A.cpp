/* implementation file of #default.A */

#include "A.h"

using namespace hc;

namespace root {
  void A::__object() {
    this->f = 0;
  }
  A::A() : Object() {
    __object();
    this->f = 0;
  }
  A::A(signed int i) : Object() {
    EQ__A();
    this->f = i;
  }
  A::A(signed int i, signed int j) : Object() {
    signed int __c3;
    __c3 = i | j;
    EQ__A(__c3);
  }
  void A::EQ__A() {
    __object();
    this->f = 0;
  }
  void A::EQ__A(signed int i) {
    EQ__A();
    this->f = i;
  }
  A::~A() {
    /* empty */
  }
}
