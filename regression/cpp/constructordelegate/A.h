/* header file of #default.A */

#ifndef HC___DEFAULT___A_H
#define HC___DEFAULT___A_H

#include "hc.h"

using namespace hc;

namespace root {
  class A : virtual public Object {
    public:
      signed int f;
      A();
      A(signed int i);
      A(signed int i, signed int j);
      virtual ~A();
    
    protected:
      void __object();
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__A();
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__A(signed int i);
  };
}

#endif /* !defined(HC___DEFAULT___A_H) */
