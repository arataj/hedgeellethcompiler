/*
 * AbstractVoxel.java
 *
 * Created on May 25, 2011, 11:09:41 AM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */
package volumes;

import hc.Mem;

/**
 * An abstract voxel. It defines packing and unpacking methods.<br>
 * 
 * A voxel may not lose data precision during packing.
 * 
 * @author Artur Rataj
 */
public abstract class AbstractVoxel {
    /**
     * A buffer to pack this voxel into.  Null if not needed yet.<br>
     * 
     * Managed by this class, do not delete in subclass' finalize().
     * Allocate if null but needed.
     */
    protected byte[] buffer;
    
    /**
     * Creates a new instance of AbstractVoxel. 
     */
    public AbstractVoxel() {
        buffer = null;
    }
    @Override
    public void finalize() throws Throwable {
        if(buffer != null)
            Mem.del(buffer);
        super.finalize();
    }
    /**
     * Returns the packed size in bytes of this voxel.
     * 
     * @return size of a byte array
     */
    abstract public int getPackedSize();
    /**
     * Packs this voxel into an array of bytes. The returned
     * array is valid until a next operation on this voxel,
     * and is managed by this voxel. The array should not
     * be modified by the caller.
     * 
     * @return a packed voxel
     */
    abstract public byte[] pack();
    /**
     * Unpacks an array of bytes into this voxel.
     * 
     * @param packed an array of packed voxels, this voxel does
     * a deep copy of a respective fragment of this array
     * @param offset index to the first element of this array's
     * fragment, that stores the values to unpack into this
     * voxel
     */
    abstract public void unpack(byte[] packed, int offset);
    /**
     * Merges the contents of another voxel with this voxel.
     * This method modifies only this voxel.<br>
     * 
     * This method should be unanimous with
     * <code>merge(byte[])</code>.
     * 
     * @param other another voxel, unaffected by this method.
     */
    abstract public void merge(AbstractVoxel other);
    /**
     * Merges the contents of another, packed voxel with this voxel.
     * This method modifies only this voxel.<br>
     * 
     * This method should be unanimous with
     * <code>merge(AbstractVoxel)</code>.
     * 
     * @param other packed another voxel, unaffected by this method;
     * must be of the class of this voxel
     */
    abstract public void merge(byte[] other);
    /**
     * <p>Multiplies subsequent values of this voxel. If the resulting value is
     * out of range, then it is clipped as necessary.</p>
     * 
     * <p>Optional.</p>
     * 
     * @param value a multiplier
     */
    abstract public void multiply(double value);
    /**
     * <p>Adds values of another voxel of the same class to this voxel.
     * If the resulting value is out of range, then it is clipped as necessary.</p>
     * 
     * <p>Optional.</p>
     * 
     * <p>This method should be unanimous with <code>add(byte[])</code>.</p>
     * 
     * @param other a voxel, not modified by this method
     */
    abstract public void add(AbstractVoxel other);
    /**
     * <p>Adds values of another voxel of the same class to this voxel.
     * If the resulting value is out of range, then it is clipped as necessary.</p>
     * 
     * <p>Optional.</p>
     * 
     * <p>This method should be unanimous with <code>add(byte[])</code>.</p>
     * 
     * @param other a voxel, not modified by this method
     */
    abstract public void add(byte[] other);
}
