/* header file of volumes.Index */

#ifndef HC_VOLUMES___INDEX_H
#define HC_VOLUMES___INDEX_H

#include "hc.h"

#include "AbstractIntLocation.h"

namespace volumes {
  class Size;
}

using namespace hc;

namespace volumes {
  class Index : public volumes::AbstractIntLocation {
    public:
      static volumes::Index* ORIGIN;
      static signed int MAX_COORD_INDEX;
      Index();
      Index(signed int ix, signed int iy, signed int iz);
      Index(volumes::Index* index);
      virtual void set(signed int coordIndex, signed int value);
      virtual signed int get(signed int coordIndex);
      virtual ~Index();
      using volumes::AbstractIntLocation::set;
    
    protected:
      void __object();
      virtual void add(volumes::Index* other);
      virtual void subtract(volumes::Index* other);
      virtual signed int findMaxDiffCoord(volumes::Index* other);
      virtual bool within(volumes::Size* size);
    
    private:
      static volumes::Index MEM__ORIGIN;
    
  };
}

#endif /* !defined(HC_VOLUMES___INDEX_H) */
