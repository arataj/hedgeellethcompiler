package volumes;

import hc.Mem;

/**
 * This is a random code just to test the optimize and the C++ backend. 
 */
class Octree {
  final int MAX = 20;
  
  int[] voxels;
  int[] background;
  int packedSize;
  Size size;

  public boolean isHomogenous(int num) {
        boolean homogenous = true;
        if(num == size.ix*size.iy*size.iz) {
            // the sparse voxels fill the entire volume of the
            // prism
            SCAN:
            for(int i = 0; i < MAX - 1; ++i) {
                int base = i*packedSize;
                for(int j = 0; j < packedSize; ++j)
                    if(voxels[base + j] != voxels[base + packedSize + j]) {
                        homogenous = false;
                        break SCAN;
                    }
            }
        } else if(num != 0) {
            // the sparse voxels fill the prism only partially
            for(int i = 0; i < num*packedSize; ++i)
                if(voxels[i] != background[i%packedSize]) {
                    homogenous = false;
                    break;
                }
        } else {
            // there are no sparse voxels at all
            /* empty */
        }
        return homogenous;
    }
   public void denoise() {
        byte[] prev = new byte[packedSize];
        byte[] packed = new byte[packedSize];
        Index index = new Index();
        Index sideIndex = new Index();
        for(int z = 0; z < size.iz; ++z) {
            index.iz = z;
            for(int y = 0; y < size.iy; ++y) {
                index.iy = y;
                for(int x = 0; x < size.ix; ++x) {
                    index.ix = x;
                    boolean homogenous = true;
                    SCAN:
                    for(int pair = 0; pair < 3; ++pair) {
                        sideIndex.set(index);
                        for(int a = -1; a <= 1; a += 2) {
                            sideIndex.set(pair, index.get(pair) + a);
                            if(!(pair == 0 && a == -1)) {
                                for(int i = 0; i < packedSize; ++i)
                                    if(prev[i] != packed[i]) {
                                        homogenous = false;
                                        break SCAN;
                                    }
                            }
                        }
                    }
                    if(homogenous)
                        index.ix = 0;
                }
            }
        }
        Mem.del(sideIndex);
        Mem.del(index);
        Mem.del(packed);
        Mem.del(prev);
    }
}

