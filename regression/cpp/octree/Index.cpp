/* implementation file of volumes.Index */

#include "Index.h"

#include "Size.h"

using namespace hc;

namespace volumes {
  Index Index::MEM__ORIGIN = Index();

  Index* Index::ORIGIN = &Index::MEM__ORIGIN;
  signed int Index::MAX_COORD_INDEX = 3;

  void Index::__object() {
  }
  Index::Index() : AbstractIntLocation() {
    __object();
  }
  Index::Index(signed int ix, signed int iy, signed int iz) : AbstractIntLocation(ix, iy, iz) {
    __object();
  }
  Index::Index(Index* index) : AbstractIntLocation(index) {
    __object();
  }
  void Index::add(Index* other) {
    this->AbstractIntLocation::ix = this->AbstractIntLocation::ix + other->ix;
    this->AbstractIntLocation::iy = this->AbstractIntLocation::iy + other->iy;
    this->AbstractIntLocation::iz = this->AbstractIntLocation::iz + other->iz;
  }
  void Index::set(signed int coordIndex, signed int value) {
    bool __c4;
    RuntimeException* __c7;
    __c4 = coordIndex == 0;
    if(__c4)
      goto L6;
    __c4 = coordIndex == 1;
    if(__c4)
      goto L8;
    __c4 = coordIndex == 2;
    if(__c4)
      goto L10;
    else
      goto L12;
    L6:
    this->AbstractIntLocation::ix = value;
    goto L14;
    L8:
    this->AbstractIntLocation::iy = value;
    goto L14;
    L10:
    this->AbstractIntLocation::iz = value;
    goto L14;
    L12:
    __c7 = new RuntimeException("invalid coordinate's index");
    throw __c7;
    L14:
    ;
  }
  signed int Index::get(signed int coordIndex) {
    bool __c4;
    RuntimeException* __c7;
    signed int __retval;
    __c4 = coordIndex == 0;
    if(__c4)
      goto L6;
    __c4 = coordIndex == 1;
    if(__c4)
      goto L8;
    __c4 = coordIndex == 2;
    if(__c4)
      goto L10;
    else
      goto L12;
    L6:
    __retval = this->AbstractIntLocation::ix;
    return __retval;
    L8:
    __retval = this->AbstractIntLocation::iy;
    return __retval;
    L10:
    __retval = this->AbstractIntLocation::iz;
    return __retval;
    L12:
    __c7 = new RuntimeException("invalid coordinate's index");
    throw __c7;
  }
  void Index::subtract(Index* other) {
    this->AbstractIntLocation::ix = this->AbstractIntLocation::ix - other->ix;
    this->AbstractIntLocation::iy = this->AbstractIntLocation::iy - other->iy;
    this->AbstractIntLocation::iz = this->AbstractIntLocation::iz - other->iz;
  }
  signed int Index::findMaxDiffCoord(Index* other) {
    bool __c15;
    RuntimeException* __c18;
    signed int __retval;
    signed int diffX;
    signed int diffY;
    signed int diffZ;
    signed int max;
    __retval = other->ix - this->AbstractIntLocation::ix;
    diffX = abs(__retval);
    __retval = other->iy - this->AbstractIntLocation::iy;
    diffY = abs(__retval);
    __retval = other->iz - this->AbstractIntLocation::iz;
    diffZ = abs(__retval);
    __retval = fmax(diffY, diffZ);
    max = fmax(diffX, __retval);
    __c15 = diffX == max;
    if(!__c15)
      goto L12;
    __retval = 0;
    return __retval;
    L12:
    __c15 = diffY == max;
    if(!__c15)
      goto L16;
    __retval = 1;
    return __retval;
    L16:
    __c15 = diffZ == max;
    if(!__c15)
      goto L20;
    __retval = 2;
    return __retval;
    L20:
    __c18 = new RuntimeException("should not be here");
    throw __c18;
  }
  bool Index::within(Size* size) {
    bool __retval;
    __retval = 0 <= this->AbstractIntLocation::ix;
    if(!__retval)
      goto L11;
    __retval = 0 <= this->AbstractIntLocation::iy;
    if(!__retval)
      goto L11;
    __retval = 0 <= this->AbstractIntLocation::iz;
    if(!__retval)
      goto L11;
    __retval = this->AbstractIntLocation::ix < size->ix;
    if(!__retval)
      goto L11;
    __retval = this->AbstractIntLocation::iy < size->iy;
    if(!__retval)
      goto L11;
    __retval = this->AbstractIntLocation::iz < size->iz;
    L11:
    return __retval;
  }
  Index::~Index() {
    /* empty */
  }
}
