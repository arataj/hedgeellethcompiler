/* header file of volumes.Real8Voxel */

#ifndef HC_VOLUMES___REAL8_VOXEL_H
#define HC_VOLUMES___REAL8_VOXEL_H

#include "hc.h"

#include "AbstractVectorVoxel.h"
#include "AbstractVoxel.h"

using namespace hc;

namespace volumes {
  class Real8Voxel : public volumes::AbstractVoxel, public volumes::AbstractVectorVoxel {
    public:
      Real8Voxel(double value);
      virtual signed int getPackedSize();
      virtual signed char* pack();
      virtual void unpack(signed char* packed, signed int offset);
      virtual void set(double value);
      virtual double get();
      virtual signed int getScalarNum();
      virtual double getScalar(signed int index);
      virtual void setScalar(signed int index, double newValue);
      virtual void merge(volumes::AbstractVoxel* other);
      virtual void merge(signed char* other);
      virtual void multiply(double value);
      virtual void add(volumes::AbstractVoxel* other);
      virtual void add(signed char* other);
      virtual ~Real8Voxel();
      using volumes::AbstractVoxel::merge;
      using volumes::AbstractVoxel::add;
    
    protected:
      signed char value;
      void __object();
  };
}

#endif /* !defined(HC_VOLUMES___REAL8_VOXEL_H) */
