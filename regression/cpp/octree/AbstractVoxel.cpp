/* implementation file of volumes.AbstractVoxel */

#include "AbstractVoxel.h"

using namespace hc;

namespace volumes {
  void AbstractVoxel::__object() {
    this->buffer = NULL;
  }
  AbstractVoxel::AbstractVoxel() : Object() {
    __object();
    this->buffer = NULL;
  }
  AbstractVoxel::~AbstractVoxel() {
    bool __c1;
    __c1 = this->buffer != NULL;
    if(!__c1)
      goto L3;
    Mem::del(this->buffer);
    L3:
    /* super.finalize(); */
    ;
  }
}
