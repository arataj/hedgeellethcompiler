/* implementation file of volumes.Octree */

#include "Octree.h"

#include "AbstractIntLocation.h"
#include "Index.h"
#include "Size.h"

using namespace hc;

namespace volumes {
  void Octree::__object() {
    this->MAX = 20;
    this->voxels = NULL;
    this->background = NULL;
    this->packedSize = 0;
    this->size = NULL;
  }
  bool Octree::isHomogenous(signed int num) {
    signed int __c10;
    Size* __c11;
    bool __c13;
    signed int* __c17;
    signed int __c20;
    signed int* __c29;
    bool __retval;
    signed int base;
    signed int i;
    signed int i__0;
    signed int j;
    __retval = true;
    __c11 = this->size;
    __c10 = __c11->ix * __c11->iy;
    __c11 = this->size;
    __c10 = __c10 * __c11->iz;
    __c13 = num == __c10;
    if(!__c13)
      goto L26;
    i = 0;
    L8:
    __c13 = i < 19;
    if(!__c13)
      goto L41;
    base = i * this->packedSize;
    j = 0;
    L12:
    __c13 = j < this->packedSize;
    if(!__c13)
      goto L24;
    __c10 = base + j;
    __c17 = this->voxels;
    __c20 = base + this->packedSize;
    __c20 = __c20 + j;
    __c13 = __c17[__c10] != __c17[__c20];
    if(!__c13)
      goto L22;
    __retval = false;
    goto L41;
    L22:
    j = j + 1;
    goto L12;
    L24:
    i = i + 1;
    goto L8;
    L26:
    __c13 = num != 0;
    if(!__c13)
      goto L41;
    i__0 = 0;
    L29:
    __c10 = num * this->packedSize;
    __c13 = i__0 < __c10;
    if(!__c13)
      goto L41;
    __c17 = this->voxels;
    __c29 = this->background;
    __c10 = i__0 % this->packedSize;
    __c13 = __c17[i__0] != __c29[__c10];
    if(!__c13)
      goto L39;
    __retval = false;
    goto L41;
    L39:
    i__0 = i__0 + 1;
    goto L29;
    L41:
    return __retval;
  }
  void Octree::denoise() {
    Size* __c16;
    bool __c17;
    signed int __c25;
    signed int a;
    bool homogenous;
    signed int i;
    Index* index;
    signed char* packed;
    signed int pair;
    signed char* prev;
    Index* sideIndex;
    signed int x;
    signed int y;
    signed int z;
    prev = new signed char[this->packedSize]();
    packed = new signed char[this->packedSize]();
    index = new Index();
    sideIndex = new Index();
    z = 0;
    L5:
    __c16 = this->size;
    __c17 = z < __c16->iz;
    if(!__c17)
      goto L56;
    index->iz = z;
    y = 0;
    L10:
    __c16 = this->size;
    __c17 = y < __c16->iy;
    if(!__c17)
      goto L54;
    index->iy = y;
    x = 0;
    L15:
    __c16 = this->size;
    __c17 = x < __c16->ix;
    if(!__c17)
      goto L52;
    index->ix = x;
    homogenous = true;
    pair = 0;
    L21:
    __c17 = pair < 3;
    if(!__c17)
      goto L49;
    sideIndex->set(index);
    a = -1;
    L25:
    __c17 = a <= 1;
    if(!__c17)
      goto L46;
    __c25 = index->get(pair);
    __c25 = __c25 + a;
    sideIndex->set(pair, __c25);
    __c17 = pair == 0;
    if(!__c17)
      goto L33;
    __c17 = a == -1;
    L33:
    __c17 = !__c17;
    if(!__c17)
      goto L44;
    i = 0;
    L36:
    __c17 = i < this->packedSize;
    if(!__c17)
      goto L44;
    __c17 = prev[i] != packed[i];
    if(!__c17)
      goto L42;
    homogenous = false;
    goto L50;
    L42:
    i = i + 1;
    goto L36;
    L44:
    a = a + 2;
    goto L25;
    L46:
    pair = pair + 1;
    goto L21;
    if(!homogenous)
      goto L50;
    L49:
    index->ix = 0;
    L50:
    x = x + 1;
    goto L15;
    L52:
    y = y + 1;
    goto L10;
    L54:
    z = z + 1;
    goto L5;
    L56:
    Mem::del(sideIndex);
    Mem::del(index);
    Mem::del(packed);
    Mem::del(prev);
  }
  Octree::Octree() : Object() {
    __object();
  }
  Octree::~Octree() {
    /* empty */
  }
}
