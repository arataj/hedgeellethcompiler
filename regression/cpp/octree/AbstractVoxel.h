/* header file of volumes.AbstractVoxel */

#ifndef HC_VOLUMES___ABSTRACT_VOXEL_H
#define HC_VOLUMES___ABSTRACT_VOXEL_H

#include "hc.h"

using namespace hc;

namespace volumes {
  /**
   * An abstract voxel. It defines packing and unpacking methods.<br>
   * 
   * A voxel may not lose data precision during packing.
   * 
   * @author Artur Rataj
   */
  class AbstractVoxel : virtual public Object {
    public:
      /**
       * Creates a new instance of AbstractVoxel. 
       */
      AbstractVoxel();
      virtual ~AbstractVoxel();
      /**
       * Returns the packed size in bytes of this voxel.
       * 
       * @return size of a byte array
       */
      virtual signed int getPackedSize() = 0;
      /**
       * Packs this voxel into an array of bytes. The returned
       * array is valid until a next operation on this voxel,
       * and is managed by this voxel. The array should not
       * be modified by the caller.
       * 
       * @return a packed voxel
       */
      virtual signed char* pack() = 0;
      /**
       * Unpacks an array of bytes into this voxel.
       * 
       * @param packed an array of packed voxels, this voxel does
       * a deep copy of a respective fragment of this array
       * @param offset index to the first element of this array's
       * fragment, that stores the values to unpack into this
       * voxel
       */
      virtual void unpack(signed char* packed, signed int offset) = 0;
      /**
       * Merges the contents of another voxel with this voxel.
       * This method modifies only this voxel.<br>
       * 
       * This method should be unanimous with
       * <code>merge(byte[])</code>.
       * 
       * @param other another voxel, unaffected by this method.
       */
      virtual void merge(volumes::AbstractVoxel* other) = 0;
      /**
       * Merges the contents of another, packed voxel with this voxel.
       * This method modifies only this voxel.<br>
       * 
       * This method should be unanimous with
       * <code>merge(AbstractVoxel)</code>.
       * 
       * @param other packed another voxel, unaffected by this method;
       * must be of the class of this voxel
       */
      virtual void merge(signed char* other) = 0;
      /**
       * <p>Multiplies subsequent values of this voxel. If the resulting value is
       * out of range, then it is clipped as necessary.</p>
       * 
       * <p>Optional.</p>
       * 
       * @param value a multiplier
       */
      virtual void multiply(double value) = 0;
      /**
       * <p>Adds values of another voxel of the same class to this voxel.
       * If the resulting value is out of range, then it is clipped as necessary.</p>
       * 
       * <p>Optional.</p>
       * 
       * <p>This method should be unanimous with <code>add(byte[])</code>.</p>
       * 
       * @param other a voxel, not modified by this method
       */
      virtual void add(volumes::AbstractVoxel* other) = 0;
      /**
       * <p>Adds values of another voxel of the same class to this voxel.
       * If the resulting value is out of range, then it is clipped as necessary.</p>
       * 
       * <p>Optional.</p>
       * 
       * <p>This method should be unanimous with <code>add(byte[])</code>.</p>
       * 
       * @param other a voxel, not modified by this method
       */
      virtual void add(signed char* other) = 0;
    
    protected:
      /**
       * A buffer to pack this voxel into.  Null if not needed yet.<br>
       * 
       * Managed by this class, do not delete in subclass' finalize().
       * Allocate if null but needed.
       */
      signed char* buffer;
      void __object();
  };
}

#endif /* !defined(HC_VOLUMES___ABSTRACT_VOXEL_H) */
