/*
 * AbstractIntLocation.java
 *
 * Created on May 26, 2011
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package volumes;

import linearalgebra.AbstractLocation;

/**
 * An abstract location, represented by integer values.<br>
 * 
 * Objects of this class are equal to all objects with the same
 * coordinates.
 * 
 * @author Artur Rataj
 */
public class AbstractIntLocation extends AbstractLocation {
    /**
     * X coordinate.
     */
    public int ix;
    /**
     * Y coordinate.
     */
    public int iy;
    /**
     * Z coordinate
     */
    public int iz;
    
    /**
     * Creates a new instance of Index, denoting some position
     * at (0, 0, 0).
     */ 
    public AbstractIntLocation() {
        set(0, 0, 0);
    }
    /**
     * Creates a new instance of Index, denoting a given position.
     * 
     * @param ix x coordinate
     * @param iy y coordinate
     * @param iz z coordinate
     */
    public AbstractIntLocation(int ix, int iy, int iz) {
        set(ix, iy, iz);
    }
    /**
     * A copying constructor.
     * 
     * @param location a location to deep copy
     */
    public AbstractIntLocation(AbstractIntLocation location) {
        set(location);
    }
    /**
     * Sets a position.
     * 
     * @param ix x coordinate
     * @param iy y coordinate
     * @param iz z coordinate
     */
    public final void set(int ix, int iy, int iz) {
        this.ix = ix;
        this.iy = iy;
        this.iz = iz;
    }
    /**
     * Sets a position.
     * 
     * @param source location to deep copy
     */
    public final void set(AbstractIntLocation location) {
        ix = location.ix;
        iy = location.iy;
        iz = location.iz;
    }
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.ix;
        hash = 41 * hash + this.iy;
        hash = 41 * hash + this.iz;
        return hash;
    }
    @Override
    public boolean equals(Object o) {
        AbstractIntLocation other = (AbstractIntLocation)o;
        return ix == other.ix && iy == other.iy && iz == other.iz;
    }
    @Override
    public String toString() {
        return "(" + ix + "," + iy + "," + iz + ")";
    }
}
