/* implementation file of volumes.AbstractIntLocation */

#include "AbstractIntLocation.h"

using namespace hc;

namespace volumes {
  void AbstractIntLocation::__object() {
    this->ix = 0;
    this->iy = 0;
    this->iz = 0;
  }
  AbstractIntLocation::AbstractIntLocation() : linearalgebra::AbstractLocation() {
    __object();
    set(0, 0, 0);
  }
  AbstractIntLocation::AbstractIntLocation(signed int ix, signed int iy, signed int iz) : linearalgebra::AbstractLocation() {
    __object();
    set(ix, iy, iz);
  }
  AbstractIntLocation::AbstractIntLocation(AbstractIntLocation* location) : linearalgebra::AbstractLocation() {
    __object();
    set(location);
  }
  void AbstractIntLocation::set(signed int ix, signed int iy, signed int iz) {
    this->ix = ix;
    this->iy = iy;
    this->iz = iz;
  }
  void AbstractIntLocation::set(AbstractIntLocation* location) {
    this->ix = location->ix;
    this->iy = location->iy;
    this->iz = location->iz;
  }
  signed int AbstractIntLocation::hashCode() {
    signed int __retval;
    signed int hash;
    __retval = 287;
    hash = __retval + this->ix;
    __retval = 41 * hash;
    hash = __retval + this->iy;
    __retval = 41 * hash;
    __retval = __retval + this->iz;
    return __retval;
  }
  bool AbstractIntLocation::equals(Object* o) {
    bool __retval;
    AbstractIntLocation* other;
    other = dynamic_cast<AbstractIntLocation*>(o);
    __retval = this->ix == other->ix;
    if(!__retval)
      goto L6;
    __retval = this->iy == other->iy;
    if(!__retval)
      goto L6;
    __retval = this->iz == other->iz;
    L6:
    return __retval;
  }
  string AbstractIntLocation::toString() {
    string __retval;
    __retval = "(" + hc::StringTools::toString(this->ix);
    __retval = __retval + ",";
    __retval = __retval + hc::StringTools::toString(this->iy);
    __retval = __retval + ",";
    __retval = __retval + hc::StringTools::toString(this->iz);
    __retval = __retval + ")";
    return __retval;
  }
  AbstractIntLocation::~AbstractIntLocation() {
    /* empty */
  }
}
