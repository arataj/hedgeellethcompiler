/*
 * AbstractVectorVoxel.java
 *
 * Created on Jul 16, 2013
 *
 * Copyright (c) 2013  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package volumes;

/**
 * Implemented by classes of voxels, that are represented only by a
 * statically fixed number of ordered scalar elements.
 * 
 * @author Artur Rataj
 */
public interface AbstractVectorVoxel {
    /**
     * The number of ordered scalar elements, that exclusively represent
     * this voxel.
     * 
     * @return a number of elements, can be zero
     */
    public int getScalarNum();
    /**
     * Returns the value of a scalar at <code>index</code>, converted
     * to double.
     * 
     * @param index an index, <code>0 ... getScalarNum() - 1</code>
     * @return a double value
     */
    public double getScalar(int index);
    /**
     * Sets the new value of a scalar at <code>index</code>, converted
     * from a double.
     * 
     * @param index an index, <code>0 ... getScalarNum() - 1</code>
     * @param newValue a double value
     */
    public void setScalar(int index, double newValue);
}
