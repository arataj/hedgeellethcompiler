package volumes;

public class Size extends AbstractIntLocation {
    public Size() {
        super();
    }
    public Size(int sx, int sy, int sz) {
        super(sx, sy, sz);
    }
    public Size(Size size) {
        super(size);
    }
    public Size(int i) {
        this(0, i, 2);
    }
}
