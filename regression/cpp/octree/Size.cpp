/* implementation file of volumes.Size */

#include "Size.h"

using namespace hc;

namespace volumes {
  void Size::__object() {
  }
  Size::Size() : AbstractIntLocation() {
    __object();
  }
  Size::Size(signed int sx, signed int sy, signed int sz) : AbstractIntLocation(sx, sy, sz) {
    __object();
  }
  Size::Size(Size* size) : AbstractIntLocation(size) {
    __object();
  }
  Size::Size(signed int i) : AbstractIntLocation(0, i, 2) {
    EQ__Size(0, i, 2);
  }
  void Size::EQ__Size(signed int sx, signed int sy, signed int sz) {
    __object();
  }
  Size::~Size() {
    /* empty */
  }
}
