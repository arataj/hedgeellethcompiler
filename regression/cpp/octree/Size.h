/* header file of volumes.Size */

#ifndef HC_VOLUMES___SIZE_H
#define HC_VOLUMES___SIZE_H

#include "hc.h"

#include "AbstractIntLocation.h"

using namespace hc;

namespace volumes {
  class Size : public volumes::AbstractIntLocation {
    public:
      Size();
      Size(signed int sx, signed int sy, signed int sz);
      Size(volumes::Size* size);
      Size(signed int i);
      virtual ~Size();
    
    protected:
      void __object();
      /**
       * This is a constructor equivalent method, used as
       * a workaround for missing constructor delegates
       * in C++ implementations not compliant with C++0x,
       * do not use.
       */
      void EQ__Size(signed int sx, signed int sy, signed int sz);
  };
}

#endif /* !defined(HC_VOLUMES___SIZE_H) */
