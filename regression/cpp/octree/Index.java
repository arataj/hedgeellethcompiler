package volumes;

public class Index extends AbstractIntLocation {
    public final static Index ORIGIN = new Index();
    public static final int MAX_COORD_INDEX = 3;
    public Index() {
        super();
    }
    public Index(int ix, int iy, int iz) {
        super(ix, iy, iz);
    }
    public Index(Index index) {
        super(index);
    }
    void add(Index other) {
        ix += other.ix;
        iy += other.iy;
        iz += other.iz;
    }
    public void set(int coordIndex, int value) {
        switch(coordIndex) {
            case 0:
                ix = value;
                break;
                
            case 1:
                iy = value;
                break;
                
            case 2:
                iz = value;
                break;
                
            default:
                throw new RuntimeException("invalid coordinate's index");
                
        }
    }
    public int get(int coordIndex) {
        switch(coordIndex) {
            case 0:
                return ix;
                
            case 1:
                return iy;
                
            case 2:
                return iz;
                
            default:
                throw new RuntimeException("invalid coordinate's index");
                
        }
    }
    void subtract(Index other) {
        ix -= other.ix;
        iy -= other.iy;
        iz -= other.iz;
    }
    int findMaxDiffCoord(Index other) {
        int diffX = Math.abs(other.ix - ix);
        int diffY = Math.abs(other.iy - iy);
        int diffZ = Math.abs(other.iz - iz);
        int max = Math.max(diffX, Math.max(diffY, diffZ));
        if(diffX == max)
            return 0;
        if(diffY == max)
            return 1;
        if(diffZ == max)
            return 2;
        else
            throw new RuntimeException("should not be here");
    }
    boolean within(Size size) {
        return ix >= 0 && iy >= 0 && iz >= 0 &&
                ix < size.ix && iy < size.iy && iz < size.iz;
    }
}
