package volumes;

import hc.Mem;

public class Real8Voxel extends AbstractVoxel implements AbstractVectorVoxel {
    protected byte value;
    
    public Real8Voxel(double value) {
        set(value);
        buffer = null;
        if(!(this instanceof AbstractVectorVoxel))
            throw new RuntimeException("");
    }
    public int getPackedSize() {
        return 1;
    }
    public byte[] pack() {
        if(buffer == null)
            buffer = new byte[getPackedSize()];
        buffer[0] = value;
        return buffer;
    }
    public void unpack(byte[] packed, int offset) {
        value = packed[offset];
    }
    public void set(double value) {
        if(value < 0.0 || value > 1.0)
            throw new RuntimeException("value of out range");
        this.value = (byte)Math.round(value*0xff);
    }
    public double get() {
        return VoxelUtils.toDoubleUnsigned(value);
    }
    public int getScalarNum() {
        return 1;
    }
    public double getScalar(int index) {
        if(index != 0)
            throw new RuntimeException("invalid index");
        else
            return get();
    }
    public void setScalar(int index, double newValue) {
        if(index != 0)
            throw new RuntimeException("invalid index");
        else
            set(newValue);
    }
    public void merge(AbstractVoxel other) {
        Real8Voxel voxel = (Real8Voxel)other;
        set((get() + voxel.get())/2.0);
    }
    public void merge(byte[] other) {
        value = (byte)((2*(value + other[0]) + 1)/4);
    }
    public void multiply(double value) {
        VoxelUtils.multiply(this, value, 0.0, 1.0);
    }
    public void add(AbstractVoxel other) {
        add(other.pack());
    }
    public void add(byte[] other) {
        set(Math.max(0.0, Math.min(1.0, get() +
                VoxelUtils.toDoubleUnsigned(other[0]))));
    }
}
