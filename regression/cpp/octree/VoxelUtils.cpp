/* implementation file of volumes.VoxelUtils */

#include "VoxelUtils.h"

#include "AbstractVectorVoxel.h"

using namespace hc;

namespace volumes {
  void VoxelUtils::__object() {
  }
  double VoxelUtils::toDoubleUnsigned(signed char value) {
    signed int __c2;
    double __retval;
    __c2 = value & 255;
    __retval = __c2 / 255.0;
    return __retval;
  }
  void VoxelUtils::setClipped(AbstractVectorVoxel* v, signed int index, double value, double min, double max) {
    double __retval_max;
    __retval_max = fmin(max, value);
    __retval_max = fmax(min, __retval_max);
    v->setScalar(index, __retval_max);
  }
  void VoxelUtils::multiply(AbstractVectorVoxel* v, signed int index, double multiplier, double min, double max) {
    double __c6;
    __c6 = v->getScalar(index);
    __c6 = __c6 * multiplier;
    __c6 = fmin(max, __c6);
    __c6 = fmax(min, __c6);
    v->setScalar(index, __c6);
  }
  void VoxelUtils::multiply(AbstractVectorVoxel* v, double multiplier, double min, double max) {
    bool __c6;
    signed int __retval_getScalarNum;
    signed int i;
    i = 0;
    L1:
    __retval_getScalarNum = v->getScalarNum();
    __c6 = i < __retval_getScalarNum;
    if(!__c6)
      goto L7;
    VoxelUtils::multiply(v, i, multiplier, min, max);
    i = i + 1;
    goto L1;
    L7:
    ;
  }
  void VoxelUtils::add(AbstractVectorVoxel* v, signed int index, double component, double min, double max) {
    double __c6;
    __c6 = v->getScalar(index);
    __c6 = __c6 + component;
    __c6 = fmin(max, __c6);
    __c6 = fmax(min, __c6);
    v->setScalar(index, __c6);
  }
  void VoxelUtils::add(AbstractVectorVoxel* v, double component, double min, double max) {
    bool __c6;
    signed int __retval_getScalarNum;
    signed int i;
    i = 0;
    L1:
    __retval_getScalarNum = v->getScalarNum();
    __c6 = i < __retval_getScalarNum;
    if(!__c6)
      goto L7;
    VoxelUtils::add(v, i, component, min, max);
    i = i + 1;
    goto L1;
    L7:
    ;
  }
  VoxelUtils::VoxelUtils() : Object() {
    __object();
  }
  VoxelUtils::~VoxelUtils() {
    /* empty */
  }
}
