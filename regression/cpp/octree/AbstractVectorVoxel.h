/* header file of volumes.AbstractVectorVoxel */

#ifndef HC_VOLUMES___ABSTRACT_VECTOR_VOXEL_H
#define HC_VOLUMES___ABSTRACT_VECTOR_VOXEL_H

#include "hc.h"

using namespace hc;

namespace volumes {
  /**
   * Implemented by classes of voxels, that are represented only by a
   * statically fixed number of ordered scalar elements.
   * 
   * @author Artur Rataj
   */
  class AbstractVectorVoxel : virtual public Object {
    public:
      /**
       * The number of ordered scalar elements, that exclusively represent
       * this voxel.
       * 
       * @return a number of elements, can be zero
       */
      virtual signed int getScalarNum() = 0;
      /**
       * Returns the value of a scalar at <code>index</code>, converted
       * to double.
       * 
       * @param index an index, <code>0 ... getScalarNum() - 1</code>
       * @return a double value
       */
      virtual double getScalar(signed int index) = 0;
      /**
       * Sets the new value of a scalar at <code>index</code>, converted
       * from a double.
       * 
       * @param index an index, <code>0 ... getScalarNum() - 1</code>
       * @param newValue a double value
       */
      virtual void setScalar(signed int index, double newValue) = 0;
      virtual ~AbstractVectorVoxel();
  };
}

#endif /* !defined(HC_VOLUMES___ABSTRACT_VECTOR_VOXEL_H) */
