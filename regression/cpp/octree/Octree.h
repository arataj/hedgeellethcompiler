/* header file of volumes.Octree */

#ifndef HC_VOLUMES___OCTREE_H
#define HC_VOLUMES___OCTREE_H

#include "hc.h"

namespace volumes {
  class Size;
}

using namespace hc;

namespace volumes {
  /**
   * This is a random code just to test the optimize and the C++ backend. 
   */
  class Octree : virtual public Object {
    public:
      virtual bool isHomogenous(signed int num);
      virtual void denoise();
      virtual ~Octree();
    
    protected:
      signed int MAX;
      signed int* voxels;
      signed int* background;
      signed int packedSize;
      volumes::Size* size;
      void __object();
      Octree();
  };
}

#endif /* !defined(HC_VOLUMES___OCTREE_H) */
