package volumes;

public class VoxelUtils {
    public static double toDoubleUnsigned(byte value) {
        return (value&0xff)/255.0;
    }
    public static void setClipped(AbstractVectorVoxel v,
            int index, double value, double min, double max) {
        v.setScalar(index, Math.max(min, Math.min(max, value)));
    }
    public static void multiply(AbstractVectorVoxel v,
            int index, double multiplier, double min, double max) {
        v.setScalar(index, Math.max(min, Math.min(max,
                v.getScalar(index)*multiplier)));
    }
    public static void multiply(AbstractVectorVoxel v,
            double multiplier, double min, double max) {
        for(int i = 0; i < v.getScalarNum(); ++i)
            multiply(v, i, multiplier, min, max);
    }
    public static void add(AbstractVectorVoxel v,
            int index, double component, double min, double max) {
        v.setScalar(index, Math.max(min, Math.min(max,
                v.getScalar(index) + component)));
    }
    public static void add(AbstractVectorVoxel v,
            double component, double min, double max) {
        for(int i = 0; i < v.getScalarNum(); ++i)
            add(v, i, component, min, max);
    }
    
}
