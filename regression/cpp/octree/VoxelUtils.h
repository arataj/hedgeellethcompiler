/* header file of volumes.VoxelUtils */

#ifndef HC_VOLUMES___VOXEL_UTILS_H
#define HC_VOLUMES___VOXEL_UTILS_H

#include "hc.h"

namespace volumes {
  class AbstractVectorVoxel;
}

using namespace hc;

namespace volumes {
  class VoxelUtils : virtual public Object {
    public:
      static double toDoubleUnsigned(signed char value);
      static void setClipped(volumes::AbstractVectorVoxel* v, signed int index, double value, double min, double max);
      static void multiply(volumes::AbstractVectorVoxel* v, signed int index, double multiplier, double min, double max);
      static void multiply(volumes::AbstractVectorVoxel* v, double multiplier, double min, double max);
      static void add(volumes::AbstractVectorVoxel* v, signed int index, double component, double min, double max);
      static void add(volumes::AbstractVectorVoxel* v, double component, double min, double max);
      virtual ~VoxelUtils();
    
    protected:
      void __object();
      VoxelUtils();
  };
}

#endif /* !defined(HC_VOLUMES___VOXEL_UTILS_H) */
