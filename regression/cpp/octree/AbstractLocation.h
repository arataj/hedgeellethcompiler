/* header file of linearalgebra.AbstractLocation */

#ifndef HC_LINEARALGEBRA___ABSTRACT_LOCATION_H
#define HC_LINEARALGEBRA___ABSTRACT_LOCATION_H

#include "hc.h"

using namespace hc;

namespace linearalgebra {
  /**
   *
   * @author Artur Rataj
   */
  class AbstractLocation : virtual public Object {
    public:
      /**
       * Creates a new instance of AbstractLocation. 
       */
      AbstractLocation();
      virtual ~AbstractLocation();
    
    protected:
      void __object();
  };
}

#endif /* !defined(HC_LINEARALGEBRA___ABSTRACT_LOCATION_H) */
