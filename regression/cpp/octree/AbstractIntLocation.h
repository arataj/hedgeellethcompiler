/* header file of volumes.AbstractIntLocation */

#ifndef HC_VOLUMES___ABSTRACT_INT_LOCATION_H
#define HC_VOLUMES___ABSTRACT_INT_LOCATION_H

#include "hc.h"

#include "AbstractLocation.h"

using namespace hc;

namespace volumes {
  /**
   * An abstract location, represented by integer values.<br>
   * 
   * Objects of this class are equal to all objects with the same
   * coordinates.
   * 
   * @author Artur Rataj
   */
  class AbstractIntLocation : public linearalgebra::AbstractLocation {
    public:
      /**
       * X coordinate.
       */
      signed int ix;
      /**
       * Y coordinate.
       */
      signed int iy;
      /**
       * Z coordinate
       */
      signed int iz;
      /**
       * Creates a new instance of Index, denoting some position
       * at (0, 0, 0).
       */
      AbstractIntLocation();
      /**
       * Creates a new instance of Index, denoting a given position.
       * 
       * @param ix x coordinate
       * @param iy y coordinate
       * @param iz z coordinate
       */
      AbstractIntLocation(signed int ix, signed int iy, signed int iz);
      /**
       * A copying constructor.
       * 
       * @param location a location to deep copy
       */
      AbstractIntLocation(volumes::AbstractIntLocation* location);
      /**
       * Sets a position.
       * 
       * @param ix x coordinate
       * @param iy y coordinate
       * @param iz z coordinate
       */
      virtual void set(signed int ix, signed int iy, signed int iz);
      /**
       * Sets a position.
       * 
       * @param source location to deep copy
       */
      virtual void set(volumes::AbstractIntLocation* location);
      virtual signed int hashCode();
      virtual bool equals(Object* o);
      virtual string toString();
      virtual ~AbstractIntLocation();
    
    protected:
      void __object();
  };
}

#endif /* !defined(HC_VOLUMES___ABSTRACT_INT_LOCATION_H) */
