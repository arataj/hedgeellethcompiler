/*
 * Volumes.java
 *
 * Created on Apr 28, 2011, 12:04:18 PM
 *
 * Copyright (c) 2011 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package volumes;

/**
 * A class.
 *
 * @author Artur Rataj
 */
public class Volumes {
    static float i = 5;
    /** object */
    int k = 9 + (int)j;
    /** static */
    static double j = i + 4;
    int l = 12*k;

    public int hash() {
        int hash = 7;
        hash = hash + k*41;
        hash = l*41 + hash;
        return hash;
    }
    @Override
    public boolean equals(Object o) {
        Volumes other = (Volumes)o;
        return k == other.k && l == other.l;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Volumes v = new Volumes();
        throw new RuntimeException("test" + v.l);
    }
}
