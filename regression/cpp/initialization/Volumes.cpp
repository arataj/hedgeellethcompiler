/* implementation file of volumes.Volumes */

#include "Volumes.h"

using namespace hc;

namespace volumes {
  float Volumes::i = 5;
  double Volumes::j = Volumes::i + 4;

  void Volumes::__object() {
    signed int __c1;
    __c1 = (signed int)Volumes::j;
    this->k = 9 + __c1;
    this->l = 12 * this->k;
  }
  signed int Volumes::hash() {
    signed int __retval;
    signed int hash;
    __retval = this->k * 41;
    hash = 7 + __retval;
    __retval = this->l * 41;
    __retval = __retval + hash;
    return __retval;
  }
  bool Volumes::equals(Object* o) {
    bool __retval;
    Volumes* other;
    other = dynamic_cast<Volumes*>(o);
    __retval = this->k == other->k;
    if(!__retval)
      goto L4;
    __retval = this->l == other->l;
    L4:
    return __retval;
  }
  void Volumes::main(string* args) {
    string __c3;
    RuntimeException* __c4;
    Volumes* v;
    v = new Volumes();
    __c3 = "test" + hc::StringTools::toString(v->l);
    __c4 = new RuntimeException(__c3);
    throw __c4;
  }
  Volumes::Volumes() : Object() {
    __object();
  }
  Volumes::~Volumes() {
    /* empty */
  }
}
