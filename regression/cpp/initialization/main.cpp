#include "hc.h"
#include "Volumes.h"

int main(int argc, char** args) {
  try {
    volumes::Volumes::main(NULL); 
  } catch(RuntimeException* e) {
    cout << e->getMessage() << "\n";
    delete e;
  }  
}
