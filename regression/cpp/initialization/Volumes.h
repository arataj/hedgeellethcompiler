/* header file of volumes.Volumes */

#ifndef HC_VOLUMES___VOLUMES_H
#define HC_VOLUMES___VOLUMES_H

#include "hc.h"

using namespace hc;

namespace volumes {
  /**
   * A class.
   *
   * @author Artur Rataj
   */
  class Volumes : virtual public Object {
    public:
      virtual signed int hash();
      virtual bool equals(Object* o);
      /**
       * @param args the command line arguments
       */
      static void main(string* args);
      virtual ~Volumes();
    
    protected:
      static float i;
      /** object */
      signed int k;
      /** static */
      static double j;
      signed int l;
      void __object();
      Volumes();
  };
}

#endif /* !defined(HC_VOLUMES___VOLUMES_H) */
