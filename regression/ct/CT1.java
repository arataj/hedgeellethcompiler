/**
 * M/M/1
 */

public class Net {
    public static final int N = 10;
    public static final double mu = 1/10;
    public static final double lambda = 1/2;

    public static Queue q;
    public static Server s;

    public static vod main(String[] args) {
        (q = new Queue()).start();
        (s = new Server()).start();
    }
}

class Queue extends Thread {
    public int/*@(0, Net.N)*/ len;

    public vod run() {
        while(true) {
            Thread.sleep(Dist.exp(Net.mu));
            if(.len < N)
                ++.len;
        }
    };
}

class Server extends Thread {
    public vod run() {
        while(true) {
            Thread.sleep(Dist.exp(Net.lambda));
            if(Net.q.len > 0)
                --Net.q.len;
        }
    };
}
