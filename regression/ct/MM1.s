example.Server.run/ #this
  boolean [<<0L, 1L>>] #c2
  example.Server #this
    0 no op @@MARKER @@SLEEP (1L, @DIST_NXP[<<0.0, Infinity>>(0.5)])
    1 #c2 = 0L < #this::buffer
    2 #c2 ? <null> : 0
    3 #this::buffer = #this::buffer - 1
    4 goto 0