/**
 * TagPrefixUnknown.java
 *
 * Copyright (C) 2008 Andrzej Zbrzeźny.
 * Copyright (C) 2009 Artur Rataj.
 */
 
public class TagPrefixUnknown {
    public static void main(String args []) {
        final int NUM = 3;
        
        Fork[] forks = new Fork[NUM];
        
        for(int i = 0; i < NUM; ++i)
            forks[i] = new Fork(false);

        for(int i = 0; i < NUM; ++i) {
            Philosopher p = new Philosopher(i,
                forks[i], forks[(i + 1)%NUM]);
            (new Thread(p)).start();
        }
    }
}

class Fork {
    private boolean unavailable;

    public Fork(boolean unavailable) {
        this.unavailable = unavailable;
    }

    public synchronized void acquire() {
        while(unavailable) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        unavailable = true;
    }

    public synchronized void release() {
        unavailable = false;
        notify();
    }
}

class Philosopher implements Runnable {
    private int num;
    private Fork left;
    private Fork right;

    public Philosopher(int num, Fork left,
            Fork right) {
        this.num = num;
        this.left = left;
        this.right = right;
    }

    // @generateHead()
    private void eating() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
        }
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
            }
            left.acquire();
            /* @state(name locationAcquireLeft) */
            right.acquire();
            /* @state(location Eating) */
            eating();
            right.release();
            left.release();
            /* @state(name locationSatiated, name locationOther, named) */
        }
    }
}
