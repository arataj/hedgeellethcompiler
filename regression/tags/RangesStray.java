// stray ranges

class RangesStray extends Thread {
    int/*@(-1, 1)*/[] /* @ ( - 1 ,  1 ) *//*@(-10, 10)*/f;
    /*@(-1, 1)*/static /*@(-1, 1)*/int /*@(-5,4)*/g/*@(-1, 1)*/;
    
    public void /*@(-1, 1)*/run() {
        g = /* test @(-3, 9) //(<-40, 20>) */(-/* //-20, 40 */
            (/*@(-2, 1)*/g*/*@(-20, -3)*/g));
        f[2] /*@(-1, 1)*/= f[1];
    }
    
    public static void main(String[] args) {
        g = -3;
        g = /*@(-1, 1)*//* -22, -2 */(/*@(-2, 1) (-22, -2)*/(g + 1) +
            /*@(-20, -3)*/g);
        int[] /*@(-10, 11)*/l = new int[10];
        RangesStray r = new RangesStray();
        l[1] = 11/*@(-1, 1)*/;
        r.f = l;
        r.start(/*@(-1, 1)*/);
    }/*@(-1, 1)*/
}/*@(-1, 1)*/
