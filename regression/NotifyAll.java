public class NotifyAll extends Thread {
    Object lock;
    int f = -1;
    
    public NotifyAll(Object lock) {
        this.lock = lock;
    }
    public void run() {
        lock.notifyAll();
        f = 1;
        lock.wait();
    }
    public static void main(String args []) {
        Object lock = new Object();
        for(int i = 0; i < 4; ++i) {
            NotifyAll p = new NotifyAll(lock);
            p.start();
        }
    }
}
