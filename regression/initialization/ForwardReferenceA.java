class ForwardReferenceA {
  int i = j;
  static int sA = ForwardReferenceB.sB;
  int j = 9 + k;
  int a = ForwardReferenceB.sB;
  double[] array = { 1, l, 3 };
  static int k;
  int l;
}
