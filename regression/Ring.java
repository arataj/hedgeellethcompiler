/**
 * Ring.java
 *
 * Tests barrier synchronization.
 */

public class Ring {
    public static void main(String args []) {
        final int N = 3;
        final int MAX_QUEUE = 4;
        
        Node[] nodes = new Node[NUM];
        // create nodes
        for(int i = 0; i < NUM; ++i)
            nodes[i] = new Node(MAX_QUEUE);
        // create the trigger
        Trigger trigger = new Trigger();
        // start all threads
        new Thread(trigger).start();
        for(int i = 0; i < NUM; ++i)
            (new Thread(nodes[i])).start();
    }
}

class Trigger implements Runnable {
    private final int maxQueue;

    public Trigger(boolean maxQueue) {
        this.maxQueue = maxQueue;
    }
    public void run() {
        while (true) {
        }
    }
}

class Node implements Runnable {
    private final int maxQueue;

    public Node(boolean maxQueue) {
        this.maxQueue = maxQueue;
    }
    public void run() {
        while (true) {
        }
    }
}
