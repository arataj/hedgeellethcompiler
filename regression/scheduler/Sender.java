/* Author: Andrzej Zbrzezny (c) 2007 */

package modelchecking.abp.v2;

import java.util.Random;

public class Sender implements Runnable {
  private LossyChannel channel;

  public Sender(LossyChannel channel) {
    this.channel = channel;
  }

  public void run() {
    boolean protocolBit = false;
    Random random = new Random();

    while (true) {
      if (protocolBit != channel.getAckBit()) {
        System.out.println("Putting " + protocolBit);
        channel.put(protocolBit);
      } else {
        protocolBit = !protocolBit;
      }
      try {
        Thread.sleep(random.nextInt(15));
      } catch (InterruptedException e) {}
    }
  }
}

