/* Author: Andrzej Zbrzezny (c) 2007 */

package modelchecking.abp.v2;

/**
 * Scheduler IMMEDIATE.
 */
public class AltBitProtocolI {
  public static void main(String[] args) {
    LossyChannel channel = new LossyChannel();
    (new Thread(new Sender(channel))).start();
    (new Thread(new Receiver(channel))).start();
  }
}

