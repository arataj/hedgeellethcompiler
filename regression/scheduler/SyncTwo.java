class SyncNotify extends Thread {
  final Object LOCK;
  int n;
  
  public SyncNotify(Object lock) {
    LOCK = lock;
  }
  public void run() {
      n = 1;
      synchronized(LOCK) {
        LOCK.notify();
      }
      n = 2;
  }
}

class SyncWait extends Thread {
  int w;
  final Object LOCK;
  
  public SyncWait(Object lock) {
    LOCK = lock;
  }
  public void run() {
      w = 1;
      synchronized(LOCK) {
        LOCK.wait();
      }
      w = 2;
  }
}

class Main {
  public static void main(String[] args) {
    Object lock = new Object();
    SyncNotify n = new SyncNotify(lock);
    SyncWait w = new SyncWait(lock);
    n.start();
    w.start();
  }
}
