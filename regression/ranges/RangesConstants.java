// ranges optimized to constants

class RangesConstants extends Thread {
    int /*@(-10, 10)*/f = -5;
    static int g;

    public void run() {
      int/*@(f, 10)*/ l = 4 + f;
      int/*@(l, 2*-f)*/ i = 10; // (<-1, 10>)
      g = i*i;
    }
    public static void main(String[] args) {
      (new RangesConstants()).start();
    }
}
