// computing range of expression's result in compiled code,
// setting variable ranges on basis of proposals
//
// this class tests primary bounds evaluable in
// range analysis
//
// primary ranges not optimized

class RangesExpressionCompiledConstant extends Thread {
    int /*@(-4,4)*/f = -3; // -3, -2
    int /*@(f, -f*100)*/g; // -3, 52
    
    public void run() {
        if(g == f)
          f = -2;
        g = /* // -9, -6 */((-/* //6, 9 */
            (/*@(-3, 1)*/f*/*@(-20, -3)*/f)));
        g = /* // 33, 52 */ f*f*f + 60;
    }
    public static void main(String[] args) {
        (new RangesExpressionCompiledConstant()).start();
    }
}
                                  