// range check error in primary range within compiled code

class RangesExpressionCompiledCheckError extends Thread {
    int /*@(-4,4)*/f = -4;
    int g;
    
    public void run() {
        g = /* //(<-60, 20>) */((-/* //-20, 60 */
            (/*@(-3, 1)*/f*/*@(-20, -3)*/f)));
    }
    public static void main(String[] args) {
        (new RangesExpressionCompiledCheckError()).start();
    }
}
                                  