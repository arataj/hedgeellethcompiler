// mismatch of variable ranges of array references

class RangesArrayCompiledMismatch extends Thread {
    int[] /*@(-10, 10)*/f;
    int[] /*@(-6, 6)*/f1;
    int /*@(-5, 5)*/f2 = 4;

    public void run() {
        f[2] = f[1];
        f2 = f1[2];
    }
    public static void main(String[] args) {
        int[] /*@(-10, 11)*/l = new int[10];
        RangesArrayCompiledMismatch r = new RangesArrayCompiledMismatch();
        l[1] = 5;
        r.f = l;
        r.f1 = r.f;
        r.start();
    }
}
