// ranges not optimized to constants

class RangesVariables extends Thread {
    // there is a write to this field in run(), so no replacement with
    // a constant
    int /*@(-10, 10)*/f = -5;

    public void run() {
      int /*@(f, 10)*/l = 4 + f;
      int k, /*@(l, 2*-f)*/i = 10; // (<-1, 10>)
      f = i*i;
    }
    public static void main(String[] args) {
      (new RangesVariables()).start();
    }
}
