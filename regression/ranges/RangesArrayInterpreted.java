// outside variable range in interpreted code

class RangesArrayInterpreted extends Thread {
    int[] /*@(-10, 10)*/f;

    public void run() {
      f[2] = f[1];
    }
    public static void main(String[] args) {
      int[] /*@(-10, 11)*/l = new int[10];
      RangesArrayInterpreted r = new RangesArrayInterpreted();
      l[1] = 12;
      r.f = l;
      r.start();
    }
}
