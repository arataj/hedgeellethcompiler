package indexing;

// proposals on indexed arrays
// index out of range
class IndexingOutOfRange2 extends Thread {
    int[] a = {-2, 3};
    int[] b = {3, 4, 6};

    public void run() {
      b[3] = a[1];
    }
    public static void main(String[] args) {
      (new IndexingOutOfRange2()).start();
    }
}
