// ranges with invalid type

class RangesType extends Thread {
    Object /*@(0, 0)*/o;

    public void run() {
      (/*@(-1, 1)*/run());
    }
    public static void main(String[] args) {
      (/*@(-2, 2)*/new RangesType()).start();
    }
}
