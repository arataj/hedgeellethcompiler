// variable ranges with invalid type

class RangesVariableType extends Thread {
    Object /*@(-1, 1)*/o;

    public void run() {
      run();
    }
    public static void main(String[] args) {
      RangesVariableType /*
      @
      (
      -
      2
      ,
      2
      )
      */r = new RangesVariableType();
      r.start();
    }
}
