// range checking for Prism

class RangeCheckPrism extends Thread {
    static int /*@(-10, 10)*/f = 0;

    public void run() {
      while(true)
        ++f;
    }
    public static void main(String[] args) {
      (new RangeCheckPrism()).start();
    }
}
