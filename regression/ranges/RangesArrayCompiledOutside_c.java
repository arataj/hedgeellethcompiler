package test.range;

// outside range in compiled code
// array's constant elements are optimized
class RangesArrayCompiledOutside_c extends Thread {
    int[] /*@(-10, 10)*/f;
    static int[] /*@(-5, 5)*/f2;

    public void run() {
      f[2] = f[1];
      f2[4] = f[2];
    }
    public static void main(String[] args) {
      int[] /*@(-10, 11)*/l = new int[10];
      RangesArrayCompiledOutside_c r = new RangesArrayCompiledOutside_c();
      l[1] = 11;
      r.f = l;
      r.f2 = r.f;
      r.start();
    }
}
