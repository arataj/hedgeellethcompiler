package indexing;

// proposals on indexed arrays
// index out of range
class IndexingOutOfRange1 extends Thread {
    int[] a = {-2, 3};
    int[] b = {3, 4, 6};

    public void run() {
      b[3] = a[2];
    }
    public static void main(String[] args) {
      (new IndexingOutOfRange1()).start();
    }
}
