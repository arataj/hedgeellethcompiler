// proposals on indexed arrays
class Indexing extends Thread {
    int[] a = {-2, 3};
    int[] b = {3, 4, 6};
    // static int[] c = {3};

    public void run() {
      b[2] = a[1];
      // c[0] = a[0] + b[0];
    }
    public static void main(String[] args) {
      (new Indexing()).start();
    }
}
