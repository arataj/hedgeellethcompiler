// outside primary range in interpreted code

class RangesExpressionInterpreted extends Thread {
    static int /*@(-4,4)*/f = -2;
    
    public void run() {
    }
    public static void main(String[] args) {
        int i = /* -6, -2 */(/*@(-2, 1)*/f + /*@(-20, -3)*/f);
        f = 2*i;
        (new RangesExpressionInterpreted()).start();
    }
}
                                  