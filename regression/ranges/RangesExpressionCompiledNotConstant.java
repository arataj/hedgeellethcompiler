// computing range of expression's result in compiled code,
// setting variable ranges on basis of proposals
//
// this class tests primary bounds not evaluable in
// range analysis

class RangesExpressionCompiledNotConstant extends Thread {
    int /*@(-4,4)*/f = -3; // -3, 2
    int /*@(-100, 100)*/g = 0; // -7, 0
    int j; // -42, 0
    
    public void run() {
        f = 2;
        g = -7;
        g = -4;
        j = /* -42, 0 */-(/*@(f - 1, f + 1)*/g*/*@(-6, -3)*/g);
    }
    public static void main(String[] args) {
        (new RangesExpressionCompiledNotConstant()).start();
    }
}
                                  