// ranges misplaced, cause name missing errors

class RangesNameMissing extends Thread {
    int[] /*@(-10, 10)*/f;
    static int /*@(-4,4)*/g;
    /*@(-1, 1)*/
    public void run() {
      /* @
        (
      -1,  
       1
       )*/g = /* test @(-3, 9) //(<-40, 20>) */((-/* //-20, 40 */
          (/*@(-2, 1)*/g*/*@(-20, -3)*/g)));
      /*@(-1, 1)*/f[2] = f[1];
    }
    public static void main(String[] args) {
      g = (/*@(-2, 1)*/g + /*@(-20, -3)*/g);
      int[] /*@(-10, 11)*/l = new int[10];
      /*@(-1, 1)*/RangesNameMissing r = new RangesNameMissing();
      l[1] = 11;
      r.f = l;
      r.start();
    }
}
