// range checking for Prism, no source level range
//
// in such cases, tha ranges are assigned not until
// the backend, thus, the optimizer can not take
// advantage of them

class RangeCheckPrismNoSourceLevel extends Thread {
    static int f = 0;

    public void run() {
      while(true)
        ++f;
    }
    public static void main(String[] args) {
      (new RangeCheckPrismNoSourceLevel()).start();
    }
}
