// outside range in compiled code

class RangesArrayCompiledOutside extends Thread {
    int[] /*@(-10, 10)*/f;
    static int[] /*@(-5, 5)*/f2;

    public void run() {
      f[2] = f[1];
      f2[4] = f[2];
    }
    public static void main(String[] args) {
      int[] /*@(-10, 11)*/l = new int[10];
      RangesArrayCompiledOutside r = new RangesArrayCompiledOutside();
      l[1] = 11;
      r.f = l;
      r.f2 = r.f;
      r.start();
    }
}
