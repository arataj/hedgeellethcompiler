package test;

class UnrollVariableRange extends Thread {
    private static final int FIELD = 2;
    
    public void run() {
        int/*@(0, 1)*/ r = 0;
        int/*@(0, r/2)*/ s = 0;
        for(int i = 0; i < 3; ++i) {
            int/*@(r, i)*/ j = i;
        }
    }
    public static void main(String[] args) {
        UnrollVariableRange single = new UnrollVariableRange();
        single.start();
    }
}
