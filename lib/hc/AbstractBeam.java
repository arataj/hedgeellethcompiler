/*
 * AbstractBeam.java
 *
 * Created on Jun 3, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package hc;

import java.util.*;

/**
 * <p>An abstract beam to be extended by the jvm and hc implementations.</p>
 * 
 * <p>The static hook functionality must be explicitly called by a subclass.</p>
 * 
 * @author Artur Rataj
 */
public class AbstractBeam extends Thread {
    final static int MAX_STATIC_HOOK_NUM = 5;
    /**
     * Static hooks for building state classes, so that the user does
     * not need to implement them.
     */
    static AbstractBeam[] staticHook = new AbstractBeam[MAX_STATIC_HOOK_NUM];
    static int staticHooksStored = 0;
    protected Random random = new Random();

    public AbstractBeam() {
        super();
    }
    /**
     * Hooks up to a given number of beams so that their state
     * variables are accesible by the state class. If there are no
     * more free slots available for a new hook, the user must
     * implement the respective functionality himself.
     * 
     * @param beam beam to statically attach
     */
    protected static void storeHook(AbstractBeam beam) {
        if(staticHooksStored < MAX_STATIC_HOOK_NUM) {
            staticHook[staticHooksStored] = beam;
            ++staticHooksStored;
        }
    }
}
