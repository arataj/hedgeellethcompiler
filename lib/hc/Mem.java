package hc;

/**
 * <p>Memory utilities for Java sources translated to an objective language.</p>
 *
 * <p>This class is <i>not for use with the automaton backend</i>, that is,
 * it can not be the part of <code>hc</code>'s model specification.</p>
 */ 
public class Mem {
  /**
   * Free an array of bytes. Does nothing in Java. Used when compiling to
   * a language without garbage collection.
   */ 
  public static void del(byte[] byteArray) {
    /* empty */
  }
  /**
   * Free an array of characters. Does nothing in Java. Used when compiling to
   * a language without garbage collection.
   */ 
  public static void del(char[] charArray) {
    /* empty */
  }
  /**
   * Free an array of bytes. Does nothing in Java. Used when compiling to
   * a language without garbage collection.
   */ 
  public static void del(int[] intArray) {
    /* empty */
  }
  /**
   * Free an array of long integers. Does nothing in Java. Used when
   * compiling to a language without garbage collection.
   */ 
  public static void del(long[] longArray) {
    /* empty */
  }
  /**
   * Free an array of floats. Does nothing in Java. Used when
   * compiling to a language without garbage collection.
   */ 
  public static void del(float[] floatArray) {
    /* empty */
  }
  /**
   * Free an array of doubles. Does nothing in Java. Used when
   * compiling to a language without garbage collection.
   */ 
  public static void del(double[] doubleArray) {
    /* empty */
  }
  /**
   * Free an object. Does nothing in Java. Used when compiling to
   * a language without garbage collection.
   */ 
  public static void del(Object object) {
    /* empty */
  }
  /**
   * Free an array of objects. Does nothing in Java. Used when compiling to
   * a language without garbage collection.
   */ 
  public static void del(Object[] objectArray) {
    /* empty */
  }
}
