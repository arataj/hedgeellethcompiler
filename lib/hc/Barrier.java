package hc;

/**
 * Provides methods for Prism--style barrier synchronisation.
 * Used by an application within a JDK.
 *
 * Created on Oct 2, 2012
 *
 * @author Artur Rataj
 *
 * @hcSkipRest()
 */
public class Barrier extends AbstractBarrier {
    private java.util.concurrent.CyclicBarrier barrier;
    
    /**
     * Instantiates a barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.
     * 
     * @param atomicSync if an atomic synchronisation is required
     */
    public Barrier(boolean atomicSync) {
        super(atomicSync);
        barrier = null;
        throw new RuntimeException("unimplemented");
    }
    /**
     * Instantiates a barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.
     * 
     * <p>Atomic synchronisation is true for the barrier.</p>
     */
    public Barrier() {
        super();
        barrier = null;
    }
    @Override
    public synchronized void activate(int usersNum) {
        if(this.usersNum != 0)
            throw new RuntimeException("already active");
        this.usersNum = usersNum;
        barrier = new java.util.concurrent.CyclicBarrier(this.usersNum);
        directional = false;
        notifyAll();
    }
    @Override
    public synchronized void activate(int producersNum, int consumersNum) {
        if(this.usersNum != 0)
            throw new RuntimeException("already active");
        this.usersNum = producersNum + consumersNum;
        this.producersNum = producersNum;
        this.consumersNum = consumersNum;
        barrier = new java.util.concurrent.CyclicBarrier(this.usersNum);
        directional = true;
        notifyAll();
    }
    /**
     * <p>Waits on this barrier, unless all its users are waiting.
     * If so, all these users are simultaneously released.</p>
     *
     * <p>Interruption of any thread within the threads waiting on this
     * barrier makes all of these threads interrupted and then this method
     * exits.</p>
     */
    @Override
    public void barrier() {
        if(directional)
            throw new RuntimeException("can not call barrier() if directional");
        synchronized(this) {
            while(usersNum == 0) {
                try { 
                    wait();
                } catch(InterruptedException e) {
                    /* empty */
                }
            }
        }
        try {
            barrier.await();
        } catch(InterruptedException e) {
            /* empty */
        } catch (java.util.concurrent.BrokenBarrierException e) {
            // if a barrier is broken, make all threads, that wait
            // on it, interrupted
            Thread.currentThread().interrupt();
        }
    }
    /**
     * <p>Waits on this barrier, unless all its users are waiting.
     * If so, users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     * 
     * <p>Interruption of any thread within the threads waiting on this
     * barrier makes all of these threads interrupted and then this method
     * exits.</p>
     *
     * @param closure closure, that allows for passing
     */
    @Override
    public void barrier(BooleanClosure closure) {
        if(directional)
            throw new RuntimeException("can not call barrier() if directional");
        do {
            barrier();
        } while(!Thread.currentThread().isInterrupted() &&
                !closure.booleanClosure());
    }
    /**
     *  <p>Waits on this barrier, until all consumers wait as well.</p>
     */
    @Override
    public void produce() {
    }
    /**
     * <p>Waits on this barrier, until all consumers wait as well.
     * If so, these of the waiting users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     */
    @Override
    public void produce(BooleanClosure closure) {
    }
    /**
     * <p>Waits on this barrier, until all producers and consumers wait as well.</p>
     * 
     * @return -1
     */
    @Override
    public int consume() {
        return -1;
    }
    /**
     * <p>Waits on this barrier, until all producers and consumers wait as well.
     * If so, these of the waiting users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     */
    @Override
    public void consume(BooleanClosure closure) {
    }
}
