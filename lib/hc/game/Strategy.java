package hc.game;

import java.util.Random;

/**
 * A wrapper of a nondeterministic branch, without a JVM equivalent.
 * A custom strategy can thus be written for JDK.<br>
 *
 * The presume is, that an instance of <code>Strategy</code> used
 * at several places should represent the same strategy each time.
 * Prism treats each instance of a non--deterministic branch as an
 * independent one, and so that the presume is not met in its case.
 * <br>
 *
 * This imlementation can be used on the JVM side, but then it only
 * returns choices using an uniform distribution as in the case
 * of <code>Random</code>.
 *
 * This class also allows for naming each branch, these names may then
 * end up as labels in the output model.
 * 
 * @hcSkipRest()
 */
public class Strategy {
    private Random random;
    /**
     * Names of subsequent choices.
     */
    public String[] CHOICES;
    
    /**
     * Creates a new strategy. Choices are not named. Must be run in the
     * main thread.
     */
    public Strategy() {
        random = new Random();
    }
    /**
     * Creates a new strategy. Must be run in the main thread. The names
     * make the compiler generate additional labeled transitions which in
     * turn may have a negative impact on state compaction, if any. You might
     * thus consider alternately using the MD file.
     * 
     * @param choices names of subsequent choices
     */
    public Strategy(String[] choices) {
        this();
        CHOICES = choices;
    }
    /**
     * Returns a choice, in the range 0 ... <code>numOutcomes</code> - 1.
     * <br>
     *
     * This implementation calls <code>Random.nextInt(numOutcomes)</code>.
     */
    public int choose(int numOutcomes) {
        return random.nextInt(numOutcomes);
    }
}
