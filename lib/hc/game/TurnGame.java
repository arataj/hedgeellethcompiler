package hc.game;

import hc.Barrier;

/**
 * An instance of this class represent a game, in which
 * only a single player can take a turn at once.
 */
public class TurnGame {
    /**
     * Common barrier for all players in this game.
     */
    protected final Barrier BARRIER = new Barrier();
    /**
     * Number of the player that has currently a turn.
     */
    private volatile int turn = 0;

    public TurnGame() {
    }
    /**
     * Starts this game.
     *
     * @param numOfPlayers total number of players
     */    
    public void start(int numOfPlayers) {
        BARRIER.activate(numOfPlayers);
    }
    /**
     * Sets the number of the player that is about to
     * move.
     */
    public void setTurn(int turn) {
        this.turn = turn;
    }
    /**
     * Returns a number of the player that currently
     * has a move.
     */
    public int getTurn() {
        return turn;
    }
}
