package hc.game;

import hc.BooleanClosure;

/**
 * Represents a single player thread, participating in a
 * <code>TurnGame</code>.
 */
abstract public class TurnPlayer extends Thread
        implements BooleanClosure {
    /**
     * A game, in which this player participates.
     */
    protected TurnGame table;
    /**
     * Number of this player.<br>
     *
     * Currently not final due to options used in
     * <code>CompilerTest.testFinalVariables()</code>.
     */
    private int playerNum;
    
    protected TurnPlayer(TurnGame table, int playerNum) {
        this.table = table;
        this.playerNum = playerNum;
    }
    /**
     * Returns the number of this player.
     */
    public final int getNum() {
        return playerNum;
    }
    public boolean booleanClosure() {
        return table.getTurn() == playerNum;
    }
    /**
     * Waits for this player's turn.
     */
    protected void turnWait() {
        table.BARRIER.barrier(this);
    }
    /**
     * Defines, which player should make a turn next.
     *
     * @param playerNum number of the next player
     */
    protected void turnNext(int playerNum) {
        table.setTurn(playerNum);
    }
}
