package hc;

/**
 * <p>This is hc's internal class, do not use in models.</p>
 *
 * <p>Changes to these constants must be reflected in the file
 * <code>verics/lang/Sleep.verics</code>.</p>
 *
 * @hcInternalConstants()
 */
public class SleepConstants {
    /**
     * <p>Sleep type Thread.sleep().</p>
     * 
     * <p>Internal constant, do not use.</p>
     */
    public static final int SLEEP_EXACT_MILLISECONDS = 0;
    /**
     * <p>Sleep type Sleep.exact().</p>
     * 
     * <p>Internal constant, do not use.</p>
     */
    public static final int SLEEP_EXACT_SECONDS = 1;
    /**
     * <p>Sleep type Sleep.min().</p>
     * 
     * <p>Internal constant, do not use.</p>
     */
    public static final int SLEEP_MIN_SECONDS = 2;
    /**
     * <p>Sleep type Sleep.max().</p>
     * 
     * <p>Internal constant, do not use.</p>
     */
    public static final int SLEEP_MAX_SECONDS = 3;
}
