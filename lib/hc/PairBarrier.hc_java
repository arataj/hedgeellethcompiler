package hc;

/**
 * Provides methods for barrier synchronisation, where it is
 * enough that only two threads met and that the condition is true
 * to release the barrier.
 *
 * Used by an application translated by Hedgeelleth.
 *
 * Created on Oct 2, 2012
 *
 * @author Artur Rataj
 */
public class PairBarrier extends AbstractBarrier {
    /**
     * Instantiates a pair barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.

     * @param atomicSync if an atomic synchronisation is required
     */
    public PairBarrier(boolean atomicSync) {
        super(atomicSync);
    }
    /**
     * Instantiates a pair barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.
     *
     * <p>Atomic synchronisation is true for the barrier.</p>
     */
    public PairBarrier() {
        super();
    }
    /**
     * Activates this barrier as a non--directional. Until the call
     * to this method, the barrier just stops a thread infinitely.<br>
     *
     * Should be called after all objects are created and after
     * all threads are started.
     *
     * @param usersNum number of threads, that use this barrier,
     * must be correct or the barrier will work incorrectly;
     * this value is verified by <code>Hedgeelleth</code>, but
     * is not verified in a JVM
     */
    @@MARKER @@BARRIER_ACTIVATE
    public void activate(int usersNum) {
    }
    /**
     * Activates this barrier as a directional. Until the call
     * to this method, the barrier just stops a thread infinitely.<br>
     *
     * Should be called after all objects are created and after
     * all threads are started.
     *
     * @param producersNum number of threads, that use this barrier
     * as producers; must be correct or the barrier will work
     * incorrectly; this value is verified by
     * <code>Hedgeelleth</code>, but is not verified in a JVM
     * @param consumersNum number of threads, that use this barrier
     * as consumers; must be correct or the barrier will work
     * incorrectly; this value is verified by
     * <code>Hedgeelleth</code>, but is not verified in a JVM
     */
    @@MARKER @@BARRIER_ACTIVATE
    public void activate(int producersNum, int consumersNum) {
    }
    /**
     * A conditional closure. It can not be used within Java sources,
     * as multiple evaluations of the argument would not be possible,
     * and thus, is used only internally by the compiler, as
     * a replacement for <code>barrier(BooleanClosure)</code>.
     *
     * @param condition pass condition
     */
    @@MARKER @@PAIR_BARRIER_CONDITIONAL
    private void barrier(boolean condition) {
    }
    /**
     * A conditional closure. It can not be used within Java sources,
     * as multiple evaluations of the argument would not be possible,
     * and thus, is used only internally by the compiler, as
     * a replacement for <code>produce(BooleanClosure)</code>.
     *
     * @param condition pass condition
     */
    @@MARKER @@PAIR_BARRIER_PRODUCE_CONDITIONAL
    private void produce(boolean condition) {
    }
    /**
     * A conditional closure. It can not be used within Java sources,
     * as multiple evaluations of the argument would not be possible,
     * and thus, is used only internally by the compiler, as
     * a replacement for <code>produce(BooleanClosure)</code>.
     *
     * @param condition pass condition
     */
    @@MARKER @@PAIR_BARRIER_CONSUME_CONDITIONAL
    private void consume(boolean condition) {
    }
    /**
     * <p>Waits on this barrier, unless another user waits as well.
     * If so, this and the other user are simultaneously released.</p>
     */
    @@MARKER @@PAIR_BARRIER_VOID
    public void barrier() {
    }
    /**
     * <p>Waits on this barrier, unless at least two of its users are waiting.
     * If so, these of the two users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     * 
     * Interruption of any thread within the threads waiting on this
     * barrier makes all of these threads interrupted and then this method
     * exits.
     *
     * @param closure closure, that allows for passing
     */
    @@MARKER @@PAIR_BARRIER_CLOSURE
    public void barrier(BooleanClosure closure) {
    }
    /**
     *  <p>Waits on this barrier, until all consumers wait as well.</p>
     */
    @@MARKER @@PAIR_BARRIER_PRODUCE_VOID
    public void produce() {
    }
    /**
     * <p>Waits on this barrier, until all consumers wait as well.
     * If so, these of the waiting users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     */
    @@MARKER @@PAIR_BARRIER_PRODUCE_CLOSURE
    public void produce(BooleanClosure closure) {
    }
    /**
     *  <p>Waits on this barrier, until all consumers wait as well.</p>
     * 
     * @param w a value to pass to <code>int consume()</code>
     */
    @@MARKER @@PAIR_BARRIER_PRODUCE_VOID
    public void produce(int w) {
    }
    /**
     * <p>Waits on this barrier, until at least a single producer and all consumers wait as well.</p>
     *
     * @return -1, or <code>w</code> if synchronised with <code>produce(int w)</code>
     */
    @@MARKER @@PAIR_BARRIER_CONSUME_VOID
    public int consume() {
        return -1;
    }
    /**
     * <p>Waits on this barrier, until a chosen producer and all consumers wait as well.</p>
     * 
     * @param w synchronise only with <code>produce(int w)</code>
     */
    @@MARKER @@PAIR_BARRIER_CONSUME_VOID_PRODUCER
    public void consume(int w) {
    }
    /**
     * <p>Waits on this barrier, until at least a single producer and all consumers wait as well.</p>
     * If so, these of the waiting users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     */
    @@MARKER @@PAIR_BARRIER_CONSUME_CLOSURE
    public void consume(BooleanClosure closure) {
    }
}
