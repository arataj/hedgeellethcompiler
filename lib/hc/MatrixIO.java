/* @hcSkipRest() */
package hc;

import java.io.*;

/**
 * A class for reading or writing matrices from/to
 * text files. Row per line, whitespace-- or
 * comma--separated. It differs from <code>TuplesIO</code>
 * only in requiring, that each tuple is of an equal
 * length.
 */
public class MatrixIO extends TuplesIO {
    /**
     * Number of column in this matrix, -1 for not yet
     * known.
     */
    int columnsNum;
    
    /**
     * Opens a file, for either writing or reading.
     */
    public MatrixIO(String fileName) {
        super(fileName);
        columnsNum = -1;
    }
    /**
     * Reads a subsequent row. After it is called,
     * <code>addNext()</code> can not be called
     * on this object.
     *
     * @return array of the size of the matrix'
     * number of columns or null for end--of--file
     */
    @Override
    public double[] getNext() throws IOException {
        double[] row = super.getNext();
        if(row != null) {
            if(columnsNum == -1)
                columnsNum = row.length;
            else if(row.length != columnsNum)
                throw new IOException("file " + fileName +
                        ", line " + getLineIndex() + ": number of columns is " +
                        row.length + ", was " + columnsNum);
        }
        return row;
    }
    /**
     * Writes a subsequent row. After it is called,
     * <code>getNext()</code> can not be called
     * on this object.
     *
     * @param row array of the size of the matrix'
     * number of columns
     */
    @Override
    public void addNext(double[] row) throws IOException {
        if(columnsNum == -1)
            columnsNum = row.length;
        else if(row.length != columnsNum)
            throw new IOException("file " + fileName +
                    ", line " + getLineIndex() + ": number of colums is " +
                    row.length + ", was " + columnsNum);
        super.addNext(row);
    }
    @Override
    public String toString() {
        return "MatrixIO(" + getStringDescription() + ")";
    }
}
