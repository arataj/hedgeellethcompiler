/* @hcSkipRest() */
package hc;

/**
 * \code{hc}--specific sleep methods, that have a double type argument.<br>
 *
 * The sleep methods, together with distributions, can be used
 * to define probabilistic time invariants.
 */
public class Sleep {
    /**
     * Delays for an exact time. Similar to <code>Thread.sleep</code>,
     * but the time unit is seconds and not milliseconds.
     *
     * @param seconds time in seconds; in the case of JDK mutliplied by 1000,
     * rounded to integers and passed to <code>Thread.sleep()</code>
     */
    public static void exact(double seconds) {
        try {
            Thread.sleep((int)Math.round(seconds*1000.0));
        } catch(InterruptedException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    /**
     * Delays for at least a given time.
     *
     * @param seconds time in seconds
     */
    public static void min(double seconds) {
        throw new UnsupportedOperationException("not supported on the JDK side");
    }
    /**
     * Delays for at most a given time.
     *
     * @param seconds time in seconds
     */
    public static void max(double seconds) {
        throw new UnsupportedOperationException("not supported on the JDK side");
    }
}
