package hc;

/**
 * An interface of a boolean closure.
 *
 * @hcSkipRest()
 */
public interface BooleanClosure {
    /**
     * Returns a boolean value.
     *
     * @return boolean value, computed
     * by an implementing class
     */
    public boolean booleanClosure();
}
