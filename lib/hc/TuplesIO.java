/* @hcSkipRest() */
package hc;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.compiler.util.ArrayUtils;

/**
 * A class for reading or writing sequences of tuples from/to
 * text files. A tuple per line, whitespace-- or comma--separated.
 */
public class TuplesIO {
    /**
     * Name of the file accessed, as in the constructor's parameter.
     */
    protected String fileName;
    /**
     * File either read or written to, but not read/write.
     */
    protected File file;
    /**
     *  Scanner, not null only if the file is read.
     */
    private Scanner in;
    /**
     *  Writer, not null only if the file is written to.
     */
    private PrintWriter out;
    /**
     * Index of the tuple to be read or written, since 1.
     * 0 if no tuple has been accessed yet.
     */
    private int tupleCount;
    
    /**
     * Opens a file, for either writing or reading.
     */
    public TuplesIO(String fileName) {
        this.fileName = fileName;
        file = new File(this.fileName);
        tupleCount = 0;
    }
    /**
     * Returns the number of the last tuple, 0 if
     * no tuple has been read or written yet.
     *
     * @return 0 or line number
     */
    public int getLineIndex() {
        return tupleCount;
    }
    /**
     * Reads a subsequent tuple. After it is called,
     * <code>addNext()</code> can not be called
     * on this object.
     *
     * @return array of the size of the tuple length
     * or null for end--of--file
     */
    public double[] getNext() throws IOException {
        if(out != null)
            throw new IOException("file " + fileName +
                    " already open for writing");
        if(in == null) {
            if(!file.exists())
                throw new IOException("file " + fileName +
                    " not found");
            in = new Scanner(file).useLocale(Locale.ROOT);
        }
        if(in.hasNextLine()) {
            if(tupleCount == Integer.MAX_VALUE)
                throw new IOException("number of lines in " +
                        fileName + " too large");
            ++tupleCount;
            String line = in.nextLine();
            Scanner sc = new Scanner(line).
                    useLocale(Locale.ROOT).
                    useDelimiter("[\\s,]+");
            LinkedList<Double> values = new LinkedList<>();
            while(sc.hasNext()) {
                double v;
                try {
                    v = sc.nextDouble();
                } catch(InputMismatchException e) {
                    throw new IOException("file " + fileName +
                            ", line " + tupleCount + ": number expected");
                }
                values.add(v);
            }
            return ArrayUtils.convertDouble(values);
        } else
            return null;
    }
    /**
     * A convenience wrapper to <code>getNext()</code>, that
     * throws a runtime exception if the tuple to read is not
     * found. To be used with matrices whose number of rows
     * is known a priori.
     *
     * @return array of the size of the tuple length
     */
    public double[] getNextNotNull() throws IOException {
        double[] out = getNext();
        if(out == null)
            throw new RuntimeException("file " + fileName +
                ", line " + tupleCount + ": tuple not found");
        return out;
    }
    /**
     * Writes a subsequent tuple. After it is called,
     * <code>getNext()</code> can not be called
     * on this object.
     *
     * @param row array of the size of the matrix'
     * number of columns
     */
    public void addNext(double[] row) throws IOException {
        if(in != null)
            throw new IOException("file " + fileName +
                    " already open for reading");
        if(out == null)
            out = new PrintWriter(file);
        StringBuilder line = new StringBuilder();
        for(int i = 0; i < row.length; ++i) {
            if(i != 0)
                line.append(" ");
            line.append(row[i]);
        }
        out.println(line);
        if(out.checkError()) {
            out.close();
            throw new IOException("error writing to file " +
                    fileName);
        }
        if(tupleCount == Integer.MAX_VALUE)
            throw new IOException("number of lines in " +
                    fileName + " too large");
        ++tupleCount;
    }
    /**
     * Closes this object, and also any underlying
     * i/o stream.
     */
    public void close() throws IOException {
        if(in != null)
            in.close();
        if(out != null) {
            if(out.checkError()) {
                out.close();
                throw new IOException("error writing to file " +
                        fileName);
            }
            out.close();
        }
    }
    /**
     * Returns a textual description of this string, including
     * file name and, if the file has alread been accessed, then
     * the access type.
     * 
     * @return a textual description
     */
    protected String getStringDescription() {
        String s = fileName;
        if(in != null)
            s += " read";
        else if(out != null)
            s += " write";
        return s;
    }
    @Override
    public String toString() {
        return "TuplesIO(" + getStringDescription() + ")";
    }
}
