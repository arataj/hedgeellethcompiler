package hc;

/**
 * Provides methods for barrier synchronisation, where it is
 * enough that only two threads met and that the condition is true
 * to release the barrier.
 *
 * Used by an application within a JDK.
 *
 * Created on Oct 2, 2012
 *
 * @author Artur Rataj
 *
 * @hcSkipRest()
 */
public class PairBarrier extends AbstractBarrier {
    private java.util.concurrent.CyclicBarrier barrier;
    
    /**
     * Instantiates a pair barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.
     * 
     * @param atomicSync if an atomic synchronisation is required
     */
    public PairBarrier(boolean atomicSync) {
        super(atomicSync);
        barrier = null;
        throw new RuntimeException("unimplemented");
    }
    /**
     * Instantiates a pair barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.
     * 
     * <p>Atomic synchronisation is true for the barrier.</p>
     */
    public PairBarrier() {
        super();
        barrier = null;
        throw new RuntimeException("unimplemented");
    }
    @Override
    public synchronized void activate(int usersNum) {
        if(this.usersNum != 0)
            throw new RuntimeException("already active");
        this.usersNum = usersNum;
        barrier = new java.util.concurrent.CyclicBarrier(2);
        directional = false;
        notifyAll();
    }
    @Override
    public synchronized void activate(int producersNum, int consumersNum) {
        if(this.usersNum != 0)
            throw new RuntimeException("already active");
        this.usersNum = producersNum + consumersNum;
        this.producersNum = producersNum;
        this.consumersNum = consumersNum;
        barrier = new java.util.concurrent.CyclicBarrier(2);
        directional = true;
        notifyAll();
    }
    /**
     * <p>Waits on this barrier, unless another user waits as well.
     * If so, this and the other user are simultaneously released.</p>
     *
     * <p>Interruption of any thread within the threads waiting on this
     * barrier makes all of these threads interrupted and then this method
     * exits.</p>
     */
    @Override
    public void barrier() {
        if(directional)
            throw new RuntimeException("can not call barrier() if directional");
        synchronized(this) {
            while(usersNum == 0) {
                try { 
                    wait();
                } catch(InterruptedException e) {
                    /* empty */
                }
            }
        }
        try {
            barrier.await();
        } catch(InterruptedException e) {
            /* empty */
        } catch (java.util.concurrent.BrokenBarrierException e) {
            // if a barrier is broken, make all threads, that wait
            // on it, interrupted
            Thread.currentThread().interrupt();
        }
    }
    /**
     * <p>Waits on this barrier, unless at least two of its users are waiting.
     * If so, these of the two users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     * 
     * <p>Interruption of any thread within the threads waiting on this
     * barrier makes all of these threads interrupted and then this method
     * exits.</p>
     *
     * @param closure closure, that allows for passing
     */
    @Override
    public void barrier(BooleanClosure closure) {
        if(directional)
            throw new RuntimeException("can not call barrier() if directional");
        do {
            barrier();
        } while(!Thread.currentThread().isInterrupted() &&
                !closure.booleanClosure());
    }
    /**
     *  <p>Waits on this barrier, until all consumers wait as well.</p>
     */
    @Override
    public void produce() {
    }
    /**
     * <p>Waits on this barrier, until all consumers wait as well.
     * If so, these of the waiting users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     */
    @Override
    public void produce(BooleanClosure closure) {
    }
    /**
     *  <p>Waits on this barrier, until all consumers wait as well.</p>
     * 
     * @param w a value to pass to <code>int consume()</code>
     */
    public void produce(int w) {
    }
    /**
     * <p>Waits on this barrier, until at least a single producer and all consumers wait as well.</p>
     * 
     * @return -1, or <code>w</code> if synchronised with <code>produce(int w)</code>
     */
    @Override
    public int consume() {
        return -1;
    }
    /**
     * <p>Waits on this barrier, until a chosen producer and all consumers wait as well.</p>
     * 
     * @param w synchronise only with <code>produce(int w)</code>
     */
    public void consume(int w) {
    }
    /**
     * <p>Waits on this barrier, until at least a single producer and all consumers wait as well.</p>
     * If so, these of the waiting users, for which the closure returns true are released,
     * and the rest re--enters the barrier.</p>
     */
    @Override
    public void consume(BooleanClosure closure) {
    }
}
