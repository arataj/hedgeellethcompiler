package hc;

import java.util.*;

/**
 * Contains functions for controlling the output model file.
 *
 * @hcSkipRest()
 */
public class Model {
    /**
     * System properties.
     */
    private final static Properties systemProperties =
            System.getProperties();
    /**
     * Prefix of names of system properties related to the
     * hc compiler.
     */
    private final static String HC_PREFIX = "hc.";
    /**
     * Prefix of names of system properties that define external
     * constants.
     */
    private final  static String HC_PREFIX_CONST = HC_PREFIX + "const.";
    /**
     * Returned by <code>isStatistical()</code>.
     */
    private final static boolean isStatisticalHint;
    private final static Map<String, Double> externalConstants =
            new HashMap<>();
    static {
         String isStatisticalString = systemProperties.getProperty(
                 HC_PREFIX + "statistical", "true");
         if(isStatisticalString.equals("true"))
             isStatisticalHint = true;
         else if(isStatisticalString.equals("false"))
             isStatisticalHint = false;
         else
             throw new RuntimeException("system property " +
                     HC_PREFIX + "statistical has invalid value: `" +
                     isStatisticalString + "'");
// System.out.println("isStatisticalHint = " + isStatisticalHint);
        Enumeration<String> names = (Enumeration<String>)
                systemProperties.propertyNames();
        while(names.hasMoreElements()) {
            String name = names.nextElement();
            if(name.startsWith(HC_PREFIX_CONST)) {
                String valueString = systemProperties.getProperty(name, null);
                name = name.substring(HC_PREFIX_CONST.length());
                try {
                    double value = Double.parseDouble(valueString);
                    externalConstants.put(name, value);
// System.out.println("external constant " + name + " = " + value);
                } catch(NumberFormatException e) {
                    System.out.println("external constant " + name +
                            " has invalid value: `" + valueString + "'");
                    System.exit(1);
                }
            }
        }
    }
    /**
     * If the simulation is finished.
     */
    private static boolean finished = false;
    /**
     * Lock, notified within <code>finish()</code>.
     */
    private static Object finishedLock = new Object();
    
    /**
     * Defines a custom naming convention for all fields of
     * a given object.
     *
     * @param object object, for which to define the convention
     * @param prefix with what a field's name should be prefixed,
     * empty string for nothing
     * @param suffix what to append to a field's name, empty
     * string for nothing
     */
    public static void name(Object object, String prefix,
            String suffix) {
    }
    /**
     * Defines a custom naming convention for all fields of
     * a given object.<br>
     *
     * This is a convenience method.
     *
     * @param object object, for which to define the convention
     * @param suffix what to append to a field's name, empty
     * string for nothing
     */
    public static void name(Object object, String suffix) {
        name(object, "", suffix);
    }
    /**
     * Defines a custom naming convention for all fields of
     * a given object. In this convention, field names are
     * used as they are.<br>
     *
     * This is a convenience method.
     *
     * @param object object, for which to define the convention
     */
    public static void name(Object object) {
        name(object, "", "");
    }
    /**
     * Adds a property to check.
     *
     * @param name optional name of the property, null for none
     * @param property a string that represents the property; its format
     * is dependent on the model checker, for which the model
     * is generated
     */
    public static void check(String name, String property) {
    }
    /**
     * Adds a property to check, without any name.<br>
     *
     * This is a convenience method.
     *
     * @param property a string that represents the property; its format
     * is dependent on the model checker, for which the model
     * is generated
     */
    public static void check(String property) {
        check(null, property);
    }
    /**
     * Defines a state for a state formula. If there are several
     * states of a given name, they are combined using a boolean
     * OR operator.
     *
     * @param name of a state formula
     */
    public static void stateOr(String name) {
    }
    /**
     * Defines a state for a state formula. If there are several
     * states of a given name, they are combined using a boolean
     * AND operator.
     *
     * @param name of a state formula
     */
    public static void stateAnd(String name) {
    }
    /**
     * Adds a thread to a player.
     *
     * @param thread the thread, which is a part of the player
     * @param name name of the player
     */
    public static void player(Thread thread, String name) {
    }
    /**
     * Returns a hint, if this model is to be checked by an
     * analytic engine, or by a statistical engine.<br>
     *
     * What this method returns is controlled by \code{hc}'s
     * command--line option and by JDK's system property
     * \code{hc.statistical}.
     *
     * @return false if analytic, true if statistical; the
     * default is false for \code{hc}, true for JDK
     */
    public static boolean isStatistical() {
        return isStatisticalHint;
    }
    /**
     * Returns an integer constant. In the case of hc, the
     * constant may but does not need to be defined at
     * the command line, and also an integer set may be
     * defined to generate expriments; if not defined, a
     * meaningless value is returned. In the case of JVM,
     * the constant must be defined at JVM's command line,
     * or a runtime exception is thrown.
     *
     * @param name the constant's name
     * @return constant integer value
     */
    public static int intConst(String name) {
        Double d = externalConstants.get(name);
        if(d == null)
            throw new RuntimeException("external constant " + name + " undefined");
        if(Math.round(d) != d)
            throw new RuntimeException("external integer constant " + name +
                    " does not have an integer value: " + d);
        if(d < Integer.MIN_VALUE || d > Integer.MAX_VALUE)
            throw new RuntimeException("external integer constant " + name +
                    " does not fit into type int: " + (long)(double)d);
        return (int)(double)d;
    }
    /**
     * Returns a double constant. In the case of hc, the
     * constant may but does not need to be defined at
     * the command line; if not defined, a meaningless
     * value is returned. In the case of JVM, the constant
     * must be defined at JVM's command line, or a runtime
     * exception is thrown.
     *
     * @param name the constant's name
     * @return constant double value
     */
    public static double doubleConst(String name) {
        Double d = externalConstants.get(name);
        if(d == null)
            throw new RuntimeException("external constant " + name + " undefined");
        return d;
    }
    /**
     * Called by an automaton thread when nothing is left to do
     * within the simulation, but possibly waiting on a lock or
     * barrier, or exiting the <code>run()</code> method.
     */
    public static void finish() {
        synchronized(finishedLock) {
            finished = true;
            finishedLock.notify();
        }
    }
    /**
     * Main thread waits on this statement, if there were not any
     * calls to <code>finish()</code> yet. Automaton
     * threads may still be alive after this method exits,
     * although they should not alter any variable, as the main
     * thread may already gather statistics after this method
     * does not block.
     */
    public static void waitFinish() {
        synchronized(finishedLock) {
            while(!finished)
                try {
                    finishedLock.wait();
                } catch(InterruptedException e) {
                    /* empty */
                }
        }
    }
}
