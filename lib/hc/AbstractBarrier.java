/*
 * AbstractBarrier.java
 *
 * Created on Nov 21, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package hc;

/**
 * <p>An abstract barrier.</p>
 * 
 * @author Artur Rataj
 */
public abstract class AbstractBarrier {
    /**
     * <p>Number of threads, that use this barrier; 0 if the
     * barrier is inactive.</p>
     */
    protected int usersNum;
    /**
     * <p>Number of producer threads, that use this barrier. Non--zero
     * only for directional barriers.</p>
     */
    protected int producersNum;
    /**
     * <p>Number of consumer threads, that use this barrier. Non--zero
     * only for directional barriers.</p>
     */
    protected int consumersNum;
    /**
     * <p>If this barrier is conditional. Decided after a call to <code>activate()</code>.</p>
     * 
     * <p>if false, barrier() can be called, if true, produce() and consume() can be called.</p>
     */
    boolean directional;
    /**
     * <p>If a condition in its entirety and a synchronisation must be put into a common
     * transition. In the case of a directional conditonal barrier, there can be more of such
     * transitions.</p>
     * 
     * <p>The default is true, the user may cancel this constrain, if e.g. the optimizer won't
     * be able to fullfill it.</p>
     */
    final boolean atomicSync;
    
    /**
     * Instantiates a barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.
     * 
     * @param atomicSync if an atomic synchronisation is required
     */
    public AbstractBarrier(boolean atomicSync) {
        usersNum = 0;
        this.atomicSync = atomicSync;
    }
    /**
     * Instantiates a barrier. The barrier is created inactive. To be
     * activated, <code>activate()</code> must be called.
     * 
     * <p>Atomic synchronisation is true for the barrier.</p>
     */
    public AbstractBarrier() {
        this(true);
    }
    /**
     * Activates this barrier as a non--directional. Until the call
     * to this method, the barrier just stops a thread infinitely.<br>
     *
     * Should be called after all objects are created and after
     * all threads are started.
     *
     * @param usersNum number of threads, that use this barrier,
     * must be correct or the barrier will work incorrectly;
     * this value is verified by <code>Hedgeelleth</code>, but
     * is not verified in a JVM
     */
    public abstract void activate(int usersNum);
    /**
     * Activates this barrier as a directional. Until the call
     * to this method, the barrier just stops a thread infinitely.<br>
     *
     * Should be called after all objects are created and after
     * all threads are started.
     *
     * @param producersNum number of threads, that use this barrier
     * as producers; must be correct or the barrier will work
     * incorrectly; this value is verified by
     * <code>Hedgeelleth</code>, but is not verified in a JVM
     * @param consumersNum number of threads, that use this barrier
     * as consumers; must be correct or the barrier will work
     * incorrectly; this value is verified by
     * <code>Hedgeelleth</code>, but is not verified in a JVM
     */
    public abstract void activate(int producersNum, int consumersNum);
    /**
     * <p>A symmetric waiting, without a closure. See
     * a subclass for specific behaviour.</p>
     */
    public abstract void barrier();
    /**
     * <p>A symmetric waiting, without a closure. See
     * a subclass for specific behaviour.</p>
     *
     * @param closure closure, that allows for passing
     */
    public abstract void barrier(BooleanClosure closure);
    /**
     * <p>An asymmetric waiting, producer side, without
     * a closure. See a subclass for specific behaviour.</p>
     */
    public abstract void produce();
    /**
     * <p>An asymmetric waiting, producer side, without
     * a closure. See a subclass for specific behaviour.</p>
     *
     * @param closure closure, that allows for passing
     */
    public abstract void produce(BooleanClosure closure);
    /**
     * <p>An asymmetric waiting, consumer side, without
     * a closure. See a subclass for specific behaviour.</p>
     * 
     * @return -1, or <code>w</code> if synchronised with
     * <code>PairBarrier.produce(int w)</code>
     */
    public abstract int consume();
    /**
     * <p>An asymmetric waiting, consumer side, without
     * a closure. See a subclass for specific behaviour.</p>
     *
     * @param closure closure, that allows for passing
     */
    public abstract void consume(BooleanClosure closure);
}
