/*
 * Beam.java
 *
 * Created on Jun 2, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */
/* @hcSkipRest() */
package hc;

/**
 * A variant of <code>AbstractBeam</code> for Java--side simulation.
 * 
 * @author Artur Rataj
 */
public class Beam extends AbstractBeam {
    public Beam() {
        super();
        storeHookSyncWrapper(this);
    }
    /**
     * <code>hc</code> does not implement a static synchronized method,
     * and thus this wrapped is required, as threads in </code>jvm</code>
     * can actually execute in parallel.
     * 
     * @param beam beam to statically attach
     */
    private static synchronized void storeHookSyncWrapper(AbstractBeam beam) {
        storeHook(beam);
    }
    public int unknown(int bound) {
        return random.nextInt(bound);
    }
    public int unknown(int bound, int v1) {
        return random.nextInt(bound);
    }
    public int unknown(int bound, int v1, int v2) {
        return random.nextInt(bound);
    }
    public int unknown(int bound, int v1, int v2, int v3) {
        return random.nextInt(bound);
    }
    public int unknown(int bound, int v1, int v2, int v3, int v4) {
        return random.nextInt(bound);
    }
    protected void init() {
    }
    protected void step() {
    }
    protected void done() {
    }
}
