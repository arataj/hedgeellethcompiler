package hc;

/**
 * Extend to put static optimisation parameters.
 */
public abstract class Parameters_ {
    public Parameters_() {
        throw new RuntimeException("not to be instantiated");
    }
}
