/* @hcSkipRest() */
package hc;

/**
 * Probabilistic distributions. A regular distribution, as it
 * is provided by the Java subset, is at <code>Math.random()</code>.
 */
public class Dist {
    /**
     * Draws a value from a negative exponential distribution.
     *
     * @param lambda the distribution's lambda coefficient
     */
    public static final double/*@(0, 1.0/0.0)*/ nxp(double lambda) {
        return Math.log(1.0 - Math.random())/-lambda;
    }
    /**
     * Draws a value from a distribution defines by arrays. The
     * points defined by the arrays are interpolated using a linear
     * approximation.
     *
     * @param x subsequent values, sorted from minimum to maximum
     * @param y subsequent densities
     */
    public static final double/*@(0, 1.0/0.0)*/ tab(double[] x, double[] y) {
        throw new UnsupportedOperationException("not supported on the JDK side");
    }
}
