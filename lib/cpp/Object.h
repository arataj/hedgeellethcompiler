#ifndef HC_OBJECT_H
#define HC_OBJECT_H

namespace hc {
  class Object {
    public:
      /**
       * Creates a new instance of <code>Object</code>.
       */
      Object();
      virtual ~Object();
      /**
       * Checks for equality between two objects.
       */
      bool equals(Object* other);
  };
}

#endif
