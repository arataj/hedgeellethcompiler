#include "Object.h"

namespace hc {
  Object::Object() {
    /* empty */
  }
  Object::~Object() {
    /* empty */
  }
  bool Object::equals(Object* other) {
    return this == other;
  }
}
