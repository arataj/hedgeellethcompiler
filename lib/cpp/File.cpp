#include "File.h"

#include <stdio.h>
#include <unistd.h>
                     
namespace hc {
    File::File(string fileName) {
        this->fileName = fileName;
    }
    File::~File() {
        /* empty */
    }  
    bool File::exists() {
        return access(fileName.c_str(), F_OK) != -1;
    }
    bool File::remove() {
        return ::remove(fileName.c_str()) != -1;
    }
}
