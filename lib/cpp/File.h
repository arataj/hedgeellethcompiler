#ifndef HC_FILE_H
#define HC_FILE_H

#include <cstdio>

#include "Object.h"
#include "StringTools.h"

namespace hc {
  class File : public Object {
    public:
      /**
       * Name of the file.
       */
      string fileName;
      
    public:
      /**
       * Associates a name with a a file.
       *
       * @param fileName name of the file
       */
      File(string fileName);
      virtual ~File();
      /**
       * Returns if the file exists.
       *
       * @return if the file has been found
       */
      bool exists();
      /**
       * Deletes the file. Renamed from <code>delete</code> in the
       * Java counterpart, because <code>delete</code> is a C++ keyword.
       *
       * @return if the file has actually been deleted
       */
      bool remove();
  };
}

#endif
