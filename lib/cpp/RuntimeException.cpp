#include "RuntimeException.h"

namespace hc {
  RuntimeException::RuntimeException(string text) {
    message = text;
  }
  RuntimeException::~RuntimeException() {
    /* empty */
  }
  string RuntimeException::getMessage() {
    return message;
  }
}
