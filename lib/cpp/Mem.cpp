#include "Mem.h"

#include <cstring>

namespace hc {
  void Mem::copy(void* from, void* to, int length) {
    std::memmove(to, from, length);
  }
  void Mem::del(Object* object) {
    delete object;
  }
  void Mem::del(signed char* byteArray) {
    delete[] byteArray;
  }
  void Mem::del(unsigned short* charArray) {
    delete[] charArray;
  }
  void Mem::del(signed int* intArray) {
    delete[] intArray;
  }
  void Mem::del(signed long* longArray) {
    delete[] longArray;
  }
  void Mem::del(float* floatArray) {
    delete[] floatArray;
  }
  void Mem::del(double* doubleArray) {
    delete[] doubleArray;
  }
  void Mem::del(Object** objectArray) {
    delete[] objectArray;
  }
}
