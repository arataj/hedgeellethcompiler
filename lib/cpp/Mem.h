#ifndef HC_MEM_H
#define HC_MEM_H

#include "Object.h"

namespace hc {
  /**
   * Memory utility methods to be used in the code generated
   * by Hedgeelleth.
   */
  class Mem : public Object {
    public:
      /**
       * Calls <code>std::memmove(to, from, length)</code>.
       *
       * @param from a region from which to copy
       * @param to a region to which to copy
       * @param length size of the copied region in bytes
       */
      static void copy(void* from, void* to, int length);
      /**
       * Deletes an object.
       *
       * @param object the object to delete
       */
      static void del(Object* object);
      /**
       * Deletes an array of bytes.
       *
       * @param byteArray the array to delete
       */
      static void del(signed char* byteArray);
      /**
       * Deletes an array of characters.
       *
       * @param charArray the array to delete
       */
      static void del(unsigned short* charArray);
      /**
       * Deletes an array of integers.
       *
       * @param intArray the array to delete
       */
      static void del(signed int* intArray);
      /**
       * Deletes an array of long integers.
       *
       * @param longArray the array to delete
       */
      static void del(signed long* longArray);
      /**
       * Deletes an array of floats.
       *
       * @param floatArray the array to delete
       */
      static void del(float* floatArray);
      /**
       * Deletes an array of doubles.
       *
       * @param doubleArray the array to delete
       */
      static void del(double* doubleArray);
      /**
       * Deletes an array of objects.
       *
       * @param objectArray the array to delete
       */
      static void del(Object** objectArray);
  };
}

#endif
