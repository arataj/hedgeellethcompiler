#ifndef HC_RANDOM_ACCESS_FILE_H
#define HC_RANDOM_ACCESS_FILE_H

#include <cstdio>

#include "Object.h"
#include "File.h"
#include "RuntimeException.h"

namespace hc {
  class RandomAccessFile : public Object {
    protected:
      /**
       * Name of the file, retained to have the possibility
       * of finding its size.
       */
      string fileName;
      /**
       * Descriptor of the file.
       */
      FILE* handle;
      
    public:
      /**
       * Opens a file.
       *
       * @param file file to open
       * @param mode: "r" or "rw" 
       */
      RandomAccessFile(File* file, string mode);
      /**
       * Opens a file.
       *
       * @param fileName name of the file
       * @param mode: "r" or "rw" 
       */
      RandomAccessFile(string fileName, string mode);
      virtual ~RandomAccessFile();
      /**
       * Closes the file. Frees all resources connected with this
       * object. No method should be called on it after this
       * method is called.
       */
      void close();
      /**
       * Reads a byte from the file.
       *
       * @return byte read
       */
      int read();
      /**
       * Reads a number of bytes from a file to a fragment of
       * a byte array. If even not a single byte is available,
       * an exception is thrown. 
       *
       * @param buffer buffer to store the bytes read
       * @param offset index of the first element in the buffer
       * to be written to 
       * @param length number of the bytes to read
       * @return number of bytes that were actually read
       */
      int read(signed char* buffer, int offset, int length);
      /**
       * Writes a byte to the file.
       *
       * @param b byte to write
       */
      void write(int b);
      /**
       * Writes a fragment of a byte array to a file.
       *
       * @param buffer buffer containing the bytes to write
       * @param offset index of the first byte to write in the
       * buffer 
       * @param length number of the bytes to write
       */
      void write(signed char* buffer, int offset, int length);
      /**
       * Returns the length of this file.
       *
       * @return size in bytes
       */
      long length();
      /**
       * Sets the position in the file for reading or writing.
       */
      void seek(long pos);
      
    private:
      /**
       * A common initialization routine.
       */
      void init(string mode);
  };
}

#endif
