#include <iostream>
#include <sstream>

using std::stringstream;

#include "StringTools.h"

namespace hc {
  string StringTools::toString(signed int i) {
    stringstream s;
    s << i;
    return (s.str());
  }
  string StringTools::toString(signed long l) {
    stringstream s;
    s << l;
    return (s.str());
  }
  string StringTools::toString(float f) {
    stringstream s;
    s << f;
    return (s.str());
  }
  string StringTools::toString(double d) {
    stringstream s;
    s << d;
    return (s.str());
  }
}
