#ifndef HC_RUNTIME_EXCEPTION_H
#define HC_RUNTIME_EXCEPTION_H

#include "Object.h"
#include "StringTools.h"

namespace hc {
  class RuntimeException : public Object {
    protected:
      string message;
      
    public:
      /**
       * Creates a new instance of <code>RuntimeException</code>.
       */
      RuntimeException(string text);
      virtual ~RuntimeException();
      /**
       * Returns this exception's message.
       */
      string getMessage();
  };
}

#endif
