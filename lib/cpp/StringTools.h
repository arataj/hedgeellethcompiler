#ifndef HC_STRING_TOOLS_H
#define HC_STRING_TOOLS_H

#include <string>

using std::string;

namespace hc {
  class StringTools {
    public:
      static string toString(signed int i);
      static string toString(signed long l);
      static string toString(float f);
      static string toString(double d);
  };
}

#endif
