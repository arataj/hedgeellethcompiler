#include "RandomAccessFile.h"

#include <sys/types.h>
#include <sys/stat.h>
                     
namespace hc {
  RandomAccessFile::RandomAccessFile(File* file, string mode) {
    this->fileName = file->fileName;
    init(mode);
  }
  RandomAccessFile::RandomAccessFile(string fileName, string mode) {
    this->fileName = fileName;
    init(mode);
  }
  RandomAccessFile::~RandomAccessFile() {
    /* empty */
  }
  void RandomAccessFile::init(string mode) {
    const char* s;
    if(mode == "r")
      s = "r";
    else if(mode == "rw")
      s = "w+";
    else
      throw RuntimeException("unsupported mode " + mode);
    handle = fopen(fileName.c_str(), s);
    if(handle == NULL)
      throw RuntimeException("could not open file " + fileName);
    // the following should be here for compatibility, but is not,
    // as there is no alternate support for buffered i/o the
    // C++ backend
    // setvbuf(stream, (char *) NULL, _IONBF, 0); 
  }
  void RandomAccessFile::close() {
    fclose(handle);
  }
  int RandomAccessFile::read() {
    char b;
    if(fread(&b, 1, 1, handle) != 1)
      throw RuntimeException("could not read from file " + fileName);
    return b;
  }
  int RandomAccessFile::read(signed char* buffer, int offset, int length) {
    int i = fread(buffer + offset, 1, length, handle);
    if(i == 0)
      throw RuntimeException("could not read from file " + fileName);
    return i;
  }
  void RandomAccessFile::write(int b) {
    char c = b;
    if(fwrite(&c, 1, 1, handle) != 1)
      throw RuntimeException("could not write to file " + fileName);
  }
  void RandomAccessFile::write(signed char* buffer, int offset, int length) {
    if((signed int)fwrite(buffer + offset, 1, length, handle) != length)
      throw RuntimeException("could not write to file " + fileName);
  }
  long RandomAccessFile::length() {
    struct stat st;
    if(stat(fileName.c_str(), &st) != 0)
      throw RuntimeException("could not determine size of file " + fileName);
    return st.st_size;
  }
  void RandomAccessFile::seek(long pos) {
    if(fseek(handle, pos, SEEK_SET) != 0)
      throw RuntimeException("could not seek in file " + fileName +
        " to " + StringTools::toString(pos));
  }
}
