#include "hc.h"

#if defined(_WIN32) || defined(_WIN64)
double round(double x) {
  return x < 0 ? ceil(x - 0.5) : floor(x + 0.5);
}
double fmax(double x, double y) {
  return  x >= y ? x : y;
}
double fmin(double x, double y) {
  return  x <= y ? x : y;
}
#endif
