#ifndef HC_H
#define HC_H

#include <stdlib.h>
#include <cmath>
#include <iostream>

#include "Object.h"
#include "Mem.h"
#include "StringTools.h"
#include "File.h"
#include "RandomAccessFile.h"
#include "RuntimeException.h"

using std::cout;

#if defined(_WIN32) || defined(_WIN64)
double round(double x);
double fmax(double x, double y);
double fmin(double x, double y);
#endif

#endif
