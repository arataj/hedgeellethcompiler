pk verics.lang;

im$ verics.math.Dist.*;

/**
 * <p>The special logic class.</p>
 */
^class Pr implements AbstractAgent {
  /**
   * .k must be below 1 + EPSILON.
   */
  ^$ R EPSILON := 1e-12;

  /**
   * Ratio of false events.
   */
  ^# R<<0, 1>> f;
  /**
   * Ratio of true events.
   */
  ^# R<<0, 1>> t;
  /**
   * Ratio of known events = .f + .t.
   */
  ^# R<<0, 1>> k;
  /**
   * Ratio of unknown events = 1 - .k.
   */
  ^# R<<0, 1>> u;

  /**
   * @param true ratio of true events, 0 ... 1
   * @param false ratio of false events, 0 ... 1
   */  
  ^# Pr(R<<0, 1>> true, R<<0, 1>> false) {
      .f := false;
      .t := true;
      .k := .f + .t;
      .u := 1.0 - .k;
      assert .k <= 1.0 + .EPSILON else "ratio of known events can not exceed 1 + Pr.EPSILON";
  }
  /**
   * Does not allow non--determinism.
   *
   * @param true ratio of true events, 0 ... 1
   */  
  ^# Pr(R<<0, 1>> true) {
      .f := 1 - true;
      .t := true;
      .k := 1.0;
      .u := 0.0;
  }
  /**
   * Probability of true branch, primary test.
   *
   * @return <code>.t > $uni()</code>
   */
  ^# B branch1T() implements {
      return .t > $uni();
  }
  /**
   * Probability of false branch, secondary test.
   *
   * @return <code>.f/(1.0 - .t) > :uni()</code>
   */
  ^# B branch2F() implements {
      return .f/(1.0 - .t) > $uni();
  }
  ^# B next() {
      if branch1T() {
          return T;
      } else if branch2F() {
          return F;
      } else {
          return U;
      }
  }
}
