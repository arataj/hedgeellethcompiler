#
# Creates hth.jar, to be used by Hedgeelleth--compatible Java applications
# used within a JVM.
#
HC_LIBRARY=../../doc/html/Hedgeelleth/Components/hclibrary
rm -f hc/*.class hc/game/*.class && \
rm -rf javadoc && mkdir javadoc && cp stylesheet.css javadoc/ && \
rm -rf $HC_LIBRARY && mkdir $HC_LIBRARY && \
javac -cp hc:../../HedgeellethUtilities/dist/HedgeellethUtilities.jar \
	hc/*.java hc/game/*.java && \
	jar -cf hc.jar hc/*.class hc/game/*.class && \
	cp hc.jar ../../lib/ && \
	( cd hc && \
		javadoc -d ../javadoc -classpath ../../../HedgeellethUtilities/dist/HedgeellethUtilities.jar \
		-overview overview.html *.java game/*.java && \
	cd - ) && \
	cp -rp javadoc/* $HC_LIBRARY/
