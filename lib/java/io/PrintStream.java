package java.io;

/**
 * Print stream.
 */
public class PrintStream {
  /**
   * Creates a new print stream.
   */
  public PrintStream() {
  }
  /**
   * Prints a string, does not append a new line
   * character to the output.
   *
   * @param s string to print
   */
  @@MARKER @@IGNORE @@PRINT
  public void print(String s) {
  }
  /**
   * Prints a string, appends a new line character
   * to the output.
   *
   * @param s string to print
   */
  @@MARKER @@IGNORE @@PRINTLN
  public void println(String s) {
  }
  /**
   * Prints an integer value, does not append a new line
   * character to the output.
   *
   * @param s integer value to print
   */
  public void print(long value) {
    print("" + value);
  }
  /**
   * Prints a floating--point value, does not append a
   * new line character to the output.
   *
   * @param value floating--point value to print
   */
  public void print(double value) {
    print("" + value);
  }
  /**
   * Prints an integer value, appends a new line character
   * to the output.
   *
   * @param s integer value to print
   */
  public void println(long value) {
    println("" + value);
  }
  /**
   * Prints a floating--point value, appends a new line
   * character to the output.
   *
   * @param value floating--point value to print
   */
  public void println(double value) {
    println("" + value);
  }
  /**
   * Prints a new line character to the output.
   */
  public void println() {
    println("");
  }
}
