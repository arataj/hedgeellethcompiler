package java.io;

/**
 * Most basic operations on a file.<br>
 *
 * A marker class for use with the C++ backend.
 */
public class File {
  /**
   * Associates a name with a file.
   *
   * @param fileName name of the file
   */
  @@MARKER @@NEW_FILE_FILENAME
  public File(String fileName) {
  }
  /**
   * Returns if a file exists.
   *
   * @return if the file has been found
   */
  @@MARKER @@FILE_EXISTS
  public boolean exists() {
      return false;
  }
  /**
   * Deletes a file.
   *
   * @return if the file has been actualy deleted
   */
  @@MARKER @@FILE_DELETE
  public boolean delete() {
      return false;
  }
}
