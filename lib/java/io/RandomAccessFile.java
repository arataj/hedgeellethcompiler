package java.io;

/**
 * Most basic operations on a binary file.<br>
 *
 * A marker class for use with the C++ backend.
 */
public class RandomAccessFile {
  /**
   * Opens a file.
   *
   * @param file file to open
   * @param mode: "r" or "rw" 
   */
  @@MARKER @@NEW_RANDOM_ACCESS_FILE_FILE
  public RandomAccessFile(File file, String mode) {
  }
  /**
   * Opens a file.
   *
   * @param fileName name of the file
   * @param mode: "r" or "rw" 
   */
  @@MARKER @@NEW_RANDOM_ACCESS_FILE_FILENAME
  public RandomAccessFile(String fileName, String mode) {
  }
  /**
   * Closes the file. Frees all resources connected with this
   * object. No method should be called on it after this
   * method is called.
   */
  @@MARKER @@RANDOM_ACCESS_FILE_CLOSE
  public void close() {
  }
  /**
   * Reads a byte from the file.
   *
   * @return byte read
   */
  @@MARKER @@RANDOM_ACCESS_FILE_READ_BYTE
  public int read() {
    return 0;
  }
  /**
   * Reads a number of bytes from a file to a fragment of
   * a byte array. If even not a single byte is available,
   * an exception is thrown. 
   *
   * @param buffer buffer to store the bytes read
   * @param offset index of the first element in the buffer
   * to be written to 
   * @param length number of the bytes to read
   * @return number of bytes that were actually read
   */
  @@MARKER @@RANDOM_ACCESS_FILE_READ_ARRAY
  public int read(byte[] buffer, int offset, int length) {
    return 0;
  }
  /**
   * Writes a byte to the file.
   *
   * @param b byte to write
   */
  @@MARKER @@RANDOM_ACCESS_FILE_WRITE_BYTE
  public void write(int b) {
  }
  /**
   * Writes a fragment of a byte array to a file.
   *
   * @param buffer buffer containing the bytes to write
   * @param offset index of the first byte to write in the
   * buffer 
   * @param length number of the bytes to write
   */
  @@MARKER @@RANDOM_ACCESS_FILE_WRITE_ARRAY
  public void write(byte[] buffer, int offset, int length) {
  }
  /**
   * Returns the length of this file.
   *
   * @return size in bytes
   */
  @@MARKER @@RANDOM_ACCESS_FILE_LENGTH
  public long length() {
    return 0;
  }
  /**
   * Sets the position in the file for reading or writing.
   */
  @@MARKER @@RANDOM_ACCESS_FILE_SEEK
  public void seek(long pos) {
  }
}
