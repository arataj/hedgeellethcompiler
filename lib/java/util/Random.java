package java.util;

/**
 * A class for generating random integer values.<br>
 *
 * \code{hc} interprets these values as non--deterministic.
 * For uniformly distributed values, \code{hc} uses
 * <code>Math.random()</code> instead.
 */
public class Random {
  /**
   * Creates a new instance of <code>Random</code>.
   */
  public Random() {
  }
  /**
   * Returns a new value, which is random uniformly
   * distributed in the case of JDK, and non--deterministic
   * in the case of \code{hc}.
   *
   * @return a value in the range 0 ... n - 1
   */
  @@MARKER @@RANDOM
  public int nextInt(int n) {
    return -1;
  }
}
