package java.lang;

/**
 * Mathematical functions.
 */
public class Math {
  /**
   * Absolute value. 
   *
   * @return abs(v)
   */
  @@MARKER @@MATH_ABS
  public static int abs(int v) {
    return 0;
  }
  /**
   * Absolute value. 
   *
   * @return abs(v)
   */
  @@MARKER @@MATH_ABS
  public static long abs(long v) {
    return 0;
  }
  /**
   * Absolute value. 
   *
   * @return abs(v)
   */
  @@MARKER @@MATH_ABS
  public static double abs(double v) {
    return 0.0;
  }
  /**
   * Minimum of two values. 
   *
   * @return min(a, b)
   */
  @@MARKER @@MATH_MIN
  public static int min(int a, int b) {
    return 0;
  }
  /**
   * Minimum of two values. 
   *
   * @return min(a, b)
   */
  @@MARKER @@MATH_MIN
  public static long min(long a, long b) {
    return 0;
  }
  /**
   * Minimum of two values. 
   *
   * @return min(a, b)
   */
  @@MARKER @@MATH_MIN
  public static double min(double a, double b) {
    return 0.0;
  }
  /**
   * Maximum of two values. 
   *
   * @return max(a, b)
   */
  @@MARKER @@MATH_MAX
  public static int max(int a, int b) {
    return 0;
  }
  /**
   * Maximum of two values. 
   *
   * @return max(a, b)
   */
  @@MARKER @@MATH_MAX
  public static long max(long a, long b) {
    return 0;
  }
  /**
   * Maximum of two values. 
   *
   * @return max(a, b)
   */
  @@MARKER @@MATH_MAX
  public static double max(double a, double b) {
    return 0.0;
  }
  /**
   * A square root. 
   *
   * @return sqrt(v)
   */
  @@MARKER @@MATH_SQRT
  public static double sqrt(double v) {
    return 0.0;
  }
  /**
   * A sinus. 
   *
   * @return sin(v)
   */
  @@MARKER @@MATH_SIN
  public static double sin(double v) {
    return 0.0;
  }
  /**
   * A cosinus. 
   *
   * @return cos(v)
   */
  @@MARKER @@MATH_COS
  public static double cos(double v) {
    return 0.0;
  }
  /**
   * An arcus sinus. 
   *
   * @return asin(v)
   */
  @@MARKER @@MATH_ASIN
  public static double asin(double v) {
    return 0.0;
  }
  /**
   * An arcus cosinus. 
   *
   * @return acos(v)
   */
  @@MARKER @@MATH_ACOS
  public static double acos(double v) {
    return 0.0;
  }
  /**
   * Integer floor.
   *
   * @return floor(v)
   */
  @@MARKER @@MATH_FLOOR
  public static double floor(double v) {
    return 0.0;
  }
  /**
   * Integer ceiling.
   *
   * @return ceil(v)
   */
  @@MARKER @@MATH_CEIL
  public static double ceil(double v) {
    return 0.0;
  }
  /**
   * Rounded value.
   *
   * @return round(v)
   */
  @@MARKER @@MATH_ROUND
  public static long round(double v) {
    return 0;
  }
  /**
   * Power.
   *
   * @return b raised to e
   */
  @@MARKER @@MATH_POW 
  public static double pow(double b, double e) {
    return 0.0;
  }  
  /**
   * A uniform distribution, 0 ... 1.
   *
   * @return random()
   */
  @@MARKER @@DIST_UNI
  public static double /*@(0, 0.9999999999999999)*/ random() {
    return 0;
  }
}
