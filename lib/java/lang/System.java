package java.lang;

import java.io.*;

/**
 * System class.
 */
public class System {
    /**
     * Standard output stream.
     */
    public static PrintStream out = new PrintStream();
  
    /**
     * Copies a sequence of elements from one array
     * to another array.
     *
     * @param src source array
     * @param srcPos index of first element to copy in the
     * source array
     * @param dest destinatin array
     * @param destPos index of first element to write to
     * in the destination array
     * @param length number of subsequent elements to copy
     */
    @@MARKER @@ARRAY_COPY
    public static void arraycopy(Object src, int srcPos,
        Object dest, int destPos, int length) {
    }
}
