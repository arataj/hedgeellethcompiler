package java.lang;

/**
 * Root object.
 */
public class Object {
  /**
   * Creates a new instance of <code>Object</code>.
   */
  public Object() {
  }
  /**
   * Notify operation on this lock.
   */
  @@MARKER @@NOTIFY
  public void notify() {
  }
  /**
   * Notify all operation on this lock.
   */
  @@MARKER @@NOTIFY_ALL
  public void notifyAll() {
  }
  /**
   * Wait operation on this lock.
   */
  @@MARKER @@WAIT
  public void wait() {
  }
  /**
   * Finalize method. Never called by \code{hc}.
   */
  public void finalize() {
    /* empty */
  }
  /**
   * Compares two objects. This implementation
   * tests if two objects are of the same instance.
   */
  public boolean equals(Object o) {
    return this == o;
  }
  /**
   * Textual representation of this object.
   *
   * @return textual description
   */
  public String toString() {
    return null;
  }
}
