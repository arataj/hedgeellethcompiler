package java.lang;

/**
 * <p>The special array class.</p>
 */
public class Array<T> {
  /**
   * Number of elements in this array.
   */
  public int length;
  
  /**
   * Creates a new array.
   *
   * @param length number of elements in the array to create
   */
//  public Array(int length) {
//      this.length = length;
//  }
  /**
   * Creates a new array.
   *
   * <p>The constructor's argument of type long to be compatible with
   * other languages. The interpretes takes care of the value never
   * exceeding the range of int.</p>
   *
   * @param length number of elements in the* array to create
   */
  public Array(long length) {
      this.length = (int)length;
  }
}
