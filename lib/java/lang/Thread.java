package java.lang;

/**
 * Thread class.
 */
public class Thread {
  /**
   * optional runnable object.
   */
  Runnable runnable;
  
  /**
   * Creates a new thread, not started.
   */
  public Thread() {
  }
  /**
   * Creates a new thread, not started.
   *
   * @param runnable runnable object to
   * execute within this thread
   */
  public Thread(Runnable runnable) {
      this.runnable = runnable;
  }
  /**
   * Starts this thread.
   */
  @@MARKER @@START_THREAD
  public void start() {
  }
  /**
   * Run method of this thread. Override this method or
   * pass a runnable in the constructor.
   */
  public void run() {
      runnable.run();
  }
  /**
   * Join synchronising operation on this thread.
   */
  @@MARKER @@JOIN
  public void join() {
  }
  /**
   * <p>Sleeps for at least a given number of milliseconds.</p>
   *
   * <p>An equivalent that uses double type argument given in seconds,
   * see <code>Sleep.lax()</code>.</p>
   *
   * <p>For non--overriding schedulers:
   * In the case of hc, <code>time</code> is exact in the
   * case of an immediate scheduler or a constant scheduler
   * with the coefficient equal to 0. For all other schedulers
   * <code>time</code> represents a minimum delay. In
   * particular, in the case of the free scheduler there
   * is not upper limit on the delay.</p>
   *
   * @param time in milliseconds
   */
  public static void sleep(int milliseconds) {
      hc.Sleep.sleep(hc.SleepConstants.SLEEP_EXACT_MILLISECONDS,
              milliseconds);
  }
  /**
   * Allow in the main thread, but not allowed
   * to be interpreted by hc.<br>
   *
   * Used to stop models on the JVM side.
   */
  @@MARKER @@INTERRUPT
  public void interrupt() {
  }
  /**
   * Always returns false, as hc does not allow
   * <code>Thread.interrupt()</code> to be
   * executed.<br>
   *
   * Used to stop models on the JVM side.
   */
  public boolean isInterrupted() {
      return false;
  }
}
