package java.lang;

/**
 * A root exception class.
 */ 
public class Exception {
  /**
   * This exception's message.
   */
  String message;

  /**
   * Creates a new instanceof <code>Exception</code>.
   */
  public Exception(String message) {
    this.message = message;
  }
}
