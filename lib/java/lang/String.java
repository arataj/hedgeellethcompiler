package java.lang;

/**
 * The special string class.
 *
 * Hedgeelleth does not allow explicit creation of objects
 * of this class in the source code, thus the private
 * constructor.
 */
public class String {
    /**
     * Length of this string, in characters.
     */
    private long length;
}
