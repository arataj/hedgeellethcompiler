package java.lang;

/**
 * <p>A nullable real value.</p>
 */
public class Double extends AbstractNumber {
  /**
   * Value.
   */
  private double value;

  /**
   * Creates a new nullable real value.
   * Should never be called directly,
   * but instead R? should be used.
   */  
  public Double(double value) {
      this.value = value;
  }
  public double doubleValue() {
      return value;
  }
}
