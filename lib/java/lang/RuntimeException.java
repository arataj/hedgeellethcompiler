package java.lang;

/**
 * An unchecked exception class.<br>
 *
 * Throwing of this exception is treated
 * as a failed assertion by \code{hc}.
 */ 
public class RuntimeException extends Exception {
  /**
   * Creates a new instance of runtime exception.
   */
  public RuntimeException(String message) {
    super(message);
  }
}
