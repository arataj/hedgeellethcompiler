package java.lang;

/**
 * Interface of runnable objects. 
 */
public interface Runnable {
  /**
   * The run method, to be executed by
   * a thread.
   */
  public void run();
}
