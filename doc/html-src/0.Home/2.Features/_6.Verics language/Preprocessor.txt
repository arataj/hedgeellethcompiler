<p>
    <img style="float: right; padding: 10px 14px 10px 8px;" src="synchronous.png"/>
    A model in the Verics environment can consist of a number of automatons branching
    in an arbitrary way, and merged together using different sets of rules.
    This allows a free flow of control, much more free than one
    straightforwardly expressed by a multi--threaded language like the Verics
    language.
</p>

<p>
    For the most direct and explicit definition of a model, the Verics
    environment uses thus a simple assembly language <code>Vs</code>, being basically a mix of
    guard expressions and updates to the state vector. Traditionally
    assembly language are enhanced with preprocessors. In the case of
    <code>Vs</code>, the Verics language itself is used as a preprocessor, no need to become
    acquainted with a new formalism.
</p>

<p>
    In fact, the subsequent lines of the <code>Vs</code> language, from the point
    of view of the preprocessor, are merely the contents of its
    <code>println</code> statements.
</p>
