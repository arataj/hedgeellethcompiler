$LEFT(package)<p>
	This is an off&ndash;line copy. For the most current version,
	downloads and API see <a href="http://www.iitis.pl/~arataj/hedgeelleth/index.html">the homepage</a>.
</p>$RIGHT

<p>
	<font color="brown" size="3">Verics is getting an overhaul and so is this web page,
        which refers now to both the latest stable
        version and the new, experimental one, not yet ready for the end user.
        </font>
</p>

<p>
        <font color="brown" size="3">Note to the developers: a binary snapshot can be found at
        <code>bizet.itsoa.ipipan.eu:/home/arataj/local/verics/</code> and a system-wide
        <code>PATH</code> setting should already point to the binary executable
        <code>veric</code>. Examples can be found in the <code>examples/</code> subdirectory.
	</font>
</p>        

<p>
        Verics is a verification environment running a set of integrated model
        checkers. It is developed at the
        <a href="http://www.ipipan.waw.pl">Institute of Computer Science</a>,
        Polish Academy of Sciences.
        It uses our state-of-the-art methods of
        bounded and parametric model checking. One of the ideas behind Verics is
        that a model representation should
        be friendly to both computer scientists and software developers.
</p>        

<p>
        We are currently preparing a new version which will
        feature updated and new
	<a href="Features/index.html">model-checking methods</a> and a common modelling language.
        It will also support some external verification software like <a
        href="http://vas.doc.ic.ac.uk/software/mcmas/">MCMAS</a>,
        <a href="http://vas.doc.ic.ac.uk/software/mcmas/">Prism</a>,
        <a href="http://osate.org">OSATE</a> or
        <a href="http://uppaal.org">UPPAAL</a>.
</p>
