#
# Builds html files out of zeroHiperTxt's files.
# Called by export-html.sh.
#
# Parameters are passed to zeroHiperTxt.
#
java -jar lib/zeroHiperTxt.jar html-src html -m $* && \
	rm -rf VericsHtml/web/* && cp -rp html/Home/* VericsHtml/web/ && \
	cd VericsHtml && ./remote-update.sh

