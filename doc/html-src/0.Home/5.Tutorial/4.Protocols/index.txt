<p>
    A complex synchronisation between modules can be more flexibly defined using protocols.
    A protocol keeps the agent's state variables hidden, but allows the agent to
    talk to the external world about the actions it performs. Let us look again at
    the $A(Simple example:FTC) model, but with its modules synchronised by the protocols,
    or rather, $A(Synchronisation:unsynchronised), as the
    protocols exlude certain sets of statements from being synchronised.
</p>

<p>
    The environment now talks to the trains 1 and 2 through its protocol:
<pre>
    protocol {
        in1, in2 := lights == GREEN;
        out1, out2 := lights == RED;
    };
</pre>
    ignoring the 3rd train as usual. The way of synchronisation is realised explicitly
    by using guards:
<pre>
    [] lights == GREEN && (<b>${$rpt("(in$ && train$.in)", " || ", 1, .NUM - 1)}</b>) ->
                   lights := RED;
    [] lights == RED && (<b>${$rpt("(out$ && train$.out)", " || ", 1, .NUM - 1)}</b>) ->
                   lights := GREEN;
</pre>
    As seen, there is a built-in function <code>$rpt(String operand, String operator, Z min, Z max)</code>,
    which is handy if an expression of the form <code>operand1 operator operand2 operator operand3</code>
    is needed. The function is written in the Verics language and the user can similarly define
    other string&ndash;processing functions as needed. In this case, the function is expanded by the
    preprocessor as follows:
<pre>
    [] lights == GREEN && ((in1 && train1.in) || (in2 && train2.in)) ->
                   lights := RED; 
    [] lights == RED && ((out1 && train1.out) || (out2 && train2.out)) ->
                   lights := GREEN; 
</pre>
    The expressivity of boolean equations like that is what makes the protocols verbose, yet
    flexible.
</p>

<p>
    Let us look into the 1st train, as expanded by the preprocessor:
<pre>
agent train1 { 
    Z{WAIT, TUNNEL, AWAY} phase := AWAY;

    protocol {
        in, none := phase == WAIT;
        out, none := phase == TUNNEL;
        approach, none := phase == AWAY;
    }; 

    []  phase == AWAY && approach -> phase := WAIT;
    // the train <code>NUM</code> is faulty 
    []  phase == WAIT && in && environment.in1 -> phase := TUNNEL;
    []  phase == TUNNEL && out && environment.out1 -> phase := AWAY; 
}; 
</pre>
    We allow a <code>none</code> action, i.e. the train can be idle while the other
    parts of the system run.
</p>

<p>
<br>Files:
<ul>
<li><a href="../Source files/FTCp.vxs">FTCp.vxs</a>: the source of the model;</li>
<li><a href="../Source files/FTCp.ispl">FTCp.ispl</a>: the resulting ISPL file.</li>
</ul>
</p>

<p>
<br>Command line:
<ul>
<li><code>veric -oi FTCp.vxs</code></li>
</ul>
</p>
