<p>
    The multi-format output allows a multitude of analyses and applications
    using only a single input representation (i.e. the source in Java).
    The Prism part can be used to compute e.g. complex CTL properties to
    detect infinite waitings, temporal deadlocks or livelocks. The AADL
    part can be analysed in the <a href="http://osate.org">OSATE</a>
    environment, for example to check if the end-to-end flows respect the
    specifications.
</p>

<p>
<br>Files:
<ul align="left">
<li>
<a href="../Source files/Camera.java">Camera.java</a>,
<a href="../Source files/Bus.java">Bus.java</a>,
<a href="../Source files/VideoProcessing.java">VideoProcessing.java</a>,
<a href="../Source files/ImageAcquisitionSystem.java">ImageAcquisitionSystem.java</a>:
source of the model;</li>
<li><a href="../Source files/ImageAcquisitionSystem.nm">ImageAcquisitionSystem.nm</a>: resulting timed automatons;</li>
<li><a href="../Source files/ImageAcquisitionSystem.aadl">ImageAcquisitionSystem.aadl</a>: architecture;</li>
</ul>
</p>

<p>
<br>Command line:
<ul align="left">
<li><code>hc -U -v0 -op -sh -i -Da Camera.java VideoProcessing.java ImageAcquisitionSystem.java</code></li>
</ul>
</p>
