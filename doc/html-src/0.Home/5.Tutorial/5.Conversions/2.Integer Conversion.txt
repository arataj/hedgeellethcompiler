<p>
    Obviously, integer arithmetic can be of a great help when specifying a
    model. Some model checkers only support small parts of it, sometimes
    being limited only to an integer comparison. In such cases, the
    <code>veric</code>'s ability of an automated conversion of an integer
    expression into a boolean equivalent may help.
</p>

<p>
    An integer evaluation of an action allows a
    flexible definition of synchronisation patterns,
    for example the <a href="dcN.vxs">dining crypthographers</a> model contains an
    expression:

<pre>
    mod(dc1.say_equal + dc2.say_equal + dc3.say_equal, 2) == 1
</pre>

   which would be converted to

<pre>
    ( dc1.say_equal and  dc2.say_equal and  dc3.say_equal) or
    (!dc1.say_equal and !dc2.say_equal and  dc3.say_equal) or
    (!dc1.say_equal and  dc2.say_equal and !dc3.say_equal) or
    ( dc1.say_equal and !dc2.say_equal and !dc3.say_equal)
</pre>
</p>

<p>
   The MCMAS' language ISPL lacks integer operators except for
   the relational ones. Even the expressions which do not contain
   integer action evaluation must thus be converted. A part  of this initial
   state filter, again from the dining crypthographers model:

<pre>
   <b>${$rpt("dc$.payer", " + ", 1, .NUM)}</b> <= 1
</pre>

   in the case of <code>NUM</code> = 3 would be converted into the following
   ISPL expression:

<pre>
   (dc1.payer=NO  and dc2.payer=NO  and dc3.payer=NO ) or 
   (dc1.payer=YES and dc2.payer=NO  and dc3.payer=NO ) or 
   (dc1.payer=NO  and dc2.payer=YES and dc3.payer=NO ) or 
   (dc1.payer=NO  and dc2.payer=NO  and dc3.payer=YES)
</pre>

   An XML output accepts anything definable in a VXS model, and thus
   the conversion options applicable depend solely on the checker,
   which will read the output file. 
</p>
