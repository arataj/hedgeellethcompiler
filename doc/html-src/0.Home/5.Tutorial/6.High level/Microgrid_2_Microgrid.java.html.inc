<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * A microgrid demand--side management game, as described</span>
<span style="color: #60a0b0; font-style: italic"> * in [1].</span>
<span style="color: #60a0b0; font-style: italic"> *</span>
<span style="color: #60a0b0; font-style: italic"> * [1] H. Hildmann and F. Saffre. Influence of variable</span>
<span style="color: #60a0b0; font-style: italic"> * supply and load flexibility on Demand-Side Management.</span>
<span style="color: #60a0b0; font-style: italic"> * In Proc. 8th International Conference on the European</span>
<span style="color: #60a0b0; font-style: italic"> * Energy Market (EEM&#39;11), pages 63-68. 2011.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">class</span> <span style="color: #0e84b5; font-weight: bold">Microgrid</span>
</pre></td></tr></table></div>
