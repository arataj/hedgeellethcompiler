<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Override</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #902000">void</span> <span style="color: #06287e">run</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
    <span style="color: #007020; font-weight: bold">do</span> <span style="color: #666666">{</span>
        <span style="color: #60a0b0; font-style: italic">// select a household</span>
        <span style="color: #902000">int</span> address<span style="color: #666666">;</span>
        <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(</span>NON_DETERMINISTIC<span style="color: #666666">)</span>
            <span style="color: #60a0b0; font-style: italic">// non--deterministic choice</span>
            address <span style="color: #666666">=</span> strategy<span style="color: #666666">.</span><span style="color: #4070a0">nextInt</span><span style="color: #666666">(</span>Microgrid<span style="color: #666666">.</span>
                NUM_HOUSEHOLDS<span style="color: #666666">);</span>
        <span style="color: #007020; font-weight: bold">else</span>
            <span style="color: #60a0b0; font-style: italic">// probabilistic choice</span>
            address <span style="color: #666666">=</span> <span style="color: #666666">(</span><span style="color: #902000">int</span><span style="color: #666666">)</span>
                <span style="color: #666666">(</span>Math<span style="color: #666666">.</span><span style="color: #4070a0">random</span><span style="color: #666666">()*</span>Microgrid<span style="color: #666666">.</span><span style="color: #4070a0">NUM_HOUSEHOLDS</span><span style="color: #666666">);</span>
        <span style="color: #60a0b0; font-style: italic">// contact the selected household</span>
        turnNext<span style="color: #666666">(</span><span style="color: #40a070">1</span> <span style="color: #666666">+</span> address<span style="color: #666666">);</span>
        turnWait<span style="color: #666666">();</span>
        <span style="color: #60a0b0; font-style: italic">// end of the current round</span>
        Microgrid<span style="color: #666666">.</span><span style="color: #4070a0">endInterval</span><span style="color: #666666">();</span>
    <span style="color: #666666">}</span> <span style="color: #007020; font-weight: bold">while</span><span style="color: #666666">(!</span>Microgrid<span style="color: #666666">.</span><span style="color: #4070a0">isFinished</span><span style="color: #666666">());</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">finish</span><span style="color: #666666">();</span>
<span style="color: #666666">}</span>
</pre></td></tr></table></div>
