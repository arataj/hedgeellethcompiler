<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">void</span> <span style="color: #06287e">endInterval</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
    numJobs <span style="color: #666666">=</span> getNumJobs<span style="color: #666666">();</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">stateAnd</span><span style="color: #666666">(</span><span style="color: #4070a0">&quot;measure&quot;</span><span style="color: #666666">);</span>
    <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(!</span>Model<span style="color: #666666">.</span><span style="color: #4070a0">isStatistical</span><span style="color: #666666">())</span>
        numJobs <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span>
    <span style="color: #60a0b0; font-style: italic">// reduce job counters</span>
    <span style="color: #007020; font-weight: bold">for</span><span style="color: #666666">(</span><span style="color: #902000">int</span> i <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span> i <span style="color: #666666">&lt;</span> Microgrid<span style="color: #666666">.</span><span style="color: #4070a0">NUM_HOUSEHOLDS</span><span style="color: #666666">;</span> <span style="color: #666666">++</span>i<span style="color: #666666">)</span>
          households<span style="color: #666666">[</span>i<span style="color: #666666">].</span><span style="color: #4070a0">tick</span><span style="color: #666666">();</span>
    <span style="color: #666666">++</span>time<span style="color: #666666">;</span>
<span style="color: #666666">}</span>
</pre></td></tr></table></div>
