<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3
4
5
6</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #007020; font-weight: bold">final</span> <span style="color: #902000">double</span><span style="color: #666666">[]</span> DEMAND <span style="color: #666666">=</span> <span style="color: #666666">{</span>
    <span style="color: #40a070">0.0614</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0392</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0304</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0304</span><span style="color: #666666">,</span>
    <span style="color: #40a070">0.0355</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0518</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0651</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0643</span><span style="color: #666666">,</span>
    <span style="color: #40a070">0.0625</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0618</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0614</span><span style="color: #666666">,</span> <span style="color: #40a070">0.0695</span><span style="color: #666666">,</span>
    <span style="color: #40a070">0.0887</span><span style="color: #666666">,</span> <span style="color: #40a070">0.1013</span><span style="color: #666666">,</span> <span style="color: #40a070">0.1005</span><span style="color: #666666">,</span> <span style="color: #60a0b0; font-style: italic">/* 0.0762, */</span>
<span style="color: #666666">};</span>
</pre></td></tr></table></div>
