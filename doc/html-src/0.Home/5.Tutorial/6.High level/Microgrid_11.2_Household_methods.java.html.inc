<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #555555; font-weight: bold">@Override</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #902000">void</span> <span style="color: #06287e">run</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
    <span style="color: #007020; font-weight: bold">while</span><span style="color: #666666">(</span><span style="color: #007020; font-weight: bold">true</span><span style="color: #666666">)</span> <span style="color: #666666">{</span>
        turnWait<span style="color: #666666">();</span>
        <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(</span>isInterrupted<span style="color: #666666">())</span>
            <span style="color: #007020; font-weight: bold">break</span><span style="color: #666666">;</span>
        <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(</span>job <span style="color: #666666">==</span> <span style="color: #40a070">0</span> <span style="color: #666666">&amp;&amp;</span> Math<span style="color: #666666">.</span><span style="color: #4070a0">random</span><span style="color: #666666">()</span> <span style="color: #666666">&lt;</span> Microgrid<span style="color: #666666">.</span><span style="color: #4070a0">getDemand</span><span style="color: #666666">()*</span>
              EXPECTED_JOBS<span style="color: #666666">)</span> <span style="color: #666666">{</span>
            <span style="color: #60a0b0; font-style: italic">// decide if to generate a load</span>
            <span style="color: #902000">boolean</span> lowPrice <span style="color: #666666">=</span> Microgrid<span style="color: #666666">.</span><span style="color: #4070a0">getPrice</span><span style="color: #666666">()</span> <span style="color: #666666">&lt;</span> PRICE_LIMIT<span style="color: #666666">;</span>
            <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(</span>lowPrice <span style="color: #666666">||</span> Math<span style="color: #666666">.</span><span style="color: #4070a0">random</span><span style="color: #666666">()</span> <span style="color: #666666">&lt;</span> P_OVER_LIMIT <span style="color: #666666">||</span>
                    <span style="color: #60a0b0; font-style: italic">// a right to back--off is granted; decide,</span>
                    <span style="color: #60a0b0; font-style: italic">// if to generate a load anyway</span>
                    strategy<span style="color: #666666">.</span><span style="color: #4070a0">nextInt</span><span style="color: #666666">(</span><span style="color: #40a070">2</span><span style="color: #666666">)</span> <span style="color: #666666">==</span> <span style="color: #40a070">0</span><span style="color: #666666">)</span>
                job <span style="color: #666666">=</span> <span style="color: #40a070">1</span> <span style="color: #666666">+</span> <span style="color: #666666">(</span><span style="color: #902000">int</span><span style="color: #666666">)(</span>Math<span style="color: #666666">.</span><span style="color: #4070a0">random</span><span style="color: #666666">()*</span>MAX_JOB_TIME<span style="color: #666666">);</span>
        <span style="color: #666666">}</span>
        turnNext<span style="color: #666666">(</span><span style="color: #40a070">0</span><span style="color: #666666">);</span>
    <span style="color: #666666">}</span>
<span style="color: #666666">}</span>
</pre></td></tr></table></div>
