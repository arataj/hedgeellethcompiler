<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">void</span> <span style="color: #06287e">main</span><span style="color: #666666">(</span>String<span style="color: #666666">[]</span> args<span style="color: #666666">)</span> <span style="color: #666666">{</span>
    <span style="color: #60a0b0; font-style: italic">// player ids at the table: 0 scheduler,</span>
    <span style="color: #60a0b0; font-style: italic">// 1..NUM_HOUSEHOLDS households</span>
    TurnGame table <span style="color: #666666">=</span> <span style="color: #007020; font-weight: bold">new</span> TurnGame<span style="color: #666666">();</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">name</span><span style="color: #666666">(</span>table<span style="color: #666666">);</span>
    <span style="color: #60a0b0; font-style: italic">// create an array of households</span>
    households <span style="color: #666666">=</span> <span style="color: #007020; font-weight: bold">new</span> Household<span style="color: #666666">[</span>NUM_HOUSEHOLDS<span style="color: #666666">];</span>
    <span style="color: #007020; font-weight: bold">for</span><span style="color: #666666">(</span><span style="color: #902000">int</span> i <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span> i <span style="color: #666666">&lt;</span> NUM_HOUSEHOLDS<span style="color: #666666">;</span> <span style="color: #666666">++</span>i<span style="color: #666666">)</span> <span style="color: #666666">{</span>
        Household h <span style="color: #666666">=</span> <span style="color: #007020; font-weight: bold">new</span> Household<span style="color: #666666">(</span>table<span style="color: #666666">,</span> <span style="color: #40a070">1</span> <span style="color: #666666">+</span> i<span style="color: #666666">);</span>
        h<span style="color: #666666">.</span><span style="color: #4070a0">start</span><span style="color: #666666">();</span>
        Model<span style="color: #666666">.</span><span style="color: #4070a0">name</span><span style="color: #666666">(</span>h<span style="color: #666666">,</span> <span style="color: #4070a0">&quot;&quot;</span><span style="color: #666666">,</span> <span style="color: #4070a0">&quot;&quot;</span> <span style="color: #666666">+</span> <span style="color: #666666">(</span>i <span style="color: #666666">+</span> <span style="color: #40a070">1</span><span style="color: #666666">));</span>
        Model<span style="color: #666666">.</span><span style="color: #4070a0">player</span><span style="color: #666666">(</span>h<span style="color: #666666">,</span> <span style="color: #4070a0">&quot;h&quot;</span> <span style="color: #666666">+</span> <span style="color: #666666">(</span>i <span style="color: #666666">+</span> <span style="color: #40a070">1</span><span style="color: #666666">));</span>
        households<span style="color: #666666">[</span>i<span style="color: #666666">]</span> <span style="color: #666666">=</span> h<span style="color: #666666">;</span>
    <span style="color: #666666">}</span>
    <span style="color: #60a0b0; font-style: italic">// create the scheduler</span>
    Scheduler s <span style="color: #666666">=</span> <span style="color: #007020; font-weight: bold">new</span> Scheduler<span style="color: #666666">(</span>table<span style="color: #666666">);</span>
    s<span style="color: #666666">.</span><span style="color: #4070a0">start</span><span style="color: #666666">();</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">name</span><span style="color: #666666">(</span>s<span style="color: #666666">);</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">player</span><span style="color: #666666">(</span>s<span style="color: #666666">,</span> <span style="color: #4070a0">&quot;scheduler&quot;</span><span style="color: #666666">);</span>
    <span style="color: #60a0b0; font-style: italic">// reset time</span>
    time <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span>
    <span style="color: #60a0b0; font-style: italic">// players consist of the scheduler and of the</span>
    <span style="color: #60a0b0; font-style: italic">// households</span>
    table<span style="color: #666666">.</span><span style="color: #4070a0">start</span><span style="color: #666666">(</span><span style="color: #40a070">1</span> <span style="color: #666666">+</span> NUM_HOUSEHOLDS<span style="color: #666666">);</span>
    <span style="color: #007020; font-weight: bold">for</span><span style="color: #666666">(</span><span style="color: #902000">int</span> t <span style="color: #666666">=</span> <span style="color: #40a070">1</span><span style="color: #666666">;</span> t <span style="color: #666666">&lt;=</span> MAX_TIME<span style="color: #666666">/</span><span style="color: #40a070">4</span><span style="color: #666666">;</span> <span style="color: #666666">++</span>t<span style="color: #666666">)</span>
        Model<span style="color: #666666">.</span><span style="color: #4070a0">check</span><span style="color: #666666">(</span><span style="color: #4070a0">&quot;at time &quot;</span> <span style="color: #666666">+</span> t<span style="color: #666666">,</span>
            <span style="color: #4070a0">&quot;&lt;&lt;1, 2, 3&gt;&gt; R{\&quot;value123\&quot;}max=? &quot;</span> <span style="color: #666666">+</span>
            <span style="color: #4070a0">&quot;[F time=&quot;</span> <span style="color: #666666">+</span> t <span style="color: #666666">+</span> <span style="color: #4070a0">&quot;]&quot;</span><span style="color: #666666">);</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">waitFinish</span><span style="color: #666666">();</span>
    <span style="color: #60a0b0; font-style: italic">// anything below won&#39;t be executed by hc, but</span>
    <span style="color: #60a0b0; font-style: italic">// will within JVM</span>
    System<span style="color: #666666">.</span><span style="color: #4070a0">out</span><span style="color: #666666">.</span><span style="color: #4070a0">println</span><span style="color: #666666">(</span><span style="color: #4070a0">&quot;time = &quot;</span> <span style="color: #666666">+</span> time<span style="color: #666666">);</span>
    <span style="color: #60a0b0; font-style: italic">// releases all threads still waiting on the</span>
    <span style="color: #60a0b0; font-style: italic">// barrier</span>
    households<span style="color: #666666">[</span><span style="color: #40a070">0</span><span style="color: #666666">].</span><span style="color: #4070a0">interrupt</span><span style="color: #666666">();</span>
<span style="color: #666666">}</span>
</pre></td></tr></table></div>
