<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #007020; font-weight: bold">package</span> example<span style="color: #666666">;</span>

<span style="color: #007020; font-weight: bold">import</span> <span style="color: #0e84b5; font-weight: bold">hc.*</span><span style="color: #666666">;</span>

<span style="color: #007020; font-weight: bold">class</span> <span style="color: #0e84b5; font-weight: bold">RangeCheck</span> <span style="color: #007020; font-weight: bold">extends</span> Thread <span style="color: #666666">{</span>
  <span style="color: #007020; font-weight: bold">final</span> <span style="color: #902000">int</span> N <span style="color: #666666">=</span> <span style="color: #40a070">5</span><span style="color: #666666">;</span>
  <span style="color: #902000">int</span> <span style="color: #60a0b0; font-style: italic">/*@(0, 2*N)*/</span> sum<span style="color: #666666">;</span>
  
  <span style="color: #007020; font-weight: bold">public</span> <span style="color: #902000">void</span> <span style="color: #06287e">run</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
      <span style="color: #902000">int</span> <span style="color: #60a0b0; font-style: italic">/*@(0, N - 1)*/</span>i <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span>
      <span style="color: #007020; font-weight: bold">while</span><span style="color: #666666">(</span>i <span style="color: #666666">&lt;</span> <span style="color: #40a070">10</span><span style="color: #666666">)</span> <span style="color: #666666">{</span>
          sum <span style="color: #666666">+=</span> N<span style="color: #666666">/(</span>N<span style="color: #666666">/</span><span style="color: #40a070">2</span> <span style="color: #666666">+</span> i<span style="color: #666666">);</span>
          <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(</span>Math<span style="color: #666666">.</span><span style="color: #4070a0">random</span><span style="color: #666666">()</span> <span style="color: #666666">&lt;</span> <span style="color: #40a070">0.5</span><span style="color: #666666">)</span>
              <span style="color: #666666">++</span>i<span style="color: #666666">;</span>
      <span style="color: #666666">}</span>
  <span style="color: #666666">}</span>
  <span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">void</span> <span style="color: #06287e">main</span><span style="color: #666666">(</span>String<span style="color: #666666">[]</span> args<span style="color: #666666">)</span> <span style="color: #666666">{</span>
    RangeCheck rc <span style="color: #666666">=</span> <span style="color: #007020; font-weight: bold">new</span> RangeCheck<span style="color: #666666">();</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">name</span><span style="color: #666666">(</span>rc<span style="color: #666666">);</span>
    rc<span style="color: #666666">.</span><span style="color: #4070a0">start</span><span style="color: #666666">();</span>
  <span style="color: #666666">}</span>
<span style="color: #666666">}</span>
</pre></td></tr></table></div>
