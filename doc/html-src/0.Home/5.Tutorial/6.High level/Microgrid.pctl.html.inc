<pre><code>// at time 1
<<1, 2, 3>> R{"value123"}max=? [F time=1]
// at time 2
<<1, 2, 3>> R{"value123"}max=? [F time=2]
// at time 3
<<1, 2, 3>> R{"value123"}max=? [F time=3]
// at time 4
<<1, 2, 3>> R{"value123"}max=? [F time=4]
</code></pre>
