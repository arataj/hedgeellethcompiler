<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Households.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">static</span> Household<span style="color: #666666">[]</span> households<span style="color: #666666">;</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Current time or interval.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">int</span><span style="color: #60a0b0; font-style: italic">/*@(0, MAX_TIME)*/</span> time<span style="color: #666666">;</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Current number of loads generated.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">int</span><span style="color: #60a0b0; font-style: italic">/*@(0, NUM_HOUSEHOLDS)*/</span> numJobs<span style="color: #666666">;</span>
</pre></td></tr></table></div>
