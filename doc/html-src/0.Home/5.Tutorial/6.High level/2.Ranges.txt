An allowable range of values of a variable or a primary expression can be
defined within a comment.
Thanks to this, the model can still be a valid Java program. A range contains two
expressions, representing respectively a minimum and a maximum allowed value.
Such an expression can refer to any variable in a given scope, but <code>Verics</code> must
be able do evaluate the expression into a constant value, after the main
thread ceases to modify the application. For that end, the compiler performs
a number of optimisations in order to replace expressions with constants.

Ranges on variables are roughly translated into ranges on state variables in the output
file -- if <code>Verics</code> can not verify by itself, that a given limit won't be violated,
it widens a state variable's range, so that it can generate a PCTL property
to check for any violations of that limit. Ranges on expressions, due to their locality, are translated
into sub--automatons. An invalid value leads to an error state.

