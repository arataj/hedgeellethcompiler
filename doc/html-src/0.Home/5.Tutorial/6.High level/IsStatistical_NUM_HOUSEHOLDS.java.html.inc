<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">int</span> NUM_HOUSEHOLDS <span style="color: #666666">=</span>
    Model<span style="color: #666666">.</span><span style="color: #4070a0">isStatistical</span><span style="color: #666666">()</span> <span style="color: #666666">?</span> <span style="color: #40a070">5</span> <span style="color: #666666">:</span> <span style="color: #40a070">3</span><span style="color: #666666">;</span>
            
</pre></td></tr></table></div>
