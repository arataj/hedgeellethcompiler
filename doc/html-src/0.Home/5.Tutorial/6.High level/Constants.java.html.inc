<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10</pre></td><td><pre style="margin: 0; line-height: 125%">formula measure <span style="color: #666666">=</span> <span style="color: #666666">(</span>s3<span style="color: #666666">=</span><span style="color: #40a070">8</span><span style="color: #666666">);</span>

<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">int</span> DAYS <span style="color: #666666">=</span> <span style="color: #40a070">3</span><span style="color: #666666">;</span>
<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">int</span> EXPECTED_JOBS <span style="color: #666666">=</span> <span style="color: #40a070">9</span><span style="color: #666666">;</span>
<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">int</span> INTERVALS <span style="color: #666666">=</span> <span style="color: #40a070">16</span><span style="color: #666666">;</span>
<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">int</span> MAX_JOB_TIME <span style="color: #666666">=</span> <span style="color: #40a070">4</span><span style="color: #666666">;</span>
<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">int</span> MAX_TIME <span style="color: #666666">=</span> <span style="color: #40a070">48</span><span style="color: #666666">;</span>
<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">int</span> NUM_HOUSEHOLDS <span style="color: #666666">=</span> <span style="color: #40a070">3</span><span style="color: #666666">;</span>
<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">double</span> PRICE_LIMIT <span style="color: #666666">=</span> <span style="color: #40a070">1.5</span><span style="color: #666666">;</span>
<span style="color: #007020; font-weight: bold">const</span> <span style="color: #902000">double</span> P_OVER_LIMIT <span style="color: #666666">=</span> <span style="color: #40a070">0.8</span><span style="color: #666666">;</span>
</pre></td></tr></table></div>
