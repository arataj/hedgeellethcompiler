<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">final</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">boolean</span> NON_DETERMINISTIC <span style="color: #666666">=</span> <span style="color: #007020; font-weight: bold">false</span><span style="color: #666666">;</span>

<span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">final</span> Random strategy<span style="color: #666666">;</span>
</pre></td></tr></table></div>
