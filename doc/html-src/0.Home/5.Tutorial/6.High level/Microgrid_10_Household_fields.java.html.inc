<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22
23</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Maximum time of running a single job, in intervals.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">final</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">int</span> MAX_JOB_TIME <span style="color: #666666">=</span> <span style="color: #40a070">4</span><span style="color: #666666">;</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Expected number of jobs per day.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">final</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">int</span> EXPECTED_JOBS <span style="color: #666666">=</span> <span style="color: #40a070">9</span><span style="color: #666666">;</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Price limit, above which this household may</span>
<span style="color: #60a0b0; font-style: italic"> * back--off.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">final</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">double</span> PRICE_LIMIT <span style="color: #666666">=</span> <span style="color: #40a070">1.5</span><span style="color: #666666">;</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Probability of starting a task independently of the cost.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">final</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">double</span> P_OVER_LIMIT <span style="color: #666666">=</span> <span style="color: #40a070">0.8</span><span style="color: #666666">;</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * A running job, in intervals.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #902000">int</span><span style="color: #60a0b0; font-style: italic">/*@(0, MAX_JOB_TIME)*/</span> job <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span>

<span style="color: #007020; font-weight: bold">protected</span> <span style="color: #007020; font-weight: bold">final</span> Random strategy<span style="color: #666666">;</span>
</pre></td></tr></table></div>
