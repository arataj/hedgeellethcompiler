<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%"> 1
 2
 3
 4
 5
 6
 7
 8
 9
10
11
12
13
14
15
16
17
18
19
20
21
22</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Returns the current load demand.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">double</span> <span style="color: #06287e">getDemand</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
    <span style="color: #007020; font-weight: bold">return</span> DEMAND<span style="color: #666666">[</span>time<span style="color: #666666">%</span>INTERVALS<span style="color: #666666">];</span>
<span style="color: #666666">}</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * How many households currently generate a load.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">int</span> <span style="color: #06287e">getNumJobs</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
    <span style="color: #902000">int</span><span style="color: #60a0b0; font-style: italic">/*@(0, NUM_HOUSEHOLDS)*/</span> numJobs <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span>
    <span style="color: #007020; font-weight: bold">for</span><span style="color: #666666">(</span><span style="color: #902000">int</span> i <span style="color: #666666">=</span> <span style="color: #40a070">0</span><span style="color: #666666">;</span> i <span style="color: #666666">&lt;</span> NUM_HOUSEHOLDS<span style="color: #666666">;</span> <span style="color: #666666">++</span>i<span style="color: #666666">)</span>
        <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(</span>households<span style="color: #666666">[</span>i<span style="color: #666666">].</span><span style="color: #4070a0">job</span> <span style="color: #666666">&gt;</span> <span style="color: #40a070">0</span><span style="color: #666666">)</span>
            <span style="color: #666666">++</span>numJobs<span style="color: #666666">;</span>
    <span style="color: #007020; font-weight: bold">return</span> numJobs<span style="color: #666666">;</span>
<span style="color: #666666">}</span>
<span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * A price of a load to be bought.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #007020; font-weight: bold">static</span> <span style="color: #902000">double</span> <span style="color: #06287e">getPrice</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
    <span style="color: #007020; font-weight: bold">return</span> <span style="color: #06287e">getNumJobs</span><span style="color: #666666">()</span> <span style="color: #666666">+</span> <span style="color: #40a070">1.0</span><span style="color: #666666">;</span>
<span style="color: #666666">}</span>
</pre></td></tr></table></div>
