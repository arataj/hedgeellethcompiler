<!-- HTML generated using hilite.me --><div style="background: #f0f0f0; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .1em;padding:.2em .6em;"><table><tr><td><pre style="margin: 0; line-height: 125%">1
2
3
4
5
6
7</pre></td><td><pre style="margin: 0; line-height: 125%"><span style="color: #60a0b0; font-style: italic">/**</span>
<span style="color: #60a0b0; font-style: italic"> * Called by the scheduler after each turn.</span>
<span style="color: #60a0b0; font-style: italic"> */</span>
<span style="color: #007020; font-weight: bold">public</span> <span style="color: #902000">void</span> <span style="color: #06287e">tick</span><span style="color: #666666">()</span> <span style="color: #666666">{</span>
    <span style="color: #007020; font-weight: bold">if</span><span style="color: #666666">(</span>job <span style="color: #666666">&gt;</span> <span style="color: #40a070">0</span><span style="color: #666666">)</span>
      <span style="color: #666666">--</span>job<span style="color: #666666">;</span>
<span style="color: #666666">}</span>
</pre></td></tr></table></div>
