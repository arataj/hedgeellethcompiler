<p>
    A simple example: let there be a turnaround specified in a not globally synchronised
    MDP:
</p>

<p>
    <center><img src="turnaround.png" /></center>
<pre>
    class mdp;
   
    module train1 { 
        Z{W, E} location := W; 

        []  location == W -> location := E;
    };
    module train2 { 
        Z{W, E} location := E; 

        []  location == E -> location := W;
    };
</pre>
</p>

<p>
    If we would analyse a property of this network whether the two trains ever ride through
    the turnaround at the same time, the answer would be no: Train 1 passes through
    the turnaround always before or after Train 2. No problem, the global lock
    discussed provides that. In reality, though, a naive implementation of the model,
    i.e. only of what is <i>explicitly</i> given in the model source, would have
    the same property true.
</p>

<p>
    While in this simple model the issue is obvious, and also it is easy to remember,
    that the global lock inherent to the model's class should be implemented as well,
    this may become more complex in larger, more complicated models, including the
    implementation of the global lock, which might be much more complex than needed. One
    of the advantages of using a model checker is to found out issues like that in
    the turnaround model and
    warn us, that we should redesign the model. We may choose a synchronous class
    just for that: to make the model checker help up in a precise design of all the
    anti-clash mechanisms required.
</p>
