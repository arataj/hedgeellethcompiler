<p>
	Contents:
	<ul>
	<!--li>
		<a href="Compiler.html">Compiler</a>;
	</li-->
	<li>
		basics of <a href="Synchronisation/index.html">synchronisation</a>;
	</li>
	<li>
		an introductory <a href="Simple example/index.html">example</a>;
	</li>
	<li>
		<a href="Protocols/index.html">protocols</a> for a flexible
                specification of the way of synchronisation;
	</li>
	<li>
		<a href="Conversions/index.html">conversions</a> for a shorter and
                more intuitive specification of the model;
	</li>
	<li>
		how to create a <a href="Architecture/index.html">graph of
                the architecture</a> of the modelled system;
	</li>
	<li>
		some <a href="Source files/index.html">example models</a>.
	</li>
	</ul>
</p>
