/*
 * Camera.java
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package models.mirela.aadl;

import hc.Sleep;
import mirela.*;

/**
 *
 * @author Artur Rataj
 */
public class Camera extends PeriodicThread {
    /**
     * Exact time of acquisition.
     */
    protected double acqTime;
    /**
     * Maximum time of compressing a frame.
     */
    protected double compressMaxTime;
    
    /**
     * Creates a new camera.
     * 
     * @param system system
     * @param start a warm-up period; in order to fully randomise
     * the phase, make it equal to <code>period</code>
     * @param period a period between two frames
     * @param acqTime exact time of acquisition
     * @param sendMaxTime maximum time of sending a frame; depends of
     * the frame's complexity
     */
    public Camera(Mirela system, Delay start, double period,
            double acqTime, double sendMaxTime) {
        super(system, start, period);
        this.acqTime = acqTime;
        this.compressMaxTime = sendMaxTime;
    }
    @Override
    public void periodic() {
        Sleep.exact(acqTime);
        Sleep.max(compressMaxTime);
        produce();
    }
}
