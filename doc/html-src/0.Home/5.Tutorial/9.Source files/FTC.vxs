//
// Test "Faulty train protocol" for any number of trains 
// 
class pomdp sync;

// number of trains 
common Z NUM := 3;

environment { 
    Z{GREEN, RED} lights := GREEN; 

#for Z i~ := 1; i <= .NUM - 1; ++i { 
    [out${i}] lights == RED   -> lights := GREEN; 
    [in${i}]  lights == GREEN -> lights := RED; 
#} 
};

#for Z i~ := 1; i <= .NUM; ++i { 
agent train${i} { 
    Z{WAIT, TUNNEL, AWAY} phase := AWAY; 

    []              true -> true; 
    [approach${i}]  phase == AWAY -> phase := WAIT; 
    // the train <code>NUM</code> is faulty
    [${"in"  + i}]  phase == WAIT   -> phase := TUNNEL; 
    [${"out" + i}]  phase == TUNNEL -> phase := AWAY; 
};

def in_tunnel${i} := train${i}.phase == TUNNEL; 

#} 
property CTL*K test_train1 := !AG(K(train1,
#B first~ := T;
#for Z a~ := 1; a <= .NUM; ++a {
#    for Z b~ := 1; b <= .NUM; ++b { 
#        if a < b  { 
             ${first ? "" else "&&"} (!in_tunnel${a} || !in_tunnel${b}) 
#            first := F;
#        }
#    }
# }
)); 
