/*
 * VideoProcessing.java
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package models.mirela.aadl;

import java.util.*;

import hc.Sleep;
import mirela.*;

/**
 *
 * @author Artur Rataj
 */
public class VideoProcessing extends PeriodicThread {
    /**
     * Maximum processing time of a single frame.
     */
    protected double calculationMaxTime;
    
    /**
     * Creates a new video processor.
     * 
     * @param system system
     * @param start a warm-up period; in order to fully randomise
     * the phase, make it equal to <code>period</code>
     * @param period a period between two incoming frames
     * @param calculationMaxTime maximum time of processing
     * a single frame
     */
    public VideoProcessing(Mirela system, Delay start, double period,
            double calculationMaxTime) {
        super(system, start, period);
        this.calculationMaxTime = calculationMaxTime;
    }
    @Override
    public void periodic() {
        sync.consume();
        Sleep.max(calculationMaxTime);
        produce();
    }
}
