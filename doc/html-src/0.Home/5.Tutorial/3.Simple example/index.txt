<p>
    Let us start with a simple synchronous
    <a href="Multi Agent System.html">multi&ndash;agent system</a>.
    Thanks to the synchronicity, there is not a global mechanism, which prevents several
    automatons from performing an action at exactly the same time by executing a synchronised
    statement, see $A(Synchronisation) for details.
</p>

<p>
    Let there be three trains which share a common track in a narrow tunnel
    at a certain part of their otherwise disjoint routes.
    We need a signalling system which prevents two or more trains from entering the common segment at the same time.
    The signalling is provided by a special automaton <code>Environment</code>. There is only a single
    environment in a system of agents, and the environment differs from an agent automaton in that
    its state can be made visible anywhere.
</p>

<p>
    Let us declare, that we define an <emph>interpreted system</emph>, which is a kind of a
    synchronous partially observable Markov decision process:
<pre>
    //
    // Test "Faulty train protocol" for any number of trains.
    //
    class pomdp sync;
</pre>
    An asynchronous equivalent, i.e. an <emph>interleaved interpreted system</emph>, does not take
    the <code>sync</code> keyword.
</p>

<p>
    The Verics' assembly language is mostly related 1:1 to the automata generated, and thus,
    in order to vary the number
    of trains at the change of a constant value, we need a preprocessor. The preprocessor is simply
    an interpreter of the same <a href="../Verics language/index.html">Verics language</a> used elsewhere
    within the Verics environment.
</p>

<p>
    Let us declare the said constant:
<pre>
    // number of trains
    common Z NUM := 3;
</pre>
    A constant like that, if precedes all preprocessor directives, is visible by both the preprocessor
    and the automatons.
</p>

<p>
    Let the environment feature a lock (a railway signal, a semaphore),
    toggled to red if one of the trains arrives at
    the common track. If the train leaves the track, the semaphore is switched back to green.
<pre>
    environment {
        Z{GREEN, RED} lights := GREEN;

    <b>#for Z i~ := 1; i <= NUM - 1; ++i {</b>
        [out<b>${i}</b>] lights == RED   -> lights := GREEN;
        [in<b>${i}</b>]  lights == GREEN -> lights := RED;
    <b>#}</b>
    };
</pre>
    Let's look at the same definition, yet expanded by the preprocessor:
<pre>
    environment {
        Z{GREEN, RED} lights := GREEN;

        [out1] lights == RED   -> lights := GREEN;
        [in1]  lights == GREEN -> lights := RED;
        [out2] lights == RED   -> lights := GREEN;
        [in2]  lights == GREEN -> lights := RED;
    };
</pre>
    The identifiers in square brackets define labels, here the <emph>action</emph> ones
    (without <code>sync</code>, they would be synchronisation labels). Two automatons may
    synchronise, if they perform the same action. Then we have boolean equations,
    which <emph>guard</emph> the following <emph>updates</emph> to the state vector.
    A false guard unconditionaly prevents an action from happening, a true guard does
    not force by itself the update.
</p>

<p>
    We see, that the environment can perform two actions <code>environment.in1</code> and
    <code>environment.in2</code>, that allow the trains 1 and 2 to enter. Yet, we defined
    <code>NUM := 3</code>, and thus, what about the third train? It is faulty, and ignores
    the environment's actions completely, what translates to its ignorance about the
    tunnel's semaphore.
</p>

<p>
    Let the definiton of all trains be as follows:
<pre>
    <b>#for Z i~ := 1; i <= NUM; ++i {</b>
    agent train<b>${i}</b> { 
        Z{WAIT, TUNNEL, AWAY} phase := AWAY;

        []              true -> true;
        [approach<b>${i}</b>]  phase == AWAY   -> phase := WAIT;
        // the train <code>NUM</code> is faulty
        [<b>${"in"  + i}</b>]  phase == WAIT   -> phase := TUNNEL;
        [<b>${"out" + i}</b>]  phase == TUNNEL -> phase := AWAY;  
    };

    def in_tunnel<b>${i}</b> := train<b>${i}</b>.phase == TUNNEL;

    <b>#}</b>

</pre>    
    We see, that all trains are the same including the faulty one. The difference is,
    that the faulty train's
    actions <code>train3.in3</code> and <code>train3.out3</code> do not have their counterparts
    in the environments, i.e. there are no actions <code>environment.in3</code> and
    <code>environment.out3</code>.

    The statement beginning with <code>def</code> is a formula, which will allow
    a more terse property definition later.
</p>

<p>
    Let us model&ndash;check the model. The engineer at the train 1 would want to be always
    assured, that no crash is possible:
<pre>
    property CTL*K test_train1 := !AG(K(train1,
    <b>#B first~ := T;
    #for Z a~ := 1; a <= NUM; ++a {
    #    for Z b~ := 1; b <= NUM; ++b {
    #        if a < b  {</b>
                 <b>${first ? "" else "&&"}</b> (!in_tunnel<b>${a}</b> || !in_tunnel<b>${b}</b>)
    <b>#            first := F;
    #        }
    #    }
    # }</b>
    )); 
</pre>
which is expanded by the preprocessor into
<pre>
    property CTL*K test_train1 := !A G(K(train1,
                    (!in_tunnel1 || !in_tunnel2)
                 && (!in_tunnel1 || !in_tunnel3)
                 && (!in_tunnel2 || !in_tunnel3)
    ));
</pre>
</p>

<p>
<br>Files:
<ul>
<li><a href="../Source files/FTC.vxs">FTC.vxs</a>: the source of the model;</li>
<li><a href="../Source files/FTC.ispl">FTC.ispl</a>: the resulting ISPL file.</li>
</ul>
</p>

<p>
<br>Command line:
<ul>
<li><code>veric -L -oi FTC.vxs</code></li>
</ul>
</p>
