<p>
    Hc is variant of the <a href="Compiler.html">Hedgeelleth compiler</a>,
    that generates models for various model checkers, given a specification
    in a subset of the Java language. The subset is:
    <ul>
        <li>chosen in order to balance
            grammar complexity and assumed needs of moderate size models;
        </li>
        <li>completed with tags in comments, that define ranges on variables
            and expressions, while not making the language incompatible with
            standard Java compilers.
        </li>
    </ul>
</p>

<p>
    Hc aims at the model specification being <span class="emph">easy, readable and scalable</span>.
    These qualities, beside helping model creation, may <span class="emph">reduce bugs</span>,
    and in effect make the checked property values more <span class="emph">reliable</span>.
</p>

<p>
    <center><img src="hc.png"/></center>
</p>

<p>
    When designing hc, the following assumptions were made:
    <ul>
        <li>write models in an <span class="emph">advanced, common language</span>;</li>
        <li>possibility of having a <span class="emph">common source</span> for the implementation and for the model;</li>
        <li>support for <span class="emph">a number of model checkers</span>, but also for a plain
            Java <span class="emph">virtual machine</span>, that can straightforwardly run the model;
        <li>hooks for customising, <span class="emph">how the model is run</span> in the JVM,
            so that engines like custom optimizers can directly interact with the model run;</li>
        <li><span class="emph">ranges</span> on variables and expressions enable <span class="emph">testing of
            self&ndash;consistency</span> of the model, also
            <span class="emph">helps checker engines</span>;</li>
        <li>clearly discerned <span class="emph">probability and non&ndash;determinism</span>;</li>
        <li>support for <span class="emph">clocks</span>;</li>
        <li>synchronisation using either simple <span class="emph">conditional barriers</span> or
            <span class="emph">semaphores</span> with a standard set of features, like
            synchronised blocks;</li>
        <li>players and coalitions for modelling <span class="emph">multi&ndash;agent systems</span>;</li>
        <li>a special <span class="emph">thread just for creating and connecting</span>
            components of the model,
            as well as for <span class="emph">generation of the properties</span> to check;</li>
        <li><span class="emph">schedulers</span> &ndash; take advantage of not specifying directly the
            automatons, and generate them by applying a set of execution
            rules to the model defined.</li>
    </ul>
</p>

<p>
    An attached library adds features like barrier synchronisation, or simplifies the
    definition of certain types of models. The library, in the variant for a Java virtual machine,
    defines counterparts that, depending on their purpose, perform the same action,
    are invisible or user&ndash;definable.
</p>

<p>
    A <a href="hc-tutorial.pdf">short tutorial</a> on hc.
</p>
