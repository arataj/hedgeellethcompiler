export DOCUMENT=tutorial

latex $DOCUMENT.tex && \
bibtex $DOCUMENT && \
latex $DOCUMENT.tex && \
dvips -tS4 -o$DOCUMENT.eps $DOCUMENT.dvi && \
ps2pdf $DOCUMENT.eps && \
mv $DOCUMENT.pdf ../
