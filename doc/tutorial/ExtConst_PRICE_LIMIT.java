/**
 * Price limit, above which this household may
 * back--off.
 */
protected final static double PRICE_LIMIT =
    Model.doubleConst("PRICE_LIMIT");

