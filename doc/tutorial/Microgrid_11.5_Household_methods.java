/**
 * Called by the scheduler after each turn.
 */
public void tick() {
    if(job > 0)
      --job;
}
