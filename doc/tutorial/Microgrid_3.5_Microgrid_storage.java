/**
 * Households.
 */
protected static Household[] households;
/**
 * Current time or interval.
 */
public static int/*@(0, MAX_TIME)*/ time;
/**
 * Current number of loads generated.
 */
public static int/*@(0, NUM_HOUSEHOLDS)*/ numJobs;
