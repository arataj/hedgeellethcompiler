@Override
public void run() {
    while(true) {
        turnWait();
        if(isInterrupted())
            break;
        if(job == 0 && Math.random() < Microgrid.getDemand()*
              EXPECTED_JOBS) {
            // decide if to generate a load
            boolean lowPrice = Microgrid.getPrice() < PRICE_LIMIT;
            if(lowPrice || Math.random() < P_OVER_LIMIT ||
                    // a right to back--off is granted; decide,
                    // if to generate a load anyway
                    strategy.nextInt(2) == 0)
                job = 1 + (int)(Math.random()*MAX_JOB_TIME);
        }
        turnNext(0);
    }
}
