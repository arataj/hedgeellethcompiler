public static void endInterval() {
    numJobs = getNumJobs();
    Model.stateAnd("measure");
    if(!Model.isStatistical())
        numJobs = 0;
    // reduce job counters
    for(int i = 0; i < Microgrid.NUM_HOUSEHOLDS; ++i)
          households[i].tick();
    ++time;
}

