/**
 * If <code>MAX_TIME</code> has been reached.
 *
 * @return if the simulation is finished
 */
public static boolean isFinished() {
    return time == MAX_TIME;
}
/**
 * Called by the scheduler at the end of each interval.
 *
 */
public static void endInterval() {
    numJobs = getNumJobs();
    Model.stateAnd("measure");
    numJobs = 0;
    // reduce job counters
    for(int i = 0; i < Microgrid.NUM_HOUSEHOLDS; ++i)
          households[i].tick();
    ++time;
}
