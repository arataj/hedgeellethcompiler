formula measure = (s3=8);

const int DAYS = 3;
const int EXPECTED_JOBS = 9;
const int INTERVALS = 16;
const int MAX_JOB_TIME = 4;
const int MAX_TIME = 48;
const int NUM_HOUSEHOLDS = 3;
const double PRICE_LIMIT = 1.5;
const double P_OVER_LIMIT = 0.8;
