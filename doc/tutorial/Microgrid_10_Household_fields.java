/**
 * Maximum time of running a single job, in intervals.
 */
protected final static int MAX_JOB_TIME = 4;
/**
 * Expected number of jobs per day.
 */
protected final static int EXPECTED_JOBS = 9;
/**
 * Price limit, above which this household may
 * back--off.
 */
protected final static double PRICE_LIMIT = 1.5;
/**
 * Probability of starting a task independently of the cost.
 */
protected final static double P_OVER_LIMIT = 0.8;
/**
 * A running job, in intervals.
 */
public int/*@(0, MAX_JOB_TIME)*/ job = 0;

protected final Random strategy;
