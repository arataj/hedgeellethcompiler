/**
 * A microgrid demand--side management game, as described
 * in [1].
 *
 * [1] H. Hildmann and F. Saffre. Influence of variable
 * supply and load flexibility on Demand-Side Management.
 * In Proc. 8th International Conference on the European
 * Energy Market (EEM'11), pages 63-68. 2011.
 */
public class Microgrid
