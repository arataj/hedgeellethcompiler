@Override
public void run() {
    do {
        // select a household
        int address;
        if(NON_DETERMINISTIC)
            // non--deterministic choice
            address = strategy.nextInt(Microgrid.
                NUM_HOUSEHOLDS);
        else
            // probabilistic choice
            address = (int)
                (Math.random()*Microgrid.NUM_HOUSEHOLDS);
        // contact the selected household
        turnNext(1 + address);
        turnWait();
        // end of the current round
        Microgrid.endInterval();
    } while(!Microgrid.isFinished());
    Model.finish();
}
