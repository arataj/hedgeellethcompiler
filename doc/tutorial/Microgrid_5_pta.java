/**
 * Returns the current load demand.
 */
public static double getDemand() {
    return DEMAND[time%INTERVALS];
}
/**
 * How many households currently generate a load.
 */
public static int getNumJobs() {
    int/*@(0, NUM_HOUSEHOLDS)*/ numJobs = 0;
    for(int i = 0; i < NUM_HOUSEHOLDS; ++i)
        if(households[i].job > 0)
            ++numJobs;
    return numJobs;
}
/**
 * A price of a load to be bought.
 */
public static double getPrice() {
    return getNumJobs() + 1.0;
}
