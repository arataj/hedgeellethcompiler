/**
 * Number of days.
 */
public static final int DAYS = 3;
/**
 * Number of intervals per day.
 */
public static final int INTERVALS = 16;
/**
 * Number of rounds.
 */
public static final int MAX_TIME = DAYS*INTERVALS;

/**
 * Demand curve.
 */
public static final double[] DEMAND = {
    0.0614, 0.0392, 0.0304, 0.0304,
    0.0355, 0.0518, 0.0651, 0.0643,
    0.0625, 0.0618, 0.0614, 0.0695,
    0.0887, 0.1013, 0.1005, 0.0762,
};
/**
 * Number of households.
 */
public static final int NUM_HOUSEHOLDS = 3;
