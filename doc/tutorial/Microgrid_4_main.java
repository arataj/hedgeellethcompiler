public static void main(String[] args) {
    // player ids at the table: 0 scheduler,
    // 1..NUM_HOUSEHOLDS households
    TurnGame table = new TurnGame();
    Model.name(table);
    // create an array of households
    households = new Household[NUM_HOUSEHOLDS];
    for(int i = 0; i < NUM_HOUSEHOLDS; ++i) {
        Household h = new Household(table, 1 + i);
        h.start();
        Model.name(h, "", "" + (i + 1));
        Model.player(h, "h" + (i + 1));
        households[i] = h;
    }
    // create the scheduler
    Scheduler s = new Scheduler(table);
    s.start();
    Model.name(s);
    Model.player(s, "scheduler");
    // reset time
    time = 0;
    // players consist of the scheduler and of the
    // households
    table.start(1 + NUM_HOUSEHOLDS);
    for(int t = 1; t <= MAX_TIME/4; ++t)
        Model.check("at time " + t,
            "<<1, 2, 3>> R{\"value123\"}max=? " +
            "[F time=" + t + "]");
    Model.waitFinish();
    // anything below won't be executed by hc, but
    // will within JVM
    System.out.println("time = " + time);
    // releases all threads still waiting on the
    // barrier
    households[0].interrupt();
}
