export DOCUMENT=tutorial-sync 

pdflatex $DOCUMENT.tex && \
bibtex $DOCUMENT && \
pdflatex $DOCUMENT.tex && \
dvipdf -tS4 -o$DOCUMENT.pdf $DOCUMENT.dvi && \
mv $DOCUMENT.pdf ../
