/*
 * Regression tests of the Java2TADD compiler.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.java2tadd;

import java.util.*;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.cli.OptionManager;
import pl.gliwice.iitis.hedgeelleth.cli.TestOptions;
import pl.gliwice.iitis.hedgeelleth.cli.Compiler;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp.CPPBackend;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.NdFileIO;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.Scheduler;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.TADDOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.TADDOptions.DocOutputFormat;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.TestAssemblyBuffer;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.prism.PrismIO;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.*;
import pl.gliwice.iitis.hedgeelleth.frontend.java.JavaFrontend;
import pl.gliwice.iitis.hedgeelleth.interpreter.AbstractInterpreter;
import pl.gliwice.iitis.hedgeelleth.modules.HedgeellethCompiler;
import pl.gliwice.iitis.hedgeelleth.modules.MessageStyle;
import pl.gliwice.iitis.hedgeelleth.pta.*;
import pl.gliwice.iitis.hedgeelleth.pta.io.PTAIO;

/**
 * Regression tests of the compiler.<br>
 *
 * See <code>compile()</code> for details on how this class can be safely
 * run.
 *
 * @author Artur Rataj
 */
public class CompilerTest extends AbstractCompilerTest {
    /**
     * Default test options.
     */
    final TestOptions DEFAULT_TEST_OPTIONS = new TestOptions();
    /**
     * A scheduler for turn games, where no two automatons can
     * move at once.
     */
    final static Scheduler TURN_GAME_SCHEDULER = new Scheduler(
            SchedulerType.HIDDEN, true, false, Double.NaN, Double.NaN,
            true, true, false);

    public CompilerTest() {
        super();
    }
    /**
     * Normally just compares two strings. In special cases (bulk pattern
     * update, enabled momentarily only by the programmer) ignores certain
     * differences, overwrites the pattern file with the new version.
     * 
     * @param outputContent output
     * @param patternContent pattern
     * @param patternFile pattern file
     */
    private boolean compareOutput(String outputContent, String patternContent,
            File patternFile) {
        if(true) {
            // normal
            return outputContent.equals(patternContent);
        } else {
            int n = outputContent.length();
            if(n != patternContent.length())
                return false;
            boolean diff = false;
            for(int i = 0; i < n; ++i) {
                char o = outputContent.charAt(i);
                char p = patternContent.charAt(i);
                if(o != p) {
                    if(i > 0 && o == 't' && p == 's') {
                        if(outputContent.charAt(i - 1) == '#' &&
                                patternContent.charAt(i - 1) == '#') {
                            diff = true;
                            continue;
                        }
                    }
                    return false;
                }
            }
            if(diff) {
                try {
                    System.out.println("### PATTERN UPDATE ### " + patternFile);
                    CompilerUtils.save(patternFile.getAbsolutePath(), outputContent);
                } catch(IOException e) {
                    throw new RuntimeException("problem updating pattern: " + e.getMessage());
                }
            }
            return true;
        }
    }
    /**
     * Tests how the compiler behaves when compiling a given set of files,
     * against an error pattern.<br>
     *
     * // This method adds to the list of compiled also the files in the
     * // directory <code>library</code>.<br>
     *
     * The pattern file name can either contain internal assembly code
     * of the TADD's inline method or be in the format or Verics, Uppaal,
     * XML or Prism.<br>
     *
     * As this method calls <code>UniquePrefix.reset()</code>
     * and <code>AbstractInterpreter.resetSerialCounters()<code> for repeatable output files.
     * It is thus not reentrant, and it is not safe  to invoke this method if any interpreters are simultaneously
     * being.<br>
     *
     * If experiments are allowed and the output files are compared to pattern files,
     * only UPPAAL code can be verified, and default pattern file names are used,
     * so <code>patternFileName</code> must be null.
     *
     * @param directory                 top directory of the compilation,
     * @param files                     files to compile
     * @param pattern                   error pattern, as defined in
     *                                  <code>errorEquals</code>, or null
     *                                  if the compilation is expected
     *                                  to be successful
     * @param patternFileName           name of a pattern output file to compare,
     *                                  null or "cpp" if the compilation is
     *                                  expected to fail; "cpp" if the C++
     *                                  backend should be used instead of the
     *                                  TADD one; not null and not "cpp" if and
     *                                  only if a specific file name to compare
     *                                  must be given, that is, if
     *                                  <code>pattern</code> is null and
     *                                  <code>allowExperiments</code> is false --
     *                                  the backend is then determined by using
     *                                  the file name's extension
     * @param testOptions               options that modify the compiler's
     *                                  behaviour; null for default
     * @param scheduler                 a scheduler, null for the default one
     */
    protected void compile(PWD directory, String[] files,
            CompilerException pattern, String patternFileName,
            TestOptions testOptions, Scheduler scheduler) {
        if(testOptions == null)
            testOptions = DEFAULT_TEST_OPTIONS;
        OptionManager.OutputFormat of;
        String assemblerFileName = null;
        boolean forceCPPBackend;
        if(patternFileName != null && patternFileName.equals("cpp")) {
            forceCPPBackend = true;
            patternFileName = null;
        } else
            forceCPPBackend = false;
        if(testOptions.experimentMode ==
                HedgeellethCompiler.Options.ExperimentMode.ADAPT)
            throw new RuntimeException("experiment mode ADAPT not allowed " +
                    "in compiler testing");
        if(patternFileName != null) {
            // check against a file
            if(testOptions.experimentMode ==
                    HedgeellethCompiler.Options.ExperimentMode.TRUE)
                throw new RuntimeException("default pattern file names are used if " +
                    "experiments are allowed, but file pattern is not null");
            String extension = CompilerUtils.getFileNameExtension(
                    patternFileName, 2);
            if(extension.equals("tadd" + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.VERICS;
            } else if(extension.equals("xml" + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.UPPAAL;
            } else if(extension.equals("pxml" + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.XML;
            } else if(extension.equals(PrismIO.PRISM_MODEL_EXTENSION + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.PRISM;
            } else if(extension.equals(PrismIO.HEDGEELLETH_MODEL_EXTENSION + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.HEDGEELLETH;
            } else if(extension.equals("s0" + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.ASSEMBLER0;
                assemblerFileName = patternFileName.substring(0,
                        patternFileName.length() - (extension.length() + 1)) +
                        ".s0";
            } else if(extension.equals("s" + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.ASSEMBLER;
                assemblerFileName = patternFileName.substring(0,
                        patternFileName.length() - (extension.length() + 1)) +
                        ".s";
            } else if(extension.equals("h" + REGRESSION_STRING)) {
                of = OptionManager.OutputFormat.CPP;
//                implementationFileName = patternFileName.substring(0,
//                        patternFileName.length() - (extension.length() + 1)) +
//                        ".cpp" + REGRESSION_STRING;
            } else
                throw new RuntimeException("unknown pattern file extension");
        } else {
            if(testOptions.experimentMode ==
                    HedgeellethCompiler.Options.ExperimentMode.TRUE) {
                if(pattern == null)
                    // check against a set of UPPAAL files
                    of = OptionManager.OutputFormat.UPPAAL;
                else
                    // check against translation errors
                    of = OptionManager.OutputFormat.NONE;
            } else
                // check against translation errors
                if(forceCPPBackend)
                    of = OptionManager.OutputFormat.CPP;
                else
                    of = OptionManager.OutputFormat.NONE;
        }
        if(testOptions.forceFomat != null)
            if(testOptions.forceFomat== TADDOptions.OutputFormat.PRISM)
                of = OptionManager.OutputFormat.PRISM;
            else if(testOptions.forceFomat== TADDOptions.OutputFormat.HEDGEELLETH)
                of = OptionManager.OutputFormat.HEDGEELLETH;
            else
                throw new RuntimeException("can not force output format " + of.toString());
        System.out.println("TEST:");
        System.out.print("\tin " + directory.toString() + "\n\tfiles");
        for(String s : files)
            System.out.print(" " + s);
        System.out.print("\n\tpattern");
        if(pattern == null) {
            if(patternFileName == null) {
                if(testOptions.experimentMode ==
                        HedgeellethCompiler.Options.ExperimentMode.TRUE)
                    System.out.println(" <standard pattern file names>");
                else
                    throw new RuntimeException("missing pattern file name");
            } else
                System.out.println(" " + patternFileName);
        } else {
            if(patternFileName != null)
                throw new RuntimeException("unexpected pattern file name");
            System.out.print("\n" +
                    CompilerUtils.indent(2, pattern.toString(), "\t"));
        }
        OptionManager om = new OptionManager(of, DocOutputFormat.NONE, testOptions);
        switch(of) {
            case ASSEMBLER:
            case ASSEMBLER0:
            case CPP:
            case UPPAAL:
            case XML:
            // defaults to XML output
            case NONE:
                // supports only arrays
                om.pta.arraySupport = TADDOptions.ArraySupport.INDEX;
                break;
                
            case VERICS:
                // supports neither arrays nor their emulation
                om.pta.arraySupport = TADDOptions.ArraySupport.NONE;
                break;
                        
            case PRISM:
                // supports only array emulation
                om.pta.arraySupport = TADDOptions.ArraySupport.EMULATE;
                break;
                        
            case HEDGEELLETH:
                // supports both arrays and their emulation
                break;
        }
        if(scheduler != null)
            om.pta.scheduler = scheduler;
        if(testOptions.forceLabelDeterminism)
            om.pta.labelNondeterminism = false;
        om.pta.reducedLongRange = testOptions.reducedLongRange;
        if(testOptions.tuneToTADDs != null)
            om.hedgeelleth.optimizerTuning.tuneToTADDs = testOptions.tuneToTADDs;
        List<String> filesList = new LinkedList<>();
        filesList.addAll(Arrays.asList(files));
        List<String> outputFileNames = null;
        CompilerException error = null;
        TestAssemblyBuffer assemblyBuffer = new TestAssemblyBuffer();
        try {
            UniquePrefix.reset();
            AbstractInterpreter.resetSerialCounters();
            HedgeellethCompiler.TranslationSummary summary =
                    Compiler.translate(directory, filesList,
                    om.hedgeelleth, om.pta, om.cpp, assemblyBuffer,
                    Compiler.getCompilerLibraryDirectory(Compiler.class,
                        System.getProperty("user.dir") + "/" + directory));
            outputFileNames = summary.OUT_FILES;
        } catch(CompilerException e) {
            error = e;
        }
        /* error.toCodeString() */ 
        errorEquals(error, pattern);
        if(error == null) {
            NEXT_FILE:
            for(String outputFileName : outputFileNames) {
                // System.out.println("OUTPUT FILENAME = `" + outputFileName + "'");
                String outputContent;
                try {
                    switch(of) {
                        case ASSEMBLER0:
                        case ASSEMBLER:
                            if(outputFileName.endsWith(".pctl"))
                                continue;
                            switch(of) {
                                case ASSEMBLER0:
                                    outputContent = assemblyBuffer.inline0;
                                    break;

                                case ASSEMBLER:
                                    outputContent = assemblyBuffer.inline;
                                    break;

                                default:
                                    throw new RuntimeException("unknown format");
                            }
                            new PrintWriter(directory.getFile(assemblerFileName)).
                                    append(outputContent).close();
                            break;

                        case PRISM:
                        case HEDGEELLETH:
                            String extension;
                            if(of == OptionManager.OutputFormat.PRISM)
                                extension = PrismIO.PRISM_MODEL_EXTENSION;
                            else
                                extension = PrismIO.HEDGEELLETH_MODEL_EXTENSION;
                            if(outputFileName.endsWith(".pctl") &&
                                    patternFileName != null)
                                // modify respectively the pattern filename
                                //
                                // it is guaranteed that the property file
                                // comes after the model file, so this
                                // variable can be modified
                                patternFileName = PrismIO.toPctlFilename(extension,
                                        patternFileName.substring(0,
                                            patternFileName.length() - REGRESSION_STRING.length())) +
                                        REGRESSION_STRING;
                            // continue to the next case
                        case CPP:
                            if(patternFileName != null) {
                                // modify the pattern file name to match the output
                                // file name
                                if(!patternFileName.endsWith(".cpp" + REGRESSION_STRING) &&
                                        outputFileName.endsWith(".cpp"))
                                    patternFileName = CPPBackend.hFilenameToCppFilename(
                                            patternFileName.substring(0,
                                                patternFileName.length() - REGRESSION_STRING.length())) +
                                            REGRESSION_STRING;
                                else if(!patternFileName.endsWith(".h" + REGRESSION_STRING) &&
                                        outputFileName.endsWith(".h"))
                                    patternFileName = CPPBackend.cppFilenameToHFilename(
                                            patternFileName.substring(0,
                                                patternFileName.length() - REGRESSION_STRING.length())) +
                                            REGRESSION_STRING;
                                String prefix = outputFileName.substring(0,
                                        outputFileName.indexOf('.'));
                                patternFileName = prefix + patternFileName.substring(
                                        patternFileName.indexOf('.'));
                            }
                            // continue to the next case
                        case VERICS:
                        case UPPAAL:
                        case XML:
//                           Scanner sc = new Scanner(directory.getFile(
//                               outputFileName)).useLocale(Locale.ROOT).useDelimiter("\\Z");
//                           if(sc.hasNext())
//                               outputContent = sc.next();
//                           else
//                               outputContent = "";
                            if(outputFileName.endsWith(JavaFrontend.FILE_NAME_EXTENSION) &&
                                    !patternFileName.endsWith(JavaFrontend.FILE_NAME_EXTENSION +
                                            REGRESSION_STRING)) {
                                System.out.println("warning: file " + outputFileName + " ignored");
                                continue NEXT_FILE;
                            }
                            if(outputFileName.endsWith(NdFileIO.EXTENSION) &&
                                    !patternFileName.endsWith(NdFileIO.EXTENSION +
                                            REGRESSION_STRING)) {
                                System.out.println("warning: file " + outputFileName + " ignored");
                                continue NEXT_FILE;
                            }
                            outputContent = CompilerUtils.readFile(directory.getFile(
                                   outputFileName));
                            break;

                        default:
                            throw new RuntimeException("unknown format");

                    }
                } catch(IOException e) {
                    String errorMessage = "\tI/O ERROR<Test 1>: " + e.toString();
                    System.out.println(errorMessage);
                    throw new AssertionError(errorMessage);
                }
                try {
                    String s;
                    if(testOptions.experimentMode ==
                            HedgeellethCompiler.Options.ExperimentMode.TRUE)
                        // derive the pattern file from the output file
                        s = outputFileName + "" + REGRESSION_STRING;
                    else
                        s = patternFileName;
//                   Scanner sc = new Scanner(directory.getFile(
//                           s)).useLocale(Locale.ROOT).useDelimiter("\\Z");
//                   String patternContent;
//                   if(sc.hasNext())
//                       patternContent = sc.next();
//                   else
//                       patternContent = "";
                   String patternContent = CompilerUtils.readFile(
                            directory.getFile(s));
                   switch(of) {
                       case ASSEMBLER0:
                       case ASSEMBLER:
                           outputFileName = assemblerFileName;
                           break;

                       case VERICS:
                       case UPPAAL:
                       case XML:
                       case PRISM:
                       case HEDGEELLETH:
                       case CPP:
                           /* empty */
                           break;

                        default:
                            throw new RuntimeException("unknown format");

                   }
                   if(!compareOutput(outputContent, patternContent, directory.getFile(s))) {
                        String errorMessage = "\tOUTPUT DIFFERS: " +
                                outputFileName + " " + s;
                        System.out.println(errorMessage);
                        throw new AssertionError(errorMessage);
                   }
                   switch(of) {
                       case PRISM:
                       case HEDGEELLETH:
                       case ASSEMBLER0:
                       case ASSEMBLER:
                       case VERICS:
                       case UPPAAL:
                       case XML:
                       case CPP:
                           /* empty */
                           break;

                        default:
                            throw new RuntimeException("unknown format");
                   }
                } catch(IOException e) {
                    String errorMessage = "\tI/O ERROR<Test 2>: " + e.getMessage();
                    System.out.println(errorMessage);
                    throw new AssertionError(errorMessage);
                }
            }
        }
        System.out.println("OK.");
        ++compilationsPerformed;
    }
    /**
     * A convenience method, that calls <code>compile(directory,
     * files, pattern, patternFileName, null, null)</code>.
     *
     * @param directory passed further
     * @param files passed further
     * @param pattern passed further
     * @param patternFileName passed further
     */
    @Override
    protected void compile(PWD directory, String[] files,
            CompilerException pattern, String patternFileName) {
        compile(directory, files, pattern, patternFileName,
            null, null);
    }
    /**
     * Tests various parse errors in Java.
     */
    protected void testParseErrors() {
        testPerformed();
        String[] files = {
            "ParseErrors.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 5, 15),
                CompilerException.Code.PARSE,
                "invalid loop initializer: unexpected `-'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 5, 23),
                CompilerException.Code.PARSE,
                "invalid loop condition: unexpected `20'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 5, 36),
                CompilerException.Code.PARSE,
                "invalid loop update: unexpected `~'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 7, 9),
                CompilerException.Code.PARSE,
                "unexpected \"int ;\"")); // !!!!! int ;
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 7, 14),
                CompilerException.Code.PARSE,
                "invalid loop condition: unexpected `<'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 7, 27),
                CompilerException.Code.PARSE,
                "unexpected \")\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 8, 13),
                CompilerException.Code.PARSE,
                "unexpected \")\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 8, 14),
                CompilerException.Code.PARSE,
                "unexpected \")\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 10, 19),
                CompilerException.Code.PARSE,
                "invalid loop update: unexpected `;'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 11, 13),
                CompilerException.Code.PARSE,
                "unexpected \"int\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 12, 20),
                CompilerException.Code.PARSE,
                "unexpected \")\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 13, 11),
                CompilerException.Code.PARSE,
                "invalid switch statement: unexpected `i'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 15, 16),
                CompilerException.Code.PARSE,
                "unexpected \"i\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 16, 11),
                CompilerException.Code.PARSE,
                "orphaned \"case\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 19, 18),
                CompilerException.Code.PARSE,
                "invalid switch statement: unexpected `)'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 21, 18),
                CompilerException.Code.PARSE,
                "unexpected \":\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 28, 13),
                CompilerException.Code.PARSE,
                "unexpected \":\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 35, 11),
                CompilerException.Code.PARSE,
                "unexpected \"break\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 36, 9),
                CompilerException.Code.PARSE,
                "orphaned \"else\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 38, 9),
                CompilerException.Code.PARSE,
                "orphaned \"else\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 41, 10),
                CompilerException.Code.PARSE,
                "unexpected \"i\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 44, 11),
                CompilerException.Code.DUPLICATE,
                "duplicate declaration of i"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 45, 9),
                CompilerException.Code.PARSE,
                "orphaned \"else\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 48, 9),
                CompilerException.Code.PARSE,
                "unexpected \"whilei\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 51, 16),
                CompilerException.Code.PARSE,
                "unexpected \",\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 51, 23),
                CompilerException.Code.PARSE,
                "invalid list structure: unexpected `;'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 52, 5),
                CompilerException.Code.ILLEGAL,
                "break outside of a loop or a labeled statement"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 53, 5),
                CompilerException.Code.ILLEGAL,
                "continue outside of a loop"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 55, 7),
                CompilerException.Code.PARSE,
                "unexpected \"=\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrors.java", 56, 5),
                CompilerException.Code.PARSE,
                "unexpected \"switch\""));        
        compile(new PWD("regression/parse"), files,
                pattern, null);
        String[] filesMethodReturn = {
            "MethodReturn.java",
        };
        CompilerException patternMethodReturn =
                new CompilerException();
        patternMethodReturn.addReport(new CompilerException.Report(
                new StreamPos("MethodReturn.java", 4, 14),
                CompilerException.Code.ILLEGAL,
                "method returns a value, missing return statement"));
        patternMethodReturn.addReport(new CompilerException.Report(
                new StreamPos("MethodReturn.java", 6, 16),
                CompilerException.Code.ILLEGAL,
                "method returns a value, missing return statement"));
        compile(new PWD("regression/parse"), filesMethodReturn,
                patternMethodReturn, null);
        String[] filesMethodVoid = {
            "MethodVoid.java",
        };
        CompilerException patternMethodVoid =
                new CompilerException();
        patternMethodVoid.addReport(new CompilerException.Report(
                new StreamPos("MethodVoid.java", 8, 12),
                CompilerException.Code.ILLEGAL,
                "void method can not return a value"));
        compile(new PWD("regression/parse"), filesMethodVoid,
                patternMethodVoid, null);
    }
    /**
     * Tests various parse errors in Verics.
     */
    protected void testParseErrorsVerics() {
        testPerformed();
        String[] files = {
            "ParseErrorsVerics.verics",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 5, 13),
                CompilerException.Code.PARSE,
                "invalid loop initializer: unexpected `-'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 5, 21),
                CompilerException.Code.PARSE,
                "invalid loop condition: unexpected `20'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 5, 34),
                CompilerException.Code.PARSE,
                "block expected, found `~'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 7, 9),
                CompilerException.Code.PARSE,
                "unexpected \"Z ;\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 7, 12),
                CompilerException.Code.PARSE,
                "invalid loop condition: unexpected `<'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 7, 25),
                CompilerException.Code.PARSE,
                "unexpected \")\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 8, 13),
                CompilerException.Code.PARSE,
                "unexpected \")\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 10, 17),
                CompilerException.Code.PARSE,
                "block expected, found `;'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 10, 23),
                CompilerException.Code.PARSE,
                "unexpected \"{\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 11, 13),
                CompilerException.Code.PARSE,
                "unexpected \"Z\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 12, 21),
                CompilerException.Code.PARSE,
                "unexpected \"{\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 15, 17),
                CompilerException.Code.PARSE,
                "invalid switch statement: unexpected `)'"));
///        pattern.addReport(new CompilerException.Report(
///                new StreamPos("ParseErrorsVerics.verics", 19, 17),
///                CompilerException.Code.PARSE,
///                "invalid switch statement: unexpected `&'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 20, 11),
                CompilerException.Code.PARSE,
                "unexpected \"default\""));
///        pattern.addReport(new CompilerException.Report(
///                new StreamPos("ParseErrorsVerics.verics", 21, 18),
///                CompilerException.Code.PARSE,
///                "unexpected \":\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 28, 13),
                CompilerException.Code.PARSE,
                "unexpected \":\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 35, 11),
                CompilerException.Code.PARSE,
                "unexpected \"break\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 36, 9),
                CompilerException.Code.PARSE,
                "orphaned \"else\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 38, 9),
                CompilerException.Code.PARSE,
                "orphaned \"else\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 42, 9),
                CompilerException.Code.PARSE,
                "block expected in non-ternary if, found `;'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 44, 11),
                CompilerException.Code.DUPLICATE,
                "duplicate declaration of i"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 45, 9),
                CompilerException.Code.PARSE,
                "orphaned \"else\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 48, 9),
                CompilerException.Code.PARSE,
                "unexpected \"whilei\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 51, 15),
                CompilerException.Code.PARSE,
                "unexpected \",\""));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 51, 20),
                CompilerException.Code.PARSE,
                "invalid list structure: unexpected `;'"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 52, 5),
                CompilerException.Code.ILLEGAL,
                "break outside of a loop or a labeled statement"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseErrorsVerics.verics", 53, 5),
                CompilerException.Code.ILLEGAL,
                "continue outside of a loop"));
////        pattern.addReport(new CompilerException.Report(
////                new StreamPos("ParseErrorsVerics.verics", 55, 5),
////                CompilerException.Code.PARSE,
////                "unexpected \"i\""));
////        // expects a new class
////        pattern.addReport(new CompilerException.Report(
////                new StreamPos("ParseErrorsVerics.verics", 61, 5),
////                CompilerException.Code.PARSE,
////                "unexpected \"$\""));
        compile(new PWD("regression/parse"), files,
                pattern, null);
        String[] filesMethodParameters = {
            "MethodParameters.verics",
        };
        CompilerException patternMethodParameters =
                new CompilerException();
        patternMethodParameters.addReport(new CompilerException.Report(
                new StreamPos("MethodParameters.verics", 11, 14),
                CompilerException.Code.LOOK_UP,
                "variable not found: g"));
        patternMethodParameters.addReport(new CompilerException.Report(
                new StreamPos("MethodParameters.verics", 11, 17),
                CompilerException.Code.LOOK_UP,
                "variable not found: h"));
        patternMethodParameters.addReport(new CompilerException.Report(
                new StreamPos("MethodParameters.verics", 12, 14),
                CompilerException.Code.ILLEGAL,
                "field lacks a qualifier"));
        patternMethodParameters.addReport(new CompilerException.Report(
                new StreamPos("MethodParameters.verics", 12, 17),
                CompilerException.Code.LOOK_UP,
                "not found: h()"));
        patternMethodParameters.addReport(new CompilerException.Report(
                new StreamPos("MethodParameters.verics", 12, 26),
                CompilerException.Code.LOOK_UP,
                "variable not found: g"));
        compile(new PWD("regression/parse"), filesMethodParameters,
                patternMethodParameters, null);
        String[] filesSingleLineComment = {
            "CommentSingleLineEOF.verics",
        };
        CompilerException patternSingleLineComment =
                new CompilerException();
        patternSingleLineComment.addReport(new CompilerException.Report(
                new StreamPos("CommentSingleLineEOF.verics", 11, 33),
                CompilerException.Code.PARSE,
                "unclosed comment"));        
        compile(new PWD("regression/parse"), filesSingleLineComment,
                patternSingleLineComment, null);
        String[] filesMultiLineComment = {
            "CommentMultiLineEOF.verics",
        };
        CompilerException patternMultiLineComment =
                new CompilerException();
        patternMultiLineComment.addReport(new CompilerException.Report(
                new StreamPos("CommentMultiLineEOF.verics", 31, 3),
                CompilerException.Code.PARSE,
                "unclosed comment"));        
        compile(new PWD("regression/parse"), filesMultiLineComment,
                patternMultiLineComment, null);
    }
    /**
     * Tests accessibility of "this" in a static context.
     */
    protected void testStaticContext() {
        testPerformed();
        String[] files = {
            "StaticContext.java",
            "StaticContextVerics.verics",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("StaticContext.java", 6, 9),
                CompilerException.Code.ILLEGAL,
                "this object does not exist in a static context"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("StaticContext.java", 6, 19),
                CompilerException.Code.ILLEGAL,
                "non-static field f2 can not be accessed from a static context"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("StaticContextVerics.verics", 6, 9),
                CompilerException.Code.ILLEGAL,
                "this object does not exist in a static context"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("StaticContextVerics.verics", 6, 20),
                CompilerException.Code.ILLEGAL,
                "non-static field f2 can not be accessed from a static context"));
        compile(new PWD("regression/"), files,
                pattern, null);
    }
    /**
     * Tests flow of control statements.
     */
    protected void testControl() {
        testPerformed();
        String[] files = {
            "Control.java",
        };
        compile(new PWD("regression/control"), files,
                null, "Control.s.regression");
    }
    /**
     * Tests flow of control statements in Verics.
     */
    protected void testControlVerics() {
        testPerformed();
        String[] files = {
            "ControlVerics.verics",
            "ControlVericsMain.java",
        };
        compile(new PWD("regression/control"), files,
                null, "ControlVerics.s.regression");
    }
    /**
     * Tests escape sequences.
     */
    protected void testEscapeSequences() {
        testPerformed();
        TestOptions testOptionsVerboseVariables = new TestOptions();
        testOptionsVerboseVariables.verboseTADDVariables = true;
        String[] files = {
            "Escape.java",
        };
        compile(new PWD("regression/escape"), files,
                null, "Escape.nm.regression",
                testOptionsVerboseVariables, null);
        String[] filesParse = {
            "EscapeParse.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("EscapeParse.java", 11, 18),
                CompilerException.Code.INVALID,
                "invalid escape sequence"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("EscapeParse.java", 12, 20),
                CompilerException.Code.ILLEGAL,
                "invalid escape character"));
        compile(new PWD("regression/escape"), filesParse,
                pattern, null, testOptionsVerboseVariables, null);
    }
    /**
     * Tests detection of invalid switch labels.
     */
    protected void testSwitchLabels() {
        testPerformed();
        String[] files = {
            "SwitchLabelTest.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("SwitchLabelTest.java", 6, 12),
                CompilerException.Code.ILLEGAL,
                "only constant expression allowed here"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("SwitchLabelTest.java", 15, 14),
                CompilerException.Code.DUPLICATE,
                "duplicate constant, first occurence at SwitchLabelTest.java:9:12"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("SwitchLabelTest.java", 21, 17),
                CompilerException.Code.DUPLICATE,
                "duplicate constant, first occurence at SwitchLabelTest.java:9:12"));
        compile(new PWD("regression/"), files,
                pattern, null);
    }
    /**
     * Tests access modifiers.
     */
    protected void testAccessModifiers() {
        testPerformed();
        String[] files = {
            "A.java",
            "B.java",
            "C.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("B.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "constructor A() of test.A in A.java:3:7 has private access"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("B.java", 5, 13),
                CompilerException.Code.ILLEGAL,
                "field fieldPrivate of test.A in A.java:3:7 has private access"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("B.java", 5, 59),
                CompilerException.Code.ILLEGAL,
                "field fieldPrivate of test.A in A.java:3:7 has private access"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("B.java", 6, 5),
                CompilerException.Code.ILLEGAL,
                "method methodPrivate() of test.A in A.java:3:7 has private access"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("B.java", 10, 15),
                CompilerException.Code.ILLEGAL,
                "method int m() in test.B can not override int m() in test.A: has weaker access privileges, were protected internal"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("C.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "test.B in B.java:3:7 has internal access"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("C.java", 4, 7),
                CompilerException.Code.ILLEGAL,
                "no access to constructor B(): test.B in B.java:3:7 has internal access"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("C.java", 5, 5),
                CompilerException.Code.ILLEGAL,
                "no access to method p(): test.A in A.java:3:7 has internal access"));
        compile(new PWD("regression/accessmodifiers/"), files,
                pattern, null);
    }
    /**
     * Tests detection of unitialized variables.
     */
    protected void testInitialization() {
        testPerformed();
        String[] filesInitialization = {
            "Initialization.java",
        };
        CompilerException patternInitialization =
                new CompilerException();
        final String UNINITIALIZED_STRING =
                " used uninitialized in this expression";
        patternInitialization.addReport(new CompilerException.Report(
                new StreamPos("Initialization.java", 6, 7),
                CompilerException.Code.ILLEGAL,
                "variable i is" + UNINITIALIZED_STRING));
        patternInitialization.addReport(new CompilerException.Report(
                new StreamPos("Initialization.java", 10, 11),
                CompilerException.Code.ILLEGAL,
                "variable i might be" + UNINITIALIZED_STRING));
        patternInitialization.addReport(new CompilerException.Report(
                new StreamPos("Initialization.java", 11, 14),
                CompilerException.Code.ILLEGAL,
                "variable i might be" + UNINITIALIZED_STRING));
        patternInitialization.addReport(new CompilerException.Report(
                new StreamPos("Initialization.java", 19, 11),
                CompilerException.Code.ILLEGAL,
                "variable ff might be" + UNINITIALIZED_STRING));
        patternInitialization.addReport(new CompilerException.Report(
                new StreamPos("Initialization.java", 22, 12),
                CompilerException.Code.ILLEGAL,
                "variable i might be" + UNINITIALIZED_STRING));
        compile(new PWD("regression/initialization"), filesInitialization,
                patternInitialization, null);
        String[] filesForwardReference = {
            "ForwardReferenceA.java",
            "ForwardReferenceB.java",
        };
        CompilerException patternForwardReference =
                new CompilerException();
        patternForwardReference.addReport(new CompilerException.Report(
                new StreamPos("ForwardReferenceA.java", 2, 9),
                CompilerException.Code.ILLEGAL,
                "illegal forward field reference"));
           // possible circular references handled in the interpreter, and so
           // this error type is no longer needed
///        patternForwardReference.addReport(new CompilerException.Report(
///                new StreamPos("ForwardReferenceA.java", 3, 17),
///                CompilerException.Code.ILLEGAL,
///                "static initialization wih a foreign field"));
        patternForwardReference.addReport(new CompilerException.Report(
                new StreamPos("ForwardReferenceA.java", 6, 25),
                CompilerException.Code.ILLEGAL,
                "illegal forward field reference"));
        patternForwardReference.addReport(new CompilerException.Report(
                new StreamPos("ForwardReferenceB.java", 2, 21),
                CompilerException.Code.ILLEGAL,
                "illegal forward field reference"));
        patternForwardReference.addReport(new CompilerException.Report(
                new StreamPos("ForwardReferenceB.java", 5, 6),
                CompilerException.Code.ILLEGAL,
                "illegal forward field reference"));
        patternForwardReference.addReport(new CompilerException.Report(
                new StreamPos("ForwardReferenceB.java", 6, 7),
                CompilerException.Code.ILLEGAL,
                "illegal forward field reference"));
        patternForwardReference.addReport(new CompilerException.Report(
            new StreamPos("ForwardReferenceB.java", 7, 8),
            CompilerException.Code.ILLEGAL,
            "illegal forward field reference"));
        compile(new PWD("regression/initialization"), filesForwardReference,
                patternForwardReference, null);
        String[] filesCircular = {
            "C1.java", "C2.java"
        };
        CompilerException patternCircular =
                new CompilerException();
        patternCircular.addReport(new CompilerException.Report(
                new StreamPos("C1.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: called from C1.java:2:17: circular static reference between #default.C1, #default.C2"));
        compile(new PWD("regression/initialization"), filesCircular,
                patternCircular, null);
    }
    /**
     * Tests arrays.
     */
    protected void testArrays() {
        testPerformed();
        String[] files1 = {
            "ArrayTest.java",
        };
        String[] files1_c = {
            "ArrayTest_c.java",
        };
        TestOptions optionsNoConstantElements = new TestOptions();
        optionsNoConstantElements.test.ignoreConstantElements = true;
        compile(new PWD("regression/arrays"), files1,
                null, "ArrayTest.xml.regression", optionsNoConstantElements,
                null);
        compile(new PWD("regression/arrays"), files1_c,
                null, "ArrayTest_c.xml.regression");
        String[] files2 = {
            "Local.java",
        };
        compile(new PWD("regression/arrays"), files2,
                null, "Local.xml.regression", optionsNoConstantElements,
                null);
    }
    /**
     * Tests array initializers.
     */
    protected void testArrayInitializers() {
        testPerformed();
        TestOptions optionsNoConstantElements = new TestOptions();
        optionsNoConstantElements.test.ignoreConstantElements = true;
        String[] files = {
            "TestArrayInitializers.java",
        };
        String[] files_c = {
            "TestArrayInitializers_c.java",
        };
        compile(new PWD("regression/arrays"), files,
                null, "TestArrayInitializers.xml.regression",
                optionsNoConstantElements, null);
        compile(new PWD("regression/arrays"), files_c,
                null, "TestArrayInitializers_c.xml.regression");
    }
    /**
     * Tests custom operator -- a conditional expression.
     */
    protected void testOperators() {
        testPerformed();
        String[] files = {
            "Operators.java",
        };
        compile(new PWD("regression"), files,
                null, "Operators.s.regression");
    }
    /**
     * Tests class inheritance.
     */
    protected void testInheritance() {
        testPerformed();
        String[] files = {
            "A.java",
            "B0.java",
            "B.java",
            "C.java",
            "Inheritance.java",
        };
        compile(new PWD("regression/inheritance"), files,
                null, "Inheritance.s0.regression");
        compile(new PWD("regression/inheritance"), files,
                null, "Inheritance.xml.regression");
        String[] filesPrivate = {
            "PrivateA.verics",
            "PrivateB.verics",
            "PrivateTest1.java",
        };
        compile(new PWD("regression/inheritance"), filesPrivate,
                null, "PrivateTest1.xml.regression");
    }
    /**
     * Tests detection of ambiguous references.
     */
    protected void testAmbiguousReference() {
        testPerformed();
        String[] files = {
            "AmbiguousReference.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("AmbiguousReference.java", 12, 22),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread #default.AmbiguousReference_1: ambiguous dereference when called from #default.AmbiguousReference.run(#default.AmbiguousReference)"));
        compile(new PWD("regression/ambiguousreference"), files,
                pattern, null);
        String[] files2 = {
            "UnambiguousReference.java",
        };
        compile(new PWD("regression/ambiguousreference"), files2,
                null, "UnambiguousReference.xml.regression");
        String[] files3 = {
            "UnambiguousReference2.java",
        };
        compile(new PWD("regression/ambiguousreference"), files3,
                null, "UnambiguousReference2.xml.regression");
    }
    /**
     * Tests if a correct virtual method is chosen on basic of the
     * type of an arithmetic constant.
     */
    protected void testArithmeticVirtual() {
        testPerformed();
        String[] files = {
            "ArithmeticVirtual.java",
        };
        compile(new PWD("regression"), files,
                null, "ArithmeticVirtual.s0.regression");
        compile(new PWD("regression"), files,
                null, "ArithmeticVirtual.s.regression");
        compile(new PWD("regression"), files,
                null, "ArithmeticVirtual.xml.regression");
        DEFAULT_TEST_OPTIONS.messageStyle = MessageStyle.Type.VERICS;
        DEFAULT_TEST_OPTIONS.verboseTADDVariables = true;
        compile(new PWD("regression"), files,
                null, "ArithmeticVirtual.nm.regression");
        // switch to a PTA, to check if there is no closed loop at the
        // end, as it were in he case of an MDP
        files[0] = "ArithmeticVirtualSleep.java";
        compile(new PWD("regression"), files,
                null, "ArithmeticVirtualSleep.nm.regression");
        DEFAULT_TEST_OPTIONS.messageStyle = MessageStyle.Type.JAVA;
        DEFAULT_TEST_OPTIONS.verboseTADDVariables = false;
    }
    /**
     * Tests calls to virtual methods, that can be resolved by knowing
     * type of reference.
     */
    protected void testUnambiguousVirtual() {
        testPerformed();
        String[] files = {
            "VirtualA.java",
            "VirtualB.java",
            "VirtualC.java",
            "VirtualD.java",
            "Unambiguous.java",
        };
        compile(new PWD("regression/unambiguous"), files,
                null, "Unambiguous.s0.regression");
        compile(new PWD("regression/unambiguous"), files,
                null, "Unambiguous.xml.regression");
        String[] files2 = {
            "VirtualA.java",
            "VirtualB.java",
            "VirtualC.java",
            "VirtualD.java",
            "Ambiguous.java",
        };
        CompilerException pattern2 =
                new CompilerException();
////        // !!! can it be done with replacement of fields with constants?
////        pattern2.addReport(new CompilerException.Report(
////                new StreamPos("VirtualA.java", 4, 9),
////                CompilerException.Code.ILLEGAL,
////                "construction of PTA: type of \"this\" of a non-static method int a() required to avoid ambiguity, but local dereferences not traced during virtual call resolution"));
        compile(new PWD("regression/unambiguous"), files2,
                null, "Ambiguous.pxml.regression");
    }
    /**
     * Tests abstract classes and methods.
     */
    protected void testAbstract() {
        testPerformed();
        String[] files = {
            "Abstract1.java",
            "AbstractA1.java",
            "Abstract2.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Abstract1.java", 3, 14),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but contains abstract method int methodA1() in Abstract1.java:9:16"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Abstract1.java", 3, 14),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but contains abstract method int methodA2() in Abstract1.java:10:16"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Abstract1.java", 3, 14),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but contains abstract method int methodA2Protected() in Abstract1.java:11:26"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Abstract2.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but contains abstract method int methodA1() in Abstract1.java:9:16"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Abstract2.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but contains abstract method int methodA2() in Abstract1.java:10:16"));
        compile(new PWD("regression/abstract"), files,
                pattern, null);
    }
    protected void testCasts() {
        testPerformed();
        String[] files = {
            "A.java",
            "B0.java",
            "B.java",
            "C.java",
            "CastErrors.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("CastErrors.java", 11, 10),
                CompilerException.Code.ILLEGAL,
                "can not convert #default.B0 to #default.B"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("CastErrors.java", 12, 10),
                CompilerException.Code.ILLEGAL,
                "can not convert #default.A to #default.B"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("CastErrors.java", 13, 11),
                CompilerException.Code.ILLEGAL,
                "possible loss of precision: conversion from long to int"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("CastErrors.java", 14, 12),
                CompilerException.Code.ILLEGAL,
                "possible loss of precision: conversion from float to long"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("CastErrors.java", 16, 8),
                CompilerException.Code.ILLEGAL,
                "possible loss of precision: conversion from long to int"));        
        compile(new PWD("regression/casts"), files,
                pattern, null);
        String[] files2 = {
            "A.java",
            "B0.java",
            "B.java",
            "C.java",
            "CastRuntime.java",
        };
        CompilerException pattern2 =
                new CompilerException();
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("CastRuntime.java", 11, 12),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: runtime class cast exception: #default.B0 to #default.B"));
        compile(new PWD("regression/casts"), files2,
                pattern2, null);
        String[] files3 = {
            "A.java",
            "B0.java",
            "B.java",
            "C.java",
            "CastCompiled1.java",
        };
        compile(new PWD("regression/casts"), files3,
                null, "CastCompiled1.xml.regression");
        String[] files4 = {
            "A.java",
            "B0.java",
            "B.java",
            "C.java",
            "CastCompiled2.java",
        };
        CompilerException pattern4 =
                new CompilerException();
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("CastCompiled2.java", 5, 13),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread #default.CastCompiled1_1: runtime class cast exception: #default.B0 to #default.B"));
///U                "construction of PTA: in runtime code analysis: runtime class cast exception: #default.B0 to #default.B"));
        compile(new PWD("regression/casts"), files4,
                pattern4, null);
    }
    /**
     * Tests checking of validity of method implementing/overriding.
     */
    protected void testValidOverride() {
        testPerformed();
        // test parse errors first
        String[] files = {
            "ParseA.verics",
            "ParseB.verics",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseA.verics", 10, 11),
                CompilerException.Code.ILLEGAL,
                "method can not be still abstract and already final"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseB.verics", 21, 27),
                CompilerException.Code.PARSE,
                "abstract method can not implement"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseB.verics", 23, 25),
                CompilerException.Code.PARSE,
                "abstract method can not override"));
        compile(new PWD("regression/override"), files,
                pattern, null);
        // then semantic errors
        String[] files2 = {
            "A.verics",
            "B.verics",
        };
        CompilerException pattern2 =
                new CompilerException();
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 3, 11),
                CompilerException.Code.ILLEGAL,
                "overrides a method but declares otherwise"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 6, 11),
                CompilerException.Code.ILLEGAL,
                "overrides a method in A.verics:4:11 that does not allow overriding"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 9, 11),
                CompilerException.Code.ILLEGAL,
                "abstract method in A.verics:6:11 can not be overridden"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 9, 11),
                CompilerException.Code.ILLEGAL,
                "implements a method but declares otherwise"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 15, 11),
                CompilerException.Code.ILLEGAL,
                "implements a method but declares otherwise"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 18, 11),
                CompilerException.Code.ILLEGAL,
                "does not implement a method but declares otherwise"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 21, 11),
                CompilerException.Code.ILLEGAL,
                "does not override a method but declares otherwise"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 24, 11),
                CompilerException.Code.ILLEGAL,
                "does not implement a method but declares otherwise"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 31, 11),
                CompilerException.Code.ILLEGAL,
                "non-abstract method in A.verics:13:11 overridden by abstract one"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("B.verics", 31, 11),
                CompilerException.Code.ILLEGAL,
                "overrides a method but declares otherwise"));
        compile(new PWD("regression/override"), files2,
                pattern2, null);
        String[] filesStaticAbstract = {
            "S1.verics",
            "S2.verics",
        };
        CompilerException patternStaticAbstract =
                new CompilerException();
        patternStaticAbstract.addReport(new CompilerException.Report(
                new StreamPos("S2.verics", 2, 11),
                CompilerException.Code.ILLEGAL,
                "shadows a method that has a different context"));
        patternStaticAbstract.addReport(new CompilerException.Report(
                new StreamPos("S2.verics", 4, 11),
                CompilerException.Code.ILLEGAL,
                "shadows a method but declares otherwise"));
        patternStaticAbstract.addReport(new CompilerException.Report(
                new StreamPos("S2.verics", 6, 11),
                CompilerException.Code.ILLEGAL,
                "shadows a method but declares otherwise"));
        patternStaticAbstract.addReport(new CompilerException.Report(
                new StreamPos("S2.verics", 6, 11),
                CompilerException.Code.ILLEGAL,
                "shadows a method in S1.verics:6:11 that does not allow shadowing"));
        compile(new PWD("regression/override"), filesStaticAbstract,
                patternStaticAbstract, null);
        // test @Override in Java, and also checking pairs method/super method
        // that are not in directly related classes 
        String[] filesOverride = {
            "A.java",
            "B.java",
            "C.java",
        };
        CompilerException patternOverride =
                new CompilerException();
        patternOverride.addReport(new CompilerException.Report(
                new StreamPos("C.java", 2, 16),
                CompilerException.Code.ILLEGAL,
                "method void testPrivileges() in #default.C can not override void testPrivileges() in #default.A: has weaker access privileges, were protected internal"));
        patternOverride.addReport(new CompilerException.Report(
                new StreamPos("C.java", 5, 15),
                CompilerException.Code.ILLEGAL,
                "does not override a method but declares otherwise"));
        compile(new PWD("regression/override"), filesOverride,
                patternOverride, null);
    }
    /**
     * Tests interfaces.
     */
    protected void testInterfaces() {
        testPerformed();
        String[] files = {
            "InterfaceA1.java",
            "InterfaceA2.java",
            "InterfaceB.java",
            "Implements1.java",
            "Implements2.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but does not implement abstract interface method int getA1() in InterfaceA1.java:4:14"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but does not implement abstract interface method int getP1() in InterfaceA1.java:5:14"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but does not implement abstract interface method int getP2() in InterfaceB.java:4:14"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but does not implement abstract interface method int getP3() in InterfaceB.java:5:14"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but does not implement abstract interface method void p() in InterfaceA2.java:7:8"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but does not implement abstract interface method void r() in InterfaceA1.java:6:15"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "class is not abstract but does not implement abstract interface method void r() in InterfaceA2.java:6:15"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "interface method int getA2() in InterfaceA2.java:4:7 has internal access"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("Implements2.java", 3, 7),
                CompilerException.Code.ILLEGAL,
                "interface method void p() in InterfaceA2.java:7:8 has internal access"));
        compile(new PWD("regression/interfaces"), files,
                pattern, null);
    }
    /**
     * Tests thread synchronization if there is only a single thread.
     */
    protected void testSingle() {
        testPerformed();
        String[] files = {
            "Single.java",
        };
        compile(new PWD("regression/"), files,
                null, "Single.xml.regression");
    }
    /**
     * Tests translation of the source of Alternating Bit Protocol.
     */
    protected void testABP() {
        testPerformed();
        String[] files = {
            "AltBitProtocol.java",
            "Sender.java",
            "Receiver.java",
            "LossyChannel.java",
        };
        compile(new PWD("regression/abp"), files,
                null, "AltBitProtocol.s.regression");
        compile(new PWD("regression/abp"), files,
                null, "AltBitProtocol.xml.regression");
        compile(new PWD("regression/abp"), files,
                null, "AltBitProtocol.tadd.regression");
        TestOptions testOptionsVerboseVariables = new TestOptions();
        testOptionsVerboseVariables.messageStyle = MessageStyle.Type.JAVA;
        testOptionsVerboseVariables.verboseTADDVariables = true;
        compile(new PWD("regression/abp"), files,
                null, "AltBitProtocol_verbose.xml.regression",
                testOptionsVerboseVariables, null);
        testOptionsVerboseVariables.messageStyle = MessageStyle.Type.VERICS;
        compile(new PWD("regression/abp"), files,
                null, "AltBitProtocol.nm.regression", testOptionsVerboseVariables,
                null);
        compile(new PWD("regression/abp"), files,
                null, "AltBitProtocol.nh.regression", testOptionsVerboseVariables,
                null);
    }
    /**
     * Tests compilation of files in two different languages
     * at once.
     */
    protected void testTwoLanguages() {
        testPerformed();
        String[] files = {
            "Main.java",
            "Java.java",
            "Verics.verics",
        };
        compile(new PWD("regression/two"), files,
                null, "Main.xml.regression");
    }
    /**
     * Tests valididty of extending/implementing other classes -- if the
     * type is correct, if there are no circular dependencies.
     */
    protected void testExtends() {
        testPerformed();
        String[] files = {
            "A.java",
            "B.java",
            "C1.java",
            "C2.java",
            "D.java",
            "IA.java",
            "IB.java",
            "IC1.java",
            "IC2.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "circular class dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("B.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "circular class dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("C1.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "circular class dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("C2.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "circular class dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("C2.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "non-tree interface dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("C2.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "class implements class #default.A"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("D.java", 1, 7),
                CompilerException.Code.ILLEGAL,
                "class extends interface #default.IA"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("IA.java", 1, 11),
                CompilerException.Code.ILLEGAL,
                "non-tree interface dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("IB.java", 1, 11),
                CompilerException.Code.ILLEGAL,
                "non-tree interface dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("IC1.java", 1, 11),
                CompilerException.Code.ILLEGAL,
                "non-tree interface dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("IC2.java", 1, 11),
                CompilerException.Code.ILLEGAL,
                "non-tree interface dependency"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("IC2.java", 1, 11),
                CompilerException.Code.ILLEGAL,
                "interface implements class #default.A"));
        compile(new PWD("regression/extends"), files,
                pattern, null);
    }
    /**
     * Tests access of fields, with field prefixes enabled.
     */
    protected void testFieldPrefix() {
        testPerformed();
        String[] files = {
            "ParseFieldPrefix.verics",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseFieldPrefix.verics", 9, 9),
                CompilerException.Code.ILLEGAL,
                "field lacks a qualifier"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseFieldPrefix.verics", 10, 14),
                CompilerException.Code.LOOK_UP,
                "variable not found: f"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseFieldPrefix.verics", 11, 19),
                CompilerException.Code.LOOK_UP,
                "variable not found: f"));
        compile(new PWD("regression/fieldprefix"), files,
                pattern, null);
        String[] files2 = {
            "FieldPrefix.verics",
            "FieldPrefixMain.java",
        };
        compile(new PWD("regression/fieldprefix"), files2,
                null, "FieldPrefix.s.regression");
    }
    /**
     * Tests mutator flags.
     */
    protected void testMutatorFlag() {
        testPerformed();
        String[] files = {
            "ParseA.verics",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseA.verics", 7, 14),
                CompilerException.Code.ILLEGAL,
                "class does not allow for mutator flag"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseA.verics", 10, 12),
                CompilerException.Code.ILLEGAL,
                "class does not allow for mutator flag"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseA.verics", 10, 12),
                CompilerException.Code.ILLEGAL,
                "static method can not have mutator flag"));
        compile(new PWD("regression/mutator"), files,
                pattern, null);
        String[] files2 = {
            "A.verics",
        };
        CompilerException pattern2 =
                new CompilerException();
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 4, 12),
                CompilerException.Code.LOOK_UP,
                "class test.mutator.A does not allow mutator references"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 6, 16),
                CompilerException.Code.LOOK_UP,
                "class test.mutator.A does not allow mutator references"));
        compile(new PWD("regression/mutator"), files2,
                pattern2, null);
        String[] files3 = {
            "B.verics",
            "C.verics",
        };
        CompilerException pattern3 =
                new CompilerException();
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("C.verics", 10, 12),
                CompilerException.Code.ILLEGAL,
                "type test.mutator.BB does not allow mutator method"));
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("C.verics", 13, 14),
                CompilerException.Code.ILLEGAL,
                "type test.mutator.BB does not allow mutator method"));
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("C.verics", 16, 14),
                CompilerException.Code.ILLEGAL,
                "can not convert test.mutator.BB to test.mutator.BB~: target type requires mutator flag"));
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("C.verics", 17, 12),
                CompilerException.Code.ILLEGAL,
                "can not convert test.mutator.BB to test.mutator.BB~: target type requires mutator flag"));
        pattern3.addReport(new CompilerException.Report(
            new StreamPos("C.verics", 18, 16),
            CompilerException.Code.ILLEGAL,
            "return type mismatch: can not convert test.mutator.BB to test.mutator.BB~: target type requires mutator flag"));
        compile(new PWD("regression/mutator"), files3,
                pattern3, null);
    }
    /**
     * Tests final variables.
     */
    protected void testFinalVariables() {
        testPerformed();
        TestOptions testOptionsNoBlanks = new TestOptions();
        testOptionsNoBlanks.blankFinalsAllowed = false;
        testOptionsNoBlanks.messageStyle = MessageStyle.Type.JAVA;
        String[] files1 = {
            "Final1.java",
        };
        CompilerException pattern1 =
                new CompilerException();
        pattern1.addReport(new CompilerException.Report(
                new StreamPos("Final1.java", 4, 15),
                CompilerException.Code.ILLEGAL,
                "blank final variables not allowed"));
        compile(new PWD("regression/finalvariable"), files1,
                pattern1, null, testOptionsNoBlanks, null);
        String[] files2 = {
            "Final2.java",
            "FinalVerics.verics",
        };
        CompilerException pattern2 =
                new CompilerException();
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 6, 12),
                CompilerException.Code.ILLEGAL,
                "final variable ff can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 9, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fa can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 13, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 14, 9),
                CompilerException.Code.ILLEGAL,
                "final variable ff can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 15, 11),
                CompilerException.Code.ILLEGAL,
                "final variable fa can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 16, 11),
                CompilerException.Code.ILLEGAL,
                "final variable fl can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 17, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 6, 13),
                CompilerException.Code.ILLEGAL,
                "final variable ff can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 9, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fa can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 12, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 14, 9),
                CompilerException.Code.ILLEGAL,
                "final variable ff can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 15, 11),
                CompilerException.Code.ILLEGAL,
                "final variable fa can not be assigned outside declaration"));
        pattern2.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 16, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl can not be assigned outside declaration"));
        // this test disallows final blanks, even that they are available
        // in Verics
        testOptionsNoBlanks.test.ignoreInternalBlankFinals = true;
        compile(new PWD("regression/finalvariable"), files2,
                pattern2, null, testOptionsNoBlanks, null);
        String[] files3 = {
            "FinalSwitchLabel.java",
        };
        // this copes with finals by providing default initializers
        compile(new PWD("regression/finalvariable"), files3,
                null, "FinalSwitchLabel.s.regression", testOptionsNoBlanks,
                null);
        testOptionsNoBlanks.test.ignoreInternalBlankFinals = false;
        CompilerException pattern2b =
                new CompilerException();
        pattern2b.addReport(new CompilerException.Report(
                new StreamPos("FinalSwitchLabel.java", 3, 13),
                CompilerException.Code.ILLEGAL,
                "final variable label2 might be used uninitialized, reason: statement at FinalSwitchLabel.java:7:18 reads label2 before"));
        // this refuses to add default initializers to finals
        compile(new PWD("regression/finalvariable"), files3,
                pattern2b, null);
        String[] files4 = {
            "Blank.verics",
        };
        CompilerException pattern3 =
                new CompilerException();
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("Blank.verics", 8, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f3 might be used uninitialized"));
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("Blank.verics", 11, 13),
                CompilerException.Code.ILLEGAL,
                "final variable f1 is already initialized"));
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("Blank.verics", 14, 16),
                CompilerException.Code.ILLEGAL,
                "final variable l1 is already initialized"));
        pattern3.addReport(new CompilerException.Report(
                new StreamPos("Blank.verics", 16, 13),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might already be initialized"));        
        compile(new PWD("regression/finalvariable"), files4,
                pattern3, null);
        // this test has already been run, but this time blank
        // finals are allowed
        CompilerException pattern4 =
                new CompilerException();
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 6, 12),
                CompilerException.Code.ILLEGAL,
                "final variable ff is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 9, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fa is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 13, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 14, 9),
                CompilerException.Code.ILLEGAL,
                "final variable ff is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 15, 11),
                CompilerException.Code.ILLEGAL,
                "final variable fa is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 16, 11),
                CompilerException.Code.ILLEGAL,
                "final variable fl is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("Final2.java", 17, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 6, 13),
                CompilerException.Code.ILLEGAL,
                "final variable ff is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 9, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fa is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 12, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 14, 9),
                CompilerException.Code.ILLEGAL,
                "final variable ff is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 15, 11),
                CompilerException.Code.ILLEGAL,
                "final variable fa is already initialized"));
        pattern4.addReport(new CompilerException.Report(
                new StreamPos("FinalVerics.verics", 16, 12),
                CompilerException.Code.ILLEGAL,
                "final variable fl is already initialized"));        
        compile(new PWD("regression/finalvariable"), files2,
                pattern4, null);
        String[] files4Java = {
            "Blank.java",
        };
        CompilerException pattern3Java =
                new CompilerException();
        pattern3Java.addReport(new CompilerException.Report(
                new StreamPos("Blank.java", 8, 12),
                CompilerException.Code.ILLEGAL,
                "final variable f3 might be used uninitialized"));
        pattern3Java.addReport(new CompilerException.Report(
                new StreamPos("Blank.java", 11, 12),
                CompilerException.Code.ILLEGAL,
                "final variable f1 is already initialized"));
        pattern3Java.addReport(new CompilerException.Report(
                new StreamPos("Blank.java", 14, 16),
                CompilerException.Code.ILLEGAL,
                "final variable l1 is already initialized"));
        pattern3Java.addReport(new CompilerException.Report(
                new StreamPos("Blank.java", 16, 12),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might already be initialized"));        
        // this test has already been run, but this time blank
        // finals are allowed
        compile(new PWD("regression/finalvariable"), files4Java,
                pattern3Java, null);
        String[] files5 = {
            "BlankStatic.verics",
        };
        CompilerException pattern5 =
                new CompilerException();
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 6, 11),
                CompilerException.Code.ILLEGAL,
                "final variable test.BlankStatic.f3 might be used uninitialized"));
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 10, 9),
                CompilerException.Code.ILLEGAL,
                "final variable test.BlankStatic.f1 is already initialized"));
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 13, 12),
                CompilerException.Code.ILLEGAL,
                "final variable l1 is already initialized"));
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 15, 9),
                CompilerException.Code.ILLEGAL,
                "final variable test.BlankStatic.f2 might already be initialized"));
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 20, 13),
                CompilerException.Code.ILLEGAL,
                "final variable test.BlankStatic.f1 is already initialized"));
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 22, 17),
                CompilerException.Code.ILLEGAL,
                "final variable test.BlankStatic.f2 is already initialized"));
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 23, 16),
                CompilerException.Code.ILLEGAL,
                "final variable l1 is already initialized"));
        pattern5.addReport(new CompilerException.Report(
                new StreamPos("BlankStatic.verics", 25, 13),
                CompilerException.Code.ILLEGAL,
                "final variable test.BlankStatic.f2 is already initialized"));
        compile(new PWD("regression/finalvariable"), files5,
                pattern5, null);
        String[] files6 = {
            "FinalSwitchLabelStatic.java",
        };
        CompilerException pattern6 =
                new CompilerException();
        pattern6.addReport(new CompilerException.Report(
                new StreamPos("FinalSwitchLabelStatic.java", 3, 20),
                CompilerException.Code.ILLEGAL,
                "final variable #default.FinalSwitchLabelStatic.label2 might be used uninitialized, reason: statement at FinalSwitchLabelStatic.java:7:18 reads label2 before"));        
        // this refuses to add default initializers to a static final
        compile(new PWD("regression/finalvariable"), files6,
                pattern6, null);
        String[] files7 = {
            "BlankUninitialized.verics",
        };
        CompilerException pattern7 =
                new CompilerException();
/// false alarm
///        pattern7.addReport(new CompilerException.Report(
///                new StreamPos("BlankUninitialized.verics", 15, 11),
///                CompilerException.Code.ILLEGAL,
///                "final variable f2 might be used uninitialized, reason: statement at BlankUninitialized.verics:13:16 reads f2 before"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 20, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might be used uninitialized, reason: call at BlankUninitialized.verics:22:9 is virtual and calls an overridable method"));
/// do not report same error twice
///        pattern7.addReport(new CompilerException.Report(
///                new StreamPos("BlankUninitialized.verics", 25, 11),
///                CompilerException.Code.ILLEGAL,
///                "final variable f2 might be used uninitialized, reason: call at BlankUninitialized.verics:22:9 is virtual and calls an overridable method"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 31, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might be used uninitialized, reason: call at BlankUninitialized.verics:33:9 spills this"));
/// do not report same error twice
///        pattern7.addReport(new CompilerException.Report(
///                new StreamPos("BlankUninitialized.verics", 36, 11),
///                CompilerException.Code.ILLEGAL,
///                "final variable f2 might be used uninitialized, reason: call at BlankUninitialized.verics:33:9 spills this"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 42, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might be used uninitialized, reason: statement at BlankUninitialized.verics:55:17 reads f2 before"));
/// do not report same error twice
///        pattern7.addReport(new CompilerException.Report(
///                new StreamPos("BlankUninitialized.verics", 47, 11),
///                CompilerException.Code.ILLEGAL,
///                "final variable f2 might be used uninitialized, reason: statement at BlankUninitialized.verics:55:17 reads f2 before"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 57, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might be used uninitialized, reason: statement at BlankUninitialized.verics:58:30 copies this"));
/// do not report same error twice
///        pattern7.addReport(new CompilerException.Report(
///                new StreamPos("BlankUninitialized.verics", 61, 11),
///                CompilerException.Code.ILLEGAL,
///                "final variable f2 might be used uninitialized, reason: statement at BlankUninitialized.verics:58:30 copies this"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 65, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might be used uninitialized, reason: object creation at BlankUninitialized.verics:66:21 spills this"));
/// do not report same error twice
///        pattern7.addReport(new CompilerException.Report(
///                new StreamPos("BlankUninitialized.verics", 69, 11),
///                CompilerException.Code.ILLEGAL,
///                "final variable f2 might be used uninitialized, reason: object creation at BlankUninitialized.verics:66:20 spills this"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 73, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might be used uninitialized, reason: passed as parameter at BlankUninitialized.verics:74:9"));
/// do not report same error twice
///        pattern7.addReport(new CompilerException.Report(
///                new StreamPos("BlankUninitialized.verics", 77, 11),
///                CompilerException.Code.ILLEGAL,
///                "final variable f2 might be used uninitialized, reason: passed as parameter at BlankUninitialized.verics:74:9"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 84, 13),
                CompilerException.Code.ILLEGAL,
                "final variable f2 is already initialized"));
        pattern7.addReport(new CompilerException.Report(
                new StreamPos("BlankUninitialized.verics", 87, 8),
                CompilerException.Code.ILLEGAL,
                "final variable f2 might be used uninitialized, reason: call at BlankUninitialized.verics:88:9 is virtual and calls an overridable method"));
        compile(new PWD("regression/finalvariable"), files7,
                pattern7, null);
        String[] filesForeign = {
            "ForeignFinal.java",
        };
        CompilerException patternForeign =
                new CompilerException();
        patternForeign.addReport(new CompilerException.Report(
                new StreamPos("ForeignFinal.java", 17, 29),
                CompilerException.Code.ILLEGAL,
                "final variable NON_DETERMINISTIC is already assigned"));
        patternForeign.addReport(new CompilerException.Report(
                new StreamPos("ForeignFinal.java", 23, 34),
                CompilerException.Code.ILLEGAL,
                "final variable example.Scheduler.HOUSEHOLDS is already assigned"));
        patternForeign.addReport(new CompilerException.Report(
                new StreamPos("ForeignFinal.java", 52, 33),
                CompilerException.Code.ILLEGAL,
                "final variable example.ForeignFinal.HOUSEHOLDS is already assigned"));
        compile(new PWD("regression/finalvariable"), filesForeign,
                patternForeign, null);
    }
    /**
     * Tests the operator <code>instanceof</code>.
     */
    protected void testInstanceOf() {
        testPerformed();
        String[] files = {
            "ClassA.java",
            "ClassB.java",
            "ClassC.java",
            "ParseInstanceOf.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseInstanceOf.java", 10, 10),
                CompilerException.Code.ILLEGAL,
                "inconvertible types"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseInstanceOf.java", 21, 10),
                CompilerException.Code.ILLEGAL,
                "inconvertible types"));
        compile(new PWD("regression/instanceof"), files,
                pattern, null);
        String[] files2 = {
            "ClassA.java",
            "ClassB.java",
            "ClassC.java",
            "InstanceOf.java",
        };
        compile(new PWD("regression/instanceof"), files2,
                null, "InstanceOf.xml.regression");
    }
    /**
     * Tests relations of object references.
     */
    protected void testObjectComparison() {
        testPerformed();
        String[] files = {
            "Clazz.verics",
            "ParseObjectComparison.java",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseObjectComparison.java", 14, 15),
                CompilerException.Code.ILLEGAL,
                "types of operands of binary == not convertible"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseObjectComparison.java", 20, 15),
                CompilerException.Code.ILLEGAL,
                "types of operands of binary != not convertible"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseObjectComparison.java", 27, 14),
                CompilerException.Code.ILLEGAL,
                "can not convert test.Clazz to Thread"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseObjectComparison.java", 28, 14),
                CompilerException.Code.ILLEGAL,
                "can not convert Thread to test.Clazz"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseObjectComparison.java", 29, 17),
                CompilerException.Code.ILLEGAL,
                "types of operands of binary == not convertible"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ParseObjectComparison.java", 35, 17),
                CompilerException.Code.ILLEGAL,
                "types of operands of binary != not convertible"));
        compile(new PWD("regression/objectcomparison"), files,
                pattern, null);
        String[] files2 = {
            "ObjectComparison.java",
        };
        compile(new PWD("regression/objectcomparison"), files2,
                null, "ObjectComparison.xml.regression");
    }
    /**
     * Tests translation of the source of College.
     */
    protected void testCollege() {
        testPerformed();
        String[] files = {
            "College.java",
        };
        compile(new PWD("regression"), files,
                null, "College.s0.regression");
        compile(new PWD("regression"), files,
                null, "College.s.regression");
        compile(new PWD("regression"), files,
                null, "College.xml.regression");
    }
    /**
     * Tests experiments.
     */
    protected void testExperiments() {
        testPerformed();
        String[] files = {
            "Experiment.java",
        };
        TestOptions testOptions = new TestOptions();
        testOptions.experimentMode =
                HedgeellethCompiler.Options.ExperimentMode.TRUE;
        compile(new PWD("regression/experiment"), files,
                null, null, testOptions, null);
    }
    /**
     * Tests field methods.
     */
    protected void testFieldMethods() {
        testPerformed();
        String[] files = {
            "FieldMethods.verics",
            "FieldMethodsMain.java",
        };
        compile(new PWD("regression/fieldmethods"), files,
                null, "FieldMethods.s.regression");
    }
    /**
     * Tests notifyAll(), with and without broadcast labels.
     */
    protected void testNotifyAll() {
        testPerformed();
        TestOptions testOptionsVerboseVariables = new TestOptions();
        testOptionsVerboseVariables.verboseTADDVariables = true;
        String[] files = {
            "NotifyAll.java",
        };
        compile(new PWD("regression"), files,
                null, "NotifyAll.xml.regression");
        compile(new PWD("regression"), files,
                null, "NotifyAll.nm.regression",
                testOptionsVerboseVariables, null);
    }
    /**
     * Tests detection of stray comment tags, including stray ranges.
     */
    protected void testStrayTags() {
        testPerformed();
        //
        // stray tags, with some tags having names
        //
        String[] filesNamed = {
            "NamedStray.java",
        };
        CompilerException patternNamed =
                new CompilerException();
        patternNamed.addReport(new CompilerException.Report(
                new StreamPos("NamedStray.java", 49, 17),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternNamed.addReport(new CompilerException.Report(
                new StreamPos("NamedStray.java", 49, 43),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternNamed.addReport(new CompilerException.Report(
                new StreamPos("NamedStray.java", 50, 21),
                CompilerException.Code.ILLEGAL,
                "stray comment tag"));
        patternNamed.addReport(new CompilerException.Report(
                new StreamPos("NamedStray.java", 60, 8),
                CompilerException.Code.ILLEGAL,
                "stray comment tag"));
        patternNamed.addReport(new CompilerException.Report(
                new StreamPos("NamedStray.java", 75, 16),
                CompilerException.Code.ILLEGAL,
                "stray comment tag"));
        patternNamed.addReport(new CompilerException.Report(
                new StreamPos("NamedStray.java", 81, 16),
                CompilerException.Code.ILLEGAL,
                "stray comment tag"));
        compile(new PWD("regression/tags"), filesNamed,
                patternNamed, null);
        //
        // tags with unknown prefixes
        //
        String[] filesTagPrefixUnknown = {
            "TagPrefixUnknown.java",
        };
        CompilerException patternTagPrefixUnknown =
                new CompilerException();
        patternTagPrefixUnknown.addReport(new CompilerException.Report(
                new StreamPos("TagPrefixUnknown.java", 77, 16),
                CompilerException.Code.UNKNOWN,
                "construction of PTA: unrecognized modifier location Eating in comment tag @state(location Eating)"));
        patternTagPrefixUnknown.addReport(new CompilerException.Report(
                new StreamPos("TagPrefixUnknown.java", 77, 16),
                CompilerException.Code.MISSING,
                "construction of PTA: missing modifier named name in comment tag @state(location Eating)"));
        patternTagPrefixUnknown.addReport(new CompilerException.Report(
                new StreamPos("TagPrefixUnknown.java", 81, 16),
                CompilerException.Code.UNKNOWN,
                "construction of PTA: unrecognized modifier named in comment tag @state(name locationSatiated, name locationOther, named)"));
        patternTagPrefixUnknown.addReport(new CompilerException.Report(
                new StreamPos("TagPrefixUnknown.java", 81, 16),
                CompilerException.Code.MISSING,
                "construction of PTA: duplicate modifier named name not allowed in comment tag @state(name locationSatiated, name locationOther, named)"));        
        compile(new PWD("regression/tags"), filesTagPrefixUnknown,
                patternTagPrefixUnknown, null);
        //
        // stray ranges
        //
        String[] filesStray = {
            "RangesStray.java",
        };
        CompilerException patternStray =
                new CompilerException();
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 4, 11),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 4, 28),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 5, 8),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 5, 27),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 5, 55),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 7, 20),
                CompilerException.Code.ILLEGAL,
                "primitive range not on a primitive type"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 10, 17),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 15, 16),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 19, 21),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 21, 20),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 22, 9),
                CompilerException.Code.ILLEGAL,
                "stray range tag"));
        patternStray.addReport(new CompilerException.Report(
                new StreamPos("RangesStray.java", 23, 4),
                CompilerException.Code.PARSE,
                "tag name missing"));
        compile(new PWD("regression/tags"), filesStray,
                patternStray, null);
    }
    /**
     * Tests ranges.
     */
    protected void testRanges() {
        DEFAULT_TEST_OPTIONS.test.extraOptimizations = false;
        testPerformed();
        //
        // ranges with name missing
        //
        String[] filesNameMissing = {
            "RangesNameMissing.java",
        };
        CompilerException patternNameMissing =
                new CompilerException();
        patternNameMissing.addReport(new CompilerException.Report(
                new StreamPos("RangesNameMissing.java", 6, 7),
                CompilerException.Code.PARSE,
                "tag name missing"));
        patternNameMissing.addReport(new CompilerException.Report(
                new StreamPos("RangesNameMissing.java", 8, 10),
                CompilerException.Code.PARSE,
                "tag name missing"));
        patternNameMissing.addReport(new CompilerException.Report(
                new StreamPos("RangesNameMissing.java", 14, 9),
                CompilerException.Code.PARSE,
                "tag name missing"));
        patternNameMissing.addReport(new CompilerException.Report(
                new StreamPos("RangesNameMissing.java", 19, 9),
                CompilerException.Code.PARSE,
                "tag name missing"));
        compile(new PWD("regression/ranges"), filesNameMissing,
                patternNameMissing, null);
        //
        // variable ranges with invalid type
        //
        String[] filesVariableType = {
            "RangesVariableType.java",
        };
        CompilerException patternVariableType =
                new CompilerException();
        patternVariableType.addReport(new CompilerException.Report(
                new StreamPos("RangesVariableType.java", 4, 15),
                CompilerException.Code.ILLEGAL,
                "primitive range not on a primitive type"));
        patternVariableType.addReport(new CompilerException.Report(
                new StreamPos("RangesVariableType.java", 12, 7),
                CompilerException.Code.ILLEGAL,
                "primitive range not on a primitive type"));
        compile(new PWD("regression/ranges"), filesVariableType,
                patternVariableType, null);
        //
        // primary ranges with invalid type
        //
        String[] filesPrimaryType = {
            "RangesPrimaryType.java",
        };
        CompilerException patternPrimaryType =
                new CompilerException();
        patternPrimaryType.addReport(new CompilerException.Report(
                new StreamPos("RangesPrimaryType.java", 6, 7),
                CompilerException.Code.ILLEGAL,
                "invalid type for primitive range: void"));
        patternPrimaryType.addReport(new CompilerException.Report(
                new StreamPos("RangesPrimaryType.java", 10, 11),
                CompilerException.Code.ILLEGAL,
                "invalid type for primitive range: #default.RangesPrimaryType"));
        compile(new PWD("regression/ranges"), filesPrimaryType,
                patternPrimaryType, null);
        //
        // arrays in interpreted code, value outside array's variable range
        //
        String[] filesInterpreted = {
            "RangesArrayInterpreted.java",
        };
        CompilerException patternInterpreted =
                new CompilerException();
        patternInterpreted.addReport(new CompilerException.Report(
                new StreamPos("RangesArrayInterpreted.java", 12, 12),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: value 12 is out of range <<-10, 11>> of variable l"));
        compile(new PWD("regression/ranges"), filesInterpreted,
                patternInterpreted, null);
        //
        // arrays in compiled code, initial values outside
        //
        TestOptions optionsNoConstantElements = new TestOptions();
        optionsNoConstantElements.test.ignoreConstantElements = true;
        String[] filesCompiledOutside = {
            "RangesArrayCompiledOutside.java",
        };
        CompilerException patternCompiledOutside =
                new CompilerException();
        patternCompiledOutside.addReport(new CompilerException.Report(
            new StreamPos("RangesArrayCompiledOutside.java", 8, 12),
            CompilerException.Code.ILLEGAL,
            "construction of PTA: in thread #default.RangesArrayCompiledOutside_1: initial value 11 is outside the range <<-10, 10>> of runtime array referenced by variable at RangesArrayCompiledOutside.java:4:25"));
        compile(new PWD("regression/ranges"), filesCompiledOutside,
                patternCompiledOutside, null, optionsNoConstantElements,
                null);
        String[] filesCompiledOutside_c = {
            "RangesArrayCompiledOutside_c.java",
        };
        CompilerException patternCompiledOutside_c =
                new CompilerException();
        patternCompiledOutside_c.addReport(new CompilerException.Report(
                new StreamPos("RangesArrayCompiledOutside_c.java", 10, 12),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: static analysis: indexing of array #system.#Array#106##c5: value 11 is out of range <<-10, 10>> of variable #c4"));
        compile(new PWD("regression/ranges"), filesCompiledOutside_c,
                patternCompiledOutside_c, null);
        //
        // arrays in compiled code, mismatch of variable ranges
        //
        String[] filesCompiledMismatch = {
            "RangesArrayCompiledMismatch.java",
        };
        CompilerException patternCompiledMismatch =
                new CompilerException();
        patternCompiledMismatch.addReport(new CompilerException.Report(
                new StreamPos("RangesArrayCompiledMismatch.java", 10, 12),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread #default.RangesArrayCompiledMismatch_1: range <<-6, 6>> of a variable at RangesArrayCompiledMismatch.java:5:23 does not match range <<-10, 10>> of a variable at RangesArrayCompiledMismatch.java:4:25, but both variables are indexed when referring to the same array"));
        compile(new PWD("regression/ranges"), filesCompiledMismatch,
                patternCompiledMismatch, null);
        //
        // range variables that reduce to constants
        //
        String[] filesConstants = {
            "RangesConstants.java",
        };
        TestOptions optionsPreserveRangedLocals = new TestOptions();
        optionsPreserveRangedLocals.preserveRangedLocals = true;
        optionsPreserveRangedLocals.assignmentTransportCheck = false;
        optionsPreserveRangedLocals.test.extraOptimizations = false;
        compile(new PWD("regression/ranges"), filesConstants,
                null, "RangesConstants.s.regression", optionsPreserveRangedLocals,
                null);
        //
        // range variables that do not reduce to constants
        //
        String[] filesVariables = {
            "RangesVariables.java",
        };
        CompilerException patternVariables = new CompilerException();
        patternVariables.addReport(new CompilerException.Report(
                new StreamPos("RangesVariables.java", 9, 23),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: primitive range not evaluated as constant"));
        patternVariables.addReport(new CompilerException.Report(
                new StreamPos("RangesVariables.java", 10, 28),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: primitive range not evaluated as constant"));
        compile(new PWD("regression/ranges"), filesVariables,
                patternVariables, null);
        //
        // expression in interpreter code, value outside primary range
        //
        String[] filesExpressionInterpreted = {
            "RangesExpressionInterpreted.java",
        };
        CompilerException patternExpressionInterpreted = new CompilerException();
        patternExpressionInterpreted.addReport(new CompilerException.Report(
                new StreamPos("RangesExpressionInterpreted.java", 9, 44),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: range check error, value is -2, should be in <<-20, -3>>"));
        compile(new PWD("regression/ranges"), filesExpressionInterpreted,
                patternExpressionInterpreted, null);
        //
        // range check error in primary range, that is within compiled code
        //
        String[] filesExpressionCompiledCheckError = {
            "RangesExpressionCompiledCheckError.java",
        };
        CompilerException patternExpressionCompiledCheckError = new CompilerException();
        patternExpressionCompiledCheckError.addReport(new CompilerException.Report(
                new StreamPos("RangesExpressionCompiledCheckError.java", 9, 27),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in runtime code analysis: range check error, value is -4, should be in <<-3, 1>>"));
        compile(new PWD("regression/ranges"), filesExpressionCompiledCheckError,
                patternExpressionCompiledCheckError, null);
        //
        // check computation of variable and primary primitive ranges on basis of code
        // not all primary bounds reduced to constants
        //
        String[] filesExpressionCompiledNotConstant = {
            "RangesExpressionCompiledNotConstant.java",
        };
        compile(new PWD("regression/ranges"), filesExpressionCompiledNotConstant,
                null, "RangesExpressionCompiledNotConstant.xml.regression");
        TestOptions testOptionsVerboseVariables = new TestOptions();
        testOptionsVerboseVariables.verboseTADDVariables = true;
        compile(new PWD("regression/ranges"), filesExpressionCompiledNotConstant,
                null, "RangesExpressionCompiledNotConstant.nh.regression",
                testOptionsVerboseVariables, null);
        TestOptions testOptionsNoCheck = new TestOptions();
        testOptionsNoCheck.verboseTADDVariables = true;
        testOptionsNoCheck.rangeChecking = false;
        String[] filesExpressionCompiledNotConstantNoCheck = {
            "RangesExpressionCompiledNotConstantNoCheck.java",
        };
        compile(new PWD("regression/ranges"), filesExpressionCompiledNotConstantNoCheck,
                null, "RangesExpressionCompiledNotConstantNoCheck.nh.regression",
                testOptionsNoCheck, null);
        String[] filesExpressionCompiledNotConstantS = {
            "RangesExpressionCompiledNotConstantS.java",
        };
        compile(new PWD("regression/ranges"), filesExpressionCompiledNotConstantS,
                null, "RangesExpressionCompiledNotConstantS.xml.regression",
                null, TURN_GAME_SCHEDULER);
        //
        // check computation of variable and primary primitive ranges on basis of code
        // all primary bounds reduced to constants
        //
        // ranges not optimized
        String[] filesExpressionCompiledConstant = {
            "RangesExpressionCompiledConstant.java",
        };
        TestOptions optionsNoRangeCheckOptimization = new TestOptions();
        optionsNoRangeCheckOptimization.messageStyle = MessageStyle.Type.JAVA;
        optionsNoRangeCheckOptimization.removeConstGuardsUsingRanges = false;
        compile(new PWD("regression/ranges"), filesExpressionCompiledConstant,
                null, "RangesExpressionCompiledConstant.xml.regression",
                optionsNoRangeCheckOptimization, null);
        String[] filesExpressionCompiledConstantS = {
            "RangesExpressionCompiledConstantS.java",
        };
        compile(new PWD("regression/ranges"), filesExpressionCompiledConstantS,
                null, "RangesExpressionCompiledConstantS.xml.regression",
                optionsNoRangeCheckOptimization, TURN_GAME_SCHEDULER);
        // ranges optimized
        String[] filesExpressionCompiledConstant_o = {
            "RangesExpressionCompiledConstant_o.java",
        };
        compile(new PWD("regression/ranges"), filesExpressionCompiledConstant_o,
                null, "RangesExpressionCompiledConstant_o.xml.regression");
        //
        // proposals for indexing
        //
        String[] filesIndexing = {
            "Indexing.java",
        };
        compile(new PWD("regression/ranges"), filesIndexing,
                null, "Indexing.xml.regression", optionsNoConstantElements, null);
        String[] filesIndexing_c = {
            "Indexing_c.java",
        };
        compile(new PWD("regression/ranges"), filesIndexing_c,
                null, "Indexing_c.xml.regression");
        String[] filesIndexingOutOfRange1 = {
            "IndexingOutOfRange1.java",
        };
        CompilerException patternIndexingOutOfRange1 = new CompilerException();
        patternIndexingOutOfRange1.addReport(new CompilerException.Report(
                new StreamPos("IndexingOutOfRange1.java", 10, 12),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: static analysis: indexing of array #system.#Array#107#indexing.IndexingOutOfRange1::a: index out of bounds: 2, array size is 2"));
        compile(new PWD("regression/ranges"), filesIndexingOutOfRange1,
                patternIndexingOutOfRange1, null);
        String[] filesIndexingOutOfRange2 = {
            "IndexingOutOfRange2.java",
        };
        CompilerException patternIndexingOutOfRange2 = new CompilerException();
        patternIndexingOutOfRange2.addReport(new CompilerException.Report(
                new StreamPos("IndexingOutOfRange2.java", 10, 12),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread indexing.IndexingOutOfRange2_1: index out of range: 3, length of array #system.#Array#108#indexing.IndexingOutOfRange2::b is 3"));
        compile(new PWD("regression/ranges"), filesIndexingOutOfRange2,
                patternIndexingOutOfRange2, null);
        //
        // range checking for Prism
        //
        String[] filesRangeCheckPrismNoSourceLevel = {
            "RangeCheckPrismNoSourceLevel.java",
        };
        compile(new PWD("regression/ranges"), filesRangeCheckPrismNoSourceLevel,
                null, "RangeCheckPrismNoSourceLevel.nm.regression",
                testOptionsVerboseVariables, null);
        String[] filesRangeCheckPrism = {
            "RangeCheckPrism.java",
        };
        compile(new PWD("regression/ranges"), filesRangeCheckPrism,
                null, "RangeCheckPrism.nm.regression",
                testOptionsVerboseVariables, null);
        testOptionsVerboseVariables.rangeChecking = false;
        String[] filesRangeCheckPrismNoCheck = {
            "RangeCheckPrismNoCheck.java",
        };
        compile(new PWD("regression/ranges"), filesRangeCheckPrismNoCheck,
                null, "RangeCheckPrismNoCheck.nm.regression",
                testOptionsVerboseVariables, null);
        testOptionsVerboseVariables.rangeChecking = true;
        testOptionsVerboseVariables.messageStyle = MessageStyle.Type.VERICS;
        testOptionsVerboseVariables.verboseTADDVariables = true;
        testOptionsVerboseVariables.assignmentTransportCheck = false;
        testOptionsVerboseVariables.test.extraOptimizations = false;
        String[] filesMethodRange = {
            "MethodRange.verics",
        };
        compile(new PWD("regression/ranges"), filesMethodRange,
                null, "MethodRange.s.regression", testOptionsVerboseVariables,
                null);
        testOptionsVerboseVariables.preserveRangedLocals = true;
        compile(new PWD("regression/ranges"), filesMethodRange,
                null, "MethodRange.nh.regression", testOptionsVerboseVariables,
                null);
        String[] filesMethodRangeNotFinal = {
            "MethodRangeNotFinal.verics",
        };
        CompilerException patternMethodRangeNotFinal =
                new CompilerException();
        patternMethodRangeNotFinal.addReport(new CompilerException.Report(
                new StreamPos("MethodRangeNotFinal.verics", 7, 24),
                CompilerException.Code.ILLEGAL,
                "method with ranged return type must be final"));
        patternMethodRangeNotFinal.addReport(new CompilerException.Report(
                new StreamPos("MethodRangeNotFinal.verics", 10, 31),
                CompilerException.Code.ILLEGAL,
                "method with ranged return type must be final"));
        patternMethodRangeNotFinal.addReport(new CompilerException.Report(
                new StreamPos("MethodRangeNotFinal.verics", 13, 31),
                CompilerException.Code.ILLEGAL,
                "method with ranged return type must be final"));
        compile(new PWD("regression/ranges"), filesMethodRangeNotFinal,
                patternMethodRangeNotFinal, null);
        String[] filesMethodEmptyRange = {
            "MethodEmptyRange.verics",
        };
        CompilerException patternMethodEmptyRange =
                new CompilerException();
        patternMethodEmptyRange.addReport(new CompilerException.Report(
                new StreamPos("MethodEmptyRange.verics", 16, 10),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in runtime code analysis: invalid bounds of i#0#default.MethodRange_obj_G_#retval detected: [src]<<70L, 15L>>"));
        compile(new PWD("regression/ranges"), filesMethodEmptyRange,
                patternMethodEmptyRange, null);
        DEFAULT_TEST_OPTIONS.test.extraOptimizations = true;
        // ranges in method arguments
        String[] filesMethodArgRange1 = {
            "MethodArgRange1.verics",
        };
        CompilerException patternMethodArgRange1 =
                new CompilerException();
        patternMethodArgRange1.addReport(new CompilerException.Report(
                new StreamPos("MethodArgRange1.verics", 29, 43),
                CompilerException.Code.ILLEGAL,
                "field lacks a qualifier"));
        patternMethodArgRange1.addReport(new CompilerException.Report(
                new StreamPos("MethodArgRange1.verics", 29, 52),
                CompilerException.Code.LOOK_UP,
                "variable not found: false"));        
        compile(new PWD("regression/ranges"), filesMethodArgRange1,
                patternMethodArgRange1, null);
        String[] filesMethodArgRange2 = {
            "MethodArgRange2.verics",
        };
        CompilerException patternMethodArgRange2 =
                new CompilerException();
        patternMethodArgRange2.addReport(new CompilerException.Report(
                new StreamPos("MethodArgRange2.verics", 29, 23),
                CompilerException.Code.ILLEGAL,
                "this referenced before superclass is constructed"));
        compile(new PWD("regression/ranges"), filesMethodArgRange2,
                patternMethodArgRange2, null);
        /* prologue and epilogue, to be possibly done
        String[] filesMethodArgRange3 = {
            "MethodArgRange3.verics",
        };
        compile(new PWD("regression/ranges"), filesMethodArgRange3,
                null, "MethodArgRange3.s.regression");
         */
    }
    /**
     * Tests resolution of exit states, so that they are not treated by the
     * checker as deadlock states. Exit states are ones without any output
     * transitions, that is, they are states that finish a thread.
     */
    protected void testExit() {
        testPerformed();
        TestOptions testOptionsVerboseVariables = new TestOptions();
        testOptionsVerboseVariables.verboseTADDVariables = true;
        String[] filesMinimal = {
            "Minimal.java",
        };
        // outputting to XML does not require any special resolution of
        // exit states
        compile(new PWD("regression/exit"), filesMinimal,
                null, "Minimal.xml.regression");
        compile(new PWD("regression/exit"), filesMinimal,
                null, "Minimal.nm.regression",
                testOptionsVerboseVariables, null);
        String[] filesExit = {
            "Exit.java",
        };
        compile(new PWD("regression/exit"), filesExit,
                null, "Exit.xml.regression");
        compile(new PWD("regression/exit"), filesExit,
                null, "Exit.nm.regression",
                testOptionsVerboseVariables, null);
    }
    /**
     * Tests assertions, including source level ranges.
     */
    protected void testAssertions() {
        testPerformed();
        TestOptions testOptionsVerboseVariables = new TestOptions();
        testOptionsVerboseVariables.verboseTADDVariables = true;
        String[] filesRangeCheckFalse = {
            "RangeCheckFalse.java",
        };
        TestOptions optionsVerbose = new TestOptions();
        optionsVerbose.verboseTADDVariables = true;
        // expression nesting disabled
        compile(new PWD("regression/assertions"), filesRangeCheckFalse,
                null, "RangeCheckFalse.nh.regression", optionsVerbose,
                null);
        String[] filesRangeCheckFalseNm = {
            "RangeCheckFalseNm.java",
        };
        // expression nesting not done because of field
        // access conflicts
        compile(new PWD("regression/assertions"), filesRangeCheckFalseNm,
                null, "RangeCheckFalse.nm.regression",
                testOptionsVerboseVariables, null);
        String[] filesRangeCheckTrue = {
            "RangeCheckTrue.java",
        };
        // expression nesting disabled
        compile(new PWD("regression/assertions"), filesRangeCheckTrue,
                null, "RangeCheckTrue.nh.regression", optionsVerbose, null);
        String[] filesRangeCheckTrueNm = {
            "RangeCheckTrueNm.java",
        };
        // expression nesting not done because of field
        // access conflicts
        compile(new PWD("regression/assertions"), filesRangeCheckTrueNm,
                null, "RangeCheckTrueNm.nm.regression",
                testOptionsVerboseVariables, null);
        String[] filesAssertions = {
            "Assertions.java",
        };
        compile(new PWD("regression/assertions"), filesAssertions,
                null, "Assertions.s0.regression");
        String[] filesAssertionsFailure = {
            "AssertionsFailure.java",
        };
        CompilerException patternAssertionsFailure =
                new CompilerException();
        patternAssertionsFailure.addReport(new CompilerException.Report(
                new StreamPos("AssertionsFailure.java", 13, 28),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: exception"));        
        compile(new PWD("regression/assertions"), filesAssertionsFailure,
                patternAssertionsFailure, null);
        String[] filesAssertElseParse = {
            "AssertElseParse.verics",
        };
        CompilerException patternAssertElse =
                new CompilerException();
        patternAssertElse.addReport(new CompilerException.Report(
            new StreamPos("AssertElseParse.verics", 12, 15),
            CompilerException.Code.PARSE,
            "assert has no condition and no message"));
        compile(new PWD("regression/assertions"), filesAssertElseParse,
                patternAssertElse, null);
        String[] filesAssertElseUnreachable = {
            "AssertElseUnreachable.verics",
        };
        CompilerException patternAssertElseUnreachable =
                new CompilerException();
        patternAssertElseUnreachable.addReport(new CompilerException.Report(
                new StreamPos("AssertElseUnreachable.verics", 24, 13),
                CompilerException.Code.ILLEGAL,
                "unreachable code"));
        patternAssertElseUnreachable.addReport(new CompilerException.Report(
                new StreamPos("AssertElseUnreachable.verics", 24, 16),
                CompilerException.Code.ILLEGAL,
                "unreachable code"));
        compile(new PWD("regression/assertions"), filesAssertElseUnreachable,
                patternAssertElseUnreachable, null);
        String[] filesAssertElseInitialization = {
            "AssertElseParseInitialization.verics",
        };
        CompilerException patternAssertElseInitialization =
                new CompilerException();
        patternAssertElseInitialization.addReport(new CompilerException.Report(
                new StreamPos("AssertElseParseInitialization.verics", 39, 18),
                CompilerException.Code.ILLEGAL,
                "variable IPP_TYPE might be used uninitialized in this expression"));
        patternAssertElseInitialization.addReport(new CompilerException.Report(
                new StreamPos("AssertElseParseInitialization.verics", 113, 18),
                CompilerException.Code.ILLEGAL,
                "variable IPP_TYPE might be used uninitialized in this expression"));
        compile(new PWD("regression/assertions"), filesAssertElseInitialization,
                patternAssertElseInitialization, null);
    }
    /**
     * Tests the C++ backend.
     */
    protected void testCPP() {
        testPerformed();
        String[] filesInitialization = {
            "Volumes.java",
        };
        compile(new PWD("regression/cpp/initialization"), filesInitialization,
                null, "Volumes.h.regression");
        String[] filesCircularDependency = {
            "A.java",
            "B.java",
            "C.java",
        };
        CompilerException patternCircularDependencyError = new CompilerException();
        patternCircularDependencyError.addReport(new CompilerException.Report(
            new StreamPos("B.java", 1, 7),
            CompilerException.Code.ILLEGAL,
            "C++ compiler--level covariance of return types caused a circular dependency with C.java:1:7"));
        compile(new PWD("regression/cpp/circular"), filesCircularDependency,
                patternCircularDependencyError, "cpp");
        // tests constructor delegates with the workaround that
        // uses constructor--equivalent method
        String[] filesConstructorDelegate = {
            "A.java",
            "B.java",
        };
        compile(new PWD("regression/cpp/constructordelegate"), filesConstructorDelegate,
                null, "A.h.regression");
        // tests a random code taken from the Octree library and modified
        // for the purspose of this test
        String[] filesOctree = {
            "AbstractLocation.java",
            "AbstractIntLocation.java",
            "Index.java",
            "Size.java",
            "Octree.java",
            "AbstractVoxel.java",
            "AbstractVectorVoxel.java",
            "Real8Voxel.java",
            "VoxelUtils.java",
        };
        compile(new PWD("regression/cpp/octree"), filesOctree,
                null, "AbstractLocation.h.regression");
    }
    /**
     * Tests errors caused by attempts to use a non--initialized
     * class.
     */
    protected void testSuper() {
        testPerformed();
        String[] files = {
            "A.verics",
            "B.verics",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
            new StreamPos("B.verics", 5, 5),
            CompilerException.Code.ILLEGAL,
            "this referenced before superclass is constructed"));
        pattern.addReport(new CompilerException.Report(
            new StreamPos("B.verics", 5, 11),
            CompilerException.Code.ILLEGAL,
            "this referenced before superclass is constructed"));
        compile(new PWD("regression/super"), files,
                pattern, null);
    }
    /**
     * Tests type conversions.
     */
    protected void testTypeConversion() {
        testPerformed();
        // illegal conversion from void
        String[] voidFiles = {
            "VoidConversion.verics",
        };
        CompilerException voidPattern =
                new CompilerException();
        voidPattern.addReport(new CompilerException.Report(
                new StreamPos("VoidConversion.verics", 5, 9),
                CompilerException.Code.ILLEGAL,
                "can not convert between arithmetic and non--arithmetic type: V to Z"));
        voidPattern.addReport(new CompilerException.Report(
                new StreamPos("VoidConversion.verics", 6, 21),
                CompilerException.Code.ILLEGAL,
                "illegal operands: String, V for binary +"));
        voidPattern.addReport(new CompilerException.Report(
                new StreamPos("VoidConversion.verics", 7, 23),
                CompilerException.Code.ILLEGAL,
                "illegal operand V for unary -"));        
        compile(new PWD("regression/typeconv"), voidFiles,
                voidPattern, null);
    }
    /**
     * Tests missing packages, classes or members in import statements.
     */
    protected void testMissingImport() {
        testPerformed();
        String[] filesMissing = {
            "MissingImport.verics",
        };
        CompilerException patternMissing =
                new CompilerException();
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 3, 1),
                CompilerException.Code.MISSING,
                "package P2 does not exist"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 4, 1),
                CompilerException.Code.MISSING,
                "class P0.C1 does not exist"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 5, 1),
                CompilerException.Code.MISSING,
                "class P0.C2 does not exist"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 6, 1),
                CompilerException.Code.MISSING,
                "class verics.math.C3 does not exist"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 7, 1),
                CompilerException.Code.MISSING,
                "qualified member name expected"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 8, 1),
                CompilerException.Code.MISSING,
                "qualified class name expected"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 9, 1),
                CompilerException.Code.MISSING,
                "qualified member name expected"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 11, 1),
                CompilerException.Code.MISSING,
                "public static member g does not exist"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 12, 1),
                CompilerException.Code.MISSING,
                "public static member h does not exist"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 14, 1),
                CompilerException.Code.MISSING,
                "public static member j does not exist"));
        patternMissing.addReport(new CompilerException.Report(
                new StreamPos("MissingImport.verics", 15, 1),
                CompilerException.Code.MISSING,
                "public static member k does not exist"));        
        compile(new PWD("regression/"), filesMissing,
                patternMissing, null);
    }
    /**
     * Tests static imports.
     */
    protected void testStaticImport() {
        testPerformed();
        String[] files = {
            "P1.verics",
            "P2.verics",
            "A.verics",
        };
        CompilerException pattern =
                new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 12, 15),
                CompilerException.Code.AMBIGUOUS,
                "ambiguous field, found in test.P1 and test.P2"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 13, 9),
                CompilerException.Code.AMBIGUOUS,
                "ambiguous call, found in test.P1 and test.P2"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 14, 13),
                CompilerException.Code.LOOK_UP,
                "symbol not found: g"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 15, 9),
                CompilerException.Code.LOOK_UP,
                "method not found: get(R)"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 16, 9),
                CompilerException.Code.LOOK_UP,
                "symbol not found: P1"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 17, 15),
                CompilerException.Code.LOOK_UP,
                "symbol not found: P2"));        
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 17, 23),
                CompilerException.Code.ILLEGAL,
                "field or method expected"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("A.verics", 18, 36),
                CompilerException.Code.ILLEGAL,
                "method o(B) of test.P2 in P2.verics:3:8 has ex access"));
        compile(new PWD("regression/staticimport"), files,
                pattern, null);
    }
    /**
     * Tests continuous time models.
     */
    protected void testCT() {
        testPerformed();
        String[] files = {
            "MM1.verics",
        };
        compile(new PWD("regression/ct"), files,
                null, "MM1.s.regression");
        compile(new PWD("regression/ct"), files,
                null, "MM1.nh.regression");
        String[] files_l = {
            "MM1_l.verics",
        };
        compile(new PWD("regression/ct"), files_l,
                null, "MM1_l.nh.regression");
        String[] filesBranch = {
            "Branch.verics",
        };
        compile(new PWD("regression/ct"), filesBranch,
                null, "Branch.nh.regression");
        String[] filesNondeterministic = {
            "Nondeterministic.verics",
        };
        compile(new PWD("regression/ct"), filesNondeterministic,
                null, "Nondeterministic.nh.regression");
        String[] filesBranchVariable = {
            "BranchVariable.verics",
        };
        compile(new PWD("regression/ct"), filesBranchVariable,
                null, "BranchVariable.nh.regression");
        String[] filesBranchVariableS = {
            "BranchVariableS.verics",
        };
        compile(new PWD("regression/ct"), filesBranchVariableS,
                null, "BranchVariableS.nh.regression", null,
                TURN_GAME_SCHEDULER);
        compile(new PWD("regression/ct"), filesBranchVariable,
                null, "BranchVariable.pxml.regression");
    }
    /**
     * Tests flattening of conditionial trees.
     */
    protected void testConditionalTrees() {
        testPerformed();
        TestOptions optionsNoRangeCheckOptimization = new TestOptions();
        optionsNoRangeCheckOptimization.verboseTADDVariables = true;
        optionsNoRangeCheckOptimization.removeConstGuardsUsingRanges = false;
        String[] files = {
            "ConditionalTree.verics",
        };
        compile(new PWD("regression"), files,
                null, "ConditionalTree.nh.regression",
                optionsNoRangeCheckOptimization, null);
        String[] filesS = {
            "ConditionalTreeS.verics",
        };
        compile(new PWD("regression"), filesS,
                null, "ConditionalTreeS.nh.regression",
                optionsNoRangeCheckOptimization,
                TURN_GAME_SCHEDULER);
        String[] files_o = {
            "ConditionalTree_o.verics",
        };
        compile(new PWD("regression"), files_o,
                null, "ConditionalTree_o.nh.regression");
    }
    /**
     * Tests constant indexing.
     */
    protected void testConstantIndexing() {
        testPerformed();
        String[] files = {
            "ConstantIndexing.verics",
        };
        compile(new PWD("regression/ci"), files,
                null, "ConstantIndexing.s.regression");
        TestOptions optionsNoConstantElements = new TestOptions();
        optionsNoConstantElements.messageStyle = MessageStyle.Type.VERICS;
        optionsNoConstantElements.verboseTADDVariables = true;
        optionsNoConstantElements.test.ignoreConstantElements = true;
        compile(new PWD("regression/ci"), files,
                null, "ConstantIndexing.nh.regression",
                optionsNoConstantElements, null);
        String[] filesS = {
            "ConstantIndexingS.verics",
        };
        compile(new PWD("regression/ci"), filesS,
                null, "ConstantIndexingS.nh.regression",
                optionsNoConstantElements, TURN_GAME_SCHEDULER);
        String[] filesOutOfRange = {
            "ConstantIndexingOutOfRange.verics",
        };
        compile(new PWD("regression/ci"), filesOutOfRange,
                null, "ConstantIndexingOutOfRange.nh.regression",
                optionsNoConstantElements, null);
        String[] filesOutOfRangeS = {
            "ConstantIndexingOutOfRangeS.verics",
        };
        compile(new PWD("regression/ci"), filesOutOfRangeS,
                null, "ConstantIndexingOutOfRangeS.nh.regression",
                optionsNoConstantElements, TURN_GAME_SCHEDULER);
        String[] filesPartial = {
            "ConstantIndexingPartial.verics",
        };
        compile(new PWD("regression/ci"), filesPartial,
                null, "ConstantIndexingPartial.nm.regression");
        String[] filesPartialWrite = {
            "ConstantIndexingPartialWrite.verics",
        };
        compile(new PWD("regression/ci"), filesPartialWrite,
                null, "ConstantIndexingPartialWrite.nh.regression");
        compile(new PWD("regression/ci"), filesPartialWrite,
                null, "ConstantIndexingPartialWrite.nm.regression");
        String[] filesResult = {
            "ConstantIndexingResult.verics",
        };
        compile(new PWD("regression/ci"), filesResult,
                null, "ConstantIndexingResult.nh.regression");
    }
    /**
     * Tests unrolling of for--like loops.
     */
    protected void testUnroll() {
        testPerformed();
        String[] files = {
            "Unroll.verics",
        };
        compile(new PWD("regression/unroll"), files,
                null, "Unroll.nh.regression");
        String[] filesVariableRange = {
            "UnrollVariableRange.java",
        };
        CompilerException patternVariableRange =
                new CompilerException();
        patternVariableRange.addReport(new CompilerException.Report(
                new StreamPos("UnrollVariableRange.java", 10, 28),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: primitive range not evaluated as constant"));
        compile(new PWD("regression/unroll"), filesVariableRange,
                patternVariableRange, null);
    }
    /**
     * Tests the Microgrid example game.
     */
    protected void testIndexFormula() {
        testPerformed();
        // array formulas that require index checking
        String[] filesIndexFormulaCheck = {
            "IndexFormulaCheck.java",
        };
        compile(new PWD("regression/indexformula"), filesIndexFormulaCheck,
                null, "IndexFormulaCheck.nm.regression", null,
                TURN_GAME_SCHEDULER);
    }
    /**
     * Tests barriers, using the Microgrid example.
     */
    protected void testBarrier() {
        testPerformed();
        String[] filesBadCount = {
            "MicrogridBarrierBadCount.java",
        };
        CompilerException patternBadCount =
                new CompilerException();
        patternBadCount.addReport(new CompilerException.Report(
                null,
                CompilerException.Code.ILLEGAL,
                "construction of PTA: barrier hc.Barrier#109 declares a total of 3 parties, but their actual number is 4"));
        compile(new PWD("regression/barrier"), filesBadCount,
               patternBadCount, null);
        String[] filesMicrogrid = {
            "Microgrid.java",
        };
        compile(new PWD("regression/barrier"), filesMicrogrid,
                null, "Microgrid.nm.regression", null,
                TURN_GAME_SCHEDULER);
        String[] filesBarrierInterrupted = {
            "BarrierInterrupted.java",
        };
        compile(new PWD("regression/barrier"), filesBarrierInterrupted,
                null, "BarrierInterrupted.nm.regression", null,
                TURN_GAME_SCHEDULER);
        // to test <code>testPTAIO()</code>
        compile(new PWD("regression/barrier"), filesBarrierInterrupted,
                null, "BarrierInterrupted.nh.regression", null,
                TURN_GAME_SCHEDULER);
    }
    /**
     * Compiles a test of a given scheduler.
     * 
     * @param type type of the scheduler
     * @param override if the scheduler overrides
     */
    private void compileScheduler(SchedulerType type, boolean override) {
        String suffix;
        switch(type) {
            case FREE:
                suffix = "F";
                break;
                
            case IMMEDIATE:
                suffix = "I";
                break;
                
            case HIDDEN:
                suffix = "H";
                break;
                
            case CONST:
                suffix = "C";
                break;
                
            case NXP:
                suffix = "N";
                break;
                
            default:
                throw new RuntimeException("unknown type");
                
        }
        if(override)
            suffix += "O";
        String[] files = {
            "AltBitProtocol" + suffix + ".java",
            "Sender.java",
            "Receiver.java",
            "LossyChannel.java",
        };
        compile(new PWD("regression/scheduler"), files,
                null, "AltBitProtocol" + suffix + ".nh.regression",
                null, new Scheduler(type, false, override,
                    1.0, 1.1, true, true, false));
    }
    /**
     * Tests different schedulers.
     */
    protected void testScheduler() {
        testPerformed();
        String[] files = {
            "CoinFlipFixed.java",
        };
        compile(new PWD("regression/scheduler"), files,
                null, "CoinFlipFixed.nm.regression",
                null, new Scheduler(SchedulerType.HIDDEN, true, false,
                    1.0, 1.1, true, true, false));
        compileScheduler(SchedulerType.FREE, false);
        compileScheduler(SchedulerType.FREE, true);
        compileScheduler(SchedulerType.IMMEDIATE, false);
        compileScheduler(SchedulerType.IMMEDIATE, true);
        compileScheduler(SchedulerType.HIDDEN, false);
        compileScheduler(SchedulerType.HIDDEN, true);
        compileScheduler(SchedulerType.CONST, false);
        compileScheduler(SchedulerType.CONST, true);
        compileScheduler(SchedulerType.NXP, false);
        compileScheduler(SchedulerType.NXP, true);
    }
    /**
     * Tests the Microgrid example game.
     */
    protected void testMicrogrid() {
        testPerformed();
        String[] filesP = {
            "MicrogridP.java",
        };
        CompilerException patternP =
                new CompilerException();
        patternP.addReport(new CompilerException.Report(
                new StreamPos("MicrogridP.java", 87, 15),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: example.Household#118 already has a player name"));
        compile(new PWD("regression/microgrid"), filesP,
                patternP, null);
        String[] filesPendingFormula = {
            "MicrogridPendingFormula.java",
        };
        compile(new PWD("regression/microgrid"), filesPendingFormula,
                null, "MicrogridPendingFormula.nh.regression");
        String[] filesNullArray = {
            "MicrogridNullArray.java",
        };
        CompilerException patternNullArray =
                new CompilerException();
        patternNullArray.addReport(new CompilerException.Report(
                new StreamPos("MicrogridNullArray.java", 68, 34),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread example.Scheduler_0: dereference on a null array"));        
        compile(new PWD("regression/microgrid"), filesNullArray,
                patternNullArray, null, null,
                TURN_GAME_SCHEDULER);
        String[] filesNameClash = {
            "MicrogridNameClash.java",
        };
        CompilerException patternNameClash =
                new CompilerException();
        patternNameClash.addReport(new CompilerException.Report(
                new StreamPos("MicrogridNameClash.java", 74, 16),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread example.Household_2: naming convention for example.Household#116 caused name clash: job1"));
        compile(new PWD("regression/microgrid"), filesNameClash,
                patternNameClash, null, null,
                TURN_GAME_SCHEDULER);
        DEFAULT_TEST_OPTIONS.test.extraOptimizations = false;
        String[] filesParticipants = {
            "MicrogridParticipants.java",
        };
        compile(new PWD("regression/microgrid"), filesParticipants,
                null, "MicrogridParticipants.nm.regression", null,
                TURN_GAME_SCHEDULER);
        String[] filesParticipants2 = {
            "MicrogridParticipants2.java",
        };
        compile(new PWD("regression/microgrid"), filesParticipants2,
                null, "MicrogridParticipants2.nm.regression", null,
                TURN_GAME_SCHEDULER);
        // needed in <code>testPTAIO()</code>
        compile(new PWD("regression/microgrid"), filesParticipants,
                null, "MicrogridParticipants.nh.regression", null,
                TURN_GAME_SCHEDULER);
        DEFAULT_TEST_OPTIONS.test.extraOptimizations = true;
        String[] files = {
            "Microgrid.java",
        };
        compile(new PWD("regression/microgrid"), files,
                null, "Microgrid.nm.regression", null,
                TURN_GAME_SCHEDULER);
        compile(new PWD("regression/microgrid"), files,
                null, "Microgrid.nh.regression", null,
                TURN_GAME_SCHEDULER);
        String[] filesFast = {
            "MicrogridFast.java",
        };
        compile(new PWD("regression/microgrid"), filesFast,
                null, "MicrogridFast.nm.regression", null,
                TURN_GAME_SCHEDULER);
        TestOptions optionsArraysSupported = new TestOptions();
        optionsArraysSupported.messageStyle = MessageStyle.Type.VERICS;
        optionsArraysSupported.verboseTADDVariables = true;
        optionsArraysSupported.arraysSupported = true;
        String[] filesBackendArrays = {
            "BackendArrays.java",
        };
        // arrays and also tabular delays
        compile(new PWD("regression/microgrid"), filesBackendArrays,
                null, "BackendArrays.nh.regression", optionsArraysSupported,
                TURN_GAME_SCHEDULER);
        String[] filesMethodRange = {
            "MicrogridMethodRange.java",
        };
        compile(new PWD("regression/microgrid"), filesMethodRange,
                null, "MicrogridMethodRange.nm.regression", null,
                TURN_GAME_SCHEDULER);
        TestOptions optionsNoRangeChecking = new TestOptions();
        optionsNoRangeChecking.messageStyle = MessageStyle.Type.VERICS;
        optionsNoRangeChecking.verboseTADDVariables = true;
        optionsNoRangeChecking.rangeChecking = false;
        optionsNoRangeChecking.generateStateClass = true;
        String[] filesMethodRangeNoCheck = {
            "MicrogridMethodRangeNoCheck.java",
        };
        compile(new PWD("regression/microgrid"), filesMethodRangeNoCheck,
                null, "MicrogridMethodRangeNoCheck.nm.regression",
                optionsNoRangeChecking, TURN_GAME_SCHEDULER);
        // state class compared in <code>testStateClass()</code>
    }
    /**
     * Tests generation a state class.
     */
    protected void testStateClass() {
        testPerformed();
        // piggyback on <code>testMicrogrid()</code>
        compare(new PWD("regression/microgrid"),
                "MicrogridMethodRangeNoCheckState.java", null);
        TestOptions optionsStateClass = new TestOptions();
        optionsStateClass.messageStyle = MessageStyle.Type.VERICS;
        optionsStateClass.verboseTADDVariables = true;
        optionsStateClass.generateStateClass = true;

        String[] filesStaticArrayPath = {
            "CoinFlipStaticArrayPath.java",
        };
        optionsStateClass.generateNdFile = true;
        compile(new PWD("regression/stateclass"), filesStaticArrayPath,
                null, "CoinFlipStaticArrayPath.nm.regression",
                optionsStateClass, new Scheduler(SchedulerType.HIDDEN, true, false,
                    1.0, 1.1, true, true, false));
        optionsStateClass.generateNdFile = false;
        compare(new PWD("regression/stateclass"),
                "CoinFlipStaticArrayPathState.java", null);
        compare(new PWD("regression/stateclass"),
                "CoinFlipStaticArrayPath.nd", null);
        String[] filesStateClassTypes = {
            "StateClassTypes.java",
        };
        compile(new PWD("regression/stateclass"), filesStateClassTypes,
                null, "StateClassTypes.nh.regression",
                optionsStateClass, TURN_GAME_SCHEDULER);
        compare(new PWD("regression/stateclass"),
                "StateClassTypesState.java", null);
        String[] filesStateless = {
            "Stateless.java",
        };
        compile(new PWD("regression/stateclass"), filesStateless,
                null, "Stateless.nh.regression",
                optionsStateClass, TURN_GAME_SCHEDULER);
        compare(new PWD("regression/stateclass"),
                "StatelessState.java", null);
        String[] filesStateClassArray = {
            "StateClassArray.verics",
        };
        compile(new PWD("regression/stateclass"), filesStateClassArray,
                null, "StateClassArray.nh.regression",
                optionsStateClass, TURN_GAME_SCHEDULER);
        compare(new PWD("regression/stateclass"),
                "StateClassArrayState.java", null);
        String[] filesStateClassArrayNonStatic = {
            "StateClassArrayNonStatic.verics",
        };
        compile(new PWD("regression/stateclass"), filesStateClassArrayNonStatic,
                null, "StateClassArrayNonStatic.nh.regression",
                optionsStateClass, TURN_GAME_SCHEDULER);
        compare(new PWD("regression/stateclass"),
                "StateClassArrayNonStaticState.java", null);
    }
    /**
     * Loads and saves a model in a given file.
     * 
     * @param directory directory of the file
     * @param file name of the file
     */
    private void ptaioProcess(PWD directory, String file) {
        String in = null;
        try {
            String outFile = file + "_c";
            in = directory.getFile(file).getAbsolutePath();
            String out = directory.getFile(outFile).getAbsolutePath();
            PTASystem s = PTAIO.read(in);
            PTAIO.write(out, s, DEFAULT_TEST_OPTIONS.compactInstances);
            compare(directory, file, outFile);
        } catch(IOException | ParseException e) {
            System.out.println(in + ": " + e.getMessage());
            throw new AssertionError(e.getMessage());
        }
    }
    /**
     * Tests loading and saving of models by <code>PTAIO</code>.
     */
    protected void testPTAIO() {
        testPerformed();
        ptaioProcess(new PWD("regression/ct"),
                "BranchVariable.nh");
        ptaioProcess(new PWD("regression/barrier"),
                "BarrierInterrupted.nh");
        ptaioProcess(new PWD("regression/microgrid"),
                "Microgrid.nh");
        ptaioProcess(new PWD("regression/microgrid"),
                "MicrogridParticipants.nh");
        ptaioProcess(new PWD("regression/microgrid"),
                "BackendArrays.nh");
        ptaioProcess(new PWD("regression/stateclass"),
                "Stateless.nh");
    }
    /**
     * Tests the functionality of <code>TuplesIO</code>
     * and <code>MatrixIO</code>.
     */
    protected void testTuplesIO() {
        testPerformed();
        PWD directory = new PWD("regression/matrixio");
        TestOptions optionsVericsOutput = new TestOptions();
        optionsVericsOutput.messageStyle = MessageStyle.Type.VERICS;
        optionsVericsOutput.verboseTADDVariables = true;
        optionsVericsOutput.forceFomat = TADDOptions.OutputFormat.HEDGEELLETH;
        String[] filesMatriAccess = {
            "MatrixAccess.java",
        };
        CompilerException patternNoArrays =
                new CompilerException();
        patternNoArrays.addReport(new CompilerException.Report(
            null,
            CompilerException.Code.IO,
            "backend Prism, flavour HEDGEELLETH: ../../lib/hc/Sleep.hc_java:14:9: density of type tab not supported: array parameter can not be emulated: #system.#Array#124hc.MatrixIO#123_line1"));
        compile(directory, filesMatriAccess,
                patternNoArrays, null,
                optionsVericsOutput, TURN_GAME_SCHEDULER);
        TestOptions optionsArraysSupported = new TestOptions();
        optionsArraysSupported.messageStyle = MessageStyle.Type.VERICS;
        optionsArraysSupported.verboseTADDVariables = true;
        optionsArraysSupported.forceFomat = TADDOptions.OutputFormat.HEDGEELLETH;
        optionsArraysSupported.arraysSupported = true;
        compile(directory, filesMatriAccess,
                null, "MatrixAccess.nh.regression",
                optionsArraysSupported, TURN_GAME_SCHEDULER);
        compare(directory, "MatrixAccess_out.txt", "MatrixAccess_out.txt.regression");
        String[] filesMatrixReadShort = {
            "MatrixAccessReadShort.java",
        };
        CompilerException patternMatrixReadShort =
                new CompilerException();
        patternMatrixReadShort.addReport(new CompilerException.Report(
                new StreamPos("MatrixAccessReadShort.java", 148, 33),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread example.Scheduler_0: null array does not represent any PTA variable"));        
        compile(directory, filesMatrixReadShort,
                patternMatrixReadShort, null,
                optionsArraysSupported, TURN_GAME_SCHEDULER);
        String[] filesMatrixReadShortException = {
            "MatrixAccessReadShortException.java",
        };
        CompilerException patternMatrixReadShortException =
                new CompilerException();
        patternMatrixReadShortException.addReport(new CompilerException.Report(
                new StreamPos("../../lib/hc/TuplesIO.hc_java", 44, 13),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: called from MatrixAccessReadShortException.java:131:17, MatrixAccessReadShortException.java:101:23: exception: row not found"));
        compile(directory, filesMatrixReadShortException,
                patternMatrixReadShortException, null,
                optionsArraysSupported, TURN_GAME_SCHEDULER);
        String[] filesStreamNotClosed = {
            "StreamNotClosed.java",
        };
        CompilerException patternStreamNotClosed =
                new CompilerException();
        patternStreamNotClosed.addReport(new CompilerException.Report(
                null,
                CompilerException.Code.IO,
                "construction of PTA: stream MatrixIO(regression/matrixio/density.txt read) is not closed after the main thread finishes"));
        compile(directory, filesStreamNotClosed,
                patternStreamNotClosed, null,
                optionsArraysSupported, TURN_GAME_SCHEDULER);
        // checking if the file is flushed properly
        //
        // this file is also needed by <code>NotMatrix.java</code>
        compare(directory, "StreamNotClosed_out.txt", "StreamNotClosed_out.txt.regression");
        String[] filesNotMatrix = {
            "NotMatrix.java",
        };
        CompilerException patternNotMatrix =
                new CompilerException();
        patternNotMatrix.addReport(new CompilerException.Report(
                new StreamPos("NotMatrix.java", 135, 17),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: called from NotMatrix.java:100:23: file regression/matrixio/StreamNotClosed_out.txt, line 3: number of columns is 2, was 3"));        
        compile(directory, filesNotMatrix,
                patternNotMatrix, null,
                optionsArraysSupported, TURN_GAME_SCHEDULER);
    }
    /**
     * Tests <code>Model.isStatistical()</code>.
     */
    protected void testIsStatistical() {
        testPerformed();
        PWD directory = new PWD("regression/isstatistical");
        TestOptions optionsIsStatistical = new TestOptions();
        optionsIsStatistical.messageStyle = MessageStyle.Type.VERICS;
        optionsIsStatistical.verboseTADDVariables = true;
        optionsIsStatistical.isStatisticalHint = false;
        String[] filesIsStatisticalFalse = {
            "IsStatisticalFalse.java",
        };
        compile(directory, filesIsStatisticalFalse,
                null, "IsStatisticalFalse.nh.regression",
                optionsIsStatistical, TURN_GAME_SCHEDULER);
        optionsIsStatistical.isStatisticalHint = true;
        String[] filesIsStatisticalTrue = {
            "IsStatisticalTrue.java",
        };
        compile(directory, filesIsStatisticalTrue,
                null, "IsStatisticalTrue.nm.regression",
                optionsIsStatistical, TURN_GAME_SCHEDULER);
    }
    /**
     * Tests <code>TraceValues.invertBranches()</code>.
     */
    protected void testInvertBranch() {
        testPerformed();
        PWD directory = new PWD("regression/invertbranch");
        String[] filesInvertBranchNeutral = {
            "InvertBranchNeutral.java",
        };
        compile(directory, filesInvertBranchNeutral,
                null, "InvertBranchNeutral.nh.regression",
                null, null);
        String[] filesInvertBranchNeutralStarts = {
            "InvertBranchNeutralStarts.java",
        };
        compile(directory, filesInvertBranchNeutralStarts,
                null, "InvertBranchNeutralStarts.nh.regression",
                null, null);
    }
    /**
     * Tests external definition of constants, either at the compiler's
     * command line or by a model checker.
     */
    protected void testExternalConstants() {
        testPerformed();
        PWD directory = new PWD("regression/extconst");
        TestOptions testOptions = new TestOptions();
        testOptions.messageStyle = MessageStyle.Type.VERICS;
        testOptions.verboseTADDVariables = true;
        testOptions.experimentMode =
                HedgeellethCompiler.Options.ExperimentMode.TRUE;
        testOptions.compactInstances = true;
        String[] filesExtConstCompilerNoFit = {
            "ExtConstCompilerNoFit.java",
        };
        List<Literal> list = new LinkedList<>();
        list.add(new Literal(1L));
        list.add(new Literal(6000000000L));
        list.add(new Literal(10L));
        testOptions.constants.defines.put("DAYS", list);
        list = new LinkedList<>();
        list.add(new Literal(5000000L));
        testOptions.constants.defines.put("INTERVALS", list);
        CompilerException patternNoFit =
                new CompilerException();
        patternNoFit.addReport(new CompilerException.Report(
               new StreamPos("ExtConstCompilerNoFit.java", 16, 51),
               CompilerException.Code.PARSE,
               "external constant 6000000000 does not fit into type int"));
        patternNoFit.addReport(new CompilerException.Report(
                new StreamPos("ExtConstCompilerNoFit.java", 20, 43),
                CompilerException.Code.ILLEGAL,
                "loss of precision: conversion from int constant of 5000000 to short"));
        compile(directory, filesExtConstCompilerNoFit,
                patternNoFit, null, testOptions, null);
        String[] filesExtConstCompilerNoFitExpr = {
            "ExtConstCompilerNoFitExpr.java",
        };
        CompilerException patternNoFitExpr =
                new CompilerException();
        patternNoFitExpr.addReport(new CompilerException.Report(
                new StreamPos("ExtConstCompilerNoFitExpr.java", 16, 34),
                CompilerException.Code.ILLEGAL,
                "possible loss of precision: conversion from double to int"));
        patternNoFitExpr.addReport(new CompilerException.Report(
                new StreamPos("ExtConstCompilerNoFitExpr.java", 20, 41),
                CompilerException.Code.ILLEGAL,
                "possible loss of precision: conversion from int to short"));
        compile(directory, filesExtConstCompilerNoFitExpr,
                patternNoFitExpr, null, testOptions, null);
        String[] filesExtConstCompiler = {
            "ExtConstCompiler.java",
        };
        list = new LinkedList<>();
        list.add(new Literal(1L));
        list.add(new Literal(3L));
        list.add(new Literal(10L));
        testOptions.constants.defines.put("DAYS", list);
        list = new LinkedList<>();
        list.add(new Literal(16L));
        testOptions.constants.defines.put("INTERVALS", list);
        testOptions.forceFomat = TADDOptions.OutputFormat.PRISM;
        compile(directory, filesExtConstCompiler,
                null, null, testOptions, null);
        // an undefined constant, but needed by the
        // interpreter
        testOptions.experimentMode =
                HedgeellethCompiler.Options.ExperimentMode.FALSE;
        testOptions.constants.defines.remove("DAYS");
        list = new LinkedList<>();
        list.add(new Literal(16L));
        testOptions.constants.defines.put("INTERVALS", list);
        CompilerException patternInterpretedUndefined =
                new CompilerException();
        patternInterpretedUndefined.addReport(new CompilerException.Report(
                new StreamPos("ExtConstCompiler.java", 23, 44),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: constant undefined but required by the interpreter: DAYS"));
        compile(directory, filesExtConstCompiler,
                patternInterpretedUndefined, null, testOptions, null);
        testOptions.experimentMode =
                HedgeellethCompiler.Options.ExperimentMode.TRUE;
        String[] filesExtConstCheckerNotUnique = {
            "ExtConstCheckerNotUnique.java",
        };
        list = new LinkedList<>();
        list.add(new Literal(1L));
        list.add(new Literal(3L));
        testOptions.constants.defines.put("DAYS", list);
        CompilerException patternExtConstCheckerNotUnique =
                new CompilerException();
        patternExtConstCheckerNotUnique.addReport(new CompilerException.Report(
                new StreamPos("ExtConstCheckerNotUnique.java", 177, 35),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: static field that is an unknown constant must have an unique name: P_OVER_LIMIT"));
        compile(directory, filesExtConstCheckerNotUnique,
                patternExtConstCheckerNotUnique, null, testOptions, null);
        String[] filesExtConstCheckerNotConstant = {
            "ExtConstCheckerNotConstant.java",
        };
        CompilerException patternExtConstCheckerNotConstant =
                new CompilerException();
        patternExtConstCheckerNotConstant.addReport(new CompilerException.Report(
                new StreamPos("ExtConstCheckerNotConstant.java", 172, 29),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: called from ExtConstCheckerNotConstant.java:101:27: external constant must be final: PRICE_LIMIT"));
        compile(directory, filesExtConstCheckerNotConstant,
                patternExtConstCheckerNotConstant, null, testOptions, null);
        String[] filesExtConstCheckerWritten = {
            "ExtConstCheckerWritten.java",
        };
        CompilerException patternExtConstCheckerWritten =
                new CompilerException();
        patternExtConstCheckerWritten.addReport(new CompilerException.Report(
                new StreamPos("ExtConstCheckerWritten.java", 204, 25),
                CompilerException.Code.ILLEGAL,
                "final variable example.Household.PRICE_LIMIT is already initialized"));
        compile(directory, filesExtConstCheckerWritten,
                patternExtConstCheckerWritten, null, testOptions, null);
        String[] filesExtConstChecker = {
            "ExtConstChecker.java",
        };
        compile(directory, filesExtConstChecker,
                null, null, testOptions, null);
    }
    /**
     * Tests the Mirela library.
     * 
     * @param allocTrace sets DEFAULT_TEST_OPTIONS.traceAllocInCallResolution;
     * the latter value is restored before return
     */
    protected void testMirela(boolean allocTrace) {
        testPerformed();
        boolean tmpTraceAllocInCallResolution = DEFAULT_TEST_OPTIONS.traceAllocInCallResolution;
        DEFAULT_TEST_OPTIONS.traceAllocInCallResolution = allocTrace;
        String[] files2 = {
            "Simple2.java",
        };
        compile(new PWD("regression/mirela"), files2,
                null, "Simple2.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    1.0, 0.0, false, false, false));
        String[] files3 = {
            "Simple3.java",
        };
        compile(new PWD("regression/mirela"), files3,
                null, "Simple3.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    1.0, 0.0, false, false, false));
        String[] filesSleepFree = {
            "SleepRangeFree.java",
        };
        compile(new PWD("regression/mirela"), filesSleepFree,
                null, "SleepRangeFree.nm.regression",
                null, null);
        String[] filesSleep = {
            "SleepRange.java",
        };
        compile(new PWD("regression/mirela"), filesSleep,
                null, "SleepRange.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    1.0, 0.0, false, false, false));
        String[] filesConditionalAtomicSymmetricFalse = {
            "ConditionalAtomicSymmetricFalse.java",
        };
        compile(new PWD("regression/mirela"), filesConditionalAtomicSymmetricFalse,
                null, "ConditionalAtomicSymmetricFalse.nm.regression",
                null, null);
        String[] filesConditionalAtomicSymmetricTrue = {
            "ConditionalAtomicSymmetricTrue.java",
        };
        compile(new PWD("regression/mirela"), filesConditionalAtomicSymmetricTrue,
                null, "ConditionalAtomicSymmetricTrue.nm.regression",
                null, null);
        String[] filesConditionalAtomicSymmetricSimple = {
            "ConditionalAtomicSymmetricSimple.java",
        };
        compile(new PWD("regression/mirela"), filesConditionalAtomicSymmetricSimple,
                null, "ConditionalAtomicSymmetricSimple.nm.regression",
                null, null);
        String[] filesConditionalAtomicSymmetricComplex = {
            "ConditionalAtomicSymmetricComplex.java",
        };
        CompilerException pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ConditionalAtomicSymmetricComplex.java", 27, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: example.Producer#110::example.Producer.run/::#c2<<0, 4>>(<<0>>)!=0 is not in an atomic synchronisation, but the barrier specifies otherwise"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ConditionalAtomicSymmetricComplex.java", 27, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: example.Producer#111::example.Producer.run/::#c2<<0, 4>>(<<0>>)!=0 is not in an atomic synchronisation, but the barrier specifies otherwise"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ConditionalAtomicSymmetricComplex.java", 51, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: example.Consumer#113::example.Consumer.run/::#c1<<-2, 3>>(<<0>>)!=0 is not in an atomic synchronisation, but the barrier specifies otherwise"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("ConditionalAtomicSymmetricComplex.java", 54, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: example.Consumer#113::example.Consumer.run/::#c1<<-2, 3>>(<<0>>)!=0 is not in an atomic synchronisation, but the barrier specifies otherwise"));
        TestOptions optionsDeterministicLabels = new TestOptions();
        optionsDeterministicLabels.forceLabelDeterminism = true;
        if(allocTrace)
            // ignores that argument anyway, do not thus rerun twice
            compile(new PWD("regression/mirela"), filesConditionalAtomicSymmetricComplex,
                    pattern, null,
                    optionsDeterministicLabels, null);
        String[] filesConditionalSymmetricComplex = {
            "ConditionalSymmetricComplex.java",
        };
        compile(new PWD("regression/mirela"), filesConditionalSymmetricComplex,
                null, "ConditionalSymmetricComplex.nm.regression",
                null, null);
        String[] filesBarrierAsymmetricSimpleMismatchParties = {
            "BarrierAsymmetricSimpleMismatchParties.java",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
            null,
            CompilerException.Code.ILLEGAL,
            "construction of PTA: barrier hc.Barrier#107 declares a total of 5 parties, but their actual number is 3; declares 3 producer parties, but their actual number is 2; declares 2 consumer parties, but their actual number is 1"));
        compile(new PWD("regression/mirela"), filesBarrierAsymmetricSimpleMismatchParties,
                pattern, null,
                null, null);
        String[] filesBarrierAsymmetricSimple = {
            "BarrierAsymmetricSimple.java",
        };
        compile(new PWD("regression/mirela"), filesBarrierAsymmetricSimple,
                null, "BarrierAsymmetricSimple.nm.regression",
                null, null);
        String[] filesAtomicAsymmetricComplex1vs1 = {
            "AtomicAsymmetricComplex1vs1.java",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("AtomicAsymmetricComplex1vs1.java", 30, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: 0<example.Producer#110::example.Producer.run/::#c2<<0, 4>>(<<0>>) is not in an atomic synchronisation, but the barrier specifies otherwise"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("AtomicAsymmetricComplex1vs1.java", 54, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: example.Consumer#114::example.Consumer.run/::#c1<<-16, 3>>(<<0>>)<=0 is not in an atomic synchronisation, but the barrier specifies otherwise"));
        if(allocTrace)
            compile(new PWD("regression/mirela"), filesAtomicAsymmetricComplex1vs1,
                    pattern, null,
                    optionsDeterministicLabels, null);
        String[] filesBarrierAsymmetricComplex1vs1 = {
            "BarrierAsymmetricComplex1vs1.java",
        };
        compile(new PWD("regression/mirela"), filesBarrierAsymmetricComplex1vs1,
                null, "BarrierAsymmetricComplex1vs1.nm.regression",
                null, null);
        String[] filesBarrierAsymmetricComplex = {
            "BarrierAsymmetricComplex.java",
        };
        compile(new PWD("regression/mirela"), filesBarrierAsymmetricComplex,
                null, "BarrierAsymmetricComplex.nm.regression",
                null, null);
        String[] filesBarrierAsymmetricSimpleTimed = {
            "BarrierAsymmetricSimpleTimed.java",
        };
        compile(new PWD("regression/mirela"), filesBarrierAsymmetricSimpleTimed,
                null, "BarrierAsymmetricSimpleTimed.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    1.0, 0.0, false, false, false));
        String[] filesAtomicAsymmetricTimed = {
            "AtomicAsymmetricTimed.java",
        };
        compile(new PWD("regression/mirela"), filesAtomicAsymmetricTimed,
                null, "AtomicAsymmetricTimed.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    1.0, 0.0, false, false, false));
        String[] filesPairsMismatch = {
            "PairsMismatch.java",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("PairsMismatch.java", 22, 28),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread example.Producer_0: a non-directional barrier has a producer"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("PairsMismatch.java", 23, 29),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread example.Producer_0: a directional barrier has a symmetric party"));
        compile(new PWD("regression/mirela"), filesPairsMismatch,
                pattern, null,
                null, null);
        String[] filesPairs = {
            "Pairs.java",
        };
        compile(new PWD("regression/mirela"), filesPairs,
                null, "Pairs.nm.regression",
                null, null);
        String[] filesProducerNum = {
            "ProducerNum.java",
        };
        compile(new PWD("regression/mirela"), filesProducerNum,
                null, "ProducerNum.nm.regression",
                null, null);
        String[] filesSelectProducer = {
            "SelectProducer.java",
        };
        compile(new PWD("regression/mirela"), filesSelectProducer,
                null, "SelectProducer.nm.regression",
                null, null);
        String[] filesMirela = {
            "Mirela1.java",
            "mirela/AbstractDelay.java",
            "mirela/AbstractNode.java",
            "mirela/AbstractConsumer.java",
            "mirela/Aperiodic.java",
            "mirela/Both.java",
            "mirela/Delay.java",
            "First2.java",
            "mirela/Memory.java",
            "mirela/Mirela.java",
            "mirela/Periodic.java",
            "mirela/Priority.java",
            "mirela/Rendering.java",
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("First2.java", 54, 21),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: static analysis: indexing of array #system.#Array#118#acquireDelay: index out of bounds: 1, array size is 1"));
        if(allocTrace)
            compile(new PWD("regression/mirela"), filesMirela,
                    pattern, null,
                    optionsDeterministicLabels, null);
        filesMirela[0] = "Mirela2.java";
        filesMirela[2] = "AbstractNode2.java";
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("AbstractNode2.java", 81, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread mirela.Periodic_0: runtime class cast exception: mirela.First to mirela.Both"));
///U                "construction of PTA: in runtime code analysis: runtime class cast exception: mirela.First to mirela.Both"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("First2.java", 41, 14),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in thread mirela.Periodic_0: null dereference of mirela.First i#0mirela.AbstractNode_produce__#c3"));
        compile(new PWD("regression/mirela"), filesMirela,
                pattern, null,
                null, null);
        filesMirela[0] = "Mirela3.java";
        filesMirela[2] = "mirela/AbstractNode.java";
        filesMirela[7] = "mirela/First.java";
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("mirela/Memory.java", 48, 13),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: called from Mirela3.java:46:15: exception: delay array too short"));
        compile(new PWD("regression/mirela"), filesMirela,
                pattern, null,
                null, new Scheduler(SchedulerType.IMMEDIATE, true, false,
                    0.0, 0.0, false, false, false));
        filesMirela[0] = "Mirela4.java";
        filesMirela[2] = "AbstractNodeInvalidCast.java";
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("AbstractNodeInvalidCast.java", 81, 10),
                CompilerException.Code.INVALID,
                "construction of PTA: in thread mirela.Periodic_0: class cast exception when resolving call: referenced object can not be cast to mirela.AbstractConsumer"));
        compile(new PWD("regression/mirela"), filesMirela,
                pattern, null,
                null, new Scheduler(SchedulerType.CONST, false, false,
                    0.0, 0.0, false, false, false));
        filesMirela[2] = "mirela/AbstractNode.java";
        filesMirela[0] = "Mirela4Plain.java";
        compile(new PWD("regression/mirela"), filesMirela,
                null, "Mirela4Plain.nm.regression",
                null, null);
        filesMirela[0] = "Mirela4Timed.java";
        compile(new PWD("regression/mirela"), filesMirela,
                null, "Mirela4Timed.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    3.0, 0.0, false, false, false));
        filesMirela[0] = "Mirela4TimedNoLoops.java";
        compile(new PWD("regression/mirela"), filesMirela,
                null, "Mirela4TimedNoLoops.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    0.0, 0.0, false, false, false));
        filesMirela[0] = "MirelaPairBarrierSelective.java";
        compile(new PWD("regression/mirela"), filesMirela,
                null, "MirelaPairBarrierSelective.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    0.0, 0.0, false, false, false));
        // a complex, untimed model
        String[] filesMirela2 = {
            "MirelaEx2c.java",
            "mirela2/AbstractDelay.java",
            "mirela2/AbstractNode.java",
            "mirela2/AbstractConsumer.java",
            "mirela2/Aperiodic.java",
            "mirela2/Both.java",
            "mirela2/Delay.java",
            "mirela2/First.java",
            "mirela2/Memory.java",
            "mirela2/Mirela.java",
            "mirela2/Periodic.java",
            "mirela2/Priority.java",
            "mirela2/PriorityS.java",
            "mirela2/Rendering.java",
        };
        compile(new PWD("regression/mirela"), filesMirela2,
                null, "MirelaEx2c.nm.regression",
                null, new Scheduler(SchedulerType.FREE, false, true,
                    0.0, 0.0, false, false, false));
        DEFAULT_TEST_OPTIONS.traceAllocInCallResolution = tmpTraceAllocInCallResolution;
    }
    /**
     * Tests urgent synchronisation, using the Mirela library.
     */
    protected void testUrgentSync() {
        testPerformed();
        String[] filesMirela = {
            "Urgent.java",
            "mirela/AbstractDelay.java",
            "mirela/AbstractNode.java",
            "mirela/AbstractConsumer.java",
            "mirela/Aperiodic.java",
            "mirela/Both.java",
            "mirela/Delay.java",
            "mirela/First.java",
            "mirela/Memory.java",
            "mirela/Mirela.java",
            "mirela/Periodic.java",
            "mirela/Rendering.java",
        };
        compile(new PWD("regression/mirela"), filesMirela,
                null, "Urgent.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    0.0, 0.0, false, false, true));
        filesMirela[0] = "UrgentLock.java";
        compile(new PWD("regression/mirela"), filesMirela,
                null, "UrgentLock.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    0.0, 0.0, false, false, true));
        filesMirela[0] = "Mirela4Urgent.java";
        compile(new PWD("regression/mirela"), filesMirela,
                null, "Mirela4Urgent.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    0.0, 0.0, false, false, true));
    }
    /**
     * Tests probabilistic Mirela models.
     */
    protected void testMirelaProb() {
        testPerformed();
        String[] filesMirela = {
            "Mirela1Prob.java",
            "mirela/AbstractDelay.java",
            "mirela/AbstractNode.java",
            "mirela/AbstractConsumer.java",
            "mirela/Aperiodic.java",
            "mirela/Both.java",
            "mirela/Delay.java",
            "mirela/First.java",
            "mirela/Link.java",
            "mirela/MarginDelay.java",
            "mirela/Memory.java",
            "mirela/Mirela.java",
            "mirela/Periodic.java",
            "mirela/Rendering.java",
        };
        compile(new PWD("regression/mirela"), filesMirela,
                null, "Mirela1Prob.nm.regression",
                null, new Scheduler(SchedulerType.CONST, false, false,
                    0.0, 0.0, false, false, true));
        filesMirela[0] = "Mirela4Urgent.java";
    }
    /**
     * Tests models of interacting agents
     */
    protected void testPMGC() {
        testPerformed();
        DEFAULT_TEST_OPTIONS.rangeChecking= false;
        // a canonical nondetermininstic cross with an unused branch
        String[] files = {
            "DialogueUnusedBranch.java"
        };
        Scheduler implicitScheduler = new Scheduler(
                SchedulerType.HIDDEN, true, false, 0.0, 0.0, false, false, false);
        compile(new PWD("regression/pmgc"), files,
                null, "DialogueUnusedBranch.nm.regression",
                null, implicitScheduler);
        // fields reading fields through method calls
        files[0] = "Dialogue2.java";
        compile(new PWD("regression/pmgc"), files,
                null, "Dialogue2.nm.regression",
                null, implicitScheduler);
        DEFAULT_TEST_OPTIONS.rangeChecking= true;
    }
    /**
     * Tests Verics's static statements outside a static block.
     */
    protected void testVericsStatic() {
        testPerformed();
        String[] files = {
            "Static.verics"
        };
        compile(new PWD("regression/verics"), files,
                null, "Static.nm.regression",
                null, null);
        String[] filesParse = {
            "StaticParse.verics"
        };
        CompilerException pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("StaticParse.verics", 26, 7),
                CompilerException.Code.DUPLICATE,
                "duplicate declaration of k"));
        pattern.addReport(new CompilerException.Report(
                new StreamPos("StaticParse.verics", 31, 5),
                CompilerException.Code.ILLEGAL,
                "continue outside of a loop"));        
        compile(new PWD("regression/verics"), filesParse,
                pattern, null,
                null, null);
        //
        // field can not be declared within a statement
        //
        String[] filesParse2 = {
            "StaticParse2.verics"
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("StaticParse2.verics", 4, 9),
                CompilerException.Code.PARSE,
                "unexpected \"pv\""));
        compile(new PWD("regression/verics"), filesParse2,
                pattern, null,
                null, null);
    }
    /**
     * Tests integer overflow.
     */
    protected void testOverflow() {
        testPerformed();
        String[] filesStatic = {
            "OverflowStatic.java"
        };
        CompilerException pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("OverflowStatic.java", 6, 14),
                CompilerException.Code.ARITHMETIC,
                "integer overflow"));        
        compile(new PWD("regression/overflow"), filesStatic,
                pattern, null,
                null, null);
        String[] filesInterpreter = {
            "OverflowInterpreter.verics"
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("OverflowInterpreter.verics", 9, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: value -2147483648L does not fit into type of variable #default.OverflowInterpreter.a"));
        compile(new PWD("regression/overflow"), filesInterpreter,
                pattern, null,
                null, null);
        String[] filesInterpreter2 = {
            "OverflowInterpreter2.verics"
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("OverflowInterpreter2.verics", 9, 18),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: integer overflow"));        
        compile(new PWD("regression/overflow"), filesInterpreter2,
                pattern, null,
                null, null);
    }
    /**
     * Tests operations on strings.
     */
    protected void testSubstring() {
        testPerformed();
        String[] filesSubstringInterpreter1 = {
            "SubstringInterpreter1.verics"
        };
        CompilerException pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("../../lib/verics/lang/String.verics", 29, 13),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: called from SubstringInterpreter1.verics:10:25: index out of bounds: start position = -1 is negative"));
        compile(new PWD("regression/string"), filesSubstringInterpreter1,
                pattern, null,
                null, null);
        String[] filesSubstringInterpreter2 = {
            "SubstringInterpreter2.verics"
        };
        pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                new StreamPos("../../lib/verics/lang/String.verics", 29, 13),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: in interpreted code: called from SubstringInterpreter2.verics:10:25: index out of bounds: stop position = 7 past the end"));        
        compile(new PWD("regression/string"), filesSubstringInterpreter2,
                pattern, null,
                null, null);
        String[] filesSubstring = {
            "Substring.verics"
        };
        compile(new PWD("regression/string"), filesSubstring,
                null, "Substring.nm.regression",
                null, null);
    }
    /**
     * Tests generation of Nd files.
     */
    protected void testNdFile() {
        testPerformed();
        TestOptions optionsStateClass = new TestOptions();
        optionsStateClass.messageStyle = MessageStyle.Type.VERICS;
        optionsStateClass.verboseTADDVariables = true;
        optionsStateClass.generateNdFile = true;
        Scheduler scheduler = new Scheduler(SchedulerType.HIDDEN, true, false,
                1.0, 1.1, true, true, false);
        String[] filesArgs = {
            "CoinFlipArgs.java",
        };
        compile(new PWD("regression/ndfile"), filesArgs,
                null, "CoinFlipArgs.nm.regression",
                optionsStateClass, scheduler);
        compare(new PWD("regression/ndfile"),
                "CoinFlipArgs.nd", null);
        String[] filesChoice3 = {
            "CoinFlipChoice3.java",
        };
        compile(new PWD("regression/ndfile"), filesChoice3,
                null, "CoinFlipChoice3.nm.regression",
                optionsStateClass, scheduler);
        compare(new PWD("regression/ndfile"),
                "CoinFlipChoice3.nd", null);
        String[] filesChoice4 = {
            "CoinFlipChoice4.java",
        };
        compile(new PWD("regression/ndfile"), filesChoice4,
                null, "CoinFlipChoice4.nm.regression",
                optionsStateClass, scheduler);
        compare(new PWD("regression/ndfile"),
                "CoinFlipChoice4.nd", null);
        String[] filesNoChoice = {
            "CoinFlipNoChoice.java",
        };
        compile(new PWD("regression/ndfile"), filesNoChoice,
                null, "CoinFlipNoChoice.nm.regression",
                optionsStateClass, scheduler);
        compare(new PWD("regression/ndfile"),
                "CoinFlipNoChoice.nd", null);
        String[] filesChoiceComplex = {
            "CoinFlipChoiceComplex.java",
        };
        CompilerException patternChoiceComplex = new CompilerException();
        patternChoiceComplex.addReport(new CompilerException.Report(
                null,
                CompilerException.Code.IO,
                "CoinFlipChoiceComplex.java:18:12: could not simplify a non--deterministic choice: formula too complex"));
        optionsStateClass.forceFomat = TADDOptions.OutputFormat.PRISM;
        compile(new PWD("regression/ndfile"), filesChoiceComplex,
                patternChoiceComplex, null,
                optionsStateClass, scheduler);
        optionsStateClass.forceFomat = null;
        optionsStateClass.rangeChecking = false;
        String[] filesChoiceSwitch = {
            "CoinFlipChoiceSwitch.java",
        };
        compile(new PWD("regression/ndfile"), filesChoiceSwitch,
                null, "CoinFlipChoiceSwitch.nm.regression",
                optionsStateClass, scheduler);
        compare(new PWD("regression/ndfile"),
                "CoinFlipChoiceSwitch.nd", null);
        String[] filesChoiceSwitchDefault = {
            "CoinFlipChoiceSwitchDefault.java",
        };
        compile(new PWD("regression/ndfile"), filesChoiceSwitchDefault,
                null, "CoinFlipChoiceSwitchDefault.nm.regression",
                optionsStateClass, scheduler);
        compare(new PWD("regression/ndfile"),
                "CoinFlipChoiceSwitchDefault.nd", null);
        String[] filesChoiceDupArgs = {
            "CoinFlipChoiceDupArgs.java",
        };
        CompilerException patternChoiceDupArgs = new CompilerException();
        patternChoiceDupArgs.addReport(new CompilerException.Report(
                null,
                CompilerException.Code.IO,
                "CoinFlipChoiceDupArgs.java:16:14: duplicate argument to nd function: flips"));
        optionsStateClass.forceFomat = TADDOptions.OutputFormat.PRISM;
        compile(new PWD("regression/ndfile"), filesChoiceDupArgs,
                patternChoiceDupArgs, null,
                optionsStateClass, scheduler);
        optionsStateClass.forceFomat = null;
    }
    /**
     * Tests the class <code>Logic</code>.
     */
    protected void testTernaryLogic() {
        testPerformed();
        TestOptions optionsPrismOutput = DEFAULT_TEST_OPTIONS.shallowCopy();
        optionsPrismOutput.forceFomat = TADDOptions.OutputFormat.PRISM;
        String[] filesDensity = {
            "TernaryDensity.verics"
        };
        CompilerException pattern = new CompilerException();
        pattern.addReport(new CompilerException.Report(
                null,
                CompilerException.Code.IO,
                "backend Prism, flavour PRISM: TernaryDensity.verics:11:16: Prism format does not support densities"));
        compile(new PWD("regression/ternary"), filesDensity,
                pattern, null,
                optionsPrismOutput, null);
        compile(new PWD("regression/ternary"), filesDensity,
                null, "TernaryDensity.nh.regression",
                null, null);
        String[] files = {
            "Ternary.verics"
        };
        compile(new PWD("regression/ternary"), files,
                null, "Ternary.s0.regression",
                null, null);
        compile(new PWD("regression/ternary"), files,
                null, "Ternary.s.regression",
                null, null);
        DEFAULT_TEST_OPTIONS.tuneToTADDs = true;
        String[] filesTADD = {
            "TernaryTADD.verics"
        };
        compile(new PWD("regression/ternary"), filesTADD,
                null, "TernaryTADD.s.regression",
                null, null);
        DEFAULT_TEST_OPTIONS.tuneToTADDs = null;
        compile(new PWD("regression/ternary"), files,
                null, "Ternary.nm.regression",
                null, null);
        String[] filesNative = {
            "TernaryNative.verics"
        };
        compile(new PWD("regression/ternary"), filesNative,
                null, "TernaryNative.nm.regression",
                null, null);
        String[] filesParse = {
            "TernaryParse.verics"
        };
        CompilerException patternParse = new CompilerException();
        patternParse.addReport(new CompilerException.Report(
                new StreamPos("TernaryParse.verics", 13, 21),
                CompilerException.Code.PARSE,
                "block expected in non-ternary if, found `;'"));
        patternParse.addReport(new CompilerException.Report(
                new StreamPos("TernaryParse.verics", 14, 10),
                CompilerException.Code.ILLEGAL,
                "assignment requires a ternary if"));
        patternParse.addReport(new CompilerException.Report(
                new StreamPos("TernaryParse.verics", 19, 10),
                CompilerException.Code.ILLEGAL,
                "assignment requires a ternary if"));
        patternParse.addReport(new CompilerException.Report(
                new StreamPos("TernaryParse.verics", 30, 11),
                CompilerException.Code.ILLEGAL,
                "an unknown block requires a ternary if"));
        patternParse.addReport(new CompilerException.Report(
                new StreamPos("TernaryParse.verics", 33, 20),
                CompilerException.Code.PARSE,
                "can not use else-if without braces in a ternary if"));
        compile(new PWD("regression/ternary"), filesParse,
                patternParse, null,
                null, null);
        String[] filesDeclaration = {
            "TernaryDeclaration.verics"
        };
        compile(new PWD("regression/ternary"), filesDeclaration,
                null, "TernaryDeclaration.nm.regression",
                null, null);
    }
    /**
     * Tests distributions represented by arrays.
     */
    protected void testTabbedDist() {
        testPerformed();
        String[] filesTabbed = {
            "Tabbed.verics"
        };
        compile(new PWD("regression/td"), filesTabbed,
                null, "Tabbed.nh.regression",
                null, null);
        String[] filesTabbedInt = {
            "TabbedInt.verics"
        };
        compile(new PWD("regression/td"), filesTabbedInt,
                null, "Tabbed.nm.regression",
                null, null);
    }
    /**
     * Tests allocation in PTA.
     */
    protected void testAllocationInPTA() {
        testPerformed();
        String[] filesNoLeak1 = {
            "NoLeak1.verics"
        };
        compile(new PWD("regression/apta"), filesNoLeak1,
                null, "NoLeak1.nm.regression",
                null, null);
        String[] filesLeak1 = {
            "Leak1.verics"
        };
        CompilerException patternLeak1 = new CompilerException();
        patternLeak1.addReport(new CompilerException.Report(
                new StreamPos("Leak1.verics", 22, 23),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: could not inline allocation: could not exclude simultaneous storage of previous allocation"));
        compile(new PWD("regression/apta"), filesLeak1,
                patternLeak1, null,
                null, null);
        String[] filesLeak2 = {
            "Leak2.verics"
        };
        CompilerException patternLeak2 = new CompilerException();
        patternLeak2.addReport(new CompilerException.Report(
                new StreamPos("Leak2.verics", 22, 23),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: could not inline allocation: external leak"));
        patternLeak2.addReport(new CompilerException.Report(
                new StreamPos("Leak2.verics", 33, 30),
                CompilerException.Code.ILLEGAL,
                "construction of PTA: could not inline allocation: external leak"));
        compile(new PWD("regression/apta"), filesLeak2,
                patternLeak2, null,
                null, null);
        String[] filesTernaryNull = {
            "Allocating.verics",
            "TernaryNull.verics",
        };
        CompilerException patternTernaryNull = new CompilerException();
        patternTernaryNull.addReport(new CompilerException.Report(
            new StreamPos("TernaryNull.verics", 23, 20),
            CompilerException.Code.ILLEGAL,
            "construction of PTA: in thread #default.TernaryNull_1: TernaryNull.verics:23:20: runtime class cast exception not allowing null: null to Allocating"));
        compile(new PWD("regression/apta"), filesTernaryNull,
                patternTernaryNull, null,
                null, null);
        String[] filesTernaryNullField = {
            "Allocating.verics",
            "TernaryNullField.verics",
        };
        CompilerException patternTernaryNullField = new CompilerException();
        patternTernaryNullField.addReport(new CompilerException.Report(
            new StreamPos("TernaryNullField.verics", 24, 21),
            CompilerException.Code.ILLEGAL,
            "construction of PTA: in thread #default.TernaryNullField_1: TernaryNullField.verics:24:21: runtime class cast exception not allowing null: null to Allocating"));
        compile(new PWD("regression/apta"), filesTernaryNullField,
                patternTernaryNullField, null,
                null, null);
        String[] filesNestedNoAllocTrace = {
            "AllocatingRecursive.verics",
            "NestedNoAllocTrace.verics",
        };
        TestOptions optionsNoAllocTrace = DEFAULT_TEST_OPTIONS.shallowCopy();
        optionsNoAllocTrace.traceAllocInCallResolution = false;
        compile(new PWD("regression/apta"), filesNestedNoAllocTrace,
                null, "NestedNoAllocTrace.nm.regression",
                optionsNoAllocTrace, null);
        String[] filesNested = {
            "AllocatingRecursive.verics",
            "Nested.verics",
        };
        compile(new PWD("regression/apta"), filesNested,
                null, "Nested.nm.regression",
                null, null);
        String[] filesVarProb = {
            "VarProb.verics",
        };
        TestOptions optionsNoRangeChecking = DEFAULT_TEST_OPTIONS.shallowCopy();
        optionsNoRangeChecking.rangeChecking = false;
        compile(new PWD("regression/apta"), filesVarProb,
                null, "VarProb.nm.regression",
                optionsNoRangeChecking, null);
    }
    /**
     * Tests user-defined branch labels.
     */
    protected void testBranchLabels() {
        testPerformed();
        String[] filesNondeterministic = {
            "CD.java"
        };
        TestOptions optionsNoRangeChecking = DEFAULT_TEST_OPTIONS.shallowCopy();
        optionsNoRangeChecking.rangeChecking = false;
        compile(new PWD("regression/branchlabel"), filesNondeterministic,
                null, "CD.nm.regression",
                optionsNoRangeChecking, TURN_GAME_SCHEDULER);
        String[] filesNondeterministicRanges = {
            "CD_ranges.java"
        };
        compile(new PWD("regression/branchlabel"), filesNondeterministicRanges,
                null, "CD_ranges.nm.regression",
                null, TURN_GAME_SCHEDULER);
    }
    /**
     * Tests boxing/unboxing.
     */
    protected void testBoxing() {
        testPerformed();
        // how the instantiations/calls are optimized
        String[] filesOptimize = {
            "Optimize.java"
        };
        compile(new PWD("regression/boxing"), filesOptimize,
                null, "Optimize.nm.regression",
                null, null);
        // if boxing/unboxing works
        String[] filesBoxing = {
            "Boxing.verics"
        };
        compile(new PWD("regression/boxing"), filesBoxing,
                null, "Boxing.nm.regression",
                null, null);
    }
    @Override
    protected void test() {
        DEFAULT_TEST_OPTIONS.messageStyle = MessageStyle.Type.JAVA;
        DEFAULT_TEST_OPTIONS.verboseTADDVariables = false;
        DEFAULT_TEST_OPTIONS.compactInstances = false;
        DEFAULT_TEST_OPTIONS.traceAllocInCallResolution = false;
        /*testParseErrors();
        testParseErrorsVerics();
        testStaticContext();
        testControl();
        testControlVerics();
        testEscapeSequences();
        testSwitchLabels();
        testAccessModifiers();
        testInitialization();
        testArrays();
        testArrayInitializers();
        testOperators();
        testInheritance();
        testAmbiguousReference();
        testArithmeticVirtual();
        testUnambiguousVirtual();
        testAbstract();
        testCasts();
        testValidOverride();
        testInterfaces();
        testSingle();
        testABP();
        testTwoLanguages();
        testExtends();
        testFieldPrefix();
        testMutatorFlag();
        testFinalVariables();
        testInstanceOf();
        testObjectComparison();
        testCollege();
        testExperiments();
        testNotifyAll();
        testStrayTags();
        testRanges();
        testExit();
        testAssertions();
        testCPP();
        testSuper();*/
        DEFAULT_TEST_OPTIONS.messageStyle = MessageStyle.Type.VERICS;
        DEFAULT_TEST_OPTIONS.verboseTADDVariables = true;
        /*testTypeConversion();
        testMissingImport();
        testStaticImport();
        testCT();
        testConditionalTrees();
        testConstantIndexing();
        testScheduler();
        testUnroll();
        testIndexFormula();
        testBarrier();
        testMicrogrid();
        testStateClass();
        testPTAIO();
        testTuplesIO();
        testIsStatistical();
        testInvertBranch();*/
        DEFAULT_TEST_OPTIONS.compactInstances = true;
        /*testExternalConstants();
        testMirela(false);
        testMirela(true);
        testUrgentSync();
        testMirelaProb();
        testPMGC();
        testVericsStatic();
        testOverflow();
        testSubstring();
        testNdFile();
        testTernaryLogic();
        testTabbedDist();*/
        DEFAULT_TEST_OPTIONS.traceAllocInCallResolution = true;
        /*testBranchLabels();
        testAllocationInPTA();*/
        testBoxing();
        /* NOT IMPLEMENTED testFieldMethods(); */
        // !!! integer division
        // a test of casts of hierarchy of interfaces !!!
        // ^ in range code static analysis
        TESTS_PERFORMED = 68;
        COMPILATIONS_PERFORMED = 331;
    }
}
