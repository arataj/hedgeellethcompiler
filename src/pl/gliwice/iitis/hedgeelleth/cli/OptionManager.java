/*
 * OptionManager.java
 *
 * Created on Nov 5, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.cli;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp.CPPOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.modules.HedgeellethCompiler;

/**
 * Contains options used by various parts of the translator,
 * depending on the output format. Sets their default values.
 * Contains the list of files to translate.
 * 
 * @author Artur Rataj
 */
public class OptionManager {
    /**
     * Output format.
     */
    public enum OutputFormat {
        // no output generation
        NONE,
        // without runtime static analysis
        ASSEMBLER0,
        // after runtime static analysis
        ASSEMBLER,
        // Verics' output format for the BMC checker
        VERICS,
        // Uppaal's output format
        UPPAAL,
        // pure XML format
        XML,
        // Prism's output format
        PRISM,
        // Hedgeelleth automaton output format
        HEDGEELLETH,
        // C++ output format
        CPP,
    };
    
    /**
     * Compiler's options.
     */
    public HedgeellethCompiler.Options hedgeelleth;
    /**
     * Options for the PTA backend.
     */
    public TADDOptions pta;
    /**
     * Options specific to the CPP backend. Null if the backend
     * is not used.
     */
    public CPPOptions cpp;
    /**
     * Files to translate. Empty list for none.
     */
    public List<String> files;
    /**
     * If to print help information.
     */
    public boolean help;
    
    /**
     * Creates a new set of default, consistent options, depending on
     * the output format.
     * 
     * @param of output format, can be overriden by
     * <code>testOptions.forceFomat</code>
     * @param doc output format for documentation,
     * <code>DocOutputFormat.NONE</code> for none
     * @param testOptions optional test options, for tuning needed only by
     * compiler testing; normally null
     */
    public OptionManager(OutputFormat of, TADDOptions.DocOutputFormat dof, TestOptions testOptions) {
        pta = new TADDOptions();
        cpp = null;
        pta.verbosityLevel = FileFormatOptions.MAX_VERBOSITY_LEVEL;
        pta.cleanExperimentDirectory = false;
        pta.labelNondeterminism = true;
        boolean tuneToTADDs = false;
        if(testOptions != null && testOptions.forceFomat != null) {
            switch(testOptions.forceFomat) {
                case PRISM:
                    of = OutputFormat.PRISM;
                    break;
                    
                case HEDGEELLETH:
                    of = OutputFormat.HEDGEELLETH;
                    break;
                    
                case UPPAAL:
                    of = OutputFormat.UPPAAL;
                    break;
                    
                case VERICS_BMC:
                    of = OutputFormat.VERICS;
                    break;
                    
                case XML:
                    of = OutputFormat.XML;
                    break;
                    
                default:
                    throw new RuntimeException("unknown format");
            }
        }
        switch(of) {
            case ASSEMBLER0:
            case ASSEMBLER:
                /* empty */
                break;

            case VERICS:
                pta.setOutputFormat(TADDOptions.OutputFormat.VERICS_BMC);
                tuneToTADDs = true;
                break;

            case UPPAAL:
                pta.setOutputFormat(TADDOptions.OutputFormat.UPPAAL);
                tuneToTADDs = true;
                break;

            case XML:
                pta.setOutputFormat(TADDOptions.OutputFormat.XML);
                tuneToTADDs = true;
                break;

            case PRISM:
                pta.labelNondeterminism = false;
                pta.allowFpVariables = false;
                pta.setOutputFormat(TADDOptions.OutputFormat.PRISM);
                tuneToTADDs = true;
                break;

            case HEDGEELLETH:
                pta.labelNondeterminism = false;
                pta.setOutputFormat(TADDOptions.OutputFormat.HEDGEELLETH);
                tuneToTADDs = true;
                break;

            case CPP:
                pta.setOutputFormat(null);
                cpp = new CPPOptions();
                cpp.usePreamble = false;
                break;

            case NONE:
                // XML just supports everything, so default to it
                pta.setOutputFormat(TADDOptions.OutputFormat.XML);
                break;

            default:
                throw new RuntimeException("unknown format");

        }
        pta.setDocOutputFormat(dof);
        hedgeelleth = new HedgeellethCompiler.Options();
        if(testOptions != null) {
            pta.experimentMode = testOptions.experimentMode;
            pta.removeConstGuardsUsingRanges = testOptions.
                    removeConstGuardsUsingRanges;
            hedgeelleth.blankFinalsAllowed =
                    testOptions.blankFinalsAllowed;
            hedgeelleth.messageStyle =
                    testOptions.messageStyle;
            hedgeelleth.optimizerTuning.preserveRangedLocals =
                testOptions.preserveRangedLocals;
            hedgeelleth.optimizerTuning.assignmentTransportCheck =
                testOptions.assignmentTransportCheck;
            hedgeelleth.constants = testOptions.constants;
            hedgeelleth.test = testOptions.test;
            pta.rangeChecking = testOptions.rangeChecking;
            pta.isStatisticalHint = testOptions.isStatisticalHint;
            pta.generateStateClass = testOptions.generateStateClass;
            pta.generateNdFile = testOptions.generateNdFile;
            if(testOptions.arraysSupported &&
                    pta.arraySupport == TADDOptions.ArraySupport.EMULATE)
                pta.arraySupport = TADDOptions.ArraySupport.INDEX;
            pta.dtmcFallback = testOptions.dtmcFallback;
            pta.compactInstances = testOptions.compactInstances;
            pta.verboseTADDVariables =
                    testOptions.verboseTADDVariables;
            pta.test = testOptions.test;
            hedgeelleth.optimizerTuning.traceAllocInCallResolution =
                    testOptions.traceAllocInCallResolution;
        }
        hedgeelleth.optimizerTuning.tuneToTADDs =
                tuneToTADDs;
        files = new LinkedList<>();
        help = false;
    }
}
