/*
 * Compiler.java
 *
 * Created on October 29, 2007, 10:37 AM
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.cli;

import java.util.*;
import java.util.regex.Pattern;
import java.io.*;

import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp.CPPBackend;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.cpp.CPPOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.code.optimizer.Optimizer;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.frontend.java.JavaFrontend;
import pl.gliwice.iitis.hedgeelleth.modules.*;
import pl.gliwice.iitis.verics.VericsFrontend;
 
/**
 * Translates subset of Java or subset of Verics sources into TADDs.
 * 
 * @author Artur Rataj
 */
public class Compiler {
    /**
     * A default extension of Java source files, to be translated either
     * by <code>Hedgeelleth</code> or by <code>javac</code>.<br>
     * 
     * Some of these files might require <code>hth.jar</code> when
     * used inside a JDK.
     */
    static final String HEDGEELLETH_EXTENSION_JAVA = JavaFrontend.FILE_NAME_EXTENSION;
    /**
     * A special extension of Java source files with Hedgeelleth
     * annotations added, to be translated only by <code>Hedgeelleth</code>.
     */
    static final String HEDGEELLETH_EXTENSION_HC_JAVA = "hc_java";
    /**
     * An extension of Verics source files.
     */
    static final String HEDGEELLETH_EXTENSION_VERICS = VericsFrontend.FILE_NAME_EXTENSION;
    /**
     * Only these files are loaded if not full version of the library is requested.
     */
    static final String[] CORE_LIBRARY = {
        "Array.java",
        "Exception.java",
        "Math.java",
        "Object.java",
        "RuntimeException.java",
        "String.java",
        "System.java",
        "Array.verics",
        "E.verics",
        "Math.verics",
        "Object.verics",
        "RuntimeE.verics",
        "String.verics",
        "System.verics",
    };
    /**
     * Adds library files.
     * 
     * @param files library files are attached to the end of this list
     * @param topDirectory top compilation directory
     * @param libraryPath library directory
     * @param javaFound if to add library Java files
     * @param vericsFound if to add library Verics files
     * @param full do not save on omitting some less--used files from the standard
     * library; typically true for CLI, can be false in order to speed--up large number of
     * compilations of small programs via API calls
     * @param subsetForTestBlankFinalsAllowed false only for some special compiler test cases,
     * see this method's body for details
     */
    public static void addLibraryFiles(List<String> files,
            PWD topDirectory, String libraryPath,
            boolean javaFound, boolean vericsFound, boolean full,
            boolean subsetForTestBlankFinalsAllowed) throws CompilerException {
        if(libraryPath != null) {
            try {
// System.out.println("library path = " + libraryPath);
// System.out.println("top dir = " + topDirectory.toString());
                List<String> library = new LinkedList<>();
                if(javaFound) {
                    library.addAll(CompilerUtils.getAllFiles(topDirectory.getFile(libraryPath),
                            Pattern.compile(".*\\." + HEDGEELLETH_EXTENSION_JAVA)));
                    library.addAll(CompilerUtils.getAllFiles(topDirectory.getFile(libraryPath),
                            Pattern.compile(".*\\." + HEDGEELLETH_EXTENSION_HC_JAVA)));
                }
                if(vericsFound) {
                    library.addAll(CompilerUtils.getAllFiles(topDirectory.getFile(libraryPath),
                            Pattern.compile(".*\\.verics")));
                }
                PWD d = new PWD(new File(topDirectory.toString()).getAbsolutePath());
                for(String f : library) {
                    String name = f.substring(f.lastIndexOf('/') + 1);
                    boolean whitened = false;
                    if(full)
                        whitened = true;
                    else
                        for(String white : CORE_LIBRARY)
                            if(name.equals(white)) {
                                whitened = true;
                                break;
                            }
                    if(whitened)
                        files.add(d.getRelativePath(f));
                }
                if(!subsetForTestBlankFinalsAllowed) {
                    // this is a work--around solely for: some test cases from
                    // <code>CompilerTest.testFinalVariables</code>,
                    // e,g, one which mixes Java and Verics and disallows
                    // final blanks, even that they are available in Verics
                    files.remove(libraryPath + "/verics/math/Dist.verics");
                    for(String s : new LinkedList<>(files))
                        if(s.startsWith(libraryPath + "/hc/mirela/") ||
                                s.contains("Barrier.") ||
                                s.contains("TurnGame.") ||
                                s.contains("TurnPlayer.") ||
                                s.contains("/verics/lang/Array.") ||
                                s.contains("/verics/lang/E.") ||
                                // only because it extends <code>E</code>
                                s.contains("/verics/lang/RunE.") ||
                                s.contains("/verics/lang/Pr.") ||
                                s.contains("/verics/lang/Real."))
                            files.remove(s);
                }
                // `:' is used by <code>StreamPos</code> as
                // a separator
                // `$' is used by <code>StreamPos</code> as
                // a variable prefix
                char[] charactersNotAllowed = { ':', '$', };
                for(String f : files)
                    for(char c : charactersNotAllowed)
                        if(f.indexOf(c) != -1)
                            throw new IOException(f + ": character `" + c + "' not allowed " +
                                    "in the path or name of an input file");
            } catch(IOException e) {
                String errorMessage = "\tI/O ERROR<Library>: " + e.getMessage();
// System.out.println("*** IO error = " + e.toString());
                throw new CompilerException(null, CompilerException.Code.IO,
                        errorMessage);
            }
        }
    }
    /**
     * <p>Translates a set of files.</p>
     * 
     * <p>File names within comments in the output files may contain a
     * variable enclosed in <code>${}</code>, named as specified by
     * <code>Hedgeelleth.LIBRARY_PATH_VARIABLE_NAME_STRING</code>.
     * That variable's value is that of <code>libraryPath</code>.</p>
     * 
     * @param topDirectory              top directory of both the compiled
     *                                  and the output files
     * @param files                     names of files to translate
     * @param hedgeellethOptions        base for options of the translator, null for
     *                                  default; partially overridden by <code>taddOptions<code>
     *                                  in this method
     * @param taddOptions               tadd creation options
     * @param cppOptions                C++ backend specific translation options,
     *                                  that do not fit into <code>taddOptions</code>,
     *                                  can be not null only if the backend is
     *                                  actually used
     *                                  C++ has a separate backend
     * @param assemblyBuffer            an assembly buffer for code without
     *                                  runtime static analysis, as defined
     *                                  by the constructor of
     *                                  <code>TADDBackend</code>
     * @param libraryPath               if not null, searches for library
     *                                  files of all languages translated,
     *                                  whose in turn are determined by
     *                                  extensions of <code>files</code>,
     *                                  the name is either fully qualified
     *                                  or relative to <code>topDirectory</code>
     * @return                          list of the generated
     *                                  output files, as defined in
     *                                  <code>HedgeellethCompiler.translate</code>
     */
    public static HedgeellethCompiler.TranslationSummary translate(
            PWD topDirectory, List<String> files, 
            HedgeellethCompiler.Options hedgeellethOptions,
            TADDOptions taddOptions, CPPOptions cppOptions,
            TestAssemblyBuffer assemblyBuffer, String libraryPath)
                throws CompilerException {
        try {
            files = new LinkedList<>(files);
            boolean javaFound = false;
            boolean vericsFound = false;
            for(String s : files) {
                String extension = CompilerUtils.getFileNameExtension(s, 1);
                if(extension.equals(HEDGEELLETH_EXTENSION_JAVA) ||
                        extension.equals(HEDGEELLETH_EXTENSION_HC_JAVA))
                    javaFound = true;
                else if(extension.equals(HEDGEELLETH_EXTENSION_VERICS))
                    vericsFound = true;
                else
                    throw new CompilerException(null, CompilerException.Code.UNKNOWN,
                            "unknown type of file \"" + s + "\"");
            }
            if(libraryPath != null)
                addLibraryFiles(files, topDirectory, libraryPath, javaFound, vericsFound, true,
                        hedgeellethOptions.blankFinalsAllowed);
            Map<String, AbstractFrontend> frontends = new HashMap<>();
            if(javaFound) {
                AbstractFrontend f = new JavaFrontend();
                frontends.put(HEDGEELLETH_EXTENSION_JAVA, f);
                frontends.put(HEDGEELLETH_EXTENSION_HC_JAVA, f);
            }
            if(vericsFound)
                frontends.put(HEDGEELLETH_EXTENSION_VERICS, new VericsFrontend());
            if(hedgeellethOptions == null)
                    hedgeellethOptions = new HedgeellethCompiler.Options();
            hedgeellethOptions.enableGeneratorTags = taddOptions.enableGeneratorTags;
            hedgeellethOptions.experimentMode = taddOptions.experimentMode;
            hedgeellethOptions.cleanExperimentsDirectory = taddOptions.cleanExperimentDirectory;
            hedgeellethOptions.generateExperimentSources = taddOptions.generateExperimentSources;
            if(javaFound && !vericsFound)
                hedgeellethOptions.messageStyle = MessageStyle.Type.JAVA;
            // the following might increase proposal ranges, and thus effective variable ranges
            // hedgeellethOptions.primaryRangeChecking = taddOptions.rangeChecking;
            AbstractBackend backend;
            if(cppOptions != null)
                backend = new CPPBackend(cppOptions);
            else {
                backend = new TADDBackend(taddOptions, assemblyBuffer);
                ((TADDBackend)backend).optimizerTuning = hedgeellethOptions.optimizerTuning;
            }
            HedgeellethCompiler compiler = new HedgeellethCompiler(
                    frontends, backend);
            List<Optimizer.Optimization> optimizations = new LinkedList<>();
            optimizations.add(Optimizer.Optimization.BASIC);
            if(cppOptions != null) {
                optimizations.add(Optimizer.Optimization.TRACED);
                optimizations.add(Optimizer.Optimization.STATIC);
            }
// System.out.println(taddOptions.scheduler.toString());
            return compiler.translate(
                    topDirectory, topDirectory, libraryPath, files,
                    optimizations, hedgeellethOptions);
        } catch(CompilerException e) {
            applyMessageStyle(e, hedgeellethOptions.messageStyle, libraryPath);
            throw e;
        }
    }
    /**
     * Returns this compiler's directory.
     * 
     * @param base base directory, if null then the absolute path is always returned
     * @return directory
     */
    public static String  getCompilerDirectory(Class clazz, String base) {
        String path = clazz.getResource("").getPath();
        if(path.startsWith("file:"))
            path = path.substring(5, path.lastIndexOf("dist/Hedgeelleth"));
        else
            path = path.substring(0, path.lastIndexOf("build/classes/pl/"));
        // System.out.println("path2 = " + path);
        if(base != null) {
            PWD pwd = new PWD(base);
            path = pwd.getRelativePath(path);
        }
        return path;
    }
    /**
     * Returns this compiler's standard library directory.
     * 
     * @param base base directory, if null then the absolute path is always returned
     * @return directory
     */
    public static String  getCompilerLibraryDirectory(Class clazz, String base) {
        String path = getCompilerDirectory(clazz, base);
        path = new PWD(path).getFile("lib").getPath();
        return path;
    }
    /**
     * Apples a message style to reports in a compiler  exception.
     * 
     * @param e exception
     * @param messageStyle message style to apply
     * @param libraryPath library path
     */
    public static void applyMessageStyle(CompilerException e,
            MessageStyle.Type messageStyle, String libraryPath) {
        Map<String, String> variables = new HashMap();
        StringBuilder s = new StringBuilder();
        for(int i = 0; i < libraryPath.length(); ++i) {
            String c = "" + libraryPath.charAt(i);
            if(c.equals("/"))
                c = "\\/";
            s.append(c);
        }
        s.append("/");
        variables.put(Hedgeelleth.LIBRARY_PATH_VARIABLE_NAME_STRING,
                s.toString());
        MessageStyle.apply(messageStyle,
                variables, e);
    }
}
