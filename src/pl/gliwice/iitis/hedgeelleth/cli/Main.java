/*
 * Main.java
 *
 * Created on October 29, 2007, 10:37 AM
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 2 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.cli;

import java.util.*;

import com.martiansoftware.jsap.*;

import pl.gliwice.iitis.hedgeelleth.About;
import pl.gliwice.iitis.hedgeelleth.Hedgeelleth;
import pl.gliwice.iitis.hedgeelleth.pta.SchedulerType;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.*;
import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.*;
import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.Scheduler;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.languages.IntegerSetExpression;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.scope.BlockScope;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamPos;
import pl.gliwice.iitis.hedgeelleth.frontend.java.parser.JavaParser;
import pl.gliwice.iitis.hedgeelleth.frontend.java.parser.JavaParserConstants;
import pl.gliwice.iitis.hedgeelleth.modules.HedgeellethCompiler;
import pl.gliwice.iitis.hedgeelleth.util.SnippetParserWrapper;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;

/**
 * Translates subset of Java or Verics sources into TADDs.
 * 
 * @author Artur Rataj
 */
public class Main {
    /**
     * Parsers a list of constants, which can be integers, lists of integers or
     * strings. To be used with command--line options.
     * 
     * @param constants sequence of strings, each of the
     * for $lt;constant's name&gt;=&lt;value(s)&gt;, where
     * &lt;value(s)&gt; has the syntax as in <code>IntegerSetInitializer.grammar</code>.
     * @param true if a generic string is also possible
     * @return a map of names to sorted sequences of values
     */
    public static CliConstants parseCLIConstants(String[] constants,
            boolean enableAlphanumeric) throws CompilerException {
        CliConstants out = new CliConstants();
        for(int index = 0; index < constants.length; ++index) {
            String c = constants[index];
            int pos = c.indexOf('=');
            if(pos == -1)
                throw new CompilerException(null, CompilerException.Code.PARSE,
                        "invalid constant declaration `" + c + "': missing `=': " + c);
            String name = c.substring(0, pos).trim();
            if(name.isEmpty())
                throw new CompilerException(null, CompilerException.Code.PARSE,
                        "invalid constant declaration `" + c + "': missing name: " + c);
            String value = c.substring(pos + 1).trim();
            if(value.isEmpty())
                throw new CompilerException(null, CompilerException.Code.PARSE,
                        "invalid constant declaration `" + c + "': missing value: " + c);
            String streamName = "command line constants";
            List<Literal> list = new LinkedList<>();
            boolean array;
            CliConstants.CType type;
            try {
                // is it a single integer?
                long l = Long.parseLong(value);
                list.add(new Literal(l));
                array = false;
                type = CliConstants.CType.INTEGER;
            } catch(NumberFormatException e) {
                try {
                    // is it a single double?
                    double d = Double.parseDouble(value);
                    list.add(new Literal(d));
                    array = false;
                    type = CliConstants.CType.DOUBLE;
                } catch(NumberFormatException f) {
                    // is it a list of integers?
                    JavaParser parser = new JavaParser(null, value,
                            new StreamPos(null, index + 1, 1), true);
                    parser.hedgeelleth = new Hedgeelleth(parser, null, null, streamName);
                    SnippetParserWrapper<IntegerSetExpression> w =
                            new SnippetParserWrapper<IntegerSetExpression>(parser) {
                        @Override
                        public IntegerSetExpression call() throws pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException {
                            IntegerSetExpression set = ((JavaParser)PARSER).IntegerSetInitializer(
                                    new BlockScope(null, "block"), "meaningless");
                            if(PARSER.getNextToken().kind != JavaParserConstants.EOF)
                                throw new pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException(
                                        null, pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException.Code.PARSE,
                                        "constant declaration: unexpected `" + PARSER.getNextToken() + "'");
                            return set;
                        }
                    };
                    w.parse();
                    if(w.error() != null) {
                        // can it be some other string?
                        if(enableAlphanumeric) {
                            list.add(new Literal(value, null));
                            array = false;
                            type = CliConstants.CType.STRING;
                        } else {
                            for(CompilerException.Report r : w.error().getReports()) {
                                r.pos = null;
                                r.message = "invalid constant declaration: " +
                                        r.message;
                            }
                            throw w.error();
                        }
                    } else {
                        IntegerSetExpression expr = w.parsed();
                        for(long l : expr.values)
                            list.add(new Literal(l));
                        array = true;
                        type = CliConstants.CType.INTEGER;
                    }
                }
            }
            out.defines.put(name, list);
            out.array.put(name, array);
            out.type.put(name, type);
        }
        return out;
    }
    /**
     * Parses command line, returns translation options and
     * files to translate.
     * 
     * @param args command line arguments
     * @return a new option manager
     */
    protected static OptionManager parseOptions(String[] args) {
        boolean translate = true;
        final String HELP = "help";
        final String SCHEDULER = "S[T]";
        final String IMPLICIT_SCHEDULER = "implicit scheduler";
        final String SCHEDULER_OVERRIDES = "scheduler overrides";
        final String SCHEDULER_C = "coefficient of the constant scheduler";
        final String SCHEDULER_LAMBDA = "coefficient of the nexp scheduler";
        final String STRICT_FIELD_ACCESS = "strict field access";
        final String RANGE_CHECKING = "range checking";
        final String IS_STATISTICAL = "a hint Model.isStatistical()";
        final String URGENT_SYNC = "prioritise synced transitions";
        final String DTMC = "fall back frm mdp to dtmc if no nondeterminism";
        final String GET_CONST = "constant";
        final String VERBOSITY = "verbosity";
        final String OUTPUT_FORMAT = "output format";
        final String DOC_OUTPUT_FORMAT = "documentation output format";
        final String EXPERIMENTS_MODE = "experiments mode";
        final String STATE_CLASS = "generate state class";
        final String ND_FILE = "generate nd file";
        final String ARRAYS_SUPPORTED = "enable arrays in the output model";
        final String REPORT_OUTPUT = "report any output file generated " +
                "with a machine--readable prefix: " +
                HcOutputFiles.MODEL_FILE_PREFIX + ", " +
                HcOutputFiles.PROPERTY_FILE_PREFIX + ", " +
                HcOutputFiles.STATE_FILE_PREFIX + ", " +
                HcOutputFiles.MODEL_TYPE_PREFIX;
        final String INPUT_FILES = "source_file_";
        final char FORMAT_VERICS_BMC = 'b';
        final char FORMAT_XML = 'x';
        final char FORMAT_UPPAAL = 'u';
        final char FORMAT_PRISM = 'p';
        final char FORMAT_HEDGEELLETH = 'h';
        final char DOC_FORMAT_NONE = 'n';
        final char DOC_FORMAT_AADL = 'a';
        // normally unused
        final char FORMAT_CPP = 'U';
        
        final char SCHEDULER_FREE = 'f';
        final char SCHEDULER_IMMEDIATE = 'i';
        final char SCHEDULER_HIDDEN = 'h';
        final char SCHEDULER_NEXP = 'n';
        final char SCHEDULER_CONST = 'c';
        
        OptionManager om = null;
        try {
            //
            // declare options
            //
            JSAP jsap = new JSAP();
            // -h --help
            Switch helpParam = new Switch(HELP).
                setShortFlag('h').
                setLongFlag("help");
            helpParam.setHelp("show help and exit");
            jsap.registerParameter(helpParam);
            // -s --scheduler
            FlaggedOption schedulerParam = new FlaggedOption(SCHEDULER)
                                    .setStringParser(JSAP.STRING_PARSER)
                                    .setDefault("" + SCHEDULER_FREE + "LY") 
                                    .setRequired(false) 
                                    .setShortFlag('s') 
                                    .setLongFlag("scheduler");
            schedulerParam.setHelp("S specifies scheduler: " +
                    SCHEDULER_FREE + " free (default), " +
                    SCHEDULER_IMMEDIATE + " immediate, " +
                    SCHEDULER_HIDDEN + " hidden, " +
                    SCHEDULER_NEXP + " negative exponential, " +
                    SCHEDULER_CONST + " constant; " +
                    "T specifies extra delay parameters: " +
                    "l|L -- extra transition after sleeps: disable/enable if demanded (default), " +
                    "y|Y -- do not prepend/prepend emulated urgent sync subautomaton with extra transition (default)");
            schedulerParam.setCategory("MODEL");
            jsap.registerParameter(schedulerParam);
            // -i --scheduler-implicit
            Switch implicitSchedulerParam = new Switch(IMPLICIT_SCHEDULER).
                setShortFlag('i').
                setLongFlag("implicit-scheduler");
            implicitSchedulerParam.setHelp("if the scheduler should not add " +
                    "clock conditions to the output model");
            implicitSchedulerParam.setCategory("MODEL");
            jsap.registerParameter(implicitSchedulerParam);
            // -o --scheduler-overrides
            Switch schedulerOverridesParam = new Switch(SCHEDULER_OVERRIDES).
                setShortFlag('d').
                setLongFlag("scheduler-overrides");
            schedulerOverridesParam.setHelp("if the scheduler should override " +
                    "existing clock conditions");
            schedulerOverridesParam.setCategory("MODEL");
            jsap.registerParameter(schedulerOverridesParam);
            // -c --scheduler-c
            FlaggedOption schedulerCParam = new FlaggedOption(SCHEDULER_C)
                                    .setStringParser(JSAP.DOUBLE_PARSER)
                                    .setDefault("1") 
                                    .setRequired(false) 
                                    .setShortFlag('c') 
                                    .setLongFlag("scheduler-c");
            schedulerCParam.setHelp("specify the constant scheduler's " +
                    "time unit");
            schedulerCParam.setCategory("MODEL");
            jsap.registerParameter(schedulerCParam);
            // -l --scheduler-lambda
            FlaggedOption schedulerLambdaParam = new FlaggedOption(SCHEDULER_LAMBDA)
                                    .setStringParser(JSAP.DOUBLE_PARSER)
                                    .setDefault("1") 
                                    .setRequired(false) 
                                    .setShortFlag('l') 
                                    .setLongFlag("scheduler-lambda");
            schedulerLambdaParam.setHelp("specify the negative exponential scheduler's " +
                    "lambda");
            schedulerLambdaParam.setCategory("MODEL");
            jsap.registerParameter(schedulerLambdaParam);
            Switch isUrgentSync = new Switch(URGENT_SYNC).
                setShortFlag('U').
                setLongFlag("urgent-sync");
            isUrgentSync.setHelp("prioritise synced transitions by making them urgent or, " +
                    "if urgency not supported, emulate with subautomatons");
            isUrgentSync.setCategory("MODEL");
            jsap.registerParameter(isUrgentSync);
            // --strict-field-access
            Switch strictFieldAccessParam = new Switch(STRICT_FIELD_ACCESS).
                setShortFlag(JSAP.NO_SHORTFLAG).
                setLongFlag("strict-field-access");
            strictFieldAccessParam.setHelp("disallow move of field access to neighbouring " +
                    "transition even if there are no explicit time constraints and no violation " +
                    "of the order of field access");
            strictFieldAccessParam.setCategory("MODEL");
            jsap.registerParameter(strictFieldAccessParam);
            // -r --range-checking
            Switch rangeCheckingParam = new Switch(RANGE_CHECKING).
                setShortFlag('r').
                setLongFlag("range-checking");
            rangeCheckingParam.setHelp("if checking of ranges of variables, methods and primary ranges " +
                    "should be generated; surpresses checking of other properties");
            rangeCheckingParam.setCategory("MODEL");
            jsap.registerParameter(rangeCheckingParam);
            // -S --statistical
            Switch isStatisticalParam = new Switch(IS_STATISTICAL).
                setShortFlag('S').
                setLongFlag("statistical");
            isStatisticalParam.setHelp("sets hint Model.isStatistical() to true");
            isStatisticalParam.setCategory("MODEL");
            jsap.registerParameter(isStatisticalParam);
            // --dtmc
            Switch dtmcFallbackParam = new Switch(DTMC).
                setShortFlag(JSAP.NO_SHORTFLAG).
                setLongFlag("dtmc");
            dtmcFallbackParam.setHelp("fall back from mdp to dtmc if deterministic");
            dtmcFallbackParam.setCategory("MODEL");
            jsap.registerParameter(dtmcFallbackParam);
            // --const
            FlaggedOption constParam = new FlaggedOption(GET_CONST)
                                    .setStringParser(JSAP.STRING_PARSER)
                                    .setRequired(false) 
                                    .setShortFlag(JSAP.NO_SHORTFLAG) 
                                    .setLongFlag("const")
                                    .setList(true)
                                    .setListSeparator(',');
            constParam.setHelp("defines constants: <name>=<value(s)>,... " +
                    "where <value(s)> are #-separated arithmetic constants or sets " +
                    "of integer values <min>~<max>[:<step>]");
            constParam.setCategory("MODEL");
            jsap.registerParameter(constParam);
            // -v --verbose
            FlaggedOption verbosityParam = new FlaggedOption(VERBOSITY)
                                    .setStringParser(JSAP.INTEGER_PARSER)
                                    .setDefault("" + FileFormatOptions.DEFAULT_VERBOSITY_LEVEL) 
                                    .setRequired(false) 
                                    .setShortFlag('v') 
                                    .setLongFlag("verbose");
            verbosityParam.setHelp("set comment verbosity to " +
                                "0 ... " + FileFormatOptions.MAX_VERBOSITY_LEVEL);
            verbosityParam.setCategory("OUTPUT");
            jsap.registerParameter(verbosityParam);
            // -o --output-format
            FlaggedOption outputFormatParam = new FlaggedOption(OUTPUT_FORMAT)
                                    .setStringParser(JSAP.CHARACTER_PARSER)
                                    .setDefault("" + FORMAT_HEDGEELLETH) 
                                    .setRequired(false) 
                                    .setShortFlag('o') 
                                    .setLongFlag("output-format");
            outputFormatParam.setHelp("set output format: " +
                    FORMAT_HEDGEELLETH + " Hedgeelleth, " +
                    FORMAT_PRISM + " Prism, " +
                    FORMAT_UPPAAL + " Uppaal, " +
                    FORMAT_VERICS_BMC + " Verics BMC, " + 
                    FORMAT_XML + " generic XML");
            outputFormatParam.setCategory("OUTPUT");
            jsap.registerParameter(outputFormatParam);
            // -D --doc-output-format
            FlaggedOption docOutputFormatParam = new FlaggedOption(DOC_OUTPUT_FORMAT)
                                    .setStringParser(JSAP.CHARACTER_PARSER)
                                    .setDefault("" + DOC_FORMAT_NONE) 
                                    .setRequired(false) 
                                    .setShortFlag('D') 
                                    .setLongFlag("doc-output-format");
            docOutputFormatParam.setHelp("set output format of accompanying producer/consumer documentation: " +
                    DOC_FORMAT_NONE + " none (no documentation generated), " +
                    DOC_FORMAT_AADL + " AADL");
            outputFormatParam.setCategory("OUTPUT");
            jsap.registerParameter(docOutputFormatParam);
            // --experiments
            Switch experimentsModeParam = new Switch(EXPERIMENTS_MODE).
                setShortFlag(JSAP.NO_SHORTFLAG).
                setLongFlag("experiments");
            experimentsModeParam.setHelp("structure of output files as in experiments, " +
                    "even if no sets of constants are defined");
            experimentsModeParam.setCategory("OUTPUT");
            jsap.registerParameter(experimentsModeParam);
            // -j --state-class
            Switch stateClassParam = new Switch(STATE_CLASS).
                setShortFlag('j').
                setLongFlag("state-class");
            stateClassParam.setHelp("generate source file of a Java class <main class name>State, " +
                    "that returns values of state variables");
            stateClassParam.setCategory("OUTPUT");
            jsap.registerParameter(stateClassParam);
            // -n --nd-file
            Switch ndFileParam = new Switch(ND_FILE).
                setShortFlag('n').
                setLongFlag("nd-file");
            ndFileParam.setHelp(ND_FILE);
            ndFileParam.setCategory("OUTPUT");
            jsap.registerParameter(ndFileParam);
            // -a --arrays
            Switch arraysSupportedParam = new Switch(ARRAYS_SUPPORTED).
                setShortFlag('a').
                setLongFlag("arrays");
            arraysSupportedParam.setHelp("do not emulate arrays, if possible");
            arraysSupportedParam.setCategory("OUTPUT");
            jsap.registerParameter(arraysSupportedParam);
            // -R --report-output
            Switch reportParam = new Switch(REPORT_OUTPUT).
                setShortFlag('R').
                setLongFlag("report");
            reportParam.setHelp(REPORT_OUTPUT);
            reportParam.setCategory("OUTPUT");
            jsap.registerParameter(reportParam);
            // input files
            UnflaggedOption inputFilesParam = new UnflaggedOption(INPUT_FILES)
                                    .setStringParser(JSAP.STRING_PARSER)
                                    .setRequired(false)
                                    .setGreedy(true);
            inputFilesParam.setCategory("");
            jsap.registerParameter(inputFilesParam);
            inputFilesParam.setHelp("source files: " +
                    "*." + Compiler.HEDGEELLETH_EXTENSION_JAVA +
                    " *." + Compiler.HEDGEELLETH_EXTENSION_HC_JAVA +
                    " *." + Compiler.HEDGEELLETH_EXTENSION_VERICS);
            //
            // parse options
            //
            JSAPResult config = jsap.parse(args);
            if(!config.success()) {
                Iterator<String> it = (Iterator<String>)config.getErrorMessageIterator();
                StringBuilder errors = new StringBuilder();
                while(it.hasNext()) {
                    if(errors.length() != 0)
                        errors.append("; ");
                    errors.append(it.next());
                }
                throw new JSAPException(errors.toString());
            }
            OptionManager.OutputFormat of;
            switch(config.getChar(OUTPUT_FORMAT)) {
                case FORMAT_HEDGEELLETH:
                    of = OptionManager.OutputFormat.HEDGEELLETH;
                    break;
                    
                case FORMAT_PRISM:
                    of = OptionManager.OutputFormat.PRISM;
                    break;
                    
                case FORMAT_UPPAAL:
                    of = OptionManager.OutputFormat.UPPAAL;
                    break;
                    
                case FORMAT_VERICS_BMC:
                    of = OptionManager.OutputFormat.VERICS;
                    break;
                    
                case FORMAT_XML:
                    of = OptionManager.OutputFormat.XML;
                    break;
                    
                case FORMAT_CPP:
                    of = OptionManager.OutputFormat.CPP;
                    break;
                    
                default:
                    System.out.println("unknown output format");
                    translate = false;
                    of = OptionManager.OutputFormat.NONE;
                    break;
                    
            }
            TADDOptions.DocOutputFormat dof;
            switch(config.getChar(DOC_OUTPUT_FORMAT)) {
                case DOC_FORMAT_NONE:
                    dof = TADDOptions.DocOutputFormat.NONE;
                    break;
                    
                case DOC_FORMAT_AADL:
                    dof = TADDOptions.DocOutputFormat.AADL;
                    break;
                    
                default:
                    System.out.println("unknown output format of optional producer/consumer documentation");
                    translate = false;
                    dof = TADDOptions.DocOutputFormat.NONE;
                    break;
            }
            om = new OptionManager(of, dof, null);
            if(config.getBoolean(EXPERIMENTS_MODE))
                om.pta.experimentMode = HedgeellethCompiler.Options.
                        ExperimentMode.TRUE;
            else
                om.pta.experimentMode = HedgeellethCompiler.Options.
                        ExperimentMode.ADAPT;
            // valuation is currently not used
            om.pta.valuationType =
                new ValuationType(ValuationType.Type.NONE, null);
            SchedulerType st = null;
            Boolean schedulerSleepAppend = null;
            Boolean schedulerNonZeroSyncDelay = null;
            String scheduler = config.getString(SCHEDULER);
            if(scheduler.length() != 1 && scheduler.length() != 3) {
                System.out.println("invalid scheduler parameter");
                translate = false;
            } else {
                char schedulerType = scheduler.charAt(0);
                switch(schedulerType) {
                    case SCHEDULER_FREE:
                        st = SchedulerType.FREE;
                        break;

                    case SCHEDULER_IMMEDIATE:
                        st = SchedulerType.IMMEDIATE;
                        break;

                    case SCHEDULER_HIDDEN:
                        st = SchedulerType.HIDDEN;
                        break;

                    case SCHEDULER_NEXP:
                        st = SchedulerType.NXP;
                        break;

                    case SCHEDULER_CONST:
                        st = SchedulerType.CONST;
                        break;

                    default:
                        // System.out.println("unknown scheduler");
                        translate = false;
                        st = null;
                        break;

                }
                String schedulerDelay;
                if(scheduler.length() == 1)
                    schedulerDelay = "LY";
                else
                    schedulerDelay = scheduler.substring(1, 3);
                for(int i = 0; i < 2; ++i) {
                    char c = schedulerDelay.charAt(i);
                    switch(c) {
                        case 'l':
                            schedulerSleepAppend = false;
                            break;
                            
                        case 'L':
                            schedulerSleepAppend = true;
                            break;
                            
                        case 'y':
                            schedulerNonZeroSyncDelay = false;
                            break;
                            
                        case 'Y':
                            schedulerNonZeroSyncDelay = true;
                            break;
                            
                        default:
                            System.out.println("unknown scheduler extra delay parameter: `"  + c + "'");
                            translate = false;
                            
                    }
                }
                if(schedulerSleepAppend == null) {
                    System.out.println("scheduler extra delay specified, but l|L is missing");
                    translate = false;
                }
                if(schedulerNonZeroSyncDelay == null) {
                    System.out.println("scheduler extra delay specified, but y|Y is missing");
                    translate = false;
                }
            }
            om.help = config.getBoolean(HELP);
            double schedulerC = config.getDouble(SCHEDULER_C);
            double schedulerLambda = config.getDouble(SCHEDULER_LAMBDA);
            boolean schedulerPrioritiseSync = config.getBoolean(URGENT_SYNC);
            if(st != null && translate)
                om.pta.scheduler = new Scheduler(st,
                        config.getBoolean(IMPLICIT_SCHEDULER),
                        config.getBoolean(SCHEDULER_OVERRIDES),
                        schedulerC, schedulerLambda,
                        schedulerSleepAppend, schedulerNonZeroSyncDelay,
                        schedulerPrioritiseSync);
            om.pta.strictFieldAccess = config.getBoolean(STRICT_FIELD_ACCESS);
            om.pta.rangeChecking = config.getBoolean(RANGE_CHECKING);
            om.pta.isStatisticalHint= config.getBoolean(IS_STATISTICAL);
            om.pta.dtmcFallback= config.getBoolean(DTMC);
            try {
                om.hedgeelleth.constants = parseCLIConstants(
                        config.getStringArray(GET_CONST), false);
            } catch(CompilerException e) {
                System.out.println(e.getMessage());
                translate = false;
            }
            om.pta.generateModelControlProperties = !om.pta.rangeChecking;
            om.pta.verbosityLevel = config.getInt(VERBOSITY);
            if(om.pta.verbosityLevel < 0 ||
                    om.pta.verbosityLevel > FileFormatOptions.MAX_VERBOSITY_LEVEL) {
                System.out.println("verbosity out of range");
                translate = false;
            }
            if(om.pta.getOutputFormat() != TADDOptions.OutputFormat.VERICS_BMC &&
                    om.pta.valuationType.type != ValuationType.Type.NONE) {
                System.out.println("valuation can be specified only for Verics backend");
                translate = false;
            }
            if(om.cpp != null && om.pta.experimentMode ==
                    HedgeellethCompiler.Options.ExperimentMode.TRUE) {
                System.out.println("experiments can not be specified for C++ output");
                translate = false;
            }
            om.pta.generateStateClass = config.getBoolean(STATE_CLASS);
            om.pta.generateNdFile = config.getBoolean(ND_FILE);
            boolean arraysSupported = config.getBoolean(ARRAYS_SUPPORTED);
            switch(of) {
                case HEDGEELLETH:
                    /* supports both arrays and their emulation */
                    if(arraysSupported)
                        om.pta.arraySupport = TADDOptions.ArraySupport.INDEX;
                    else
                        om.pta.arraySupport = TADDOptions.ArraySupport.EMULATE;
                    break;
                    
                case PRISM:
                    om.pta.arraySupport = TADDOptions.ArraySupport.EMULATE;
                    break;
                    
                case XML:
                case CPP:
                case UPPAAL:
                    om.pta.arraySupport = TADDOptions.ArraySupport.INDEX;
                    break;
                    
                case VERICS:
                    om.pta.arraySupport = TADDOptions.ArraySupport.NONE;
                    break;
                    
                default:
                    System.out.println("unknown output format");
                    translate = false;
                    break;
            }
            om.hedgeelleth.reportOutput = config.getBoolean(REPORT_OUTPUT);
            om.files = CompilerUtils.expandFiles(
                    Arrays.asList(config.getStringArray(INPUT_FILES)));
            if(om.help) {
                System.out.println("hc version " + About.VERSION + "\n" +
                        "command line syntax:\n\n" +
                        jsap.getHelp());
                om = null;
            }
        } catch(JSAPException e) {
            System.out.println("could not parse options: " + e.getMessage());
            translate = false;
        }
        if(translate)
            return om;
        else
            return null;
    }
    /**
     * Compiles files into one compilation environment.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] args_ = {
            //"regression/abp/AltBitProtocol.java",
            //"regression/abp/Sender.java",
            //"regression/abp/Receiver.java",
            //"regression/abp/LossyChannel.java",
            //"regression/microgrid/IsStatistical.java",
            //"example/isstatistical/Microgrid.java",
            //"regression/ci/ConstantIndexingPartial.verics",
            //"regression/extconst/ExtConstCompiler.java",
            //"example/simple/CoinFlipFixed.java",
            //"example/mm1/MM1.java",
            //"/nhome/file-srv/arataj/mirela/Mirela.java",
            //"regression/indexformula/IndexFormulaCheck.java",
            //"regression/scheduler/SyncTwo.java", "-sc",
            //"regression/mirela/SleepRange.java", "-scly", "-op",
            //"../Mirela/src/models/mirela/PriorityS.java", "../Mirela/src/models/mirela/MirelaEx2c.java", "-sfly", "-d", "-op", "-v0",
            //"/home/art/modelling/papers/pmgc/Dialogue2.java",
            //"/home/art/projects/svn/HedgeellethCompiler/regression/pmgc/DialogueUnusedBranch.java",
            //"/home/art/projects/svn/HedgeellethOptimiser/example/coinflip/CoinFlip.java",
            //"/home/art/projects/svn/HedgeellethOptimiser/example/coinflip/CoinChoice.java",
            //"-sh", "-i", "-v0", "-op", "-j", "-nR", "--const", "N=100",
            //"regression/apta/VarProb.verics", "-v0", "-op",
            //"/home/art/projects/svn/Modelling/src/pmgc/CD.java", "-v0", "-op", "-sh", "-i", "--const", "TURNS=5",
            //    "--const", "TURNS=7,CHEATING=1,DECLARATION=1",
            //"-op", "-sh", "-v0", "-a", //, "-r" //"--const", "HOUSEHOLDS=3~9:2",
            // "-j" //"-S", "-a" // "-j", "-r",
            /*
"-oU",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/AbstractLocation.java",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/Line.java",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/Mat2d.java",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/Mat4d.java",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/Point2d.java",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/Point.java",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/Polyhedron.java",
"/home/art/projects/art/dev/Volumes/src/linearalgebra/Rotation.java",
"/home/art/projects/art/dev/Volumes/src/volumes/AbstractContainer.java",
"/home/art/projects/art/dev/Volumes/src/volumes/AbstractIntLocation.java",
"/home/art/projects/art/dev/Volumes/src/volumes/AbstractOctreeContainer.java",
"/home/art/projects/art/dev/Volumes/src/volumes/AbstractVectorVoxel.java",
"/home/art/projects/art/dev/Volumes/src/volumes/AbstractVolume.java",
"/home/art/projects/art/dev/Volumes/src/volumes/AbstractVoxel.java",
"/home/art/projects/art/dev/Volumes/src/volumes/Array3d.java",
"/home/art/projects/art/dev/Volumes/src/volumes/ArrayVolume.java",
"/home/art/projects/art/dev/Volumes/src/volumes/DSTVoxel.java",
"/home/art/projects/art/dev/Volumes/src/volumes/Index.java",
"/home/art/projects/art/dev/Volumes/src/volumes/Octree.java",
"/home/art/projects/art/dev/Volumes/src/volumes/OctreeLeafDense.java",
"/home/art/projects/art/dev/Volumes/src/volumes/OctreeLeafSparse.java",
"/home/art/projects/art/dev/Volumes/src/volumes/OctreeNode.java",
"/home/art/projects/art/dev/Volumes/src/volumes/OctreeVolume.java",
"/home/art/projects/art/dev/Volumes/src/volumes/Real8Voxel.java",
"/home/art/projects/art/dev/Volumes/src/volumes/RectangularPrism.java",
"/home/art/projects/art/dev/Volumes/src/volumes/Size.java",
"/home/art/projects/art/dev/Volumes/src/volumes/VolumeIO.java",
"/home/art/projects/art/dev/Volumes/src/volumes/VolumeTools.java",
"/home/art/projects/art/dev/Volumes/src/volumes/VoxelUtils.java",
"/home/art/projects/art/dev/Volumes/src/volumes/test/Test.java",
            */
            //"../Mirela/src/mirela/*.java",
            //"../Mirela/src/mirela/aadl/*.java",
            ///"../Mirela/src/models/mirela/aadl/Camera.java",
            ///"../Mirela/src/models/mirela/aadl/VideoProcessing.java",
            ///"../Mirela/src/models/mirela/aadl/ImageAcquisitionSystem.java", "-op", "-v0",
            "regression/boxing/Optimize.verics",
            ///"-U", "-sh", "-i", "-Da",
        };
        args = args_;
        OptionManager om = parseOptions(args);
        if(om != null) {
            try {
                PWD pwd = new PWD();
                HedgeellethCompiler.TranslationSummary summary =
                        Compiler.translate(pwd, om.files,
                            om.hedgeelleth, om.pta, om.cpp, null,
                            Compiler.getCompilerLibraryDirectory(Compiler.class,
                                System.getProperty("user.dir")));
                if(om.hedgeelleth.reportOutput)
                    HcOutputFiles.reportOutput(pwd, summary.MODEL_TYPE,
                            summary.OUT_FILES);
            } catch(CompilerException e) {
                System.out.println("hc: " + e.getMessage());
                System.exit(2);
            }
        } else
            System.exit(1);
    }
}
