/*
 * TestOptions.java
 *
 * Created on Jul 2, 2012
 *
 * Copyright (c) 2012 Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.cli;

import java.util.*;

import pl.gliwice.iitis.hedgeelleth.compiler.backend.tadd.TADDOptions.OutputFormat;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.CompilationOptions;
import pl.gliwice.iitis.hedgeelleth.compiler.tree.Literal;
import pl.gliwice.iitis.hedgeelleth.modules.CliConstants;
import pl.gliwice.iitis.hedgeelleth.modules.HedgeellethCompiler;
import pl.gliwice.iitis.hedgeelleth.modules.MessageStyle;

/**
 * Options, that affect the compiler run by the test suite.
 * 
 * @author Artur Rataj
 */
public class TestOptions implements Cloneable {
    /**
     *  Copied to <code>TADDOptions.allowExperiments</code>.<br>
     * 
     * The default is <code>FALSE</code>.
     */
     public HedgeellethCompiler.Options.ExperimentMode experimentMode =
             HedgeellethCompiler.Options.ExperimentMode.FALSE;
     /**
      * Copied to <code>HedgeellethCompiler.Options.blankFinalsAllowed</code>.<br>
      * 
      * The default is true.
      */
     public boolean blankFinalsAllowed = true;
     /**
      * Copied to <code>HedgeellethCompiler.Options.messageStyle</code>.<br>
      * 
      * The default is <code>MessageStyle.Type.VERICS</code>.
      */
     public MessageStyle.Type messageStyle = MessageStyle.Type.VERICS;
    /**
        * Copied to <code>HedgeellethCompiler.Options.Tuning.preserveRangedLocals</code>.<br>
        * 
        * The default is false.
        */
    public boolean preserveRangedLocals = false;
    /**
        * Copied to <code>HedgeellethCompiler.Options.Tuning.assignmentTransportCheck</code>.<br>
        * 
        * The default is true.
        */
    public boolean assignmentTransportCheck = true;
    /**
     * Copied to <code>HedgeellethCompiler.Options.constants</code>.
     * 
     * The default is an empty map.
     */
    public CliConstants constants = new CliConstants();
     /**
      * Copied to <code>TADDOptions.verboseTADDVariables</code>.<br>
      * 
      * The default is false.
      */
     public boolean verboseTADDVariables = false;
    /**
     * Copied to <code>TADDOptions.removeConstGuardsUsingRanges</code>.
     * 
     * The default is true.
     */
    public boolean removeConstGuardsUsingRanges = true;
    /**
     * Test--only options, to be copied to <code>HedgeellethCompiler.Options</code>.
     */
    public CompilationOptions.Test test = new CompilationOptions.Test();
    /**
     * Copied to <code>TADDOptions.rangeChecking</code>.
     * 
     * The default is true.
     */
    public boolean rangeChecking = true;
    /**
     * If to use smaller long ranges, that fit into 32 bits.
     * 
     * The default is true and must be true for PTA generation.
     */
    public boolean reducedLongRange = true;
    /**
     * Copied to <code>TADDOptions.isStatisticalHint</code>.
     * 
     * The default is false.
     */
    public boolean isStatisticalHint = false;
    /**
     * Copied to <code>TADDOptions.generateStateClass</code>.
     * 
     * The default is false.
     */
    public boolean generateStateClass = false;
    /**
     * Copied to <code>TADDOptions.generateNdFile</code>.
     * 
     * The default is false.
     */
    public boolean generateNdFile = false;
    /**
     * If true, and <code>TADDOptions.arraysSupported</code> is EMULATE,
     * then changes it to INDEX.
     * 
     * The default is false.
     */
    public boolean arraysSupported = false;
    /**
     * Copied to <code>TADDOptions.dtmcFallback</code>.
     * 
     * The default is true.
     */
    public boolean dtmcFallback = true;
    /**
     * Name shortener of variables replaces instances with their position in a sorted list.
     */
    public boolean compactInstances = false;
    /**
     * If not null, forces the output format.
     * 
     * The default is null.
     */
    public OutputFormat forceFomat = null;
    /**
     * If to force deterministic labels, Prism--style.
     */
    public boolean forceLabelDeterminism;
    /**
     * If not null, copied to a respective optimizer option. If null, the option adapts
     * automatically to the output formal.
     */
    public Boolean tuneToTADDs = null;
    /**
     * Copied to Optimizer.Tuning.traceAllocInCallResolution. False only for testing,
     * when the translation is slower but tests more extensively code reduction.
     * 
     * The default is true.
     */
    public boolean traceAllocInCallResolution = true;

    /**
     * A shallow clone.
     * 
     * @return a shallow copy of these options
     */
    public TestOptions shallowCopy() {
        try {
            TestOptions copy = (TestOptions)super.clone();
            return copy;
        } catch(CloneNotSupportedException e) {
            throw new RuntimeException("unexpected");
        }
    }
}
