/*
 * SnippetParserWrapper.java
 *
 * Created on May 19, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package pl.gliwice.iitis.hedgeelleth.util;

import pl.gliwice.iitis.hedgeelleth.compiler.util.exception.ParseException;
import pl.gliwice.iitis.hedgeelleth.compiler.util.stream.StreamAttributesParser;
import pl.gliwice.iitis.hedgeelleth.frontend.java.parser.JavaParser;
import pl.gliwice.iitis.verics.parser.VericsParser;

/**
 * A wrapper around one--off snippet parsing. Used to collects all
 * possible parse errors.
 * 
 * @author Artur Rataj
 */
public abstract class SnippetParserWrapper<T> {
    final public StreamAttributesParser PARSER;
    ParseException error = null;
    T parsed = null;
    
    /**
     * Specifies the parser to use.
     * 
     * @param parser parse
     */
    public SnippetParserWrapper(StreamAttributesParser parser) {
        PARSER = parser;
    }
    /**
     * Implement with the parsing code, which uses <code>PARSER</code>.
     * 
     * @return parsed object
     */
    public abstract T call() throws ParseException;
    public void parse() {
        try {
            parsed = call();
        } catch(ParseException g) {
            error = g;
        }
        ParseException recoverable = null;
        if(PARSER instanceof JavaParser)
            recoverable = ((JavaParser)PARSER).hedgeelleth.getParseException();
        else if(PARSER instanceof VericsParser)
            recoverable = ((VericsParser)PARSER).hedgeelleth.getParseException();
        else
            throw new RuntimeException("unknown parser class");
        if(recoverable.reportsExist()) {
            if(error == null)
                error = recoverable;
            else
                error.addAllReports(recoverable);
        }
    }
    /**
     * Returns the parsed object.
     * 
     * @return parsed object, null if none
     */
    public T parsed() {
        return parsed;
    }
    /**
     * If not null, contains error reports.
     * @return 
     */
    public ParseException error() {
        return error;
    }
}
