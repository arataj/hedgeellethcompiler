/*
 * First.java
 *
 * Created on Mar 26, 2016
 *
 * Copyright (c) 2016  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import mirela.*;
import hc.*;

/**
 * A Mirela component of the type Counter.
 * 
 * @author Artur Rataj
 */
public class Counter extends AbstractConsumer {
    int limit;
    /**
     * Counting steps for each input.
     */
    int[] steps;
    PairBarrier sync;
    int counter;
    boolean overflow;
    
    /**
     * Creates a new instance of <code>Counter</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param steps counting steps for each input
     */
    public Counter(Mirela system, int limit, int[] steps) {
        super(system);
        this.limit = limit;
        this.steps = steps;
        init();
    }
    /**
     * Creates a new instance of <code>Counter</code>. A single step
     * for each input. This is a convenience constructor.
     * 
     * @param system Mirela system, to which this node will be added
     */
    public Counter(Mirela system) {
        super(system);
        this.steps = new int[1];
        this.steps[0] = 1;
        init();
    }
    /**
     * Common initialisation routine.
     */
    private void init() {
        sync = new PairBarrier();
        counter = 0;
        overflow = false;
    }
    public void put(int index) {
        sync.produce(index);
    }
    @Override
    public void active() {
        super.active();
        if(numProducers > steps.length)
            throw new RuntimeException("step array too short");
        sync.activate(numProducers, 1);
    }
    @Override
    public void run() {
        boolean commonStep = true;
        for(int i = 0; i < steps.length - 1; ++i)
            if(steps[i] != steps[i + 1]) {
                commonStep = false;
                break;
            }
        while(true) {
            if(counter > limit)
                Model.stateOr("overflow");
            int p = sync.consume();
            if(commonStep)
                counter = /*@(0, limit + steps[0])*/(counter + steps[0]);
            else {
                for(int i = 0; i < numProducers; ++i)
                    if(p == producers[i].INDEX)
                        counter = /*@(0, limit + steps[i])*/(counter + steps[i]);
            }
        }
    }
}
