/*
 * Delay.java
 *
 * Created on Nov 5, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.Sleep;

/**
 * Represents a non--deterministic delay, bounded with a minimum and
 * a maximum value.
 * 
 * @author Artur Rataj
 */
public class Delay implements AbstractDelay {
    /**
     * Minimum delay.
     */
    final public double min;
    /**
     * Maximum delay.
     */
    final public double max;
    
    /**
     * Creates a new instance of <code>Delay</code>.
     * 
     * @param min minimum delay
     * @param max maximum delay
     */
    public Delay(double min, double max) {
        this.min = min;
        this.max = max;
        if(this.min > this.max)
            throw new RuntimeException("invalid bounds");
    }
    /**
     * Creates a new instance of <code>Delay</code>. This is a
     * convenience constructor.
     * 
     * @param delay fixed delay
     */
    public Delay(double delay) {
        this(delay, delay);
    }
    /**
     * Returns the minimum delay.
     * 
     * @return time, non--negative, not greater than <code>getMax()</code>
     */
    public double getMin() {
        return min;
    }
    /**
     * Returns the maximum delay.
     * 
     * @return time, non--negative, not smaller than <code>getMin()</code>
     */
    public double getMax() {
        return max;
    }
    /* implements */
    public Delay getSum(AbstractDelay delay) {
        if(delay instanceof Delay) {
            Delay d = (Delay)delay;
            return new Delay(min + d.min, max + d.max);
        } else
            throw new RuntimeException("not implemented");
    }
    /* implements */
    public Delay getProduct(double m) {
        return new Delay(min*m, max*m);
    }
    /* implements */
    public void sleep() {
        Sleep.exact(min);
        Sleep.max(max - min);
    }
    @Override
    public boolean equals(Object other) {
        if(other instanceof Delay) {
            Delay otherDelay = (Delay)other;
            return min == otherDelay.min && max == otherDelay.max;
        } else
            return false;
    }
}
