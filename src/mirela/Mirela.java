/*
 * Mirela.java
 *
 * Created on Dec 4, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

/**
 * A single Mirela system.
 * 
 * @author Artur Rataj
 */
public class Mirela {
    /**
     * Maximum possible number of  nodes in this system.
     */
    protected final int MAX_NUM_NODES = 20;
    /**
     * Nodes, at <code>0 ... numNodes - 1<code>.
     */
    protected AbstractNode[] nodes;
    /**
     * Number of nodes in this system.
     */
    protected int numNodes;
    
    /**
     * Creates a new Mirela system.
     */
    public Mirela() {
        nodes = new AbstractNode[MAX_NUM_NODES];
    }
    /**
     * Adds a new node. Called by the constructor of
     * <code>AbstractNode</code>.
     * 
     * @param node node to add
     */
    final void add(AbstractNode node) {
        nodes[numNodes] = node;
        ++numNodes;
    }
    /**
     * <p>Enables this Mirela system.</p>
     * 
     * <p>A disabled Mirela system is non--existing in the output
     * network.</p>
     */
    public void enable() {
        for(int i = 0; i < numNodes; ++i)
            nodes[i].active();
    }
}
