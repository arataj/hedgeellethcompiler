/*
 * Both.java
 *
 * Created on Dec 1, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

import hc.*;

/**
 * A Mirela component of the type Both.
 * 
 * @author Artur Rataj
 */
public class Both extends AbstractConsumer {
    /**
     * Processing times of data from subsequent inputs.
     */
    AbstractDelay process;
    Barrier sync;
    
    /**
     * Creates a new instance of <code>First</code>.
     * 
     * @param system Mirela system, to which this node will be added
     * @param process processing times of data from subsequent inputs
     */
    public Both(Mirela system, AbstractDelay process) {
        super(system);
        this.process = process;
        sync = new Barrier();
    }
    public void put(int index) {
        sync.produce();
    }
    @Override
    public void active() {
        super.active();
        sync.activate(numProducers, 1);
    }
    @Override
    public void run() {
        while(true) {
            sync.consume();
            process.sleep();
            produce();
        }
    }
}
