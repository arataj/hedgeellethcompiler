/*
 * AbstractDelay.java
 *
 * Created on Dec 12, 2014
 *
 * Copyright (c) 2014  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package mirela;

/**
 *
 * @author Artur Rataj
 */
public interface AbstractDelay {
    /**
     * Returns a sum of this delay and another one.
     * None of these two is affected.
     * 
     * @param delay another delay
     */
    public AbstractDelay getSum(AbstractDelay delay);
    /**
     * Returns a multiplication of this delay and a scalar.
     * This delay is unaffected.
     * 
     * @param m multiplier
     */
    public AbstractDelay getProduct(double m);
    /**
     * Sleeps.
     */
    public void sleep();
}
