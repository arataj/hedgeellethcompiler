package mirela;

import hc.PairBarrier;

/**
 * A periodic thread.
 * 
 * @author Artur Rataj
 */
public class PeriodicThread extends AbstractConsumer {
    // final private boolean SYNC_USED = true;
    /**
     * For the special timer producer of the class <code>Periodic</code> which
     * calls this thread repeatedly.
     */
    private PairBarrier timer;
    /**
     * For all other producers.
     */
    protected PairBarrier sync;
    
    /**
     * A periodic thread.
     * 
     * @param system network of automatons
     * @param start a warm-up period; in order to fully randomise
     * the phase, make it equal to <code>period</code>
     * @param period a period
     */
    public PeriodicThread(Mirela system, Delay start, double period) {
        super(system);
        Periodic c = new Periodic(system,
                // randomises phase
                start,
                new Delay(period, period));
        c.addOut(this);
        timer = new PairBarrier();
        sync = new PairBarrier();
    }
    public void put(int index) {
        if(index == producers[0].INDEX)
            // the periodic thread
            timer.produce(index);
        else
            sync.produce(index);
    }
    @Override
    public void active() {
        super.active();
        timer.activate(1, 1);
        if(numProducers > 1)
            sync.activate(numProducers - 1, 1);
    }
    @Override
    public void run() {
        while(true) {
            timer.consume();
            periodic();
        }
    }
    /**
     * A periodic task, override to replace this empty implementation.
     */
    public void periodic() {
    }
}
