package example;

import java.util.Random;

import hc.*;
import hc.game.*;

public class Dialogue {
    final public static int NUM_SOURCES = 2;
    final public static int NUM_TURNS = 3;
    final public static int BASE = 10;
    final public static Source[] SOURCES = new Source[NUM_SOURCES];
    final public static int COMP_A = 0;
    final public static int COMP_B = 1;
    final public static int CONJ_ESPECIALLY = 0;
    final public static int CONJ_DESPITE    = 1;
    
    public static Listener LISTENER;
    public static int /*@(0, Dialogue.NUM_TURNS)*/ turnCount = 0;
    static int[] A_LIST = { 10, 10 };
    static int[] B_LIST = { 10, 10 };
    
    public static boolean isEnd() {
        ++turnCount;
        return turnCount == Dialogue.NUM_TURNS;
    }
    public static void main(String[] args) {
        TurnGame table = new TurnGame();
        Model.name(table);
        // create sources
        for(int i = 0; i < NUM_SOURCES; ++i) {
            Source c = new Source(table, i, A_LIST[i], B_LIST[i]);
            c.start();
            Model.name(c, "", "" + (i + 1));
            Model.player(c, "s" + (i + 1));
            SOURCES[i] = c;
        }
        LISTENER = new Listener(table, NUM_SOURCES,
            COMP_A, CONJ_DESPITE, COMP_B);
        LISTENER.start();
        Model.name(LISTENER, "", "");
        Model.player(LISTENER, "l");
        table.start(NUM_SOURCES + 1);
        /*for(int t = 1; t <= MAX_TIME/4; ++t)
            Model.check("at time " + t,
                "<<1, 2, 3>> R{\"value123\"}max=? [F time=" + t + "]");*/
        Model.waitFinish();
        System.out.println("finished");
        LISTENER.interrupt();
    }
}
class Source extends TurnPlayer {
    protected final Random strategy;
    public final int A;
    public final int B;
    public int/*@(-2,  2)*/ declareA;
    public int/*@(-2,  2)*/ declareB;
    public int outA;
    public int outB;
    
    public Source(TurnGame table, int playerNum,
            int a, int b) {
        super(table, playerNum);
        strategy = new Random();
        A = a;
        B = b;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            say();
            turnNext(getNum() + 1);
        }
    }
    protected void say() {
        declareA = strategy.nextInt(5);
        declareA = declareA - 2;
        declareB = strategy.nextInt(5);
        declareB = declareB - 2;
        if(strategy.nextInt(2) == 0)
            outA = A;
        else
            outA = -A;
        if(strategy.nextInt(2) == 0)
            outB = B;
        else
            outB = -B;
    }
}
class Listener extends TurnPlayer {
    protected final Random strategy;
    int COMP_1;
    int CONJ;
    int COMP_2;
    
    public Listener(TurnGame table, int playerNum,
            int state1, int conj, int state2) {
        super(table, playerNum);
        strategy = new Random();
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(Dialogue.isEnd())
                break;
            decide();
            turnNext(0);
        }
    }
    final int compliance = 1;
    final int defiance = 2;
    final int independence = Dialogue.BASE - compliance - defiance;
    int a;
    protected void decide() {
        a = 0;
        int w;
        for(int i = 0; i < Dialogue.NUM_SOURCES; ++i) {
            Source s = Dialogue.SOURCES[i];
            w = w + s.outA;
        }
        a = w;
    }
}

/*modelAppend(

rewards "value1"
        measure & job1>0 : 1/numJobs;
endrewards

rewards "value12"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
endrewards

rewards "value123"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
        measure & job3>0 : 1/numJobs;
endrewards

)*/
