package example;

import java.util.Random;

import hc.*;
import hc.game.*;

public class Dialogue {
    final public static int NUM_SOURCES = 2;
    final public static int NUM_TURNS = 3;
    final public static int BASE = 10;
    final public static Source[] SOURCES = new Source[NUM_SOURCES];
    final public static int COMP_A = 0;
    final public static int COMP_B = 1;
    final public static int CONJ_ESPECIALLY = 0;
    final public static int CONJ_DESPITE    = 1;
    final public static int LEVELS = 3;
    
    public static Listener LISTENER;
    public static int /*@(0, Dialogue.NUM_TURNS)*/ turnCount = 0;
    static int[] A_LIST = { 7, 7 };
    static int[] B_LIST = { 7, 7 };
    
    public static boolean isEnd() {
        ++turnCount;
        return turnCount == Dialogue.NUM_TURNS;
    }
    public static void main(String[] args) {
        TurnGame table = new TurnGame();
        Model.name(table);
        // create sources
        for(int i = 0; i < NUM_SOURCES; ++i) {
            Source c = new Source(table, i, A_LIST[i], B_LIST[i]);
            c.start();
            Model.name(c, "", "" + (i + 1));
            Model.player(c, "s" + (i + 1));
            SOURCES[i] = c;
        }
        LISTENER = new Listener(table, NUM_SOURCES,
            COMP_A, CONJ_DESPITE, COMP_B);
        LISTENER.start();
        Model.name(LISTENER, "", "");
        Model.player(LISTENER, "l");
        table.start(NUM_SOURCES + 1);
        /*for(int t = 1; t <= MAX_TIME/4; ++t)
            Model.check("at time " + t,
                "<<1, 2, 3>> R{\"value123\"}max=? [F time=" + t + "]");*/
        Model.waitFinish();
        System.out.println("finished");
        LISTENER.interrupt();
    }
}
class Source extends TurnPlayer {
    protected final Random strategy;
    public final int A;
    public final int B;
    // sensors always give some information (to avoid division by zero in the listener)
    public int/*@(getNum() == 0 ? Dialogue.LEVELS - 1 : 0, Dialogue.LEVELS - 1)*/ declareA =
        Dialogue.LEVELS - 1;
    public int/*@(getNum() == 0 ? Dialogue.LEVELS - 1 : 0, Dialogue.LEVELS - 1)*/ declareB =
        Dialogue.LEVELS - 1;
    public int outA;
    public int outB;
    
    public Source(TurnGame table, int playerNum,
            int a, int b) {
        super(table, playerNum);
        strategy = new Random();
        A = a;
        B = b;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            say();
            turnNext(getNum() + 1);
        }
    }
    protected void say() {
        if(getNum() == 0) {
        /*
            // let it be allowed to replace an intermediate value
            declareA = strategy.nextInt(Dialogue.LEVELS - 1);
            declareA = 1 + declareA;
            // let it be allowed to replace an intermediate value
            declareB = strategy.nextInt(Dialogue.LEVELS - 1);
            declareB = 1 + declareB;
         */
            // sensors give exact values
            declareA = Dialogue.LEVELS - 1;
            declareB = Dialogue.LEVELS - 1;
        } else {
            // choose declaration strength
            declareA = strategy.nextInt(Dialogue.LEVELS);
            declareB = strategy.nextInt(Dialogue.LEVELS);
        }
        // choose if to cheat
        if(getNum() == 0 || strategy.nextInt(2) == 0)
            outA = A;
        else
            outA = -A;
        if(getNum() == 0 || strategy.nextInt(2) == 0)
            outB = B;
        else
            outB = -B;
    }
}
class Listener extends TurnPlayer {
    protected final Random strategy;
    final int COMP_1;
    final int CONJ;
    final int COMP_2;
    int/*@(0, Dialogue.BASE)*/ complianceA = 7;
    int/*@(0, Dialogue.BASE)*/ defianceA = 2;
    int/*@(0, Dialogue.BASE)*/ complianceB = 5;
    int/*@(0, Dialogue.BASE)*/ defianceB = 2;
    int/*@(0, Dialogue.BASE)*/ a;
    int/*@(0, Dialogue.BASE)*/ b;
    int/*@(0, Dialogue.NUM_TURNS)*/ yes = 0;
    
    public Listener(TurnGame table, int playerNum,
            int comp1, int conj, int comp2) {
        super(table, playerNum);
        strategy = new Random();
        COMP_1 = comp1;
        CONJ = conj;
        COMP_2 = comp2;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(Dialogue.isEnd()) {
                Model.stateAnd("persuaded");
            //    if(Math.random() > yes/Dialogue.NUM_TURNS)
              //      Model.stateAnd("persuaded");
                break;
            }
            decide();
            turnNext(0);
        }
    }
    protected void decide() {
        final int MAX_WEIGHT = Dialogue.BASE*Dialogue.NUM_SOURCES*(Dialogue.LEVELS - 1);
        int/*@(0, MAX_WEIGHT*Dialogue.BASE*Dialogue.BASE)*/ sSumA = 0;
        int/*@(0, MAX_WEIGHT)*/ wSumA = 0;
        int/*@(0, MAX_WEIGHT*Dialogue.BASE*Dialogue.BASE)*/ sSumB = 0;
        int/*@(0, MAX_WEIGHT)*/ wSumB = 0;
        for(int i = 0; i < Dialogue.NUM_SOURCES; ++i) {
            Source s = Dialogue.SOURCES[i];
            sSumA = sSumA + (complianceA + defianceA)*s.declareA*
                (complianceA - defianceA)*s.outA;
            wSumA = wSumA + (complianceA + defianceA)*s.declareA;
            sSumB = sSumB + (complianceB + defianceB)*s.declareB*
                (complianceB - defianceB)*s.outA;
            wSumB = wSumB + (complianceB + defianceB)*s.declareB;
        }
        a = sSumA/wSumA;
        b = sSumB/wSumB;
        int primary;
        switch(COMP_1) {
            case Dialogue.COMP_A:
                primary = a;
                break;
                
            case Dialogue.COMP_B:
                primary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component A");
        }
        int secondary;
        switch(COMP_2) {
            case Dialogue.COMP_A:
                secondary = a;
                break;
                
            case Dialogue.COMP_B:
                secondary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component B");
        }
        if(Math.random() > 0.6*primary +
                (CONJ == Dialogue.CONJ_ESPECIALLY ? 0.1 : -0.1)*
                    (secondary - Dialogue.BASE/2)/Dialogue.BASE)
            ++yes;
        // decrease trust, if cheating discovered
        if(complianceB > 0 && Dialogue.SOURCES[0].outA != Dialogue.SOURCES[1].outA)
            --complianceB;
    }
}

/*modelAppend(

rewards "value1"
        measure & job1>0 : 1/numJobs;
endrewards

rewards "value12"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
endrewards

rewards "value123"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
        measure & job3>0 : 1/numJobs;
endrewards

)*/
