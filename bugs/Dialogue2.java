package pmgc2;

import java.util.Random;

import hc.*;
import hc.game.*;

public class Dialogue2 {
    final public static int NUM_SOURCES = 2;
    final public static int NUM_TURNS = 5;
    final public static int BASE = 100;
    final public static Source[] SOURCES = new Source[NUM_SOURCES];
    final public static int COMP_A = 0;
    final public static int COMP_B = 1;
    final public static int CONJ_ESPECIALLY = 0;
    final public static int CONJ_DESPITE    = 1;
    final public static int LEVELS = 3;
    
    public static Listener LISTENER;
    public static int /*@(0, Dialogue.NUM_TURNS)*/ turnCount = 0;
    static int[] A_LIST = { 70, 70 };
    static int[] B_LIST = { 70, 70 };
    
    public static boolean isEnd() {
        ++turnCount;
        return turnCount == Dialogue.NUM_TURNS;
    }
    public static void main(String[] args) {
        TurnGame table = new TurnGame();
        Model.name(table);
        // create sources
        for(int i = 0; i < NUM_SOURCES; ++i) {
            Source c = new Source(table, i, A_LIST[i], B_LIST[i]);
            c.start();
            Model.name(c, "", "" + (i + 1));
            Model.player(c, "s" + (i + 1));
            SOURCES[i] = c;
        }
        LISTENER = new Listener(table, NUM_SOURCES,
            COMP_A, CONJ_DESPITE, COMP_B);
        LISTENER.start();
        Model.name(LISTENER, "", "");
        Model.player(LISTENER, "l");
        table.start(NUM_SOURCES + 1);
        Model.check("game ends",
            "<<2>> Pmin=? [F turnCount=3]");
        Model.check("persuasion min",
            "<<2>> Pmin=? [F persuaded=1]");
        Model.check("persuasion max",
            "<<2>> Pmax=? [F persuaded=1]");
        Model.waitFinish();
        System.out.println("finished");
        LISTENER.interrupt();
    }
}
class Source extends TurnPlayer {
    protected final Random strategy;
    public final int A;
    public final int B;
    // sensors always give some information (to avoid division by zero in the listener)
    public int/*@(getNum() == 0 ? 1 : 0, Dialogue.LEVELS - 1)*/ declareA =
        Dialogue.LEVELS - 1;
    public int/*@(getNum() == 0 ? 1 : 0, Dialogue.LEVELS - 1)*/ declareB =
        Dialogue.LEVELS - 1;
    public int outA;
    public int outB;
    
    public Source(TurnGame table, int playerNum,
            int a, int b) {
        super(table, playerNum);
        strategy = new Random();
        A = a;
        B = b;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            say();
            turnNext(getNum() + 1);
        }
    }
    protected void say() {
        if(getNum() == 0) {
        /*
            // let it be allowed to replace an intermediate value
            declareA = strategy.nextInt(Dialogue.LEVELS - 1);
            declareA = 1 + declareA;
            // let it be allowed to replace an intermediate value
            declareB = strategy.nextInt(Dialogue.LEVELS - 1);
            declareB = 1 + declareB;
         */
            // sensors give exact values
            declareA = Dialogue.LEVELS - 1;
            declareB = Dialogue.LEVELS - 1;
        } else {
            // choose declaration strength
          declareA = strategy.nextInt(Dialogue.LEVELS);
          declareB = strategy.nextInt(Dialogue.LEVELS);
            /*declareA = 1;
            declareB = 1;
            declareA = 0;
            declareB = 0;*/
        }
        // choose if to cheat
        boolean CHEATING_POSSIBLE = true;
        if(!CHEATING_POSSIBLE || getNum() == 0 || strategy.nextInt(2) == 0)
            outA = A;
        else
            outA = -A;
        if(!CHEATING_POSSIBLE || getNum() == 0 || strategy.nextInt(2) == 0)
            outB = B;
        else
            outB = -B;
    }
}
class Listener extends TurnPlayer {
    protected final Random strategy;
    final int COMP_1;
    final int CONJ;
    final int COMP_2;
    int[]/*@(0, Dialogue.BASE)*/ compliance = {70, 50};
    int[]/*@(0, Dialogue.BASE)*/ defiance = {20, 20};
    int/*@(0, Dialogue.BASE)*/ a;
    int/*@(0, Dialogue.BASE)*/ b;
    int/*@(0, Dialogue.NUM_TURNS)*/ yes = 0;
    int/*@(0, 1)*/ persuaded = 0;
    
    public Listener(TurnGame table, int playerNum,
            int comp1, int conj, int comp2) {
        super(table, playerNum);
        strategy = new Random();
        COMP_1 = comp1;
        CONJ = conj;
        COMP_2 = comp2;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(Dialogue.isEnd()) {
                break;
            }
            decide();
            turnNext(0);
        }
        if(Math.random() > yes*1.0/Dialogue.NUM_TURNS) {
//            Model.stateAnd("persuaded");
            persuaded = 1;
        }
    }
    protected void decide() {
        final int MAX_WEIGHT = Dialogue.BASE*Dialogue.NUM_SOURCES*(Dialogue.LEVELS - 1);
        int/*@(0, MAX_WEIGHT*Dialogue.BASE*Dialogue.BASE)*/ sSumA = 0;
        int/*@(0, MAX_WEIGHT*Dialogue.BASE)*/ wSumA = 0;
        int/*@(0, MAX_WEIGHT*Dialogue.BASE*Dialogue.BASE)*/ sSumB = 0;
        int/*@(0, MAX_WEIGHT*Dialogue.BASE)*/ wSumB = 0;
        for(int i = 0; i < Dialogue.NUM_SOURCES; ++i) {
            Source s = Dialogue.SOURCES[i];
            sSumA = sSumA + (compliance[i] + defiance[i])*s.declareA*
                (compliance[i] - defiance[i])*s.outA;
            wSumA = wSumA + (compliance[i] + defiance[i])*s.declareA*
                (compliance[i] - defiance[i]);
            sSumB = sSumB + (compliance[i] + defiance[i])*s.declareB*
                (compliance[i] - defiance[i])*s.outB;
            wSumB = wSumB + (compliance[i] + defiance[i])*s.declareB*
                (compliance[i] - defiance[i]);
        }
        a = sSumA/wSumA;
        b = sSumB/wSumB;
        int primary;
        switch(COMP_1) {
            case Dialogue.COMP_A:
                primary = a;
                break;
                
            case Dialogue.COMP_B:
                primary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component A");
        }
        int secondary;
        switch(COMP_2) {
            case Dialogue.COMP_A:
                secondary = a;
                break;
                
            case Dialogue.COMP_B:
                secondary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component B");
        }
        if(Math.random() > 0.2 + 0.4*primary/Dialogue.BASE +
                (CONJ == Dialogue.CONJ_ESPECIALLY ? 0.1 : -0.1)*
                    (secondary - Dialogue.BASE/2)/Dialogue.BASE)
            ++yes;
        // decrease trust, if cheating discovered
        if(Dialogue.SOURCES[1].declareA > 0 && Dialogue.SOURCES[0].outA != Dialogue.SOURCES[1].outA) {
            compliance[1] = compliance[1]*0 - Dialogue.SOURCES[1].declareA;
            if(compliance[1] < 0)
                compliance[1] = 0;
        }
        if(Dialogue.SOURCES[1].declareB > 0 && Dialogue.SOURCES[0].outB != Dialogue.SOURCES[1].outB) {
            compliance[1] = compliance[1]*0 - Dialogue.SOURCES[1].declareB;
            if(compliance[1] < 0)
                compliance[1] = 0;
        }
    }
}

/*modelAppend(

rewards "value1"
        measure & job1>0 : 1/numJobs;
endrewards

rewards "value12"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
endrewards

rewards "value123"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
        measure & job3>0 : 1/numJobs;
endrewards

)*/
