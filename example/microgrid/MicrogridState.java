package example;

import java.lang.reflect.Field;

public class MicrogridState {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    Field f0;
    Field f1;
    example.Household o3;
    Field f4;
    example.Household o6;
    Field f7;
    example.Household o9;
    Field f10;
    hc.game.TurnGame o13;
    Field f14;
    public MicrogridState() {
        try {
            f0 = getField(example.Microgrid.class, "numJobs");
            f0.setAccessible(true);
            f1 = getField(example.Microgrid.class, "time");
            f1.setAccessible(true);
            Field f2 = getField(example.Microgrid.class, "households");
            f2.setAccessible(true);
            example.Household[] o2 = (example.Household[])f2.get(null);
            o3 = o2[0];
            f4 = getField(example.Household.class, "job");
            f4.setAccessible(true);
            Field f5 = getField(example.Microgrid.class, "households");
            f5.setAccessible(true);
            example.Household[] o5 = (example.Household[])f5.get(null);
            o6 = o5[1];
            f7 = getField(example.Household.class, "job");
            f7.setAccessible(true);
            Field f8 = getField(example.Microgrid.class, "households");
            f8.setAccessible(true);
            example.Household[] o8 = (example.Household[])f8.get(null);
            o9 = o8[2];
            f10 = getField(example.Household.class, "job");
            f10.setAccessible(true);
            Field f11 = getField(example.Microgrid.class, "households");
            f11.setAccessible(true);
            example.Household[] o11 = (example.Household[])f11.get(null);
            example.Household o12 = o11[0];
            Field f13 = getField(example.Household.class, "table");
            f13.setAccessible(true);
            o13 = (hc.game.TurnGame)f13.get(o12);
            f14 = getField(hc.game.TurnGame.class, "turn");
            f14.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int numJobs() {
        try {
            return f0.getInt(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int time() {
        try {
            return f1.getInt(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job1() {
        try {
            return f4.getInt(o3);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job2() {
        try {
            return f7.getInt(o6);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job3() {
        try {
            return f10.getInt(o9);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int turn() {
        try {
            return f14.getInt(o13);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return numJobs();
            case 1:
                return time();
            case 2:
                return job1();
            case 3:
                return job2();
            case 4:
                return job3();
            case 5:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                return numJobs();
            case 1:
                return time();
            case 2:
                return job1();
            case 3:
                return job2();
            case 4:
                return job3();
            case 5:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                return numJobs();
            case 1:
                return time();
            case 2:
                return job1();
            case 3:
                return job2();
            case 4:
                return job3();
            case 5:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 6;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "numJobs";
            case 1:
                return "time";
            case 2:
                return "job1";
            case 3:
                return "job2";
            case 4:
                return "job3";
            case 5:
                return "turn";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
