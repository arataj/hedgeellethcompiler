import java.util.Random;

class Sort extends Thread {
    static int L = 10;
    static int M = 4;
    int[]/*@(0, M)*/ array;
    Random random = new Random();
    
    public Sort(int[]/*@(0, M)*/ arrayS) {
        this.array = arrayS;
    }
    public void sort(int[]/*@(0, M)*/ arrays) {
        int N = arrays.length;
        for(int/*@(0, N)*/ i = 0; i < N; ++i)
            for(int/*@(0, N - 1)*/ j = 0; j < N - 1; ++j) {
                if(arrays[j] > arrays[j + 1]) {
                    int t = arrays[j];
                    arrays[j] = arrays[j + 1];
                    arrays[j + 1] = t; 
                }
            }
    }
    public void run() {
        int N = array.length;
        for(int/*@(0, N)*/ i = 0; i < N; ++i)
            array[i] = random.nextInt(M + 1);
        sort(array);
        for(int/*@(0, N - 1)*/ k = 0; k < N - 1; ++k)
            if(array[k] > array[k + 1])
                assert false;
        
    }
    public static void main(String[] args) {
        int[]/*@(0, M)*/ arraym = new int[L];
        new Sort(arraym).start();
    }
}
