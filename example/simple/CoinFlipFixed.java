package example;

import java.util.*;
import hc.*;

class CoinFlipFixed extends Thread {
  final int N = 100;
  Random random = new Random();
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public void run() {
    for(; flips < N; ++flips)
      if(5*(flips - N/10.0)/9 <= heads) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
  }
  public static void main(String[] args) {
    CoinFlipFixed cf = new CoinFlipFixed();
    Model.name(cf);
    cf.start();
  }
}
