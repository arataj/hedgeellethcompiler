package example;

import java.lang.reflect.Field;

public class CoinFlipState {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    example.CoinFlip o0;
    Field f1;
    example.CoinFlip o2;
    Field f3;
    public CoinFlipState() {
        try {
            Field f0 = getField(example.Static.class, "cf");
            f0.setAccessible(true);
            o0 = (example.CoinFlip)f0.get(null);
            f1 = getField(example.CoinFlip.class, "flips");
            f1.setAccessible(true);
            Field f2 = getField(example.Static.class, "cf");
            f2.setAccessible(true);
            o2 = (example.CoinFlip)f2.get(null);
            f3 = getField(example.CoinFlip.class, "heads");
            f3.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int flips() {
        try {
            return f1.getInt(o0);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int heads() {
        try {
            return f3.getInt(o2);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                return flips();
            case 1:
                return heads();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 2;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "flips";
            case 1:
                return "heads";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
