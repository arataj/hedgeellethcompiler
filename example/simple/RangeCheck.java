package example;

import hc.*;

class RangeCheck extends Thread {
  final int N = 5;
  int /*@(0, 2*N)*/ sum;
  
  public void run() {
      int /*@(0, N - 1)*/i = 0;
      while(i < 10) {
          sum += N/(N/2 + i);
          if(Math.random() < 0.5)
              ++i;
      }
  }
  public static void main(String[] args) {
    RangeCheck rc = new RangeCheck();
    Model.name(rc);
    rc.start();
  }
}
