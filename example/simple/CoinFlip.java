package example;

import java.util.*;
import hc.*;

class Static {
  static CoinFlip cf;
}
class CoinFlip extends Thread {
  final int N = 100;
  Random random = new Random();
  int /*@(0, N)*/flips = 0;
  int /*@(0, N)*/heads = 0;
  
  public void run() {
    for(; flips < N; ++flips)
      if(random.nextInt(2) == 0) {
        if(Math.random() < 0.5)
          ++heads;
      } else {
        if(Math.random() < 0.6)
          ++heads;
      }
  }
  public static void main(String[] args) {
    CoinFlip cf = new CoinFlip();
    Model.name(cf);
    cf.start();
  }
}
