dtmc

global flips : [0..100] init 0; // O(:=) = (0 ... 101)
global heads : [0..100] init 0; // O(:=) = (0 ... 101)

module CoinFlipFixed

    s0 : [0..5] init 0;

    [] (s0=0) & (flips < 100) -> (s0'=1);
    [] (s0=0) & (100 <= flips) -> (s0'=5);
    [] (s0=1) & (((5 * (flips - 10.0)) / 9) <= heads) -> (s0'=2);
    [] (s0=1) & (heads < ((5 * (flips - 10.0)) / 9)) -> (s0'=3);
    [] (s0=2) -> 0.5:(s0'=4) & (heads' = min(100, heads + 1)) + 0.5:(s0'=4);
    [] (s0=3) -> 0.4:(s0'=4) + 0.6:(s0'=4) & (heads' = min(100, heads + 1));
    [] (s0=4) -> (s0'=0) & (flips' = min(100, flips + 1));
    [] (s0=5) -> true;

endmodule
