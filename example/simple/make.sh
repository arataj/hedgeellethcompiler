hc RangeCheck.java -op -v0 -sh -i -r && prism RangeCheck.nm RangeCheck.pctl
hc CoinFlip.java -op -v0 -sh -i && \
	prism CoinFlip.nm -pctl 'Pmax=? [ F (flips=100 & heads=50) ]' -exportadv CoinFlip.tra -exportstates CoinFlip.states -s
hc CoinFlipFixed.java -op -v0 -sh -i && \
	prism CoinFlipFixed.nm -transient 400 -exportstates 100.states -exporttransient 100.pv
