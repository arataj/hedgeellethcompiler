package example;

import java.lang.reflect.Field;

public class MicrogridState5 {
    private static Field getField(Class clazz, String fieldName)
            throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
    Field f0;
    Field f1;
    Field f2;
    example.Household o4;
    Field f5;
    example.Household o7;
    Field f8;
    example.Household o10;
    Field f11;
    example.Household o13;
    Field f14;
    example.Household o16;
    Field f17;
    hc.game.TurnGame o20;
    Field f21;
    public MicrogridState5() {
        try {
            f0 = getField(example.Household.class, "PRICE_LIMIT");
            f0.setAccessible(true);
            f1 = getField(example.Microgrid.class, "numJobs");
            f1.setAccessible(true);
            f2 = getField(example.Microgrid.class, "time");
            f2.setAccessible(true);
            Field f3 = getField(example.Microgrid.class, "households");
            f3.setAccessible(true);
            example.Household[] o3 = (example.Household[])f3.get(null);
            o4 = o3[0];
            f5 = getField(example.Household.class, "job");
            f5.setAccessible(true);
            Field f6 = getField(example.Microgrid.class, "households");
            f6.setAccessible(true);
            example.Household[] o6 = (example.Household[])f6.get(null);
            o7 = o6[1];
            f8 = getField(example.Household.class, "job");
            f8.setAccessible(true);
            Field f9 = getField(example.Microgrid.class, "households");
            f9.setAccessible(true);
            example.Household[] o9 = (example.Household[])f9.get(null);
            o10 = o9[2];
            f11 = getField(example.Household.class, "job");
            f11.setAccessible(true);
            Field f12 = getField(example.Microgrid.class, "households");
            f12.setAccessible(true);
            example.Household[] o12 = (example.Household[])f12.get(null);
            o13 = o12[3];
            f14 = getField(example.Household.class, "job");
            f14.setAccessible(true);
            Field f15 = getField(example.Microgrid.class, "households");
            f15.setAccessible(true);
            example.Household[] o15 = (example.Household[])f15.get(null);
            o16 = o15[4];
            f17 = getField(example.Household.class, "job");
            f17.setAccessible(true);
            Field f18 = getField(example.Microgrid.class, "households");
            f18.setAccessible(true);
            example.Household[] o18 = (example.Household[])f18.get(null);
            example.Household o19 = o18[0];
            Field f20 = getField(example.Household.class, "table");
            f20.setAccessible(true);
            o20 = (hc.game.TurnGame)f20.get(o19);
            f21 = getField(hc.game.TurnGame.class, "turn");
            f21.setAccessible(true);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    double PRICE_LIMIT() {
        try {
            return f0.getDouble(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int numJobs() {
        try {
            return f1.getInt(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int time() {
        try {
            return f2.getInt(null);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job1() {
        try {
            return f5.getInt(o4);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job2() {
        try {
            return f8.getInt(o7);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job3() {
        try {
            return f11.getInt(o10);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job4() {
        try {
            return f14.getInt(o13);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int job5() {
        try {
            return f17.getInt(o16);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    int turn() {
        try {
            return f21.getInt(o20);
        } catch(ReflectiveOperationException e) {
            throw new RuntimeException("unexpected: " + e.toString());
        }
    }
    public double get(int index) {
        switch(index) {
            case 0:
                return PRICE_LIMIT();
            case 1:
                return numJobs();
            case 2:
                return time();
            case 3:
                return job1();
            case 4:
                return job2();
            case 5:
                return job3();
            case 6:
                return job4();
            case 7:
                return job5();
            case 8:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int getInt(int index) {
        switch(index) {
            case 0:
                throw new RuntimeException("state variable " +
                    "at index 0 has type double");
            case 1:
                return numJobs();
            case 2:
                return time();
            case 3:
                return job1();
            case 4:
                return job2();
            case 5:
                return job3();
            case 6:
                return job4();
            case 7:
                return job5();
            case 8:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public long getLong(int index) {
        switch(index) {
            case 0:
                throw new RuntimeException("state variable " +
                    "at index 0 has type double");
            case 1:
                return numJobs();
            case 2:
                return time();
            case 3:
                return job1();
            case 4:
                return job2();
            case 5:
                return job3();
            case 6:
                return job4();
            case 7:
                return job5();
            case 8:
                return turn();
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
    public int size() {
        return 9;
    }
    public String getName(int index) {
        switch(index) {
            case 0:
                return "PRICE_LIMIT";
            case 1:
                return "numJobs";
            case 2:
                return "time";
            case 3:
                return "job1";
            case 4:
                return "job2";
            case 5:
                return "job3";
            case 6:
                return "job4";
            case 7:
                return "job5";
            case 8:
                return "turn";
            default:
                throw new RuntimeException("invalid index: " +
                    index);
        }
    }
}
