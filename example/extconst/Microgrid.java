package example;

import java.util.Random;

import hc.*;
import hc.game.*;

/**
 * A microgrid demand--side management game, as described in [1].
 *
 * [1] H. Hildmann and F. Saffre. Influence of variable supply and load
 * flexibility on Demand-Side Management. In Proc. 8th International
 * Conference on the European Energy Market (EEM'11), pages 63-68. 2011.
 */
public class Microgrid {
    /**
     * Number of days.
     */
    public static final int DAYS = 3;
    /**
     * Number of intervals per day.
     */
    public static final int INTERVALS = 16;
    /**
     * Number of rounds.
     */
    public static final int MAX_TIME = DAYS*INTERVALS;
    
    /**
     * Demand curve.
     */
    public static final double[] DEMAND = {
        0.0614, 0.0392, 0.0304, 0.0304,
        0.0355, 0.0518, 0.0651, 0.0643,
        0.0625, 0.0618, 0.0614, 0.0695,
        0.0887, 0.1013, 0.1005, 0.0762,
    };
    /**
     * Number of households.
     */
    public static int NUM_HOUSEHOLDS = Model.intConst("HOUSEHOLDS");
    /**
     * Households.
     */
    protected static Household[] households;
    /**
     * Current time or interval.
     */
    public static int/*@(0, MAX_TIME)*/ time;
    /**
     * Current number of loads generated.
     */
    public static int/*@(0, NUM_HOUSEHOLDS)*/ numJobs;

    /**
     * Returns the current load demand.
     */
    public static double getDemand() {
        return DEMAND[time%INTERVALS];
    }
    /**
     * How many households currently generate a load.
     */
    public static int getNumJobs() {
        int/*@(0, NUM_HOUSEHOLDS)*/ numJobs = 0;
        for(int i = 0; i < NUM_HOUSEHOLDS; ++i)
            if(households[i].job > 0)
                ++numJobs;
        return numJobs;
    }
    /**
     * A price of a load to be bought.
     */
    public static double getPrice() {
        int n;
        if(Model.isStatistical())
            n = numJobs;
        else
            n = getNumJobs();
        return n + 1.0;
    }
    /**
     * If <code>MAX_TIME</code> has been reached.
     *
     * @return if the simulation is finished
     */
    public static boolean isFinished() {
        return time == MAX_TIME;
    }
    /**
     * Called by the scheduler at the end of each interval.
     *
     */
    public static void endInterval() {
        numJobs = getNumJobs();
        Model.stateAnd("measure");
        if(!Model.isStatistical())
            numJobs = 0;
        // reduce job counters
        for(int i = 0; i < Microgrid.NUM_HOUSEHOLDS; ++i)
              households[i].tick();
        ++time;
    }
    public static void main(String[] args) {
        // player ids at the table: 0 scheduler, 1..NUM_HOUSEHOLDS households
        TurnGame table = new TurnGame();
        Model.name(table);
        // create an array of households
        households = new Household[NUM_HOUSEHOLDS];
        for(int i = 0; i < NUM_HOUSEHOLDS; ++i) {
            Household h = new Household(table, 1 + i);
            h.start();
            Model.name(h, "", "" + (i + 1));
            Model.player(h, "h" + (i + 1));
            households[i] = h;
        }
        // create the scheduler
        Scheduler s = new Scheduler(table);
        s.start();
        Model.name(s);
        Model.player(s, "scheduler");
        // reset time
        time = 0;
        // players consist of the scheduler and of the households
        table.start(1 + NUM_HOUSEHOLDS);
        for(int t = 1; t <= MAX_TIME/4; ++t)
            Model.check("at time " + t,
                "<<1, 2, 3>> R{\"value123\"}max=? [F time=" + t + "]");
        Model.waitFinish();
        System.out.println("time = " + time);
        // won't be executed by hc, but will within JVM
        //
        // releases all threads still waiting on the barrier
        households[0].interrupt();
    }
}

class Scheduler extends TurnPlayer {
    protected final static boolean NON_DETERMINISTIC = false;
    
    protected final Random strategy;

    public Scheduler(TurnGame table) {
        super(table, 0);
        strategy = new Random();
    }
    @Override
    public void run() {
        do {
            // select a household
            int address;
            if(NON_DETERMINISTIC)
                // non--deterministic choice
                address = strategy.nextInt(Microgrid.NUM_HOUSEHOLDS);
            else
                // probabilistic choice
                address = (int)
                    (Math.random()*Microgrid.NUM_HOUSEHOLDS);
            // contact the selected household
            turnNext(1 + address);
            turnWait();
            // end of the current round
            Microgrid.endInterval();
        } while(!Microgrid.isFinished());
        Model.finish();
    }
}

class Household extends TurnPlayer {
    /**
     * Maximum time of running a single job, in intervals.
     */
    protected final static int MAX_JOB_TIME = 4;
    /**
     * Expected number of jobs per day.
     */
    protected final static int EXPECTED_JOBS = 9;
    /**
     * Price limit, above which this household may
     * back--off.
     */
    protected final static double PRICE_LIMIT =
        Model.doubleConst("PRICE_LIMIT");
    /**
     * Probability of starting a task independently of the cost.
     */
    protected final static double P_OVER_LIMIT = 0.8;
    /**
     * A running job, in intervals.
     */
    public int/*@(0, MAX_JOB_TIME)*/ job = 0;
    protected final Random strategy;
    
    public Household(TurnGame table, int playerNum) {
        super(table, playerNum);
        strategy = new Random();
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(isInterrupted())
                break;
            if(job == 0 && Math.random() < Microgrid.getDemand()*
                  EXPECTED_JOBS) {
                // decide if to generate a load
                boolean lowPrice = Microgrid.getPrice() < PRICE_LIMIT;
                if(lowPrice || Math.random() < P_OVER_LIMIT ||
                        // a right to back--off is granted; decide,
                        // if to generate a load anyway
                        strategy.nextInt(2) == 0)
                    job = 1 + (int)(Math.random()*MAX_JOB_TIME);
            }
            turnNext(0);
        }
    }
    /**
     * Called by the scheduler after each turn.
     */
    public void tick() {
        if(job > 0)
          --job;
    }
}

/*@modelAppend(

rewards "value1"
        measure & job1>0 : 1/numJobs;
endrewards

rewards "value12"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
endrewards

rewards "value123"
        measure & job1>0 : 1/numJobs;
        measure & job2>0 : 1/numJobs;
        measure & job3>0 : 1/numJobs;
endrewards

)*/
