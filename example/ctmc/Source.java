/**
 * Exponential source.
 */
import hc.*;

public class Source extends Thread {
    final double LAMBDA;
    final Queue QUEUE;
  
    public Source(final double lambda, Queue queue) {
        LAMBDA = lambda;
        QUEUE = queue;
    }
    public void run() {
        while(true) {
            Sleep.exact(Dist.nxp(LAMBDA));
            if(!queue.isFull())
                queue.add();
        }
    }
}
