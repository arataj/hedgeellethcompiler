/**
 * An M/M/1 model.
 */
import hc.*;

class Queue {
    final int MAX_LENGTH = 10;
  
    public int/*@(0, MAX_LENGTH)*/ length = 0;
}

class Source extends Thread {
    final double LAMBDA = 0.2;
    Queue queue;
  
    public Source(Queue queue) {
        this.queue = queue;
    }
    public void run() {
        while(true) {
            Sleep.exact(Dist.nxp(LAMBDA));
            if(queue.length < Queue.MAX_LENGTH)
                ++queue.length;
        }
    }
}

class Sink extends Thread {
    final double MU = 0.3;
    Queue queue;
  
    public Sink(Queue queue) {
        this.queue = queue;
    }
    public void run() {
        while(true) {
            Sleep.exact(Dist.nxp(MU));
            if(queue.length > 0)
                --queue.length;
        }
    }
}

class MM1 {
    public static void main(String[] args) {
        Queue queue = new Queue();
        Model.name(queue);
        new Source(queue).start();
        new Sink(queue).start();
    }
}
