/**
 * A queue.
 */
public class Queue {
    /**
     * Maximum size of this queue.
     */
    final int MAX_SIZE;
    /**
     * Size of this queue.
     */
    int/*(0, MAX_SIZE)*/ size;
    
    /**
     * Creates a new queue.
     *
     * @param maxSize maximum size
     * @param initialSize initial size
     */
    public Queue(int maxSize, int initialSize) {
        MAX_SIZE = maxSize;
        size = initialSize;
    }
    public boolean isEmpty() {
        return size == 0;
    }
    public boolean isFull() {
        return size == MAX_SIZE;
    }
    public void add() {
        ++size;
    }
    public void get() {
        --size;
    }
}