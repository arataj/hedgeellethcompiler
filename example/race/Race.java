/**
 * An M/M/1 model.
 */
import hc.*;

class Buffer {
    final int MAX_SIZE = 10;
  
    public int/*@(0, MAX_SIZE)*/ size = 0;
}

class Producer extends Thread {
    Buffer buffer;
  
    public Producer(Buffer buffer) {
        this.buffer = buffer;
    }
    public void run() {
        while(true) {
            if(buffer.size < Buffer.MAX_SIZE)
                ++buffer.size;
        }
    }
}

public class Race {
    public static void main(String[] args) {
        Buffer buffer = new Buffer();
        Model.name(buffer);
        new Producer(buffer).start();
        new Producer(buffer).start();
    }
}
