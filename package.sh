# NAME=hc-1.0rc1-$(date +%Y%m%d)
NAME=hc-20180405
# NAME=hc-1.0
TARGET=../archive/$NAME
# used by called scripts
export PACKAGE=$NAME.tar.gz
rm -rf $TARGET
mkdir $TARGET && \
	cp -rpv INSTALL.txt install.sh $TARGET/ && \
	mkdir $TARGET/dist $TARGET/dist/lib \
		$TARGET/bin \
		$TARGET/lib \
		$TARGET/lib/java \
		$TARGET/lib/java/lang \
		$TARGET/lib/java/io \
		$TARGET/lib/java/util \
		$TARGET/lib/hc \
		$TARGET/lib/hc/game \
		$TARGET/example \
		$TARGET/example/simple \
		$TARGET/example/microgrid \
		$TARGET/example/isstatistical \
		$TARGET/example/extconst \
		$TARGET/example/race \
		$TARGET/example/ctmc \
		$TARGET/example/sort && \
	cp -pv dist/*.jar $TARGET/dist/ && \
	cp -pv dist/lib/*.jar $TARGET/dist/lib/ && \
	rm -v $TARGET/dist/lib/javacc.jar $TARGET/dist/lib/junit.jar && \
	cp -pv lib/java/lang/*.java $TARGET/lib/java/lang/ && \
	cp -pv lib/java/io/*.java $TARGET/lib/java/io/ && \
	cp -pv lib/java/util/*.java $TARGET/lib/java/util/ && \
	cp -pv lib/hc/*.java lib/hc/*.hc_java $TARGET/lib/hc/ && \
	cp -pv lib/hc/game/*.java lib/hc/game/*.hc_java $TARGET/lib/hc/game/ && \
	cp -pv bin/hc $TARGET/bin/ && \
	cp -pv example/simple/RangeCheck.java example/simple/CoinFlip.java example/simple/make.sh $TARGET/example/simple/ && \
	cp -pv example/microgrid/Microgrid.java $TARGET/example/microgrid/ && \
	cp -pv example/isstatistical/Microgrid.java $TARGET/example/isstatistical/ && \
	cp -pv example/extconst/Microgrid.java $TARGET/example/extconst/ && \
	cp -pv example/extconst/make.sh $TARGET/example/extconst/ && \
	cp -pv example/race/Race.java $TARGET/example/race/ && \
	cp -pv example/race/make.sh $TARGET/example/race/ && \
	cp -pv example/ctmc/MM1.java $TARGET/example/ctmc/ && \
	cp -pv example/ctmc/make.sh $TARGET/example/ctmc/ && \
	cp -pv example/sort/Sort.java $TARGET/example/sort/ && \
	echo "creating documentation" && cd ../doc/html-src/0.Hedgeelleth/3.Download/build && \
	./make.sh && cd - && \
	cd ../doc && ./make-html.sh -dpackage -iDownload -iDevelopment && cd - && \
	cp -rp ../doc/html/Hedgeelleth $TARGET/doc && \
	rm -rf $TARGET/doc/Download $TARGET/doc/Development && \
	echo "packaging" && cd ../archive && tar czf $PACKAGE $NAME && cd - && \
	cp ../archive/$PACKAGE ../doc/html/Hedgeelleth/Download/
